# JavaScript / CoffeeScript Katas

Katas are programming exercises designed to help programmers improve their skills through practice and repetition.

## Getting Started

Install all the dev dependencies first:

```
npm install
```

Execute all the unit tests and generate a test coverage report:

```
npm test
```

Execute ESLint against all the JavaScript (`.js`) files in the `src` and `tests` directories (except
the JavaScript files in the `out` directory that were transpiled from CoffeeScript):

```
npm run lint
```

Watch for changes in the `src/coffeescript` directory, and automatically transpile code to JavaScript:

```
npm run watch:coffee
```

### Links

- [Developer's Website](https://bjdelacruz.dev)
- [Coderbyte](https://coderbyte.com/profile/bjpeterdelacruz)
- [Codesignal](https://app.codesignal.com/profile/bjpeter)
- [Codewars](https://www.codewars.com/users/bjpeterdelacruz/stats)
// eslint.config.js
import globals from 'globals';
import js from "@eslint/js";

export default [
    js.configs.recommended,
    {
        rules: {
            "no-unused-vars": "warn"
        },
        languageOptions: {
            globals: {
              ...globals.commonjs, ...globals.jest
            }
        }
    }
];
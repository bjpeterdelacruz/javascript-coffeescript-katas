/**
 * @param list
 */
function shuffle(list) {
    const shuffled = [];
    for (let i = 0; i < list.length; i++) {
        shuffled.push(list[i]);
    }
    for (let i = 0; i < shuffled.length; i++) {
        if (Math.random() > 0.5) {
            const j = (i+3) % shuffled.length;
            // swap lines i & j
            const tmp = shuffled[i];
            shuffled[i] = shuffled[j];
            shuffled[j] = tmp;
        }
    }
    return shuffled;
}

/**
 * @param a
 * @param b
 */
function comparator(a, b) {
    const first = a.split(/\s+/);
    const second = b.split(/\s+/);
    let c = parseInt(first[0]);
    let d = parseInt(second[0]);
    if (first[0] === 'On') {
        c = 13;
    }
    else if (first[0] === 'a') {
        c = 1;
    }
    if (second[0] === 'On') {
        d = 13;
    }
    else if (second[0] === 'a') {
        d = 1;
    }
    return d - c;
}

module.exports = { comparator, shuffle };

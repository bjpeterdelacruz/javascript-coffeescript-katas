/**
 * @param w
 * @param r
 */
function epley(w, r) {
    return w * (1 + (r / 30));
}

/**
 * @param w
 * @param r
 */
function mcglothin(w, r) {
    return 100 * w / (101.3 - (2.67123 * r));
}

/**
 * @param w
 * @param r
 */
function lombardi(w, r) {
    return w * Math.pow(r, 0.1);
}

/**
 * @param w
 * @param r
 */
function calculate1RM(w, r) {
    return r === 0 ? 0 : (r === 1 ? w : Math.round(Math.max(
        epley(w, r), mcglothin(w, r), lombardi(w, r))));
}

module.exports = calculate1RM;

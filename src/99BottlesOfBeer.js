module.exports = () => {
    const array = [];
    for (let idx = 99; idx > 1; idx--) {
        array.push(`${idx} bottles of beer on the wall, ${idx} bottles of beer.`);
        const line = 'Take one down and pass it around,';
        if (idx > 2) {
            array.push(`${line} ${idx - 1} bottles of beer on the wall.`);
        }
        else {
            array.push(`${line} 1 bottle of beer on the wall.`);
        }
    }
    array.push('1 bottle of beer on the wall, 1 bottle of beer.');
    array.push('Take one down and pass it around, no more bottles of beer on the wall.');
    array.push('No more bottles of beer on the wall, no more bottles of beer.');
    array.push('Go to the store and buy some more, 99 bottles of beer on the wall.');
    return array;
};

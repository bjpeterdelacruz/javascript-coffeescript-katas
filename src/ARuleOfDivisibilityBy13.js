/**
 * @param number
 */
function thirt(number) {
    let num = number;
    const arr1 = [1, 10, 9, 12, 3, 4];
    const sums = [];
    do {
        const nString = num.toString();
        const arr2 = [];
        let idx1 = 0;
        let idx2 = 0;
        while (idx1 < nString.length) {
            if (idx2 === arr1.length) {
                idx2 = 0;
            }
            arr2.push(arr1[idx2]);
            idx1++;
            idx2++;
        }
        let sum = 0;
        for (idx1 = 0, idx2 = nString.length - 1; idx1 < nString.length; idx1++, idx2--) {
            sum += (nString[idx1] * arr2[idx2]);
        }
        sums.push(sum);
        num = sum;
    } while (sums[sums.length - 1] !== sums[sums.length - 2]);
    return num;
}

module.exports = thirt;

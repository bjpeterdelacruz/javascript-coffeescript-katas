const questions = [{
    question: 'What\'s the currency of the USA?',
    choices: ['US dollar', 'Ruble', 'Horses', 'Gold'],
    corAnswer: 0
}, {
    question: 'Where was the American Declaration of Independence signed?',
    choices: ['Philadelphia', 'At the bottom', 'Frankie\'s Pub', 'China'],
    corAnswer: 0
}];

for (const question of questions) {
    question.usersAnswer = null;
}

module.exports = questions;

function arrAdder(arrays) {
    let maxLength = 0;
    const numberOfArrays = arrays.length;
    // Find length of longest array
    for (const array of arrays) {
        if (array.length > maxLength) {
            maxLength = array.length;
        }
    }
    const words = [];
    for (let columnIdx = 0; columnIdx < maxLength; columnIdx++) {
        let word = '';
        for (let rowIdx = 0; rowIdx < numberOfArrays; rowIdx++) {
            // Do not assume that current array has same length as longest array
            if (columnIdx < arrays[rowIdx].length) {
                word += arrays[rowIdx][columnIdx];
            }
        }
        words.push(word);
    }
    return words.join(' ');
}

module.exports = arrAdder;

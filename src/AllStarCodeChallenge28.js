/**
 * @param num
 * @param scale
 */
function convertCF(num, scale) {
    if (scale !== undefined && scale !== 'f' && scale !== 'c') {
        throw `Expected 'f' or 'c' but was '${scale}'`;
    }
    if (scale === 'f') {
        return (num * 9/5) + 32;
    }
    return (num - 32) * 5/9;
}

module.exports = convertCF;

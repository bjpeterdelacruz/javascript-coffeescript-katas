/**
 * @param str
 */
function removeVowels(str) {
    const arr = [];
    for (let idx = 0; idx < str.length; idx++) {
        if (str[idx].match(/[^aeiouAEIOU]/)) {
            arr.push(str[idx]);
        }
    }
    return arr.join('');
}

module.exports = removeVowels;

/**
 * @param n
 */
function compute(n) {
    if (n % 2 === 0 || n < 3) {
        return 0;
    }
    let sum = 1;
    for (let i = 3; i <= n; i += 2) {
        sum += (1 / Math.pow(i, 2));
    }
    return sum;
}

module.exports = compute;

/**
 * @param list
 * @param n
 */
function sortIt(list, n) {
    const arr = list.split(', ');
    arr.sort((a, b) => {
        const result = a.toLowerCase()[n - 1].localeCompare(b.toLowerCase()[n - 1]);
        if (result !== 0) {
            return result;
        }
        return a.toLowerCase().localeCompare(b.toLowerCase());
    });
    return arr.join(', ');
}

module.exports = sortIt;

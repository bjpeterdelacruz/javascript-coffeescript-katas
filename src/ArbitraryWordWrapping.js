/**
 * @param string
 */
function wordWrap(string) {
    let str = string;
    const arr = [];
    for (let count = 1; count <= str.length; count++) {
        if (str[count] === '\n') {
            arr.push(str.slice(0, count + 1));
            str = str.slice(count + 1);
            count = 1;
        }
        if (count % 25 === 0) {
            if (str[count - 1] === ' ') {
                arr.push(str.slice(0, count - 1) + '\n');
                str = str.slice(count);
                count = 1;
            }
            else if (str[count - 1] !== ' ' && str[count] === ' ') {
                arr.push(str.slice(0, count) + '\n');
                str = str.slice(count);
                count = 1;
            }
            else {
                arr.push(str.slice(0, count - 1) + '-\n');
                str = str.slice(count - 1);
                count = 1;
            }
        }
    }
    arr.push(str);
    return arr.join('').trim();
}

module.exports = wordWrap;

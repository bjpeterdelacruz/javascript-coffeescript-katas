/**
 * @param arr
 */
function isSquare(arr) {
    if (arr.length === 0) {
        return undefined;
    }
    for (let idx = 0; idx < arr.length; idx++) {
        const result = Math.floor(Math.sqrt(arr[idx]));
        if (result * result !== arr[idx]) {
            return false;
        }
    }
    return true;
}

module.exports = isSquare;

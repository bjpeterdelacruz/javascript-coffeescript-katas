Array.prototype.valueOf = function() {
    if (this.length === 0) {
        return 0;
    }
    let sum = this[0];
    for (let idx = 1; idx < this.length; idx++) {
        sum += this[idx];
    }
    return sum;
};

module.exports = Array.prototype.valueOf;

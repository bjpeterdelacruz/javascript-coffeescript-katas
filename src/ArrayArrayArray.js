/**
 * @param x
 */
function explode(x) {
    const numbers = x.filter(element => typeof element === 'number');
    if (numbers.length === 0) {
        return 'Void!';
    }
    const score = numbers.reduce((a, b) => a + b);
    return Array(score).fill(x);
}

module.exports = explode;

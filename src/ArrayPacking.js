/**
 * @param a
 */
function arrayPacking(a) {
    const binary = a.map(element => {
        let binaryString = element.toString(2);
        while (binaryString.length < 8) {
            binaryString = `0${binaryString}`;
        }
        return binaryString;
    });
    binary.reverse();
    return parseInt(binary.join(''), 2);
}

module.exports = arrayPacking;

Array.prototype.reverse = function() {
    const middle = Math.floor(this.length / 2);
    for (let idx = 0, idx2 = this.length - 1; idx < middle; idx++, idx2--) {
        const temp = this[idx];
        this[idx] = this[idx2];
        this[idx2] = temp;
    }
    return this;
};

module.exports = Array.prototype.reverse;

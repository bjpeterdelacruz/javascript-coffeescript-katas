/**
 * @param n
 */
function asteriscIt(n) {
    let arr = n;
    if (typeof n === 'string') {
        arr = n.split('');
    }
    else if (typeof n === 'number') {
        arr = n.toString().split('');
    }
    else {
        arr = arr.join('').split('');
    }
    for (let idx = 1; idx < arr.length; idx++) {
        if (arr[idx - 1] !== '*' && arr[idx - 1] % 2 === 0 && arr[idx] % 2 === 0) {
            arr.splice(idx, 0, '*');
        }
    }
    return arr.join('');
}

module.exports = asteriscIt;

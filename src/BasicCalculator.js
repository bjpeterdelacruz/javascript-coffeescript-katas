/**
 * @param num1
 * @param operation
 * @param num2
 */
function calculate(num1, operation, num2) {
    const operations = new Map();
    operations.set('+', (a, b) => a + b);
    operations.set('-', (a, b) => a - b);
    operations.set('*', (a, b) => a * b);
    operations.set('/', (a, b) => b === 0 ? null : a / b);
    return operations.has(operation) ? operations.get(operation)(num1, num2) : null;
}

module.exports = calculate;

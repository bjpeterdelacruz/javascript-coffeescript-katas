/**
 * @param string
 */
function calculate(string) {
    const str = string.split('plus').join('+').split('minus').join('-');

    const operators = [];
    const operands = [];
    let currentOperand = '';
    for (let idx = 0; idx < str.length; idx++) {
        if (str[idx] === '+' || str[idx] === '-') {
            operators.push(str[idx]);
            operands.push(currentOperand);
            if (operands.length === 2) {
                const secondOperand = operands.pop();
                const firstOperand = operands.pop();
                operands.push(performMathOperation(operators[0], firstOperand, secondOperand));
                operators.splice(0, 1);
            }
            currentOperand = '';
        }
        else {
            currentOperand += str[idx];
        }
    }
    return performMathOperation(operators[0], operands[0], currentOperand).toString();
}

/**
 * @param operator
 * @param firstOperand
 * @param secondOperand
 */
function performMathOperation(operator, firstOperand, secondOperand) {
    if (operator === '+') {
        return parseInt(firstOperand) + parseInt(secondOperand);
    }
    return parseInt(firstOperand) - parseInt(secondOperand);
}

module.exports = calculate;

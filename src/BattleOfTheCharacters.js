/**
 * @param groupOne
 * @param groupTwo
 */
function battle(groupOne, groupTwo) {
    const sumOne = groupOne.split('').map(letter => getLetterValue(letter))
        .reduce((acc, curr) => acc + curr, 0);
    const sumTwo = groupTwo.split('').map(letter => getLetterValue(letter))
        .reduce((acc, curr) => acc + curr, 0);
    return sumOne > sumTwo ? groupOne : (sumTwo > sumOne ? groupTwo : 'Tie!');
}

const upper = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
const lower = upper.toLowerCase();

/**
 * @param letter
 */
function getLetterValue(letter) {
    if (upper.indexOf(letter) === -1) {
        return (lower.indexOf(letter) + 1) / 2;
    }
    return upper.indexOf(letter) + 1;
}

module.exports = battle;

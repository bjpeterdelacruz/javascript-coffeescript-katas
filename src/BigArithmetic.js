/**
 * @param firstNumber
 * @param secondNumber
 */
function padStrings(firstNumber, secondNumber) {
    let a = firstNumber;
    let b = secondNumber;
    const numZeros = Math.abs(a.length - b.length);
    if (a.length < b.length) {
        for (let count = 0; count < numZeros; count++) {
            a = '0' + a;
        }
    }
    else {
        for (let count = 0; count < numZeros; count++) {
            b = '0' + b;
        }
    }
    return [a, b];
}

/**
 * @param a
 * @param b
 */
function isGreaterThanOrEqualTo(a, b) {
    let idx = 0;
    const [tempA, tempB] = padStrings(a, b);
    while (parseInt(tempA[idx]) === parseInt(tempB[idx])) {
        idx++;
    }
    return parseInt(tempA[idx]) > parseInt(tempB[idx]);
}

/**
 * @param a
 * @param b
 */
function getBiggerNumber(a, b) {
    return isGreaterThanOrEqualTo(a, b) ? [a, b] : [b, a];
}

/**
 * @param a
 * @param b
 */
function sumStrings(a, b) {
    const [numA, numB] = padStrings(a, b);
    let carry = 0;
    let totalSum = '';
    for (let idx = numA.length - 1; idx >= 0; idx--) {
        const sum = parseInt(numA[idx]) + parseInt(numB[idx]) + carry;
        carry = sum > 9 ? 1 : 0;
        totalSum = sum.toString()[sum.toString().length - 1] + totalSum;
    }
    totalSum = carry.toString() + totalSum;
    while (totalSum.indexOf('0') === 0) {
        totalSum = totalSum.slice(1);
    }
    return totalSum.length === 0 ? '0' : totalSum;
}

/**
 * @param a
 * @param b
 */
function diffStrings(a, b) {
    const [numA, numB] = padStrings(a, b);
    const [firstNumber, secondNumber] = getBiggerNumber(numA, numB);
    let borrow = 0;
    let totalDiff = '';
    for (let idx = firstNumber.length - 1; idx >= 0; idx--) {
        let num1 = parseInt(firstNumber[idx]) - borrow;
        const num2 = parseInt(secondNumber[idx]);
        if (num1 < num2) {
            num1 += 10;
            borrow = 1;
        }
        else {
            borrow = 0;
        }
        totalDiff = (num1 - num2) + totalDiff;
    }
    while (totalDiff.indexOf('0') === 0) {
        totalDiff = totalDiff.slice(1);
    }
    return totalDiff.length === 0 ? '0' : totalDiff;
}

/**
 * @param number
 */
function replaceNegativeSign(number) {
    let num = number;
    while (num.indexOf('-') !== -1) {
        num = num.replace('-', '');
    }
    return num;
}

/**
 * @param a
 * @param b
 */
function bigAdd(a, b) {
    let numA = a.toString();
    let numB = b.toString();
    if (numA.indexOf('-') === -1 && numB.indexOf('-') === -1) {
        return sumStrings(numA, numB);
    }
    else if (numA.indexOf('-') !== -1 && numB.indexOf('-') !== -1) {
        const sum = sumStrings(replaceNegativeSign(numA), replaceNegativeSign(numB));
        return sum === '0' ? '0' : '-' + sum;
    }
    else if (numA.indexOf('-') !== -1 && numB.indexOf('-') === -1) {
        numA = replaceNegativeSign(numA);
        const diff = diffStrings(numA, numB);
        return diff === '0' ? '0' : (isGreaterThanOrEqualTo(numA, numB) ? '-' + diff : diff);
    }
    numB = replaceNegativeSign(numB);
    const diff = diffStrings(numA, numB);
    return diff === '0' ? '0' : (isGreaterThanOrEqualTo(numA, numB) ? diff : '-' + diff);
}

/**
 * @param a
 * @param b
 */
function bigSub(a, b) {
    const numA = a.toString();
    let numB = b.toString();
    if (numA.indexOf('-') === -1 && numB.indexOf('-') === -1) {
        const diff = diffStrings(numA, numB);
        return diff === '0' ? '0' : (isGreaterThanOrEqualTo(numA, numB) ? diff : '-' + diff);
    }
    else if (numB.indexOf('-') !== -1) {
        numB = replaceNegativeSign(numB);
        return bigAdd(numA, numB);
    }
    return bigAdd(numA, '-' + numB);
}

module.exports = { bigAdd, bigSub };

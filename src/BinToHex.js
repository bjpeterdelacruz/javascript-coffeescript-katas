/**
 * @param binaryString
 */
function binToHex(binaryString) {
    let bs = binaryString;
    while (bs.length % 4 !== 0) {
        bs = '0' + bs;
    }

    const binToHexMap = new Map();
    binToHexMap.set('0000', '0');
    binToHexMap.set('0001', '1');
    binToHexMap.set('0010', '2');
    binToHexMap.set('0011', '3');
    binToHexMap.set('0100', '4');
    binToHexMap.set('0101', '5');
    binToHexMap.set('0110', '6');
    binToHexMap.set('0111', '7');
    binToHexMap.set('1000', '8');
    binToHexMap.set('1001', '9');
    binToHexMap.set('1010', 'a');
    binToHexMap.set('1011', 'b');
    binToHexMap.set('1100', 'c');
    binToHexMap.set('1101', 'd');
    binToHexMap.set('1110', 'e');
    binToHexMap.set('1111', 'f');

    const hex = [];
    for (let idx = 0; idx <= bs.length; idx++) {
        if (idx % 4 === 0) {
            hex.push(binToHexMap.get(bs.substring(idx - 4, idx)));
        }
    }

    return hex.join('');
}

/**
 * @param hexString
 */
function hexToBin(hexString) {
    const hexToBinMap = new Map();
    hexToBinMap.set('0', '0000');
    hexToBinMap.set('1', '0001');
    hexToBinMap.set('2', '0010');
    hexToBinMap.set('3', '0011');
    hexToBinMap.set('4', '0100');
    hexToBinMap.set('5', '0101');
    hexToBinMap.set('6', '0110');
    hexToBinMap.set('7', '0111');
    hexToBinMap.set('8', '1000');
    hexToBinMap.set('9', '1001');
    hexToBinMap.set('a', '1010');
    hexToBinMap.set('b', '1011');
    hexToBinMap.set('c', '1100');
    hexToBinMap.set('d', '1101');
    hexToBinMap.set('e', '1110');
    hexToBinMap.set('f', '1111');

    let binString = hexString.split('')
        .map(letter => hexToBinMap.get(letter.toLowerCase())).join('');
    while (binString[0] === '0' && binString.length > 1) {
        binString = binString.substring(1);
    }
    return binString;
}

module.exports = { binToHex, hexToBin };

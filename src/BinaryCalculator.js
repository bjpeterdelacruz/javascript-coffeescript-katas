/**
 * @param firstOperand
 * @param secondOperand
 * @param operator
 */
function calculate(firstOperand, secondOperand, operator) {
    const ops = new Map();
    ops.set('add', (a, b) => a + b);
    ops.set('subtract', (a, b) => a - b);
    ops.set('multiply', (a, b) => a * b);
    const a = binStringToInt(firstOperand);
    const b = binStringToInt(secondOperand);
    const result = ops.get(operator)(a, b);
    return intToBinString(result);
}

/**
 * @param string
 */
function binStringToInt(string) {
    if (string.indexOf('-') === 0) {
        return parseInt(string.substring(1), 2) * -1;
    }
    return parseInt(string, 2);
}

/**
 * @param number
 */
function intToBinString(number) {
    if (number < 0) {
        return `-${parseInt(number.toString().substring(1)).toString(2)}`;
    }
    return number.toString(2);
}

module.exports = { calculate, binStringToInt, intToBinString };

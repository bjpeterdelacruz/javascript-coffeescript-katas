/**
 * @param number
 */
function toBinaryString(number) {
    let currentNumber = number;
    const arr = [];
    do {
        const result = currentNumber & 1;
        arr.push(result);
        currentNumber = currentNumber >> 1;
    } while (currentNumber !== 0);
    return arr.reverse().join('');
}

module.exports = toBinaryString;

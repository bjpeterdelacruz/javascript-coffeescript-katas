/**
 * @param animals
 */
function countTheAnimals(animals) {
    let total = 0;
    for (const count of Object.values(animals)) {
        total += parseInt(count, 2);
    }
    return total;
}

module.exports = countTheAnimals;

/**
 * @param numCandles
 * @param string
 */
function cake(numCandles, string) {
    const alphabet = 'abcdefghijklmnopqrstuvwxyz';
    let sum = 0;
    for (let idx = 0; idx < string.length; idx++) {
        if (idx % 2 === 0) {
            sum += string.charCodeAt(idx);
        }
        else {
            sum += alphabet.indexOf(string[idx].toLowerCase()) + 1;
        }
    }
    return sum > numCandles * 0.7 ? 'Fire!' : 'That was close!';
}

module.exports = cake;

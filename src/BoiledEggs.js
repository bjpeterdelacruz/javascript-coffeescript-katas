function cookingTime(eggs) {
    return 5 * Math.floor(eggs / 8) + (eggs % 8 === 0 ? 0 : 5);
}

module.exports = cookingTime;

/**
 * @param wordDict
 * @param string
 */
function wordBreak(wordDict, string) {
    const dp = Array(string.length);
    const map = {};

    for (let idx = 0; idx < wordDict.length; idx++) {
        map[wordDict[idx]] = true;
    }

    const result = find(string, map, dp, 0);
    return result.length === 0 ? undefined : result[0].split(' ');
}

/**
 * @param string
 * @param map
 * @param dp
 * @param index
 */
function find(string, map, dp, index) {
    if (dp[index]) {
        return dp[index];
    }

    let str = '';
    let tmp = null;
    const len = string.length;

    dp[index] = [];

    for (let idx = index; idx < len; idx++) {
        str = string.substring(index, idx + 1);
        if (!map[str]) {
            continue;
        }
        if (idx === len - 1) {
            dp[index].push(str);
            break;
        }
        tmp = find(string, map, dp, idx + 1);
        for (let j = 0; j < tmp.length; j++) {
            dp[index].push(str + ' ' + tmp[j]);
        }
    }

    return dp[index];
}

module.exports = wordBreak;

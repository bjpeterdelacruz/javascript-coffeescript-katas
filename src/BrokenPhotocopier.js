/**
 * @param x
 */
function broken(x) {
    return x.split('').map(digit => digit === '0' ? '1' : '0').join('');
}

module.exports = broken;

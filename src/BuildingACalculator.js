const Calculator = {
    add: (operand1, operand2) => {
        return operand1 + operand2;
    },
    subtract: (operand1, operand2) => {
        return operand1 - operand2;
    },
    multiply: (operand1, operand2) => {
        return operand1 * operand2;
    },
    divide: (operand1, operand2) => {
        return operand2 === 0 ? false : operand1 / operand2;
    }
};

module.exports = Calculator;

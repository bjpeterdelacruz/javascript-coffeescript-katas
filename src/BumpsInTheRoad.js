/**
 * @param x
 */
function bump(x) {
    const count = x.split('').filter(letter => letter === 'n').length;
    return count > 15 ? 'Car Dead' : 'Woohoo!';
}

module.exports = bump;

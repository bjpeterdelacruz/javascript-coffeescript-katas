const Calculator = {
    average: function(...args) {
        return args.length === 0 ? 0 : args.reduce((acc, curr) => acc + curr) / args.length;
    }
};

module.exports = Calculator;

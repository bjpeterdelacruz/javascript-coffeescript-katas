/**
 * @param value
 * @param from
 * @param to
 */
function convert(value, from, to) {
    if (from === 'cup' && to === 'g') {
        return value * 200;
    }
    else if (from === 'cup' && to === 'ml') {
        return value * 220;
    }
    else if (from === 'tbsp' && to === 'g') {
        return value * 14;
    }
    else if (from === 'tbsp' && to === 'ml') {
        return value * 15;
    }
    return value * 5;
}

/**
 * @param value
 */
function calc(value) {
    if (value.indexOf('/') === -1) {
        return parseInt(value);
    }
    const numerator = value.split('/')[0];
    const denominator = value.split('/')[1];
    return numerator / denominator;
}

/**
 * @param available
 * @param recipe
 */
function canBake(available, recipe) {
    const missingIngredients = [];
    for (const ingredient in recipe) {
        const ingr = `${ingredient}`;
        if (!available[ingr]) {
            missingIngredients.push(ingr);
            continue;
        }
        let recipeAmount = `${recipe[ingredient]}`;
        const recipeUnit = recipeAmount.split(' ')[1];
        recipeAmount = calc(recipeAmount.split(' ')[0]);

        let availableAmount = `${available[ingredient]}`;
        const availableUnit = availableAmount.split(' ')[1];
        availableAmount = availableAmount.split(' ')[0];
        recipeAmount = convert(recipeAmount, recipeUnit, availableUnit);

        if (availableAmount < recipeAmount) {
            missingIngredients.push(ingr);
        }
    }
    if (missingIngredients.length === 0) {
        return 'You can bake';
    }
    return 'Not enough ingredients: need ' + missingIngredients.join(', ').trim();
}

module.exports = canBake;

/**
 * @param array
 */
function matrix(array) {
    const copy = [];
    for (let idx = 0; idx < array.length; idx++) {
        copy.push([...array[idx]]);
        const currentArray = copy[copy.length - 1];
        currentArray[idx] = currentArray[idx] < 0 ? 0 : 1;
    }
    return copy;
}

module.exports = matrix;

/**
 * @param string
 */
function chuckPushUps(string) {
    if (typeof string !== 'string' || string.length === 0) {
        return 'FAIL!!';
    }
    const arr = [];
    for (const str of string.split('"')) {
        arr.push(str.trim());
    }
    const decimal = [];
    for (const str of arr) {
        for (const str2 of str.split(' ')) {
            let dec = '';
            for (let idx = 0; idx < str2.length; idx++) {
                if (str2[idx] === '1' || str2[idx] === '0') {
                    dec += str2[idx];
                }
            }
            if (dec.length > 0) {
                decimal.push(parseInt(dec, 2));
            }
        }
    }
    return decimal.length === 0 ? 'CHUCK SMASH!!' : decimal.sort((a, b) => a - b).pop();
}

module.exports = chuckPushUps;

/**
 * @param list
 * @param date
 */
function addUsername(list, date) {
    return list.map(developer => {
        let username = developer.firstName.toLowerCase() + developer.lastName[0].toLowerCase();
        username = `${username}${date.getFullYear() - parseInt(developer.age)}`;
        developer.username = username;
        return developer;
    });
}

module.exports = addUsername;

/**
 * @param list
 */
function getAverageAge(list) {
    return Math.round(list.map(developer => developer.age).reduce((a, b) => a + b) / list.length);
}

module.exports = getAverageAge;

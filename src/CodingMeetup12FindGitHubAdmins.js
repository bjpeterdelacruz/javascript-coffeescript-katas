/**
 * @param list
 * @param lang
 */
function findAdmin(list, lang) {
    return list.filter(person => person.language === lang && person.githubAdmin === 'yes');
}

module.exports = findAdmin;

/**
 * @param list
 */
function isLanguageDiverse(list) {
    const devsCounts = new Map();
    list.map(developer => developer.language).forEach(language => {
        const count = devsCounts.get(language);
        devsCounts.set(language, count === undefined ? 1 : count + 1);
    });
    const allCounts = Array.from(devsCounts.entries()).map(element => element[1]);
    allCounts.sort((a, b) => b - a);
    return allCounts[allCounts.length - 1] * 2 >= allCounts[0];
}

module.exports = isLanguageDiverse;

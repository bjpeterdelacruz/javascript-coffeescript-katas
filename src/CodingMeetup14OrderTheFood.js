/**
 * @param list
 */
function orderFood(list) {
    const meals = {};
    list.map(developer => developer.meal).forEach(meal => {
        const numberOfMeals = meals[meal];
        meals[meal] = numberOfMeals ? numberOfMeals + 1 : 1;
    });
    return meals;
}

module.exports = orderFood;

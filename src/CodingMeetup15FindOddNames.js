/**
 * @param list
 */
function findOddNames(list) {
    return list.filter(developer => {
        return developer.firstName.split('').map(character => character.charCodeAt(0))
            .reduce((a, b) => a + b) % 2 === 1;
    });
}

module.exports = findOddNames;

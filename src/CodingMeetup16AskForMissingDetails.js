/**
 * @param list
 */
function askForMissingDetails(list) {
    return list.filter(developer => {
        const propertyNames = Object.getOwnPropertyNames(developer);
        const missingInfo = propertyNames.filter(property => developer[property] === null)[0];
        if (missingInfo) {
            developer.question = `Hi, could you please provide your ${missingInfo}.`;
        }
        return missingInfo;
    });
}

module.exports = askForMissingDetails;

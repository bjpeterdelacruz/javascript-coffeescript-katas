/**
 * @param list
 */
function greetDevelopers(list) {
    list.forEach(person => {
        const language = person.language;
        person.greeting = `Hi ${person.firstName}, what do you like the most about ${language}?`;
    });
    return list;
}

module.exports = greetDevelopers;

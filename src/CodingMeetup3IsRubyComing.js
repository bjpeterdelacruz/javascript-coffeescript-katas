/**
 * @param list
 */
function isRubyComing(list) {
    return list.map(dev => dev.language).filter(lang => lang === 'Ruby').length > 0;
}

module.exports = isRubyComing;

/**
 * @param list
 */
function getFirstPython(list) {
    const developer = list.find(developer => developer.language === 'Python');
    if (developer) {
        return `${developer.firstName}, ${developer.country}`;
    }
    return 'There will be no Python developers';
}

module.exports = getFirstPython;

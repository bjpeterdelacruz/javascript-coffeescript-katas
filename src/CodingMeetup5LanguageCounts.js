'use strict';

/**
 * @param list
 */
function countLanguages(list) {
    const counts = {};
    list.map(object => object.language).forEach(language => {
        if (counts[language]) {
            counts[language]++;
        }
        else {
            counts[language] = 1;
        }
    });
    return counts;
}

module.exports = countLanguages;

/**
 * @param list
 */
function findSenior(list) {
    const max = Math.max(...list.map(developer => developer.age));
    return list.filter(developer => developer.age === max);
}

module.exports = findSenior;

/**
 * @param list
 */
function allContinents(list) {
    return new Set(list.map(developer => developer.continent)).size === 5;
}

module.exports = allContinents;

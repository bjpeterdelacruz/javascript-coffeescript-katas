/**
 * @param list
 */
function isAgeDiverse(list) {
    const ages = new Set(list.map(developer => Math.floor(developer.age / 10)));
    const missingAges = [1, 2, 3, 4, 5, 6, 7, 8, 9].filter(age => !ages.has(age)).length;
    if (missingAges > 0) {
        return false;
    }
    const centenarians = [10, 11, 12, 13, 14, 15, 16, 17, 18, 19].filter(age => ages.has(age))
        .length;
    return centenarians > 0;
}

module.exports = isAgeDiverse;

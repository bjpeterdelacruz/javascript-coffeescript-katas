/**
 * @param x1
 * @param x2
 */
function quadratic(x1, x2) {
    return [1, (-1 * x2) + (-1 * x1), -1 * x1 * -1 * x2];
}

module.exports = quadratic;

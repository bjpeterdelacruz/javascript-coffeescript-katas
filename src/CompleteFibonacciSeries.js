/**
 * @param n
 */
function fibonacci(n) {
    const fib = [0, 1];
    if (n < 1) {
        return [];
    }
    if (n === 1) {
        return [0];
    }
    for (let count = 2; count < n; count++) {
        fib.push(fib[count - 2] + fib[count - 1]);
    }
    return fib;
}

module.exports = fibonacci;

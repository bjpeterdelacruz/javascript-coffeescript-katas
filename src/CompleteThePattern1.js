function pattern(number) {
    const arr = [];
    for (let idx = 1; idx <= number; idx++) {
        arr.push(idx.toString().repeat(idx));
    }
    return arr.join('\n');
}

module.exports = pattern;

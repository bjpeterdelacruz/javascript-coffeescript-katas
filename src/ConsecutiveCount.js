/**
 * @param items
 * @param key
 */
function getConsectiveItems(items, key) {
    const string = items.toString();
    const keyToFind = key.toString();
    if (string.indexOf(keyToFind) === -1) {
        return 0;
    }
    let longestCount = 0;
    let currentCount = 0;
    for (let idx = 0; idx < string.length;) {
        if (string.substring(idx, idx + keyToFind.length) === keyToFind) {
            currentCount++;
            if (currentCount > longestCount) {
                longestCount = currentCount;
            }
            idx += keyToFind.length;
        }
        else {
            currentCount = 0;
            idx++;
        }
    }
    return longestCount;
}

module.exports = getConsectiveItems;

/**
 * @param arr
 */
function sortLetters(arr) {
    const vowels = [];
    const consonants = [];
    arr.forEach(subarr => {
        subarr.map(element => element.toString().toUpperCase()).forEach(element => {
            if (!element.match(/[0-9]/)) {
                if (element.match(/[AEIOU]/)) {
                    vowels.push(element);
                }
                else {
                    consonants.push(element);
                }
            }
        });
    });
    return [vowels, consonants];
}

module.exports = sortLetters;

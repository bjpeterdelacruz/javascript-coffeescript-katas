String.prototype.contains = function(str, caseSensitive) {
    const arr = caseSensitive ? this.split(str) : this.toLowerCase().split(str.toLowerCase());
    return arr.length !== 1;
};

module.exports = String.prototype.contains;

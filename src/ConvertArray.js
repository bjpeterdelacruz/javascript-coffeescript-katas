/**
 * @param stringarray
 */
function toNumberArray(stringarray) {
    return stringarray.map(x => parseFloat(x));
}

module.exports = toNumberArray;

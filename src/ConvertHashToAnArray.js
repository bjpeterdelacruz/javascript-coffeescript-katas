function convertHashToArray(hash) {
    const arr = [];
    for (const [key, value] of Object.entries(hash)) {
        arr.push([key, value]);
    }
    arr.sort((a, b) => a[0].localeCompare(b[0]));
    return arr;
}

module.exports = convertHashToArray;

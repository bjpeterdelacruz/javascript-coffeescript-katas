/**
 * @param isbn
 */
function isbnConverter(isbn) {
    const numbers = [9, 7, 8];
    isbn.substring(0, isbn.length - 1).split('').forEach((number) => {
        if (number.match(/[0-9]/)) {
            numbers.push(Number.parseInt(number));
        }
    });
    let sum = 0;
    numbers.forEach((number, idx) => {
        sum += (number * (idx % 2 === 0 ? 1 : 3));
    });
    return `978-${isbn.substring(0, isbn.length - 1)}${sum % 10 === 0 ? 0 : 10 - (sum % 10)}`;
}

module.exports = isbnConverter;

/**
 * @param string
 */
function toUnderscore(string) {
    const snakeCase = [];
    let str = string.toString();
    for (const char of str) {
        if (char.charCodeAt(0) >= 65 && char.charCodeAt(0) <= 90) {
            snakeCase.push('_' + char.toLowerCase());
        }
        else {
            snakeCase.push(char);
        }
    }
    str = snakeCase.join('');
    if (str.charAt(0) === '_') {
        return str.substring(1);
    }
    return str;
}

module.exports = toUnderscore;

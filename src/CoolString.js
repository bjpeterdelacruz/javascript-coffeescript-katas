/**
 * @param string
 */
function coolString(string) {
    const lowercase = 'abcdefghijklmnopqrstuvwxyz';
    const uppercase = lowercase.toUpperCase();
    const letters = `${lowercase}${uppercase}`;
    const nonLettersCount = string.split('').filter(char => letters.indexOf(char) === -1).length;
    if (nonLettersCount > 0) {
        return false;
    }
    for (let idx = 0; idx < string.length; idx++) {
        if (lowercase.indexOf(string[idx]) !== -1 && lowercase.indexOf(string[idx + 1]) !== -1) {
            return false;
        }
        if (uppercase.indexOf(string[idx]) !== -1 && uppercase.indexOf(string[idx + 1]) !== -1) {
            return false;
        }
    }
    return true;
}

module.exports = coolString;

/**
 * @param string
 */
function correct(string) {
    return string.split('')
        .map(char => char === '0' ? 'O' : char)
        .map(char => char === '1' ? 'I' : char)
        .map(char => char === '5' ? 'S' : char).join('');
}

module.exports = correct;

/**
 * @param addresses
 */
function count(addresses) {
    const counts = new Map();
    for (const address of addresses) {
        if (address.state === undefined) {
            throw new Error();
        }
        const count = counts.get(address.state);
        counts.set(address.state, count === undefined ? 1 : count + 1);
    }
    return Array.from(counts.entries()).map(element => {
        const object = {};
        object.state = element[0];
        object.count = element[1];
        return object;
    });
}

module.exports = count;

/**
 * @param multiple
 * @param length
 */
function countBy(multiple, length) {
    return Array.from(Array(length).keys()).map(value => (value + 1) * multiple);
}

module.exports = countBy;

/**
 * @param input
 */
function countLettersAndDigits(input) {
    return input.split('').filter(character => character.match(/[a-zA-Z0-9]/)).length;
}

module.exports = countLettersAndDigits;

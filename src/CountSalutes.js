/**
 * @param hallway
 */
function countSalutes(hallway) {
    let salutes = 0;
    for (let idx = 0; idx < hallway.length; idx++) {
        if (hallway[idx] !== '>') {
            continue;
        }
        for (let secondIdx = idx + 1; secondIdx < hallway.length; secondIdx++) {
            if (hallway[secondIdx] === '<') {
                salutes += 2;
            }
        }
    }
    return salutes;
}

module.exports = countSalutes;

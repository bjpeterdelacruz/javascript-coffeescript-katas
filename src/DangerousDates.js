/**
 * @param date
 */
function nextFiveDays(date) {
    const dates = [];
    for (let i = 1; i <= 5; i++) {
        const day = new Date(date.getTime() + (i * 1000 * 60 * 60 * 24));
        dates.push(`${day.getMonth() + 1}/${day.getDate()}/${day.getFullYear()}`);
    }

    return dates.join(', ');
}

module.exports = nextFiveDays;

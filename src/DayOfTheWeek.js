/**
 * @param date
 */
function getDayOfTheWeek(date) {
    const daysOfWeek = new Map();
    daysOfWeek.set(0, 'Sunday');
    daysOfWeek.set(1, 'Monday');
    daysOfWeek.set(2, 'Tuesday');
    daysOfWeek.set(3, 'Wednesday');
    daysOfWeek.set(4, 'Thursday');
    daysOfWeek.set(5, 'Friday');
    daysOfWeek.set(6, 'Saturday');
    if (typeof date === 'string') {
        const [month, day, year] = date.split('/');
        return daysOfWeek.get(new Date(year, month - 1, day).getDay());
    }
    return daysOfWeek.get(date.getDay());
}

module.exports = getDayOfTheWeek;

/**
 * @param cipher
 */
function decipher(cipher) {
    const arr = [];
    for (let idx = 0; idx < cipher.length;) {
        let characterCode = cipher[idx] + cipher[idx + 1];
        if (cipher[idx] === '9') {
            idx += 2;
        }
        else {
            characterCode += cipher[idx + 2];
            idx += 3;
        }
        arr.push(String.fromCharCode(parseInt(characterCode)));
    }
    return arr.join('');
}

module.exports = decipher;

/**
 * @param matrix
 */
function sum(matrix) {
    let sum = 0;
    for (let idx = 0, idx2 = matrix.length - 1; idx < matrix.length; idx++, idx2--) {
        sum += matrix[idx][idx] + matrix[idx][idx2];
    }
    return sum;
}

module.exports = sum;

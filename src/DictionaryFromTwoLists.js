function createDict(keys, values) {
    const array = [];
    for (let idx = 0; idx < Math.max(keys.length, values.length); idx++) {
        if (idx < keys.length) {
            array.push([keys[idx], idx < values.length ? values[idx] : null]);
        }
        else {
            break;
        }
    }
    return Object.fromEntries(new Map(array));
}

module.exports = createDict;

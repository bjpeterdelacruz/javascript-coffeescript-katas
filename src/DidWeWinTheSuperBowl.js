/**
 * @param plays
 */
function didWeWin(plays) {
    const turnover = plays.filter(play => play.length > 0 && play[1] === 'turnover');
    if (turnover.length > 0) {
        return false;
    }
    let yardage = 0;
    plays.forEach(play => {
        if (play.length > 0) {
            if (play[1] === 'run' || play[1] === 'pass') {
                yardage += play[0];
            }
            else {
                yardage -= play[0];
            }
        }
    });
    return yardage > 10;
}

module.exports = didWeWin;

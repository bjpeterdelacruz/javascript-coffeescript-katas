/**
 * @param arr
 */
function diffBig2(arr) {
    let highest = -Infinity;
    let highestIdx = 0;
    for (let idx = 0; idx < arr.length; idx++) {
        if (arr[idx] > highest) {
            highest = arr[idx];
            highestIdx = idx;
        }
    }
    let secondHighest = -Infinity;
    for (let idx = 0; idx < arr.length; idx++) {
        if (arr[idx] > secondHighest && arr[idx] <= highest && idx !== highestIdx) {
            secondHighest = arr[idx];
        }
    }
    return highest - secondHighest;
}

module.exports = diffBig2;

/**
 * @param str
 */
function digitSum(str) {
    let sum = str;
    while (sum.length !== 1) {
        let total = 0;
        for (let idx = 0; idx < sum.length; idx++) {
            total += parseInt(sum[idx]);
        }
        sum = total.toString();
    }
    return sum;
}

module.exports = digitSum;

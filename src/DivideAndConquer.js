function divCon(array) {
    let total = 0;
    let subtrahend = 0;
    for (let idx = 0; idx < array.length; idx++) {
        if (typeof array[idx] === 'string') {
            subtrahend += parseInt(array[idx]);
        }
        else {
            total += array[idx];
        }
    }
    return total - subtrahend;
}

module.exports = divCon;

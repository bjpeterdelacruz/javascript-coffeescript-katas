String.prototype.repeat = function(count) {
    let string = '';
    for (let i = 0; i < count; i++) {
        string += this;
    }
    return string;
};

module.exports = String.prototype.repeat;

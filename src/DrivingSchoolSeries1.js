/**
 * @param list
 */
function passed(list) {
    const scores = list.filter(score => score <= 18);
    if (scores.length === 0) {
        return 'No pass scores registered.';
    }
    return Math.round(scores.reduce((acc, curr) => acc + curr) / scores.length);
}

module.exports = passed;

/**
 * @param mins
 */
function cost(mins) {
    let remainingMins = mins - 60;
    let numHalfHours = 0;
    while (remainingMins > 30) {
        numHalfHours++;
        remainingMins -= 30;
    }
    if (remainingMins > 5) {
        numHalfHours++;
    }
    return 30 + (numHalfHours * 10);
}

module.exports = cost;

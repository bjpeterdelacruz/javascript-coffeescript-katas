/**
 * @param base
 * @param a
 * @param b
 */
function logs(base, a, b) {
    return Math.log(a * b) / Math.log(base);
}

module.exports = logs;

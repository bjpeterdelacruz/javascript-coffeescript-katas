/**
 * @param startDate
 * @param endDate
 */
function elapsedSeconds(startDate, endDate) {
    return Math.floor((endDate.getTime() - startDate.getTime()) / 1000);
}

module.exports = elapsedSeconds;

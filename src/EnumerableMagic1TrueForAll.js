/**
 * @param arr
 * @param fun
 */
function all(arr, fun) {
    return arr.filter((value) => fun(value)).length === arr.length;
}

module.exports = all;

/**
 * @param string
 */
function postfixEvaluator(string) {
    const stack = [];
    for (const chars of string.split(/[\s]+/)) {
        if (!isNaN(chars)) {
            stack.push(parseInt(chars));
        }
        else {
            const secondNumber = parseInt(stack.pop());
            const firstNumber = parseInt(stack.pop());
            if (chars === '+') {
                stack.push(firstNumber + secondNumber);
            }
            else if (chars === '-') {
                stack.push(firstNumber - secondNumber);
            }
            else if (chars === '*') {
                stack.push(firstNumber * secondNumber);
            }
            else {
                stack.push(firstNumber / secondNumber);
            }
        }
    }
    return parseInt(stack.pop());
}

module.exports = postfixEvaluator;

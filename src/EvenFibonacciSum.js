function fibonacci(max) {
    let a = 0;
    let b = 1;
    let evenSum = 0;
    let sum = a + b;
    while (sum < max) {
        if (sum % 2 === 0) {
            evenSum += sum;
        }
        a = b;
        b = sum;
        sum = a + b;
    }
    return evenSum;
}

module.exports = fibonacci;

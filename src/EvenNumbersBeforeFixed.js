/**
 * @param sequence
 * @param fixedElement
 */
function evenNumbersBeforeFixed(sequence, fixedElement) {
    const idx = sequence.indexOf(fixedElement);
    if (idx === -1) {
        return -1;
    }
    return sequence.slice(0, idx).filter(element => element % 2 === 0).length;
}

module.exports = evenNumbersBeforeFixed;

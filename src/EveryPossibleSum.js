/**
 * @param num
 */
function digits(num) {
    const numbers = num.toString().split('').map(element => parseInt(element));
    const arr = [];
    for (let firstNumIdx = 0; firstNumIdx < numbers.length; firstNumIdx++) {
        for (let secondNumIdx = firstNumIdx + 1; secondNumIdx < numbers.length; secondNumIdx++) {
            arr.push(numbers[firstNumIdx] + numbers[secondNumIdx]);
        }
    }
    return arr;
}

module.exports = digits;

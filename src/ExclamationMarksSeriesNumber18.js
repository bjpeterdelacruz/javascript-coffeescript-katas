/**
 * @param string
 */
function slot(string) {
    if (['!!!!!', '?????'].indexOf(string) !== -1) {
        return 1000;
    }
    if (string.indexOf('!!!!') !== -1 || string.indexOf('????') !== -1) {
        return 800;
    }
    if (['???!!', '!!!??', '??!!!', '!!???'].indexOf(string) !== -1) {
        return 500;
    }
    if (string.indexOf('!!!') !== -1 || string.indexOf('???') !== -1) {
        return 300;
    }
    let str = string;
    while (str.indexOf('!!') !== -1 || str.indexOf('??') !== -1) {
        str = str.replace('!!', '..').replace('??', '..');
    }
    switch (str.split('.').length - 1) {
    case 4:
        return 200;
    case 2:
        return 100;
    default:
        return 0;
    }
}

module.exports = slot;

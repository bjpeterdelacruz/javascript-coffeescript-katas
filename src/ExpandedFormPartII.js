/**
 * @param num
 */
function expandedForm(num) {
    const s = num.toString();
    const arr = [];
    let index = 0;
    if (s.indexOf('.') === -1) {
        while (index < s.length) {
            if (s[index] !== '0') {
                arr.push(s[index] + '0'.repeat(s.length - index - 1));
            }
            index++;
        }
        return arr.join(' + ');
    }
    const wholePart = s.substr(0, s.indexOf('.'));
    while (s[index] !== '.') {
        if (s[index] !== '0') {
            arr.push(s[index] + '0'.repeat(wholePart.length - index - 1));
        }
        index++;
    }
    let countZeros = 1;
    while (++index < s.length) {
        if (s[index] !== '0') {
            arr.push(s[index] + '/1' + '0'.repeat(countZeros));
        }
        countZeros++;
    }
    return arr.join(' + ');
}

module.exports = expandedForm;

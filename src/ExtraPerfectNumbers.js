/**
 * @param n
 */
function extraPerfect(n) {
    const arr = [];
    for (let counter = 1; counter <= n; counter += 2) {
        arr.push(counter);
    }
    return arr;
}

module.exports = extraPerfect;

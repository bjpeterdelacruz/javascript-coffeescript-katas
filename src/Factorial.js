/**
 * @param number
 */
function factorial(number) {
    if (number < 0 || number > 12) {
        throw Error('number must be between 0 and 12, inclusive');
    }
    if (number === 0) {
        return 1;
    }
    let num = number;
    let result = 1;
    do {
        result *= num;
    } while (--num > 0);
    return result;
}

module.exports = factorial;

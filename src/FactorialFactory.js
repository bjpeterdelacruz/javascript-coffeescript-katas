/**
 * @param n
 */
function factorial(n) {
    if (n === 0) {
        return 1;
    }
    if (n < 0) {
        return null;
    }
    let product = 1;
    for (let idx = 2; idx <= n; idx++) {
        product *= idx;
    }
    return product;
}

module.exports = factorial;

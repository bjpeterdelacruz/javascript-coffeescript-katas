/**
 * @param a
 * @param b
 * @param n
 */
function find(a, b, n) {
    if (a === 0 && b === 0) {
        return 0;
    }
    let string = a + '' + b;
    let sum = a + b;
    while (string.length <= n) {
        string += sum;
        sum = parseInt(string[string.length - 2]) + parseInt(string[string.length - 1]);
        if (string.indexOf('1123581347') !== -1) {
            return parseInt('1123581347'[(n - string.indexOf('1123581347')) % 10]);
        }
        if (string.indexOf('1459') !== -1) {
            return parseInt('1459'[(n - string.indexOf('1459')) % 4]);
        }
    }
    return parseInt(string[n]);
}

module.exports = find;

/**
 * @param files
 */
function solve(files) {
    const counts = {};
    let max = 0;
    for (const file of files) {
        const idx = file.lastIndexOf('.');
        if (idx === -1) {
            continue;
        }
        const ext = file.substr(idx);
        if (counts[ext]) {
            counts[ext]++;
        }
        else {
            counts[ext] = 1;
        }
        if (counts[ext] > max) {
            max = counts[ext];
        }
    }
    const results = [];
    for (const [key, value] of Object.entries(counts)) {
        if (value === max) {
            results.push(key);
        }
    }
    return results.sort();
}

module.exports = solve;

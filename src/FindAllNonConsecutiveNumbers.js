/**
 * @param arr
 */
function allNonConsecutive(arr) {
    const numbers = [];
    for (let num = arr[0]; num <= arr[arr.length - 1]; num++) {
        numbers.push(num);
    }
    const missing = numbers.filter(num => arr.indexOf(num) === -1);
    const results = [];
    for (const number of missing) {
        if (arr.indexOf(number + 1) !== -1) {
            results.push({ i: arr.indexOf(number + 1), n: number + 1 });
        }
    }
    return results;
}

module.exports = allNonConsecutive;

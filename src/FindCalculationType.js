/**
 * @param a
 * @param b
 * @param res
 */
function calcType(a, b, res) {
    if (a + b === res) {
        return 'addition';
    }
    if (a - b === res || b - a === res) {
        return 'subtraction';
    }
    if (a * b === res) {
        return 'multiplication';
    }
    return 'division';
}

module.exports = calcType;

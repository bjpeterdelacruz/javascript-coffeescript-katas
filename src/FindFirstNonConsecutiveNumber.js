/**
 * @param arr
 */
function firstNonConsecutive(arr) {
    let idx = 0;
    while (idx + 1 < arr.length) {
        if (arr[idx] + 1 !== arr[idx + 1]) {
            return arr[idx + 1];
        }
        idx++;
    }
    return null;
}

module.exports = firstNonConsecutive;

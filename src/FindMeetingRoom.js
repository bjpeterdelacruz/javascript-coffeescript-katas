/**
 * @param x
 */
function meeting(x) {
    for (let idx = 0; idx < x.length; idx++) {
        if (x[idx] === 'O') {
            return idx;
        }
    }
    return 'None available!';
}

module.exports = meeting;

/**
 * @param str
 */
function findMiddle(str) {
    if (typeof str !== 'string') {
        return -1;
    }
    const numbers = str.split('').filter(char => char.match(/[0-9]/));
    if (numbers.length === 0) {
        return -1;
    }
    let product = 1;
    for (const number of numbers) {
        product *= parseInt(number);
    }
    const prodStr = product.toString();
    const length = prodStr.length;
    if (length % 2 === 0) {
        const middle2 = length / 2;
        const middle1 = middle2 - 1;
        return parseInt(prodStr.substring(middle1, middle2 + 1));
    }
    return parseInt(prodStr[Math.floor(length / 2)]);
}

module.exports = findMiddle;

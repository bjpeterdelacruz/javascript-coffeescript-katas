/**
 * @param arr
 */
function getMinMax(arr) {
    if (arr.length === 0) {
        return [];
    }
    let min = Infinity;
    let max = -Infinity;
    for (const element of arr) {
        if (element < min) {
            min = element;
        }
        if (element > max) {
            max = element;
        }
    }
    return [min, max];
}

module.exports = getMinMax;

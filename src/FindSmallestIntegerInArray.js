class SmallestIntegerFinder {
    static findSmallestInt(args) {
        let min = Infinity;
        for (let idx = 0; idx < args.length; idx++) {
            if (args[idx] < min) {
                min = args[idx];
            }
        }
        return min;
    }
}

module.exports = SmallestIntegerFinder.findSmallestInt;

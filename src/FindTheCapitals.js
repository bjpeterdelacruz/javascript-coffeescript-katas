/**
 * @param word
 */
function capitals(word) {
    const alphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    const indexes = [];
    word.split('').forEach((letter, index) => {
        if (alphabet.indexOf(letter) !== -1) {
            indexes.push(index);
        }
    });
    return indexes;
}

module.exports = capitals;

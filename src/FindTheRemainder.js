/**
 * @param a
 * @param b
 */
function remainder(a, b) {
    if (a === 0 || b === 0) {
        return NaN;
    }
    return a >= b ? a % b : b % a;
}

module.exports = remainder;

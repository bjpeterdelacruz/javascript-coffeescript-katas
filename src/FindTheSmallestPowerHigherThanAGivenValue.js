function findNextPower(value, power) {
    let base = 1;
    let result = Math.pow(base, power);
    while (result <= value) {
        base++;
        result = Math.pow(base, power);
    }
    return result;
}

module.exports = findNextPower;

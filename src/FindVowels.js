/**
 * @param word
 */
function vowelIndices(word) {
    const vowels = 'aeiouyAEIOUY';
    return word.split('').map((element, idx) => vowels.indexOf(element) === -1 ? '' : idx + 1)
        .filter(element => element !== '');
}

module.exports = vowelIndices;

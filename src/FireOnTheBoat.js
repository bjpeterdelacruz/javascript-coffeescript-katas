/**
 * @param string
 */
function fireFight(string) {
    return string.split('Fire').join('~~');
}

module.exports = fireFight;

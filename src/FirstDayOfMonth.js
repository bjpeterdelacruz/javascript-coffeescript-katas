/**
 * @param yearInit
 * @param yearEnd
 */
function getTotalSundays(yearInit, yearEnd) {
    const start = yearInit;
    const end = yearEnd ? yearEnd : start;
    let count = 0;
    for (let year = start; year <= end; year++) {
        for (let month = 0; month < 12; month++) {
            const firstDay = new Date(year, month, 1);
            if (firstDay.getDay() === 0) {
                count++;
            }
        }
    }
    return count;
}

module.exports = getTotalSundays;

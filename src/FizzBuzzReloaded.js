/**
 * @param start
 * @param stop
 * @param step
 * @param functions
 */
function fizzBuzzReloaded(start, stop, step, functions) {
    const results = [];
    let currentNumber = start;
    while (true) {
        if (step > 0) {
            if (currentNumber > stop) {
                break;
            }
        }
        else if (currentNumber < stop) {
            break;
        }
        const funcResults = [];
        for (const func of functions) {
            if (func[1](currentNumber)) {
                funcResults.push(func[0]);
            }
        }
        results.push(funcResults.length === 0 ? currentNumber : funcResults.join(''));
        currentNumber += step;
    }
    return results.join(' ');
}

module.exports = fizzBuzzReloaded;

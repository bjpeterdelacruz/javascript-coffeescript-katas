/**
 * @param n
 */
function fizzBuzz(n) {
    while (n % 3 === 0 && n % 5 === 0) {
        return 'FizzBuzz';
    }
    while (n % 3 === 0) {
        return 'Fizz';
    }
    while (n % 5 === 0) {
        return 'Buzz';
    }
    return n;
}

module.exports = fizzBuzz;

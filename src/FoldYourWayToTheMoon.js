function foldTo(distance) {
    if (distance < 0) {
        return null;
    }
    let currentFold = 0;
    while (Math.pow(2, currentFold) * 0.0001 < distance) {
        currentFold++;
    }
    return currentFold;
}

module.exports = foldTo;

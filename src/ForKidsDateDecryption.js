/**
 * @param dateStr
 */
function translateDate(dateStr) {
    return dateStr.split('').map(letter => {
        if (letter === '-') {
            return '-';
        }
        const charCode = letter.charCodeAt(0) - 50;
        return charCode < 10 ? `0${charCode}` : charCode;
    }).join('');
}

module.exports = translateDate;

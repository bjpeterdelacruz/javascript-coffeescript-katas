const encrypt = (str) => String.fromCharCode(parseInt(str) + 50);

/**
 * @param dateStr
 */
function translateDate(dateStr) {
    const [year, month, day] = dateStr.split('-');
    return [encrypt(year.substring(0, 2)) + encrypt(year.substring(2)),
        encrypt(month), encrypt(day)].join('-');
}

module.exports = translateDate;

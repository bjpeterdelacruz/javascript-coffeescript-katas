/**
 * @param date
 */
function dayOfTheWeek(date) {
    const daysOfWeek = new Map();
    daysOfWeek.set(0, 'Sunday');
    daysOfWeek.set(1, 'Monday');
    daysOfWeek.set(2, 'Tuesday');
    daysOfWeek.set(3, 'Wednesday');
    daysOfWeek.set(4, 'Thursday');
    daysOfWeek.set(5, 'Friday');
    daysOfWeek.set(6, 'Saturday');
    const [day, month, year] = date.split('/');
    return daysOfWeek.get(new Date(year, month - 1, day).getDay());
}

module.exports = dayOfTheWeek;

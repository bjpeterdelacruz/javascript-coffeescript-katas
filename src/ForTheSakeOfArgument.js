/**
 * @param {...any} args
 */
function numbers(...args) {
    return args.filter(element => typeof element === 'number').length === args.length;
}

module.exports = numbers;

/**
 * @param words
 */
function formatWords(words) {
    if (words === null || words.length === 0) {
        return '';
    }
    const sentences = words.filter(word => word.length > 0);
    if (sentences.length === 0) {
        return '';
    }
    if (sentences.length === 1) {
        return sentences[0];
    }
    if (sentences.length === 2) {
        return sentences[0] + ' and ' + sentences[1];
    }
    const string =  sentences.slice(0, sentences.length - 2).join(', ') + ', ';
    return string + sentences[sentences.length - 2] + ' and ' + sentences[sentences.length - 1];
}

module.exports = formatWords;

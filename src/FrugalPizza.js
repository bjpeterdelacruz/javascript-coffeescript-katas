/**
 * @param diameter
 * @param price
 */
function pizzaPrice(diameter, price) {
    if (typeof diameter !== 'number' || typeof price !== 'number') {
        return 0;
    }
    const radius = diameter / 2;
    const area = Math.PI * radius * radius;
    return Math.round(price / area * 100) / 100;
}

module.exports = pizzaPrice;

/**
 * @param data
 * @param next
 */
function Node(data, next = null) {
    this.data = data;
    this.next = next;
}

/**
 * @param head
 * @param value
 */
function indexOf(head, value) {
    let next = head;
    let idx = 0;
    while (next) {
        if (next.data === value) {
            return idx;
        }
        next = next.next;
        idx++;
    }
    return -1;
}

module.exports = { Node, indexOf };

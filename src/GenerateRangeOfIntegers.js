/**
 * @param min
 * @param max
 * @param step
 */
function generateRange(min, max, step) {
    const arr = [];
    let minimum = min;
    while (minimum <= max) {
        arr.push(minimum);
        minimum += step;
    }
    return arr;
}

module.exports = generateRange;

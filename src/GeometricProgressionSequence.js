/**
 * @param first
 * @param constant
 * @param count
 */
function geometricSequenceElements(first, constant, count) {
    const arr = [];
    for (let curr = first, num = 0; num < count; curr *= constant, num++) {
        arr.push(curr);
    }
    return arr.join(', ');
}

module.exports = geometricSequenceElements;

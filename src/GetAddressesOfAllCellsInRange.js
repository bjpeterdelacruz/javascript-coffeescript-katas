function getCellAddresses(range) {
    const alphaCols = range.match(/[A-Z]+/g).map(x => x.charCodeAt(0));
    if (alphaCols.length !== 2 || alphaCols[0] > alphaCols[1]) {
        return [];
    }

    const numericRows = range.match(/[0-9]+/g).map(x => parseInt(x));
    if (alphaCols[0] === alphaCols[1] && numericRows[0] === numericRows[1]) {
        return [];
    }

    const start = numericRows[0] < numericRows[1] ? numericRows[0] : numericRows[1];
    const end = numericRows[0] > numericRows[1] ? numericRows[0] : numericRows[1];

    const cells = [];
    for (let col = alphaCols[0]; col <= alphaCols[1]; col++) {
        for (let row = start; row <= end; row++) {
            cells.push(`${String.fromCharCode(col)}${row}`);
        }
    }

    cells.sort((a, b) => parseInt(a.substring(1)) - parseInt(b.substring(1)));

    return cells;
}

module.exports = getCellAddresses;

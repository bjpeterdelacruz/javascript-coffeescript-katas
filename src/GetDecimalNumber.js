/**
 * @param n
 */
function getDecimal(n) {
    const string = n.toString();
    if (string.indexOf('.') === -1) {
        return 0;
    }
    return parseFloat(string.substring(string.indexOf('.')));
}

module.exports = getDecimal;

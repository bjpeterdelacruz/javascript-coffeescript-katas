/**
 * @param data
 */
function keysAndValues(data) {
    const keys = [];
    const values = [];
    for (const pair of Object.entries(data)) {
        keys.push(pair[0]);
        values.push(pair[1]);
    }
    return [keys, values];
}

module.exports = keysAndValues;

/**
 * @param data
 */
function getNames(data) {
    return data.map(element => element.name);
}

module.exports = getNames;

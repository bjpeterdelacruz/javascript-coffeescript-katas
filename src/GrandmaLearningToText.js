/**
 * @param str
 */
function textin(str) {
    return str.replace(/too/gi, '2').replace(/to/gi, '2').replace(/two/gi, '2');
}

module.exports = textin;

const isConsecutive = (str) => {
    const set = new Set();
    for (let idx = 0; idx < str.length - 1; idx++) {
        if (str.charAt(idx) !== str.charAt(idx + 1)) {
            if (set.has(str.charAt(idx + 1))) {
                return false;
            }
            set.add(str.charAt(idx));
        }
    }
    return true;
};

module.exports = isConsecutive;

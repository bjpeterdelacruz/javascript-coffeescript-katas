/**
 * @param x
 */
function sequence(x) {
    const arr = [];
    for (let idx = 1; idx <= x; idx++) {
        arr.push(`${idx}`);
    }
    return arr.sort().map(element => parseInt(element));
}

module.exports = sequence;

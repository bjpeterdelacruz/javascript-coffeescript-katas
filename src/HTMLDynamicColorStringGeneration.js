/**
 *
 */
function generateValue() {
    const color = Math.floor(Math.random() * 256).toString(16);
    return color.length < 2 ? `0${color}` : color;
}

/**
 *
 */
function generateColor() {
    return `#${generateValue()}${generateValue()}${generateValue()}`.toUpperCase();
}

module.exports = generateColor;

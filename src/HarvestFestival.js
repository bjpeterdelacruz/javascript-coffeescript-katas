/**
 * @param seed
 * @param water
 * @param fert
 * @param temp
 */
function plant(seed, water, fert, temp) {
    const numFlowers = temp < 20 || temp > 30 ? 0 : fert;
    const stem = '-'.repeat(water);
    const flowers = seed.repeat(numFlowers);
    return `${stem}${flowers}`.repeat(water) + (numFlowers === 0 ? seed : '');
}

module.exports = plant;

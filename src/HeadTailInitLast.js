/**
 * @param arr
 */
function head(arr) {
    return arr[0];
}

/**
 * @param arr
 */
function tail(arr) {
    if (arr.length === 1) {
        return [];
    }
    return arr.slice(1);
}

/**
 * @param arr
 */
function init(arr) {
    if (arr.length === 1) {
        return [];
    }
    return arr.slice(0, arr.length - 1);
}

/**
 * @param arr
 */
function last(arr) {
    return arr[arr.length - 1];
}

module.exports = { head, tail, init, last };

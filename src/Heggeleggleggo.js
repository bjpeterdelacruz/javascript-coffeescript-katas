/**
 * @param word
 */
function heggeleggleggo(word) {
    const consonants = 'BCDFGHJKLMNPQRSTVWXYZ';
    const arr = [];
    for (const letter of word.split('')) {
        if (consonants.indexOf(letter.toUpperCase()) !== -1) {
            arr.push(`${letter}egg`);
        }
        else {
            arr.push(letter);
        }
    }
    return arr.join('');
}

module.exports = heggeleggleggo;

class Warrior {
    constructor(name) {
        this.personName = name;
    }

    toString() {
        return `Hi! my name's ${this.personName}`;
    }

    name(n) {
        if (n) {
            this.personName = n;
        }
        return this.personName;
    }
}

module.exports = Warrior;

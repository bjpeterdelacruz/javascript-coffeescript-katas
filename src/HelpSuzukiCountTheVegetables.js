function countVegetables(string) {
    const vegetables = [
        'cabbage', 'carrot', 'celery', 'cucumber', 'mushroom',
        'onion', 'pepper', 'potato', 'tofu', 'turnip'
    ];
    const counts = new Map();

    for (const item of string.split(/\s+/)) {
        if (vegetables.indexOf(item.toLowerCase()) === -1) {
            continue;
        }
        if (!counts.has(item.toLowerCase())) {
            counts.set(item.toLowerCase(), 0);
        }
        counts.set(item.toLowerCase(), counts.get(item.toLowerCase()) + 1);
    }

    return Array.from(counts.entries())
        .map(count => [count[1], count[0]])
        .sort(
            (a, b) => a[0] === b[0] ?
                (a[1] < b[1] ? 1 : -1) : (a[0] > b[0] ? -1 : 1));
}

module.exports = countVegetables;

/**
 * @param code
 */
function hexHash(code) {
    if (code === null || code === '') {
        return 0;
    }
    const asciiHex = code.split('').map(character => character.charCodeAt(0).toString(16));
    return asciiHex.join('').split('').filter(character => !isNaN(parseInt(character)))
        .map(character => parseInt(character)).reduce((acc, curr) => acc + curr);
}

module.exports = hexHash;

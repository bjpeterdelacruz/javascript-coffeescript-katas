function high(string) {
    const alphabet = 'abcdefghijklmnopqrstuvwxyz';
    const words = string.split(/\s+/);
    let maximum = 0;
    let greatestWordIdx = 0;
    for (let currentWordIdx = 0; currentWordIdx < words.length; currentWordIdx++) {
        let sum = 0;
        let currentLetterIdx = 0;
        do {
            sum += alphabet.indexOf(words[currentWordIdx][currentLetterIdx]) + 1;
        } while (++currentLetterIdx < words[currentWordIdx].length);
        if (sum > maximum) {
            maximum = sum;
            greatestWordIdx = currentWordIdx;
        }
    }
    return words[greatestWordIdx];
}

module.exports = high;

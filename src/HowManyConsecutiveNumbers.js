/**
 * @param arr
 */
function consecutive(arr) {
    const set = new Set();
    let min = Infinity;
    let max = -Infinity;
    for (let idx = 0; idx < arr.length; idx++) {
        if (arr[idx] > max) {
            max = arr[idx];
        }
        if (arr[idx] < min) {
            min = arr[idx];
        }
        set.add(arr[idx]);
    }
    let count = 0;
    for (let idx = min + 1; idx < max; idx++) {
        if (!set.has(idx)) {
            count++;
        }
    }
    return count;
}

module.exports = consecutive;

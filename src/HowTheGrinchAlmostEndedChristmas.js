/**
 * @param missiles
 */
function prioritizeMissiles(missiles) {
    const missilesMap = new Map();
    let times = new Set();

    missiles.forEach(element => {
        const time = element.distance / element.speed;
        if (!(time in missilesMap)) {
            missilesMap[time] = [];
        }
        missilesMap[time].push(element.name);
        times.add(time);
    });

    times = Array.from(times);
    times.sort((a, b) => {
        return a - b;
    });
    const missileNames = [];
    times.forEach(element => {
        missilesMap[element].forEach(element => {
            missileNames.push(element);
        });
    });
    return missileNames;
}

module.exports = prioritizeMissiles;

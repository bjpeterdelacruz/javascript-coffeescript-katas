/**
 * @param ip
 */
function ipToNum(ip) {
    return parseInt(ip.split('.').map(number => {
        let temp = parseInt(number).toString(2);
        while (temp.length < 8) {
            temp = `0${temp}`;
        }
        return temp;
    }).join(''), 2);
}

/**
 * @param num
 */
function numToIp(num) {
    let binaryString = num.toString(2);
    while (binaryString.length < 32) {
        binaryString = `0${binaryString}`;
    }
    const ipAddress = [];
    for (let idx = 8; idx <= 32; idx += 8) {
        ipAddress.push(parseInt(binaryString.substring(idx - 8, idx), 2));
    }
    return ipAddress.join('.');
}

module.exports = { ipToNum, numToIp };

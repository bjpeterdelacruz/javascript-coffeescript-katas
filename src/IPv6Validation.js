/**
 * @param address
 */
function validateIPv6Address(address) {
    // only one :: is allowed
    let doubleColonCount = 0;
    for (let idx = 0; idx < address.length; idx++) {
        if (address[idx] === ':') {
            doubleColonCount++;
            if (doubleColonCount > 2) {
                // three : in a row is not allowed
                return false;
            }
        }
        else {
            doubleColonCount = 0;
        }
    }
    doubleColonCount = (address.match(/::/g) || []).length;
    if (doubleColonCount > 1) {
        return false;
    }
    const colonCount = (address.match(/:/g) || []).length;
    if (colonCount > 7 || (doubleColonCount === 0 && colonCount !== 7)) {
        return false;
    }
    // only A-F are allowed
    const illegalCharactersCount = (address.match(/[g-zG-Z]+/g) || []).length;
    if (illegalCharactersCount >= 1) {
        return false;
    }
    // each 8-bit octet must only contain 0-9 and/or A-F
    const groups = address.split(':');
    for (const string of groups) {
        if (string !== '' && (string.match(/[^a-fA-F0-9]+/g) || string.length > 4)) {
            return false;
        }
    }
    return true;
}

/**
 * @param ipv6Address
 */
function contractV6(ipv6Address) {
    let address = ipv6Address;
    if (!validateIPv6Address(address)) {
        return false;
    }

    // convert all hexadecimal characters to lowercase
    address = address.toLowerCase();

    let newAddress = [];

    // normalize IP address by expanding contraction
    if (address.indexOf('::') !== -1) {
        if (address.startsWith('::')) {
            address = address.replace('::', 'X:');
        }
        else if (address.endsWith('::')) {
            address = address.replace('::', ':X');
        }
        else {
            address = address.replace('::', ':X:');
        }
    }
    newAddress = address.split(':');
    for (let idx = 0; newAddress.length !== 8; idx++) {
        if (newAddress[idx] === 'X') {
            newAddress.splice(idx, 0, 'X');
        }
    }
    address = newAddress.join(':');
    newAddress = [];

    // remove leading 0's in front of non-zero octets
    for (const string of address.split(':')) {
        if (string === 'X' || string === '0' || string === '00' || string === '000'
            || string === '0000') {
            newAddress.push('0');
        }
        else {
            let temp = string;
            while (temp[0] === '0') {
                temp = temp.slice(1);
            }
            newAddress.push(temp);
        }
    }

    let endIdx = 0;
    let currentCount = 0;
    let longestCount = 0;
    for (let idx = 0; idx < newAddress.length; idx++) {
        if (newAddress[idx] === '0') {
            currentCount++;
            if (currentCount > longestCount) {
                longestCount = currentCount;
                endIdx = idx;
            }
        }
        else {
            currentCount = 0;
        }
    }
    if (longestCount <= 1) {
        return newAddress.join(':');
    }
    const startIdx = endIdx - longestCount + 1;
    if (startIdx > 0 && endIdx < newAddress.length - 1) {
        newAddress.splice(startIdx, longestCount);
        newAddress.splice(startIdx, 0, '');
    }
    else if (startIdx === 0) {
        newAddress.splice(startIdx, longestCount);
        newAddress.splice(startIdx, 0, ':');
    }
    else {
        newAddress.splice(startIdx, longestCount);
        newAddress.splice(endIdx, 0, ':');
    }
    if (newAddress.length === 1 && newAddress[0] === ':') {
        return '::';
    }
    return newAddress.join(':');
}

module.exports = contractV6;

/**
 * @param name
 */
function initializeNames(name) {
    const arr = name.split(/\s+/);
    if (arr.length <= 2) {
        return name;
    }
    const newName = [arr[0]];
    for (let idx = 1; idx < arr.length - 1; idx++) {
        newName.push(`${arr[idx][0]}.`);
    }
    newName.push(arr[arr.length - 1]);
    return newName.join(' ');
}

module.exports = initializeNames;

/**
 * @param m
 * @param n
 */
function listSquared(m, n) {
    const results = [];
    for (let current = m; current <= n; current++) {
        const squareRoot = Math.floor(Math.sqrt(current));
        const divisors = new Set();
        divisors.add(1);
        divisors.add(current * current);
        for (let divisor = 2; divisor <= squareRoot; divisor++) {
            if (current % divisor === 0) {
                const result = current / divisor;
                divisors.add(result * result);
                divisors.add(divisor * divisor);
            }
        }
        const sum = Array.from(divisors).reduce((a, b) => a + b, 0);
        if (Math.sqrt(sum) % 1 === 0) {
            results.push([current, sum]);
        }
    }
    return results;
}

module.exports = listSquared;

/**
 * @param n
 */
function notVisibleCubes(n) {
    return n > 2 ? (n - 2) ** 3 : 0;
}

module.exports = notVisibleCubes;

String.prototype.vowel = function() {
    return this.length === 1 && this.toLowerCase().match(/[aeiou]/) !== null;
};

module.exports = String.prototype.vowel;

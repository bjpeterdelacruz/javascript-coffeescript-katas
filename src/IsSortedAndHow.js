/**
 * @param array
 */
function isSortedAndHow(array) {
    const copy = [...array];
    copy.sort((a, b) => a - b);
    if (copy.toString() === array.toString()) {
        return 'yes, ascending';
    }
    copy.sort((a, b) => b - a);
    return copy.toString() === array.toString() ? 'yes, descending' : 'no';
}

module.exports = isSortedAndHow;

/**
 * @param w1
 * @param r1
 * @param w2
 * @param r2
 * @param timePeriod
 */
function sweetDate(w1, r1, w2, r2, timePeriod) {
    const john = [];
    const anne = [];
    populateSchedule(w1, r1, timePeriod, john);
    populateSchedule(w2, r2, timePeriod, anne);
    let count = 0;
    for (let idx = 0; idx < timePeriod; idx++) {
        if (john[idx] === anne[idx] && john[idx] === 'R') {
            count++;
        }
    }
    return count;
}

/**
 * @param w
 * @param r
 * @param t
 * @param arr
 */
function populateSchedule(w, r, t, arr) {
    for (let idx = 0, work = 0, rest = 0; idx < t; idx++) {
        if (work < w) {
            arr.push('W');
            work++;
        }
        if (work === w) {
            arr.push('R');
            rest++;
            if (rest === r) {
                work = 0;
                rest = 0;
            }
        }
    }
}

module.exports = sweetDate;

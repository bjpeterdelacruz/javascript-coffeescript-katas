/**
 * @param key
 */
function KamaSutraCipher(key) {
    this.cipherMap = new Map();

    for (let idx = 0; idx < key.length; idx++) {
        this.cipherMap[key[idx][0]] = key[idx][1];
        this.cipherMap[key[idx][1]] = key[idx][0];
    }

    this.transformText = function(str) {
        let newString = '';
        for (let idx = 0; idx < str.length; idx++) {
            if (this.cipherMap[str[idx]]) {
                newString += this.cipherMap[str[idx]];
            }
            else {
                newString += str[idx];
            }
        }
        return newString;
    };

    this.encode = function(str) {
        return this.transformText(str);
    };
    this.decode = function(str) {
        return this.transformText(str);
    };
}

module.exports = KamaSutraCipher;

/**
 * @param numbers
 */
function computerToPhone(numbers) {
    let result = '';
    for (let idx = 0; idx < numbers.length; idx++) {
        const num = parseInt(numbers.substring(idx, idx + 1));
        if (num > 6) {
            result += (num - 6);
        }
        else if (num < 4 && num > 0) {
            result += (num + 6);
        }
        else {
            result += num;
        }
    }
    return result;
}

module.exports = computerToPhone;

/**
 * @param abc
 * @param keyword
 */
function KeywordCipher(abc, keyword) {
    this.encodeMap = new Map();
    this.decodeMap = new Map();

    let key = '';
    for (let idx = 0; idx < keyword.length; idx++) {
        if (!key.includes(keyword[idx])) {
            key += keyword[idx];
        }
    }

    for (let idx = 0; idx < abc.length; idx++) {
        if (!key.includes(abc[idx])) {
            key += abc[idx];
        }
    }

    for (let idx = 0; idx < key.length; idx++) {
        this.encodeMap[abc[idx]] = key[idx];
        this.decodeMap[key[idx]] = abc[idx];
    }

    this.transformText = function(str, map) {
        let newString = '';
        for (let idx = 0; idx < str.length; idx++) {
            if (map[str[idx]]) {
                newString += map[str[idx]];
            }
            else {
                newString += str[idx];
            }
        }
        return newString;
    };
    this.encode = function(str) {
        return this.transformText(str, this.encodeMap);
    };
    this.decode = function(str) {
        return this.transformText(str, this.decodeMap);
    };
}

module.exports = KeywordCipher;

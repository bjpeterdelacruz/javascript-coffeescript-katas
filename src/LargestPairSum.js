class MaxIndex {
    maximum = -Infinity;
    index = 0;
}

/**
 * @param numbers
 */
function largestPairSum(numbers) {
    const maxIndex1 = findMaxIndex(numbers);
    const maxIndex2 = findMaxIndex(numbers, maxIndex1.index);
    return maxIndex1.maximum + maxIndex2.maximum;
}

/**
 * @param numbers
 * @param indexToIgnore
 */
function findMaxIndex(numbers, indexToIgnore) {
    const maxIndex = new MaxIndex();
    for (let idx = 0; idx < numbers.length; idx++) {
        if (numbers[idx] >= maxIndex.maximum && idx !== indexToIgnore) {
            maxIndex.maximum = numbers[idx];
            maxIndex.index = idx;
        }
    }
    return maxIndex;
}

module.exports = largestPairSum;

/**
 * @param input
 */
function greatestProduct(input) {
    const numbers = [];
    for (let idx = 0; idx < input.length; idx++) {
        let number = '';
        for (let count = idx; count < idx + 5 && count < input.length; count++) {
            number += input[count];
        }
        if (number.length === 5) {
            numbers.push(number);
        }
    }
    if (numbers.length === 0) {
        return 0;
    }

    let largest = -1;
    for (const number of numbers) {
        let product = 1;
        for (const character of number) {
            product *= parseInt(character);
        }
        if (product > largest) {
            largest = product;
        }
    }
    return largest;
}

module.exports = greatestProduct;

/**
 * @param letters
 * @param coords
 */
function lastSurvivor(letters, coords) {
    const arr = letters.split('');
    while (arr.length !== 1) {
        arr.splice(coords.splice(0, 1)[0], 1);
    }
    return arr[0];
}

module.exports = lastSurvivor;

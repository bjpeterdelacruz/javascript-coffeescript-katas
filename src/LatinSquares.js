function makeLatinSquare(n) {
    const arr = Array.from(new Array(n), (x, i) => i + 1);

    const twoDimArr = [];
    for (let idx = 1; idx <= n; idx++) {
        twoDimArr.push(rot(arr, idx));
    }
    return twoDimArr.reverse();
}

function rot(arr, times) {
    const copy = [...arr];
    for (let count = 0; count < times; count++) {
        copy.push(copy.shift());
    }
    return copy;
}

module.exports = makeLatinSquare;

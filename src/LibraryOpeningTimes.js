const startTimes = {
    'Monday': '08:00',
    'Tuesday': '08:00',
    'Wednesday': '08:00',
    'Thursday': '08:00',
    'Friday': '08:00',
    'Saturday': '10:00',
    'Sunday': '12:00'
};

const endTimes = {
    'Monday': '20:00',
    'Tuesday': '20:00',
    'Wednesday': '20:00',
    'Thursday': '20:00',
    'Friday': '20:00',
    'Saturday': '18:00',
    'Sunday': '16:30'
};

const weekdays = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday'];
const weekends = ['Saturday', 'Sunday'];
const week = [weekends[1], ...weekdays, weekends[0]];

/**
 * @param str
 */
function timeToMinutes(str) {
    const p = str.split(':');
    const hours = parseInt(p[0], 10);
    if (hours < 0 || hours > 23 || isNaN(hours)) {
        return null;
    }
    const minutes = parseInt(p[1], 10);
    if (minutes < 0 || minutes > 59 || isNaN(minutes)) {
        return null;
    }
    let s = 0;
    let m = 1;

    while (p.length > 0) {
        s += m * parseInt(p.pop(), 10);
        m *= 60;
    }

    return s;
}

/**
 * @param str
 */
function openingTimes(str) {
    const arr =  str.split(/\b\s+/);
    const dayOfWeek = arr[0][0].toUpperCase() + arr[0].slice(1).toLowerCase();
    const start = startTimes[dayOfWeek];
    if (!start) {
        return 'Invalid time!';
    }
    const time = timeToMinutes(arr[1]);
    if (time === null) {
        return 'Invalid time!';
    }
    const startTime = timeToMinutes(start);
    if (time < startTime) {
        return 'Library opens: today ' + start;
    }
    const end = endTimes[dayOfWeek];
    const endTime = timeToMinutes(end);
    if (time >= startTime && time < endTime) {
        return 'Library closes at ' + end;
    }
    const nextDay = week[(week.indexOf(dayOfWeek) + 1) % week.length];
    return 'Library opens: ' + nextDay + ' ' + startTimes[nextDay];
}

module.exports = openingTimes;

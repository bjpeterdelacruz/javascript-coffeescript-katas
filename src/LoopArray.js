/**
 * @param arr
 * @param direction
 * @param steps
 */
function loopArr(arr, direction, steps) {
    const copy = [...arr];
    for (let idx = 0; idx < steps; idx++) {
        if (direction === 'left') {
            copy.push(copy.splice(0, 1)[0]);
        }
        else {
            copy.unshift(copy.splice(copy.length - 1, 1)[0]);
        }
    }
    return copy;
}

module.exports = loopArr;

const symmetricDifference = (arr1, arr2) =>
    arr1.filter(element => !arr2.includes(element))
        .concat(arr2.filter(element => !arr1.includes(element)));

const isSquareNumber = (number) =>
    Math.floor(Math.sqrt(number)) * Math.floor(Math.sqrt(number)) === number;

/**
 * @param arr1
 * @param arr2
 */
function makeMatrix(arr1, arr2) {
    let sorted = symmetricDifference(arr1, arr2).sort((a, b) => a - b);
    while (!isSquareNumber(sorted.length)) {
        sorted = sorted.splice(0, sorted.length - 1);
    }
    const rowLength = Math.sqrt(sorted.length);
    const matrix = [];
    while (sorted.length > 0) {
        matrix.push(sorted.splice(0, rowLength));
    }
    return matrix;
}

module.exports = { symmetricDifference, isSquareNumber, makeMatrix };

/**
 * @param obj
 */
function toQueryString(obj) {
    const queryString = [];
    for (const pair of Object.entries(obj)) {
        if (Array.isArray(pair[1])) {
            for (const value of pair[1]) {
                queryString.push(`${pair[0]}=${value}`);
            }
        }
        else {
            queryString.push(`${pair[0]}=${pair[1]}`);
        }
    }
    return queryString.join('&');
}

module.exports = toQueryString;

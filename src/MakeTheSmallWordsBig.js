/**
 * @param sentence
 */
function smallWordHelper(sentence) {
    return sentence.split(/\s+/).map(word => {
        return word.length <= 3 ? word.toUpperCase() : word.replace(/[aeiouAEIOU]+/g, '');
    }).join(' ');
}

module.exports = smallWordHelper;

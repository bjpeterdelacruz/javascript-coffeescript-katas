/**
 * @param arr
 */
function mathEngine(arr) {
    if (!arr) {
        return 0;
    }
    const productArr = arr.filter(element => element >= 0);
    let product = 1;
    if (productArr.length !== 0) {
        product = productArr.reduce((acc, curr) => acc * curr);
    }
    const sumArr = arr.filter(element => element < 0);
    let sum = 0;
    if (sumArr.length !== 0) {
        sum = sumArr.reduce((acc, curr) => acc + curr);
    }
    return product + sum;
}

module.exports = mathEngine;

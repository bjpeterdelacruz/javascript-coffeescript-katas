/**
 * @param n
 */
function fibonacci(n) {
    const fib = [0, 1];
    for (let m = 2; m <= n; m++) {
        fib.push(fib[m - 2] + fib[m - 1]);
    }
    return fib[n];
}

module.exports = fibonacci;

/**
 * @param a
 * @param b
 */
function mergeArrays(a, b) {
    const maxLength = a.length > b.length ? a.length : b.length;
    const combinedArray = [];
    for (let idx = 0; idx < maxLength; idx++) {
        if (idx < a.length) {
            combinedArray.push(a[idx]);
        }
        if (idx < b.length) {
            combinedArray.push(b[idx]);
        }
    }
    return combinedArray;
}

module.exports = mergeArrays;

/**
 * @param l
 * @param d
 * @param x
 */
function minAndMax(l, d, x) {
    const numbers = [];
    for (let i = l; i <= d; i++) {
        const sum = i.toString().split('').map(n => parseInt(n)).reduce((a, c) => a + c);
        if (sum === x) {
            numbers.push(i);
        }
    }
    return [numbers[0], numbers[numbers.length - 1]];
}

module.exports = minAndMax;

/**
 * @param data
 */
function mirror(data) {
    const numbers = [...data];
    numbers.sort((a, b) => a - b);
    const leftSide = [...numbers];
    leftSide.pop();
    numbers.reverse();
    return [...leftSide, ...numbers];
}

module.exports = mirror;

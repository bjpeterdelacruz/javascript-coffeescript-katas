/**
 * @param word1
 * @param word2
 */
function mispelled(word1, word2) {
    let diff = Math.abs(word1.length - word2.length);
    if (diff > 1) {
        return false;
    }
    // if longer word does not contain shorter word, then
    // there are more characters in longer word that are different
    if (word1.length < word2.length) {
        return word2.indexOf(word1) !== -1;
    }
    if (word2.length < word1.length) {
        return word1.indexOf(word2) !== -1;
    }
    // words are same length, so diff is 0
    for (let idx = 0; idx < word1.length; idx++) {
        if (word1[idx] !== word2[idx]) {
            diff++;
        }
    }
    return diff === 1;
}

module.exports = mispelled;

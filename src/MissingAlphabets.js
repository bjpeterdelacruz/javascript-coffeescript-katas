/**
 * @param s
 */
function missingAlphabets(s) {
    const alphabet = 'abcdefghijklmnopqrstuvwxyz';
    const missing = new Map();
    let maxCount = 1;
    for (const char of s) {
        if (char in missing) {
            missing[char]++;
            if (missing[char] > maxCount) {
                maxCount = missing[char];
            }
        }
        else {
            missing[char] = 1;
        }
    }
    let missingLetters = '';
    for (const char of alphabet) {
        if (char in missing && missing[char] < maxCount) {
            missingLetters += char.repeat(maxCount - missing[char]);
        }
        else if (!(char in missing)) {
            missingLetters += char.repeat(maxCount);
        }
    }
    return missingLetters;
}

module.exports = missingAlphabets;

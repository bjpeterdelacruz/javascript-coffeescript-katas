/**
 * @param string
 */
function solve(string) {
    const counts = new Map();
    let maximum = -Infinity;
    for (let idx = 0; idx < string.length; idx++) {
        if (!counts.has(string[idx])) {
            const value = string.lastIndexOf(string[idx]) - idx;
            counts.set(string[idx], value);
            if (value > maximum) {
                maximum = value;
            }
        }
    }
    const keys = Array.from(counts.keys()).sort();
    for (const key of keys) {
        if (counts.get(key) === maximum) {
            return key;
        }
    }
}

module.exports = solve;

/**
 * @param input
 */
function moveVowel(input) {
    const vowels = input.split('').filter(character => 'aeiou'.indexOf(character) !== -1).join('');
    return input.split('').filter(character => 'aeiou'.indexOf(character) === -1).join('') + vowels;
}

module.exports = moveVowel;

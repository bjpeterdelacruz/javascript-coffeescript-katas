/**
 * @param array
 */
function multipleOfIndex(array) {
    const result = [];
    for (let idx = 0; idx < array.length; idx++) {
        if (array[idx] % idx === 0) {
            result.push(array[idx]);
        }
    }
    return result;
}

module.exports = multipleOfIndex;

/**
 * @param number
 */
function multiTable(number) {
    return Array.from(Array(10).keys())
        .map(num => `${num + 1} * ${number} = ${(num + 1) * number}`).join('\n');
}

module.exports = multiTable;

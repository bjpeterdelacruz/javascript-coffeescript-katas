/**
 * @param str
 * @param loc
 * @param num
 */
function modifyMultiply(str, loc, num) {
    const word = str.split(' ')[loc];
    const arr = [];
    for (let idx = 0; idx < num; idx++) {
        arr.push(word);
    }
    return arr.join('-');
}

module.exports = modifyMultiply;

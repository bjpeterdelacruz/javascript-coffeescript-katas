/**
 * @param num1
 * @param num2
 * @param idx1
 * @param idx2
 * @param carry
 * @param multiply
 */
function performOperation(num1, num2, idx1, idx2, carry, multiply) {
    const first = parseInt(num1.charAt(idx1));
    const second = parseInt(num2.charAt(idx2));
    return (multiply ? (first * second) : (first + second)) + carry;
}

/**
 * @param a
 * @param b
 */
function multiply(a, b) {
    const results = [];
    let maxLength = 0;
    for (let firstIdx = a.length - 1, carry = 0, numZeroes = 0;
        firstIdx >= 0;
        firstIdx--, numZeroes++, carry -= carry) {
        let product = '';
        for (let secondIdx = b.length - 1; secondIdx >= 0; secondIdx--) {
            const prod = performOperation(a, b, firstIdx, secondIdx, carry, true);
            carry = prod > 9 ? parseInt(prod.toString()[0]) : 0;
            product = prod.toString().charAt(prod.toString().length - 1) + product;
        }
        product = carry + product;
        product += '0'.repeat(Math.max(0, numZeroes));
        maxLength = Math.max(maxLength, product.length);
        results.push(product);
    }
    for (let idx = 0; idx < results.length; idx++) {
        for (let length = results[idx].length; length < maxLength; length++) {
            results[idx] = '0' + results[idx];
        }
    }
    let finalSum = '0'.repeat(maxLength);
    for (const result of results) {
        const first = result.toString();
        let carry = 0;
        let temp = '';
        for (let idx = first.length - 1; idx >= 0; idx--) {
            const sum = performOperation(first, finalSum, idx, idx, carry, false);
            carry = sum > 9 ? parseInt(sum.toString()[0]) : 0;
            temp = sum.toString().charAt(sum.toString().length - 1) + temp;
        }
        finalSum = temp;
    }
    while (finalSum.charAt(0) === '0') {
        finalSum = finalSum.slice(1);
    }
    return finalSum === '' ? '0' : finalSum;
}

module.exports = multiply;

/**
 * @param num1
 * @param num2
 * @param idx1
 * @param idx2
 * @param carry
 * @param multiply
 */
function performOperation(num1, num2, idx1, idx2, carry, multiply) {
    const first = parseInt(num1.charAt(idx1));
    const second = parseInt(num2.charAt(idx2));
    return (multiply ? (first * second) : (first + second)) + carry;
}

/**
 * @param number
 */
function stripZeros(number) {
    let a = number;
    if (a.indexOf('-') !== -1) {
        a = a.substring(1);
    }
    while (a.charAt(0) === '0') {
        a = a.slice(1);
    }
    if (a === '') {
        return 0;
    }
    if (a.indexOf('.') !== -1) {
        while (a.charAt(a.length - 1) === '0') {
            a = a.substring(0, a.length - 1);
        }
    }
    return a === '.' ? 0 : a;
}

/**
 * @param a
 */
function countDecimalPlace(a) {
    if (a.indexOf('.') === -1) {
        return 0;
    }
    let count = 0;
    let idx = a.length - 1;
    while (a.charAt(idx) !== '.') {
        count++;
        idx--;
    }
    return count;
}

/**
 * @param a
 * @param b
 */
function countDecimalPlaces(a, b) {
    return countDecimalPlace(a) + countDecimalPlace(b);
}

/**
 * @param firstNumber
 * @param secondNumber
 */
function multiply(firstNumber, secondNumber) {
    let a = firstNumber;
    let b = secondNumber;
    let resultIsNegative = false;
    if (a.indexOf('-') !== -1 && b.indexOf('-') === -1) {
        resultIsNegative = true;
    }
    else if (a.indexOf('-') === -1 && b.indexOf('-') !== -1) {
        resultIsNegative = true;
    }
    a = stripZeros(a);
    b = stripZeros(b);
    if (a === 0 || b === 0) {
        return '0';
    }

    const countDecimals = countDecimalPlaces(a, b);
    a = a.replace('.', '');
    b = b.replace('.', '');

    const results = [];
    let maxLength = 0;
    for (let firstIdx = a.length - 1, carry = 0, numZeroes = 0;
        firstIdx >= 0;
        firstIdx--, numZeroes++, carry -= carry) {
        let product = '';
        for (let secondIdx = b.length - 1; secondIdx >= 0; secondIdx--) {
            const prod = performOperation(a, b, firstIdx, secondIdx, carry, true);
            carry = prod > 9 ? parseInt(prod.toString()[0]) : 0;
            product = prod.toString().charAt(prod.toString().length - 1) + product;
        }
        product = carry + product;
        product += '0'.repeat(Math.max(0, numZeroes));
        maxLength = Math.max(maxLength, product.length);
        results.push(product);
    }
    for (let idx = 0; idx < results.length; idx++) {
        for (let length = results[idx].length; length < maxLength; length++) {
            results[idx] = '0' + results[idx];
        }
    }
    let finalSum = '0'.repeat(maxLength);
    for (const result of results) {
        const first = result.toString();
        let carry = 0;
        let temp = '';
        for (let idx = first.length - 1; idx >= 0; idx--) {
            const sum = performOperation(first, finalSum, idx, idx, carry, false);
            carry = sum > 9 ? parseInt(sum.toString()[0]) : 0;
            temp = sum.toString().charAt(sum.toString().length - 1) + temp;
        }
        finalSum = temp;
    }

    if (countDecimals > 0) {
        finalSum = finalSum.slice(0, finalSum.length - countDecimals) + '.'
            + finalSum.slice(finalSum.length - countDecimals);
    }
    finalSum = stripZeros(finalSum);
    if (finalSum[0] === '.') {
        finalSum = '0' + finalSum;
    }
    if (finalSum[finalSum.length - 1] === '.') {
        finalSum = finalSum.substring(0, finalSum.length - 1);
    }
    return resultIsNegative ? '-' + finalSum : finalSum;
}

module.exports = multiply;

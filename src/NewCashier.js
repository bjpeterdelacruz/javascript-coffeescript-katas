/**
 * @param order
 */
function getOrder(order) {
    let orderString = order;
    const menu = ['Burger', 'Fries', 'Chicken', 'Pizza', 'Sandwich', 'Onionrings', 'Milkshake',
        'Coke'];
    const customerOrder = [];
    for (const menuItem of menu) {
        while (orderString.indexOf(menuItem.toLowerCase()) !== -1) {
            customerOrder.push(menuItem);
            orderString = orderString.replace(menuItem.toLowerCase(), '');
        }
    }
    return customerOrder.join(' ');
}

module.exports = getOrder;

/**
 * @param birthday
 */
function nextBirthdayOfTheWeek(birthday) {
    let year = birthday.getFullYear() + 1;
    const dayOfWeek = birthday.getDay();
    while (new Date(year, birthday.getMonth(), birthday.getDate()).getDay() !== dayOfWeek) {
        year++;
    }
    return year - birthday.getFullYear();
}

module.exports = nextBirthdayOfTheWeek;

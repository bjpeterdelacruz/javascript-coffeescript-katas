function isNice(arr) {
    for (let i = 0; i < arr.length; i++) {
        let found = false;
        for (let j = 0; j < arr.length; j++) {
            if (i === j) {
                continue;
            }
            if (arr[i] - 1 === arr[j] || arr[i] + 1 === arr[j]) {
                found = true;
                break;
            }
        }
        if (!found) {
            return false;
        }
    }
    return arr.length > 0;
}

module.exports = isNice;

/**
 * @param array
 * @param value
 */
function check(array, value) {
    return array.filter(element => element === value).length > 0;
}

module.exports = check;

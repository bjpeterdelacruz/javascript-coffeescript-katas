function nthFibo(position) {
    if (position === 1) {
        return 0;
    }
    if (position < 4) {
        return 1;
    }
    let sum = 0;
    let a = 1;
    let b = 1;
    let pos = 4;
    while (true) {
        sum = a + b;
        a = b;
        b = sum;
        if (pos++ === position) {
            return sum;
        }
    }
}

module.exports = nthFibo;

/**
 * @param string
 */
function cypher(string) {
    return string.replace(/[lI]/g, '1')
        .replace(/[zR]/g, '2')
        .replace(/[eE]/g, '3')
        .replace(/[aA]/g, '4')
        .replace(/[sS]/g, '5')
        .replace(/[bG]/g, '6')
        .replace(/[tT]/g, '7')
        .replace(/B/g, '8')
        .replace(/g/g, '9')
        .replace(/[oO]/g, '0');
}

module.exports = cypher;

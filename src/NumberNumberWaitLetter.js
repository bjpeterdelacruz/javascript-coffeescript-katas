/**
 * @param s
 */
function doMath(s) {
    const letters = new Map();
    for (const number of s.split(' ')) {
        for (let idx = 0; idx < number.length; idx++) {
            if (number[idx].match(/[a-zA-Z]/)) {
                const integer = parseInt(number.substring(0, idx) + number.substring(idx + 1));
                if (letters.has(number[idx])) {
                    letters.get(number[idx]).push(integer);
                }
                else {
                    letters.set(number[idx], [integer]);
                }
                break;
            }
        }
    }
    const keys = Array.from(letters.keys());
    keys.sort();

    const stack = [];
    const operations = ['+', '-', '*', '/'];
    let idx = 0;
    for (const key of keys) {
        const values = letters.get(key);
        for (const value of values) {
            stack.push(value);
            if (stack.length !== 2) {
                continue;
            }
            const secondNumber = stack.pop();
            const firstNumber = stack.pop();
            if (idx === 4) {
                idx = 0;
            }
            switch (operations[idx]) {
            case '+':
                stack.push(firstNumber + secondNumber);
                break;
            case '-':
                stack.push(firstNumber - secondNumber);
                break;
            case '*':
                stack.push(firstNumber * secondNumber);
                break;
            case '/':
                stack.push(firstNumber / secondNumber);
                break;
            }
            idx++;
        }
    }
    return Math.round(stack.pop());
}

module.exports = doMath;

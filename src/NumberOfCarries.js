/**
 * @param a
 * @param b
 */
function numberOfCarries(a, b) {
    const aString = a.toString();
    const bString = b.toString();

    let shorterString = aString.length < bString.length ? aString : bString;
    const longerString = aString.length >= bString.length ? aString : bString;

    for (let numZeros = longerString.length - shorterString.length; numZeros > 0; numZeros--) {
        shorterString = '0' + shorterString;
    }

    let count = 0;
    let carry = 0;
    for (let idx = longerString.length - 1; idx >= 0; idx--) {
        const num1 = parseInt(shorterString[idx]);
        const num2 = parseInt(longerString[idx]);
        const sum = num1 + num2 + carry;
        count += (sum > 9 ? 1 : 0);
        carry = sum > 9 ? parseInt(sum.toString()[0]) : 0;
    }

    return count;
}

module.exports = numberOfCarries;

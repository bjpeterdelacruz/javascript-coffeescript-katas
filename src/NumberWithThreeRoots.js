/**
 * @param n
 */
function perfectRoots(n) {
    return Math.pow(n, (1/2)) % 1 === 0 && Math.pow(n, (1/4)) % 1 === 0
        && Math.pow(n, (1/8)) % 1 === 0;
}

module.exports = perfectRoots;

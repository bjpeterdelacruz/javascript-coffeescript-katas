/**
 * @param number
 * @param system
 */
function sysNums(number, system) {
    if (!system) {
        return number;
    }
    let result = number;
    let arr = [];
    while (result >= system) {
        arr.push(result % system);
        result = Math.floor(result / system);
    }
    arr.push(result % system);
    arr = arr.reverse().map(element => {
        if (system !== 16) {
            return element;
        }
        switch (element) {
        case 10:
            return 'a';
        case 11:
            return 'b';
        case 12:
            return 'c';
        case 13:
            return 'd';
        case 14:
            return 'e';
        case 15:
            return 'f';
        default:
            return element;
        }
    });
    const lettersCount = arr.filter(element => 'abcdef'.indexOf(element) !== -1).length;
    return lettersCount === 0 ? parseInt(arr.join('')) : arr.join('');
}

module.exports = sysNums;

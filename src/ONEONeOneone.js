/**
 * @param nums
 */
function consecutiveOnes(nums) {
    let max = 0;
    let count = 0;
    for (let idx = 0; idx < nums.length; idx++) {
        if (nums[idx] !== 1) {
            if (count > max) {
                max = count;
            }
            count = 0;
        }
        else {
            count++;
        }
    }
    return count > max ? count : max;
}

module.exports = consecutiveOnes;

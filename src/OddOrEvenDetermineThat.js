/**
 * @param n
 */
function oddOrEven(n) {
    if (n % 2 === 0) {
        return (n / 2) % 2 === 0 ? 'Even' : 'Odd';
    }
    return 'Either';
}

module.exports = oddOrEven;

/**
 * @param x
 * @param s
 */
function bears(x, s) {
    const results = [];
    for (let idx = 0; idx < s.length - 1; idx++) {
        if ((s[idx] === 'B' && s[idx + 1] === '8') || (s[idx] === '8' && s[idx + 1] === 'B')) {
            results.push(s[idx] + s[idx + 1]);
            idx++;
        }
    }
    return [results.join(''), results.length >= x];
}

module.exports = bears;

function palindrome(string) {
    const alphanum = 'abcdefghijklmnopqrstuvwxyz0123456789';
    let newString = '';
    for (let idx = 0; idx < string.length; idx++) {
        const char = string.toLowerCase().charAt(idx);
        if (alphanum.indexOf(char) !== -1) {
            newString += char;
        }
    }
    return newString === newString.split('').reverse().join('');
}

module.exports = palindrome;

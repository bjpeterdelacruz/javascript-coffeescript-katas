/**
 * @param {number} x
 * @returns {boolean}
 */
const isPalindrome = function(x) {
    const strX = x.toString();
    for (let startIdx = 0, endIdx = strX.length - 1; startIdx < endIdx; startIdx++, endIdx--) {
        if (strX[startIdx] !== strX[endIdx]) {
            return false;
        }
    }
    return true;
};

module.exports = isPalindrome;

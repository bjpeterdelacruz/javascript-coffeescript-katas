/**
 * @param intStrs
 */
function parseNumbers(intStrs) {
    return intStrs.map((num) => parseInt(num));
}

module.exports = parseNumbers;

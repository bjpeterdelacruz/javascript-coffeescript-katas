class Node {
    constructor(data, next = null) {
        this.data = data;
        this.next = next;
    }
}

/**
 * @param head
 * @param data
 */
function push(head, data) {
    if (head === null) {
        return new Node(data);
    }
    const newHead = new Node(data);
    newHead.next = head;
    return newHead;
}

/**
 * @param string
 */
function parse(string) {
    if (string === 'null') {
        return null;
    }
    const nodes = string.split(' -> ');
    nodes.pop();
    nodes.reverse();
    let head = null;
    for (const data of nodes) {
        head = push(head, parseInt(data));
    }
    return head;
}

module.exports = { Node, parse };

/**
 * @param snapshot
 * @param penguins
 */
function calculateWinners(snapshot, penguins) {
    const lanes = snapshot.split('\n');
    let results = [];
    let idx = 0;
    for (const lane of lanes) {
        let position = lane.indexOf('P');
        if (position === -1) {
            position = lane.indexOf('p');
        }
        let remaining = lane.substring(position + 1, lane.length - 1);
        while (remaining.indexOf('~') !== -1) {
            remaining = remaining.replace(/~/g, '--');
        }
        results.push([remaining.length, penguins[idx]]);
        idx++;
    }
    results.sort((a, b) => a[0] - b[0]);
    results = results.map(a => a[1]);
    return 'GOLD: ' + results[0] + ', SILVER: ' + results[1] + ', BRONZE: ' + results[2];
}

module.exports = calculateWinners;

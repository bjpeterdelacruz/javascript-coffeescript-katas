/**
 * @param s
 */
function pillow(s) {
    if (s[0].indexOf('n') === -1 || s[1].indexOf('B') === -1) {
        return false;
    }
    const shortest = s[0].length < s[1].length ? s[0] : s[1];
    for (let idx = 0; idx < shortest.length; idx++) {
        if (s[0][idx] === 'n' && s[1][idx] === 'B') {
            return true;
        }
    }
    return false;
}

module.exports = pillow;

function draw(deck) {

    if (deck.length === 0) {
        return [];
    }

    const drawnCards = [];

    const copy = [...deck];

    while (copy.length > 1) {
        drawnCards.push(copy[0]);
        copy.splice(0, 1);
        copy.push(copy[0]);
        copy.splice(0, 1);
    }
    drawnCards.push(copy[0]);

    return drawnCards;
}

module.exports = draw;

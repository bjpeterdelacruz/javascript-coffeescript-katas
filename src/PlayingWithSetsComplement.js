/**
 * @param s1
 * @param s2
 */
function diff(s1, s2) {
    return new Set(Array.from(s1).filter(element => !s2.has(element)));
}

module.exports = diff;

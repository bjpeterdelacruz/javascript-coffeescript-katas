/**
 * @param s1
 * @param s2
 */
function union(s1, s2) {
    return new Set([...s1, ...s2]);
}

module.exports = union;

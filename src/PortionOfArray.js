function portion(array, index, numberOfElements) {
    if (Math.abs(index) + numberOfElements - 1 >= array.length) {
        return -1;
    }
    const copy = index < 0 ? [...array].reverse() : [...array];
    const newArray = [];
    for (let idx = Math.abs(index), count = 0; count < numberOfElements; idx++, count++) {
        newArray.push(copy[idx]);
    }

    return index < 0 ? newArray.reverse() : newArray;
}

module.exports = portion;

/**
 * @param number
 * @param power
 */
function numberToPower(number, power) {
    if (power === 0) {
        return 1;
    }
    let result = 1;
    const positive = Math.abs(power);
    for (let i = 1; i <= positive; i++) {
        result *= number;
    }
    return power < 0 ? 1 / result : result;
}

module.exports = numberToPower;

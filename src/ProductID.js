/**
 * @param url
 */
function getProductId(url) {
    let arr = url.split('-p-');
    arr = arr[arr.length - 1];
    return arr.split('-')[0];
}

module.exports = getProductId;

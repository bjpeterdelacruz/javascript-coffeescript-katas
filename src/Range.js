/**
 * @param start
 * @param stop
 * @param step
 */
function range(start, stop, step) {
    const arr = [];
    if (stop === undefined && step === undefined) {
        for (let number = 0; number < start; number++) {
            arr.push(number);
        }
        return arr;
    }

    if (step === undefined) {
        if (start > stop) {
            return arr;
        }
        for (let number = start; number < stop; number++) {
            arr.push(number);
        }
        return arr;
    }
    if (step === 0) {
        for (let number = start; number < stop; number++) {
            arr.push(start);
        }
        return arr;
    }

    if (start < stop && step < 0) {
        return arr;
    }
    if (start > stop && step > 0) {
        return arr;
    }

    if (start > stop && step < 0) {
        for (let number = start; number > stop; number += step) {
            arr.push(number);
        }
    }
    else {
        for (let number = start; number < stop; number += step) {
            arr.push(number);
        }
    }
    return arr;
}

module.exports = range;

/**
 * @param arr
 */
function grow(arr) {
    return arr.reduce((a, b) => a * b);
}

module.exports = grow;

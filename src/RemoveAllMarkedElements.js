Array.prototype.remove_ = function(integers, values) {
    const vals = new Set(values);
    return integers.filter(integer => !vals.has(integer));
};

module.exports = Array.prototype.remove_;

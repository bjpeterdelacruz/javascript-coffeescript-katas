/**
 * @param string
 */
function removeExclamationMarks(string) {
    let str = string;
    while (str.indexOf('!') !== -1) {
        str = str.replace('!', '');
    }
    return str;
}

module.exports = removeExclamationMarks;

function countSel(lst) {
    const results = [];
    results.push(lst.length);
    results.push(new Set(lst).size);

    const counts = new Map();
    for (let idx = 0; idx < lst.length; idx++) {
        counts.set(lst[idx], counts.has(lst[idx]) ? counts.get(lst[idx]) + 1 : 1);
    }

    let onlyOnce = 0;
    let maxCount = 0;
    counts.forEach(value => {
        if (value === 1) {
            onlyOnce++;
        }
        if (value > maxCount) {
            maxCount = value;
        }
    });
    results.push(onlyOnce);

    const maxValues = [];
    counts.forEach((value, key) => {
        if (value === maxCount) {
            maxValues.push(key);
        }
    });
    maxValues.sort((a, b) => a - b);
    results.push([maxValues, maxCount]);
    return results;
}

module.exports = countSel;

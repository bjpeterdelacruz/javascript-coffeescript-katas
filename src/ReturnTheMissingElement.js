/**
 * @param arr
 */
function getMissingElement(arr) {
    const set = new Set(arr);
    return Array.from(Array(10).keys()).filter(number => !set.has(number))[0];
}

module.exports = getMissingElement;

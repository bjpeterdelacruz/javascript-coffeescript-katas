/**
 * @param firstChar
 * @param secondChar
 */
function areBothSameType(firstChar, secondChar) {
    const alpha = 'abcdefghijklmnopqrstuvwxyz';
    const numbers = '0123456789';
    const alphanumeric = alpha + numbers;
    if (alpha.indexOf(firstChar.toLowerCase()) !== -1 &&
        alpha.indexOf(secondChar.toLowerCase()) !== -1) {
        return true;
    }
    else if (numbers.indexOf(firstChar) !== -1 && numbers.indexOf(secondChar) !== -1) {
        return true;
    }
    return alphanumeric.indexOf(firstChar.toLowerCase()) === -1 &&
           alphanumeric.indexOf(secondChar.toLowerCase()) === -1;
}


/**
 * @param arr
 */
function reverseAll(arr) {
    const reversedArr = [...arr].reverse();
    for (let idx = 0; idx < reversedArr.length; idx++) {
        const substrings = reversedArr[idx].split('\n');
        const arrElement = [];
        for (let idx2 = substrings.length - 1; idx2 >= 0; idx2--) {
            const substring = substrings[idx2];
            let currentGroup = [substring[0]];
            const characters = [];
            for (let idx3 = 1; idx3 < substring.length; idx3++) {
                if (areBothSameType(currentGroup[currentGroup.length - 1], substring[idx3])) {
                    currentGroup.push(substring[idx3]);
                }
                else {
                    characters.push(currentGroup.reverse().join(''));
                    currentGroup = [substring[idx3]];
                }
            }
            characters.push(currentGroup.reverse().join(''));
            arrElement.push(characters.join(''));
        }
        reversedArr[idx] = arrElement.join('\n');
    }
    return reversedArr;
}

module.exports = reverseAll;

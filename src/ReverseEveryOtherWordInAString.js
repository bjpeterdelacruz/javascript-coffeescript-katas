/**
 * @param str
 */
function reverse(str) {
    const arr = str.trim().split(/\s+/);
    const newArr = [];
    let position = 0;
    for (const elem of arr) {
        if (position % 2 === 1) {
            newArr.push(elem.split('').reverse().join(''));
        }
        else {
            newArr.push(elem);
        }
        position++;
    }
    return newArr.join(' ');
}

module.exports = reverse;

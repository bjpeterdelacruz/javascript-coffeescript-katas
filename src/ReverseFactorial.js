/**
 * @param num
 */
function reverseFactorial(num) {
    let product = 1;
    let counter = 2;
    while (product < num) {
        product *= counter;
        counter++;
    }
    if (product > num) {
        return 'None';
    }
    return `${counter - 1}!`;
}

module.exports = reverseFactorial;

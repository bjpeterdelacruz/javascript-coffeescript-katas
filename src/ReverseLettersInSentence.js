/**
 * @param sentence
 */
function reverser(sentence) {
    const indexes = [];
    for (let idx = 0; idx < sentence.length; idx++) {
        if (sentence[idx].match(/\s/)) {
            indexes.push([idx, sentence[idx]]);
        }
    }
    const words = sentence.split(/\s+/);
    let reversedWords = words.map(word => word.split('').reverse().join('')).join('');
    for (const pair of indexes) {
        reversedWords = reversedWords.slice(0, pair[0]) + pair[1] + reversedWords.slice(pair[0]);
    }
    return reversedWords;
}

module.exports = reverser;

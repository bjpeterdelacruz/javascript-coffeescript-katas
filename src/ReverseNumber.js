/**
 * @param n
 */
function reverseNumber(n) {
    if (n < 0) {
        return parseFloat(`-${n.toString().substring(1).split('').reverse().join('')}`);
    }
    return parseFloat(n.toString().split('').reverse().join(''));
}

module.exports = reverseNumber;

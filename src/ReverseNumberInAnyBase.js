function reverseNumber(n, b) {
    if (b === 1n) {
        return n;
    }
    let number = Number(n);
    const base = Number(b);

    // Convert from base-10 to base-x
    const remainders = [number % base];
    while (number % base !== number) {
        number = Math.floor(number / base);
        remainders.push(number % base);
    }

    remainders.reverse();

    // Convert from base-x to base-10
    let sum = 0;
    for (let idx = 0; idx < remainders.length; idx++) {
        sum += remainders[idx] * Math.pow(base, idx);
    }
    return BigInt(sum);
}

module.exports = reverseNumber;

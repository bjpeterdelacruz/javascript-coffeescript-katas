/**
 * @param p1
 * @param p2
 */
function rps(p1, p2) {
    const arr = ['scissors', 'paper', 'rock'];
    if ((arr.indexOf(p1) + 1) % 3 === arr.indexOf(p2)) {
        return 'Player 1 won!';
    }
    if ((arr.indexOf(p2) + 1) % 3 === arr.indexOf(p1)) {
        return 'Player 2 won!';
    }
    return 'Draw!';
}

module.exports = rps;

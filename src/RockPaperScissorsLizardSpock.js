/**
 * @param player1
 * @param player2
 */
function rpsls(player1, player2) {
    const map = new Map();
    map.set('scissors', ['paper', 'lizard']);
    map.set('paper', ['rock', 'spock']);
    map.set('rock', ['lizard', 'scissors']);
    map.set('lizard', ['spock', 'paper']);
    map.set('spock', ['scissors', 'rock']);

    if (map.get(player1).includes(player2)) {
        return 'Player 1 Won!';
    }
    if (map.get(player2).includes(player1)) {
        return 'Player 2 Won!';
    }
    return 'Draw!';
}

module.exports = rpsls;

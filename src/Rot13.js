/**
 * @param message
 */
function rot13(message) {
    let encodedMessage = '';
    [...message].forEach((element) => {
        const code = element.charCodeAt(0);
        if ((code >= 65 && code <= 90) || (code >= 97 && code <= 122)) {
            const offset = code >= 65 && code <= 90 ? 65 : 97;
            encodedMessage += String.fromCharCode((code - offset + 13) % 26 + offset);
        }
        else {
            encodedMessage += element;
        }
    });
    return encodedMessage;
}

module.exports = rot13;

const isNumber = (value) =>
    value !== null && value !== undefined && value.toString().match(/^-?[0-9]+$/) !== null;

/**
 * @param array
 */
function secondLargest(array) {
    if (!Array.isArray(array)) {
        return undefined;
    }
    const numbers = array.filter(element => isNumber(element)).map(element => parseInt(element));
    const unique = new Set(numbers);
    if (unique.size < 2) {
        return undefined;
    }
    const sorted = Array.from(unique).sort((a, b) => a - b);
    return sorted[sorted.length - 2];
}

module.exports = { isNumber, secondLargest };

/**
 * @param s
 */
function lettersToNumbers(s) {
    const alphabet = 'abcdefghijklmnopqrstuvwxyz';
    const uppercase = alphabet.toUpperCase();

    let sum = 0;
    for (const character of s) {
        if (character.match(/[a-z]/)) {
            sum += alphabet.indexOf(character) + 1;
        }
        else if (character.match(/[A-Z]/)) {
            sum += (uppercase.indexOf(character) + 1) * 2;
        }
        else if (character.match(/[0-9]/)) {
            sum += parseInt(character);
        }
    }
    return sum;
}

module.exports = lettersToNumbers;

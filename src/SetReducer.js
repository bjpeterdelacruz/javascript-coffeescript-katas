function setReducer(input) {
    if (input.length === 0) {
        return 0;
    }
    let temp = input;
    let counts = [];
    while (temp.length > 1) {
        let count = 0;
        for (let idx = 0; idx < temp.length - 1; idx++) {
            count++;
            if (temp[idx] !== temp[idx + 1]) {
                counts.push(count);
                count = 0;
            }
            if (idx + 2 === temp.length) {
                counts.push(temp[idx] === temp[idx + 1] ? ++count : 1);
            }
        }
        temp = counts;
        counts = [];
    }
    return temp[0];
}

module.exports = setReducer;

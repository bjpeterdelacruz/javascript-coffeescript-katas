/**
 * @param words
 */
function shiritori(words) {
    if (words.length === 0 || words[0].length === 0) {
        return [];
    }
    if (words.length === 1) {
        return words;
    }
    const result = [];
    if (words[0][words[0].length - 1] === words[1][0]) {
        result.push(words[0]);
        result.push(words[1]);
    }
    else {
        return [words[0]];
    }
    for (let idx = 2; idx < words.length; idx++) {
        const prev = words[idx - 1];
        if (prev[prev.length - 1] === words[idx][0]) {
            result.push(words[idx]);
        }
        else {
            break;
        }
    }
    return result;
}

module.exports = shiritori;

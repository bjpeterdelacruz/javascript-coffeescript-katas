/**
 * @param a
 * @param b
 */
function shorterReverseLonger(a, b) {
    if (a.length >= b.length) {
        return `${b}${a.split('').reverse().join('')}${b}`;
    }
    return `${a}${b.split('').reverse().join('')}${a}`;
}

module.exports = shorterReverseLonger;

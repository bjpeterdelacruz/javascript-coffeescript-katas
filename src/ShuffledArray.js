/**
 * @param shuffled
 */
function shuffledArray(shuffled) {
    for (let currentIndex = 0; currentIndex < shuffled.length; currentIndex++) {
        let currentSum = 0;
        for (let idx = 0; idx < shuffled.length; idx++) {
            if (idx !== currentIndex) {
                currentSum += shuffled[idx];
            }
        }
        if (currentSum === shuffled[currentIndex]) {
            shuffled.splice(currentIndex, 1);
            break;
        }
    }
    shuffled.sort((a, b) => a - b);
    return shuffled;
}

module.exports = shuffledArray;

/**
 * @param number
 */
function sign(number) {
    if (number === undefined) {
        return NaN;
    }
    if (!isNumeric(number.toString())) {
        return NaN;
    }
    const num = parseFloat(number);
    return num < 0 ? -1 : (num === 0 ? 0 : 1);
}

/**
 * @param string
 */
function isNumeric(string) {
    return !isNaN(string) && !isNaN(parseFloat(string));
}

module.exports = sign;

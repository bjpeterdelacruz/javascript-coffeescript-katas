/**
 * @param a
 * @param b
 */
function additionWithoutCarrying(a, b) {
    const aString = a.toString();
    const bString = b.toString();

    let shorterString = aString.length < bString.length ? aString : bString;
    const longerString = aString.length >= bString.length ? aString : bString;

    for (let numZeros = longerString.length - shorterString.length; numZeros > 0; numZeros--) {
        shorterString = '0' + shorterString;
    }

    let result = '';
    for (let idx = longerString.length - 1; idx >= 0; idx--) {
        const num1 = parseInt(shorterString[idx]);
        const num2 = parseInt(longerString[idx]);
        const sum = num1 + num2;
        result = sum.toString()[sum.toString().length - 1] + result;
    }

    return parseInt(result);
}

module.exports = additionWithoutCarrying;

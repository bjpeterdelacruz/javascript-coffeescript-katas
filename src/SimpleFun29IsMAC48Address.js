/**
 * @param inputString
 */
function isMAC48Address(inputString) {
    const arr = /^(\w+)-(\w+)-(\w+)-(\w+)-(\w+)-(\w+)$/.exec(inputString);
    if (!arr) {
        return false;
    }
    for (let idx = 1; idx <= 6; idx++) {
        const match = arr[idx].match(/[0-9a-fA-F]+/);
        if (!match || match[0].length !== 2) {
            return false;
        }
    }
    return true;
}

module.exports = isMAC48Address;

/**
 * @param evidences
 */
function isInformationConsistent(evidences) {
    let set = new Set();
    for (let columnIndex = 0; columnIndex < evidences[0].length; columnIndex++) {
        for (let rowIndex = 0; rowIndex < evidences.length; rowIndex++) {
            if (evidences[rowIndex][columnIndex] === 1) {
                set.add(1);
            }
            else if (evidences[rowIndex][columnIndex] === -1) {
                set.add(-1);
            }
            if (set.size === 2) {
                return false;
            }
        }
        set = new Set();
    }
    return true;
}

module.exports = isInformationConsistent;

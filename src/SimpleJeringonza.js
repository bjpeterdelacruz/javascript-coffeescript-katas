/**
 * @param string
 */
function jeringonza(string) {
    const vowels = ['a', 'e', 'i', 'o', 'u'];
    let newString = string;
    for (const vowel of vowels) {
        newString = newString.split(vowel).join(`${vowel}p${vowel}`);
        const uppercase = vowel.toUpperCase();
        newString = newString.split(uppercase).join(`${uppercase}P${uppercase}`);
    }
    return newString;
}

module.exports = jeringonza;

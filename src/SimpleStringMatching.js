/**
 * @param a
 * @param b
 */
function solve(a, b) {
    return a.indexOf('*') === -1 ? a === b : new RegExp(`^${a.replace('*', '.*')}$`).test(b);
}

module.exports = solve;

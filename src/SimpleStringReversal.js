/**
 * @param str
 */
function solve(str) {
    const indices = [];
    for (let idx = 0; idx < str.length; idx++) {
        if (str[idx].match(/[\s]/)) {
            indices.push([idx, str[idx]]);
        }
    }
    const newStr = str.split(/[\s]/).join('').split('').reverse();
    for (let idx = 0; idx < indices.length; idx++) {
        newStr.splice(indices[idx][0], 0, indices[idx][1]);
    }
    return newStr.join('');
}

module.exports = solve;

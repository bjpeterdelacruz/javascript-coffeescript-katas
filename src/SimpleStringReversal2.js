/**
 * @param string
 * @param start
 * @param end
 */
function solve(string, start, end) {
    const substr = string.substring(start, end + 1);
    return string.replace(substr, substr.split('').reverse().join(''));
}

module.exports = solve;

/**
 * @param x
 */
function bald(x) {
    const count = x.split('').filter(letter => letter === '/').length;
    const head = x.split('').map(_ => '-').join('');
    switch (count) {
    case 0:
        return [head, 'Clean!'];
    case 1:
        return [head, 'Unicorn!'];
    case 2:
        return [head, 'Homer!'];
    case 3:
    case 4:
    case 5:
        return [head, 'Careless!'];
    default:
        return [head, 'Hobo!'];
    }
}

module.exports = bald;

/**
 * @param a
 */
function sortTransform(a) {
    const arr = [createString(a)];
    arr.push(createString(a.sort((a, b) => a - b)));
    arr.push(createString(a.sort((a, b) => b - a)));
    arr.push(createString(a.map(num => String.fromCharCode(num)).sort()));
    return arr.join('-');
}

/**
 * @param a
 */
function createString(a) {
    let str = '';
    for (const idx of [0, 1, a.length - 2, a.length - 1]) {
        str += typeof a[idx] === 'number' ? String.fromCharCode(a[idx]) : a[idx];
    }
    return str;
}

module.exports = sortTransform;

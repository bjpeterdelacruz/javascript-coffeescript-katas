/**
 * @param arr
 */
function sortMe(arr) {
    const map = new Map();
    for (let idx = 0; idx < arr.length; idx++) {
        const word = arr[idx];
        const lastCharacter = word.toString()[word.toString().length - 1];
        if (map.has(lastCharacter)) {
            map.get(lastCharacter).push(word);
        }
        else {
            map.set(lastCharacter, [word]);
        }
    }
    const sortedKeys = Array.from(map.keys()).sort();
    const sortedArr = [];
    for (const key of sortedKeys) {
        for (const word of map.get(key)) {
            sortedArr.push(word);
        }
    }
    return sortedArr;
}

module.exports = sortMe;

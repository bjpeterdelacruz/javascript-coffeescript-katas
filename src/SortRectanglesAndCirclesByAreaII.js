function sortByArea(array) {
    const areas = [];
    for (const element of array) {
        if (typeof element === 'number') {
            areas.push([Math.PI * element * element, element]);
        }
        else {
            areas.push([element[0] * element[1], element]);
        }
    }
    return areas.sort((a, b) => a[0] - b[0]).map((a) => a[1]);
}

module.exports = sortByArea;

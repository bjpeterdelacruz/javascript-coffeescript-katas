/**
 * @param uint32
 */
function sortBytes(uint32) {
    let binaryString = uint32.toString(2);
    while (binaryString.length < 32) {
        binaryString = '0' + binaryString;
    }
    let bytes = [binaryString.substr(0, 8), binaryString.substr(8, 8),
        binaryString.substr(16, 8), binaryString.substr(24, 8)];
    bytes = bytes.map(byte => parseInt(byte, 2));
    bytes.sort((a, b) => b - a);
    bytes = bytes.map(byte => {
        let b = byte.toString(2);
        while (b.length < 8) {
            b = '0' + b;
        }
        return b;
    });
    return parseInt(bytes.join(''), 2);
}

module.exports = sortBytes;

/**
 * @param s
 */
function sortVowels(s) {
    if (typeof s !== 'string') {
        return '';
    }
    const vowels = 'aeiouAEIOU';
    const sorted = s.split('').map(
        letter => vowels.indexOf(letter) === -1 ? `${letter}|` : `|${letter}`).join('\n');
    return sorted.substring(0, sorted.length);
}

module.exports = sortVowels;

/**
 * @param arr
 * @param predicate
 */
function span(arr, predicate) {
    const firstArr = [];
    let idx = 0;
    while (idx < arr.length && predicate(arr[idx])) {
        firstArr.push(arr[idx++]);
    }
    const secondArr = [];
    while (idx < arr.length) {
        secondArr.push(arr[idx++]);
    }
    return [firstArr, secondArr];
}

module.exports = span;

/**
 * @param arr
 * @param prop
 */
function split(arr, prop) {
    if (!prop) {
        return [[], arr];
    }
    const truthy = [];
    const falsey = [];
    for (const obj of arr) {
        if (!new Set(Object.keys(obj)).has(prop)) {
            falsey.push(obj);
            continue;
        }
        const value = Object.entries(obj).filter(pair => pair[0] === prop)[0][1];
        if (value) {
            truthy.push(obj);
        }
        else {
            falsey.push(obj);
        }
    }
    return [truthy, falsey];
}

module.exports = split;

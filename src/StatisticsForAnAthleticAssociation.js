/**
 * @param num
 */
function intToTime(num) {
    const hours = Math.floor(num / 3600);
    const minutes = Math.floor(num % 3600 / 60);
    const seconds = Math.floor(num % 3600 % 60);
    let time = (hours <= 9 ? '0' + hours : hours) + '|';
    time += (minutes <= 9 ? '0' + minutes : minutes) + '|';
    return seconds <= 9 ? time + '0' + seconds : time + seconds;
}

/**
 * @param string
 */
function stat(string) {
    if (string === '') {
        return '';
    }
    const stats = [];
    for (const chars of string.split(',')) {
        const time = chars.trim().split('|');
        const hours = parseInt(time[0]) * 60 * 60;
        const minutes = parseInt(time[1]) * 60;
        const seconds = parseInt(time[2]);
        stats.push(hours + minutes + seconds);
    }
    stats.sort((a, b) => a - b);
    const range = stats[stats.length - 1] - stats[0];
    const average = Math.floor(stats.reduce((a, b) => a + b, 0) / stats.length);
    let median = stats[Math.floor(stats.length / 2)];
    if (stats.length % 2 === 0) {
        const second = stats.length / 2;
        const first = second - 1;
        median = Math.floor((stats[first] + stats[second]) / 2);
    }
    return 'Range: ' + intToTime(range) + ' Average: ' + intToTime(average)
        + ' Median: ' + intToTime(median);
}

module.exports = stat;

/**
 * @param stones
 */
function solve(stones) {
    let count = 0;
    for (let idx = 1; idx < stones.length; idx++) {
        if (stones[idx - 1] === stones[idx]) {
            count++;
        }
    }
    return count;
}

module.exports = solve;

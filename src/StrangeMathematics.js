/**
 * @param n
 * @param k
 */
function strangeMath(n, k) {
    return [...Array(n).keys()].map(i => (i + 1).toString()).sort().indexOf(k.toString()) + 1;
}

module.exports = strangeMath;

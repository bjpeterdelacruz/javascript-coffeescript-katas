/**
 * @param n
 * @param string
 */
function stringBreakers(n, string) {
    const arr = [];
    let temp = '';
    const str = string.split(/[\s]+/).join('');
    for (let idx = 0, count = 0; idx <= str.length; idx++) {
        if (count % n === 0) {
            arr.push(temp);
            if (idx === str.length) {
                break;
            }
            temp = str[idx];
            count = 1;
        }
        else {
            temp += str[idx];
            count++;
        }
    }
    const leftover = str.replace(arr.join(''), '');
    return (arr.join('\n') + '\n' + leftover).trim();
}

module.exports = stringBreakers;

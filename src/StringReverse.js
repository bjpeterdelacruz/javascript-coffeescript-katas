String.prototype.reverse = function() {
    return this.split('').reverse().join('');
};

module.exports = String.prototype.reverse;

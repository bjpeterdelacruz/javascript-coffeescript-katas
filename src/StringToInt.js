/**
 * @param str
 */
function myParseInt(str) {
    if (typeof str !== 'string') {
        return 'NaN';
    }
    const num = str.trim();
    if (num.match(/^[0-9]+$/)) {
        return parseInt(num);
    }
    return 'NaN';
}

module.exports = myParseInt;

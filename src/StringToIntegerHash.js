/**
 * @param str
 */
function hashCode(str) {
    let hash = 0;
    const n = str.length;
    for (let idx = 0; idx < n; idx++) {
        // All intermediate values are 32-bit
        hash += Math.imul(str.charCodeAt(idx), pow32(31, n - (idx + 1)));
        hash = hash | 0; // same as Math.imul(hash, 1)
    }
    return hash;
}

/**
 * @param x
 * @param y
 */
function pow32(x, y) {
    let result = 1;
    let exp = y;
    while (exp > 0) {
        result = Math.imul(result, x);
        exp--;
    }
    return result;
}

module.exports = { hashCode, pow32 };

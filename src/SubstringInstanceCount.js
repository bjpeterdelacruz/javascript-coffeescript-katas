/**
 * @param fullText
 * @param searchText
 */
function solution(fullText, searchText) {
    return fullText.split(searchText).length - 1;
}

module.exports = solution;

/**
 * @param firstNumber
 * @param secondNumber
 */
function padStrings(firstNumber, secondNumber) {
    let a = firstNumber;
    let b = secondNumber;
    const numZeros = Math.abs(a.length - b.length);
    if (a.length < b.length) {
        for (let count = 0; count < numZeros; count++) {
            a = '0' + a;
        }
    }
    else {
        for (let count = 0; count < numZeros; count++) {
            b = '0' + b;
        }
    }
    return [a, b];
}

/**
 * @param a
 * @param b
 */
function isGreaterThanOrEqualTo(a, b) {
    let idx = 0;
    const [tempA, tempB] = padStrings(a, b);
    while (parseInt(tempA[idx]) === parseInt(tempB[idx])) {
        idx++;
    }
    return parseInt(tempA[idx]) > parseInt(tempB[idx]);
}

/**
 * @param a
 * @param b
 */
function getBiggerNumber(a, b) {
    return isGreaterThanOrEqualTo(a, b) ? [a, b] : [b, a];
}

/**
 * @param a
 * @param b
 */
function sum(a, b) {
    const [numA, numB] = padStrings(a, b);
    let carry = 0;
    let totalSum = '';
    for (let idx = numA.length - 1; idx >= 0; idx--) {
        const sum = parseInt(numA[idx]) + parseInt(numB[idx]) + carry;
        carry = sum > 9 ? 1 : 0;
        totalSum = sum.toString()[sum.toString().length - 1] + totalSum;
    }
    totalSum = carry.toString() + totalSum;
    while (totalSum.indexOf('0') === 0) {
        totalSum = totalSum.slice(1);
    }
    return totalSum.length === 0 ? '0' : totalSum;
}

/**
 * @param a
 * @param b
 */
function difference(a, b) {
    const [numA, numB] = padStrings(a, b);
    const [firstNumber, secondNumber] = getBiggerNumber(numA, numB);
    let borrow = 0;
    let totalDiff = '';
    for (let idx = firstNumber.length - 1; idx >= 0; idx--) {
        let num1 = parseInt(firstNumber[idx]) - borrow;
        const num2 = parseInt(secondNumber[idx]);
        if (num1 < num2) {
            num1 += 10;
            borrow = 1;
        }
        else {
            borrow = 0;
        }
        totalDiff = (num1 - num2) + totalDiff;
    }
    while (totalDiff.indexOf('0') === 0) {
        totalDiff = totalDiff.slice(1);
    }
    return totalDiff.length === 0 ? '0' : totalDiff;
}

/**
 * @param number
 */
function replaceNegativeSign(number) {
    let num = number;
    while (num.indexOf('-') !== -1) {
        num = num.replace('-', '');
    }
    return num;
}

/**
 * @param firstNumber
 * @param secondNumber
 */
function add(firstNumber, secondNumber) {
    let a = firstNumber;
    const b = secondNumber;
    if (a.indexOf('-') === -1 && b.indexOf('-') === -1) {
        return sum(a, b);
    }
    else if (a.indexOf('-') !== -1 && b.indexOf('-') !== -1) {
        const total = sum(replaceNegativeSign(a), replaceNegativeSign(b));
        return total === '0' ? '0' : '-' + total;
    }
    a = replaceNegativeSign(a);
    const diff = difference(a, b);
    return isGreaterThanOrEqualTo(a, b) ? '-' + diff : diff;
}

/**
 * @param a
 * @param b
 */
function subtract(a, b) {
    if (a.indexOf('-') === -1 && b.indexOf('-') === -1) {
        const diff = difference(a, b);
        return diff === '0' ? '0' : (isGreaterThanOrEqualTo(a, b) ? diff : '-' + diff);
    }
    else if (b.indexOf('-') !== -1) {
        return add(a, replaceNegativeSign(b));
    }
    return add(a, '-' + b);
}

module.exports = subtract;

/**
 * @param arr
 */
function arraySum(arr) {
    let sum = 0;
    arr.forEach(element => {
        if (Array.isArray(element)) {
            sum += arraySum(element);
        }
        else if (typeof element === 'number') {
            sum += element;
        }
    });
    return sum;
}

module.exports = arraySum;

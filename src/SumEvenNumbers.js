/**
 * @param input
 */
function sumEvenNumbers(input) {
    const filtered = input.filter(num => num % 2 === 0);
    return filtered.length === 0 ? 0 : filtered.reduce((acc, curr) => acc + curr);
}

module.exports = sumEvenNumbers;

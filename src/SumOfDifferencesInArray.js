/**
 * @param arr
 */
function sumOfDifferences(arr) {
    if (arr.length < 2) {
        return 0;
    }
    const sortedArray = [...arr].sort((a, b) => {
        return b - a;
    });
    let sum = 0;
    for (let idx = 1; idx < sortedArray.length; idx++) {
        sum += (sortedArray[idx - 1] - sortedArray[idx]);
    }
    return sum;
}

module.exports = sumOfDifferences;

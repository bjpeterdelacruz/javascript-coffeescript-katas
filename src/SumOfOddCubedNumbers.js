/**
 * @param arr
 */
function cubeOdd(arr) {
    let sum = 0;
    for (let idx = 0; idx < arr.length; idx++) {
        const num = arr[idx];
        if (num === undefined) {
            return undefined;
        }
        const str = num.toString();
        if (str === '' || str.match(/[^-0-9]/)) {
            return undefined;
        }
        if (Math.abs(num % 2) === 1) {
            sum += Math.pow(num, 3);
        }
    }
    return sum;
}

module.exports = cubeOdd;

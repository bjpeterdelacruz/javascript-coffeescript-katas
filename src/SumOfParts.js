/**
 * @param ls
 */
function partsSums(ls) {
    let sum = ls.reduce((a, b) => a + b, 0);
    const arr = [sum];
    for (let idx = 0; idx < ls.length; idx++) {
        sum -= ls[idx];
        arr.push(sum);
    }
    return arr;
}

module.exports = partsSums;

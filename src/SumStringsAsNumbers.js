/**
 * @param a
 * @param b
 */
function sumStrings(a, b) {
    let shortestString = a.length < b.length ? a : b;
    const longestString = a.length >= b.length ? a : b;
    const numZeros = longestString.length - shortestString.length;
    for (let count = 0; count < numZeros; count++) {
        shortestString = '0' + shortestString;
    }
    let carry = 0;
    let totalSum = '';
    for (let idx = shortestString.length - 1; idx >= 0; idx--) {
        const sum = parseInt(shortestString[idx]) + parseInt(longestString[idx]) + carry;
        carry = sum > 9 ? 1 : 0;
        totalSum = sum.toString()[sum.toString().length - 1] + totalSum;
    }
    totalSum = carry.toString() + totalSum;
    while (totalSum.indexOf('0') === 0) {
        totalSum = totalSum.slice(1);
    }
    return totalSum;
}

module.exports = sumStrings;

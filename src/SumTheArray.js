Array.prototype.sum = function() {
    if (this.length === 0) {
        return 0;
    }
    return this.reduce((acc, curr) => acc + curr);
};

module.exports = Array.prototype.sum;

/**
 * @param nums
 */
function summaryRanges(nums) {
    const unique = Array.from(new Set(nums)).sort((a, b) => a - b);
    const result = [];
    let arr = [];
    for (let idx = 0; idx <= unique.length - 1; idx++) {
        const diff = Math.abs(unique[idx] - unique[idx + 1]);
        if (diff > 1) {
            arr.push(unique[idx]);
            result.push(arr);
            arr = [];
        }
        else {
            arr.push(unique[idx]);
        }
    }
    if (arr.length !== 0) {
        result.push(arr);
    }
    return result.map(element => {
        if (element.length === 1) {
            return `${element[0]}`;
        }
        return `${element[0]}->${element[element.length - 1]}`;
    });
}

module.exports = summaryRanges;

/**
 * @param string
 */
function transform(string) {
    const counts = new Map();
    for (let idx = 0; idx < string.length; idx++) {
        const letter = string.charAt(idx);
        if (counts.has(letter)) {
            counts.set(letter, counts.get(letter) + 1);
        }
        else {
            counts.set(letter, 1);
        }
    }
    let stringCounts = '';
    const distinct = new Set();
    for (let idx = 0; idx < string.length; idx++) {
        const letter = string.charAt(idx);
        if (!distinct.has(letter)) {
            stringCounts += letter;
            const count = counts.get(letter);
            if (count > 1) {
                stringCounts += count;
            }
            distinct.add(letter);
        }
    }
    return stringCounts;
}

module.exports = transform;

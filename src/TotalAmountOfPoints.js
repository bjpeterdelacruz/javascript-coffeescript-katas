/**
 * @param games
 */
function points(games) {
    let totalPoints = 0;
    for (const game of games) {
        const [teamX, teamY] = game.split(':');
        const x = parseInt(teamX);
        const y = parseInt(teamY);
        totalPoints += x > y ? 3 : (x < y ? 0 : 1);
    }
    return totalPoints;
}

module.exports = points;

/**
 * @param arr1
 * @param arr2
 */
function maxMin(arr1, arr2) {
    const diffs = [];
    arr1.forEach((element, idx) => diffs.push(Math.abs(element - arr2[idx])));
    return [Math.max(...diffs), Math.min(...diffs)];
}

module.exports = maxMin;

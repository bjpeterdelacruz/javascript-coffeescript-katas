Array.prototype.transpose = function() {
    if (this.length === 0) {
        return this;
    }
    if (this[0].length === 0) {
        return [[]];
    }

    const arr = [];
    for (let row = 0; row < this.length; row++) {
        for (let col = 0; col < this[0].length; col++) {
            if (!arr[col]) {
                arr.push([]);
            }
            arr[col].push(this[row][col]);
        }
    }
    return arr;
};

module.exports = Array.prototype.transpose;

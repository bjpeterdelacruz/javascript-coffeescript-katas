/**
 * @param number
 */
function isTriangleNumber(number) {
    if (isNaN(number)) {
        return false;
    }
    let n = 0;
    while (true) {
        const result = n * (n + 1) / 2;
        if (result > number) {
            return false;
        }
        if (result === number) {
            return true;
        }
        n++;
    }
}

module.exports = isTriangleNumber;

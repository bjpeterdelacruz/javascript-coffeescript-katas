/**
 * @param key
 * @param period
 * @param data
 */
function trifidEncode(key, period, data) {
    const keyMap = __buildKeyMap(key);
    const horizontalCodes = __generateCodes(keyMap, period, data, true);
    return __getLettersFromCodes(keyMap, period, horizontalCodes, true);
}

/**
 * @param key
 * @param period
 * @param data
 */
function trifidDecode(key, period, data) {
    const keyMap = __buildKeyMap(key);
    const verticalCodes = __generateCodes(keyMap, period, data, false);
    return __getLettersFromCodes(keyMap, period, verticalCodes, false);
}

/**
 * @param key
 */
function __buildKeyMap(key) {
    const keyMap = {};
    let idx = 0;
    for (let square = 1; square <= 3; square++) {
        keyMap[square] = [];
        for (let row = 1; row <= 3; row++) {
            const keyRow = [];
            keyMap[square].push(keyRow);
            for (let col = 1; col <= 3; col++) {
                keyRow.push(key[idx++]);
            }
        }
    }
    return keyMap;
}

/**
 * @param keyMap
 * @param letter
 */
function __getCodeForLetter(keyMap, letter) {
    for (let square = 1; square <= 3; square++) {
        for (let row = 0; row < 3; row++) {
            for (let col = 0; col < 3; col++) {
                if (keyMap[square][row][col] === letter) {
                    return square + '' + (row + 1) + '' + (col + 1);
                }
            }
        }
    }
}

/**
 * @param keyMap
 * @param period
 * @param codes
 * @param encode
 */
function __getLettersFromCodes(keyMap, period, codes, encode) {
    let data = '';
    [...codes].forEach(code => {
        const letterCodes = [];
        if (encode) {
            for (let idx = 0; idx < code.length; idx++) {
                if (idx > 0 && idx % 3 === 0) {
                    letterCodes.push(code.substring(idx - 3, idx));
                }
            }
            letterCodes.push(code.substring(code.length - 3, code.length));
        }
        else {
            let p = period;
            if (code.length < period * 3) {
                while (code.length / p !== 3) {
                    p--;
                }
            }
            for (let idx = 0; idx < p; idx++) {
                letterCodes.push(code[idx] + code[idx + p] + code[idx + (p * 2)]);
            }
        }
        letterCodes.forEach(code => {
            const square = parseInt(code.substring(0, 1));
            const row = parseInt(code.substring(1, 2)) - 1;
            const col = parseInt(code.substring(2, 3)) - 1;
            data += keyMap[square][row][col];
        });
    });
    return data;
}

/**
 * @param keyMap
 * @param period
 * @param data
 * @param encode
 */
function __generateCodes(keyMap, period, data, encode) {
    const strings = [];
    let chars = '';
    for (let idx = 0; idx < data.length; idx++) {
        if (idx > 0 && idx % period === 0) {
            strings.push(chars);
            chars = '';
        }
        chars += data[idx];
    }
    strings.push(chars);
    const verticalCodes = [];
    strings.forEach(string => {
        let code = '';
        [...string].forEach(char => {
            code += __getCodeForLetter(keyMap, char);
        });
        verticalCodes.push(code);
    });
    if (!encode) {
        return verticalCodes;
    }
    const horizontalCodes = [];
    verticalCodes.forEach(code => {
        let horiz = '';
        for (let outer = 0; outer < 3; outer++) {
            for (let idx = 0; idx < period * 3 && idx < code.length; idx += 3) {
                horiz += code[outer + idx];
            }
        }
        horizontalCodes.push(horiz);
    });
    return horizontalCodes;
}

module.exports = { trifidEncode, trifidDecode };

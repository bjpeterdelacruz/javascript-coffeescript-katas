String.prototype.trim = function() {
    let startIdx = 0;
    while (this[startIdx].match(/[\s]/)) {
        startIdx++;
        if (startIdx === this.length) {
            return '';
        }
    }
    let endIdx = this.length - 1;
    while (this[endIdx].match(/[\s]/)) {
        endIdx--;
    }
    return this.substring(startIdx, endIdx + 1);
};

module.exports = String.prototype.trim;

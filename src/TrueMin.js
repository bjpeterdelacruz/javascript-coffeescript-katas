/**
 * @param first
 * @param second
 */
function min(first, second) {
    let a = first;
    let b = second;
    if (a === null) {
        a = 0;
    }
    if (b === null) {
        b = 0;
    }
    if (typeof a !== 'number' || typeof b !== 'number') {
        return NaN;
    }
    if (isNaN(a) || isNaN(b)) {
        return NaN;
    }
    return a > b ? b : a;
}

module.exports = min;

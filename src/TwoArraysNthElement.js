/**
 * @param array1
 * @param array2
 * @param n
 */
function twoArraysNthElement(array1, array2, n) {
    const combinedArray = [];
    let firstIndex = 0;
    let secondIndex = 0;
    while (true) {
        if (firstIndex < array1.length && secondIndex < array2.length) {
            if (array1[firstIndex] > array2[secondIndex]) {
                combinedArray.push(array2[secondIndex++]);
            }
            else {
                combinedArray.push(array1[firstIndex++]);
            }
        }
        else if (firstIndex < array1.length) {
            combinedArray.push(array1[firstIndex++]);
        }
        else {
            combinedArray.push(array2[secondIndex++]);
        }
        if (combinedArray.length - 1 === n) {
            return combinedArray.pop();
        }
    }
}

module.exports = twoArraysNthElement;

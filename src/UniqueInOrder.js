/**
 * @param iterable
 */
function uniqueInOrder(iterable) {
    const unique = new Set();
    const newIterable = [];
    const tempIterable = typeof iterable === 'string' ? iterable.split('') : iterable;
    for (let idx = 0; idx < tempIterable.length; idx++) {
        const curr = tempIterable[idx];
        if (!unique.has(curr)) {
            unique.add(curr);
            newIterable.push(curr);
        }
        if (idx + 1 < tempIterable.length) {
            const next = tempIterable[idx + 1];
            if (curr !== next) {
                unique.delete(next);
            }
        }
    }
    return newIterable;
}

module.exports = uniqueInOrder;

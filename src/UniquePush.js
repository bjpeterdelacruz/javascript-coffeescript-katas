/**
 * @param arr
 * @param obj
 */
function uniquePush(arr, obj) {
    const phoneNumbers = new Set(arr.map((person) => person.phoneNumber));
    if (obj.phoneNumber && !phoneNumbers.has(obj.phoneNumber)) {
        arr.push(obj);
        return true;
    }
    return false;
}

module.exports = uniquePush;

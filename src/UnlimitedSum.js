/**
 * @param {...any} args
 */
function sum(...args) {
    return args.filter(arg => typeof arg === 'number' && arg.toString().match(/^-?[0-9]+$/))
        .reduce((a, b) => a + b, 0);
}

module.exports = sum;

/**
 * @param string
 */
function replaceCharacters(string) {
    const s = string.split(/[^a-zA-Z]+/).join('');
    if (s.indexOf('I') !== -1 && s.indexOf('J') !== -1) {
        if (s.indexOf('I') < s.indexOf('J')) {
            return s.split('J').join('');
        }
        else {
            return s.split('I').join('');
        }
    }
    return s;
}

/**
 * @param key
 */
function createMatrixAndCipherKey(key) {
    const matrix = [];
    let alpha = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    let uppercase = replaceCharacters(key.toUpperCase());
    if (uppercase.indexOf('J') !== -1) {
        alpha = alpha.replace('I', '');
    }
    else {
        alpha = alpha.replace('J', '');
    }
    uppercase += alpha;
    const arr = [];
    for (const character of uppercase) {
        if (arr.indexOf(character) === -1) {
            arr.push(character);
        }
    }
    const cipherKey = arr.join('');
    let count = 0;
    let row = [];
    for (const character of cipherKey) {
        row.push(character);
        count++;
        if (count % 5 === 0) {
            matrix.push(row);
            row = [];
            count = 0;
        }
    }
    return [matrix, cipherKey];
}

/**
 * @param string
 */
function insertX(string) {
    let temp = string;
    for (let current = 0, next = 1; next < temp.length; current++, next++) {
        if (temp[current] === temp[next]) {
            temp = temp.slice(0, next) + 'X' + temp.slice(next);
        }
    }
    while (temp.length % 2 !== 0) {
        temp += 'X';
    }
    return temp;
}

/**
 * @param matrix
 * @param letter
 */
function getRowColumn(matrix, letter) {
    for (let rowIdx = 0; rowIdx < matrix.length; rowIdx++) {
        const row = matrix[rowIdx];
        if (row.indexOf(letter) !== -1) {
            return [rowIdx, row.indexOf(letter)];
        }
    }
}

/**
 * @param matrix
 * @param pair
 */
function getRowColumnIndices(matrix, pair) {
    const firstLetter = getRowColumn(matrix, pair[0]);
    const secondLetter = getRowColumn(matrix, pair[1]);
    return [firstLetter[0], firstLetter[1], secondLetter[0], secondLetter[1]];
}

/**
 * @param matrix
 * @param pair
 */
function encodeCharacters(matrix, pair) {
    const indices = getRowColumnIndices(matrix, pair);
    // same row
    if (indices[0] === indices[2]) {
        return matrix[indices[0]][(indices[1] + 1) % matrix.length]
            + matrix[indices[2]][(indices[3] + 1) % matrix.length];
    }
    // same column
    if (indices[1] === indices[3]) {
        return matrix[(indices[0] + 1) % matrix.length][indices[1]]
            + matrix[(indices[2] + 1) % matrix.length][indices[3]];
    }
    return matrix[indices[0]][indices[3]] + matrix[indices[2]][indices[1]];
}

/**
 * @param s
 * @param key
 */
function encipher(s, key) {
    let arr = createMatrixAndCipherKey(key);
    const matrix = arr[0];
    const cipherKey = arr[1];

    const whitespaces = [];
    for (let idx = 0; idx < s.length; idx++) {
        if (s[idx].match(/\s/)) { // keep track of any kind of whitespace character
            whitespaces.push([idx, s[idx]]);
        }
    }

    let str = '';
    for (const character of s.toUpperCase()) {
        if (cipherKey.indexOf(character) !== -1) {
            str += character;
        }
        // "I" is not part of cipherkey, so replace it with "J" in string before encoding
        else if (character === 'I' && cipherKey.indexOf('I') === -1) {
            str += 'J';
        }
        else if (character === 'J' && cipherKey.indexOf('J') === -1) {
            str += 'I';
        }
    }

    const newString = insertX(str);

    const pairs = [];
    for (let idx = 1; idx < newString.length; idx++) {
        if (idx % 2 !== 0) {
            pairs.push(newString.slice(idx - 1, idx + 1));
        }
    }

    arr = [];
    for (const pair of pairs) {
        arr.push(encodeCharacters(matrix, pair));
    }

    let encodedString = arr.join('');
    for (const ws of whitespaces) {
        encodedString = encodedString.slice(0, ws[0]) + ws[1] + encodedString.slice(ws[0]);
    }

    return encodedString;
}

module.exports = encipher;

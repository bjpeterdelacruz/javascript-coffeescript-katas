/* eslint comma-spacing: "off", no-undef: "off", eqeqeq: "off" */

f=areCurlyBracesMatchedProperly=s => (t=s.replace(/{}/,''))==s?!s:f(t);

module.exports = f;

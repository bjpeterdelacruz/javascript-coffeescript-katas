function validParentheses(paren) {
    let openCount = 0;
    let closeCount = 0;
    for (let idx = 0; idx < paren.length; idx++) {
        if (paren.charAt(idx) === '(') {
            openCount++;
        }
        else if (paren.charAt(idx) === ')') {
            closeCount++;
            if (closeCount > openCount) {
                return false;
            }
        }
        else {
            return false;
        }
    }
    return openCount === closeCount;
}

module.exports = validParentheses;

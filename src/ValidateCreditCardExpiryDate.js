/**
 * @param date
 * @param currentDate
 */
function checkExpiryValid(date, currentDate) {
    let [month, year] = date.indexOf('/') !== -1 ? date.split('/') : date.split('-');
    month = parseInt(month.trim());
    year = year.trim();
    if (year.length === 2) {
        year = `20${year}`;
    }
    year = parseInt(year);
    if (year < currentDate.getFullYear()) {
        return false;
    }
    return year !== currentDate.getFullYear() || month >= currentDate.getMonth();
}

module.exports = checkExpiryValid;

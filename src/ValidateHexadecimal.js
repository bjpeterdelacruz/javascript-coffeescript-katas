/**
 * @param str
 */
function isHex(str) {
    if (str.length !== 3 && str.length !== 6) {
        return false;
    }
    const validHexChars = '0123456789ABCDEF';
    for (const character of str) {
        if (validHexChars.indexOf(character.toUpperCase()) === -1) {
            return false;
        }
    }
    return true;
}

module.exports = isHex;

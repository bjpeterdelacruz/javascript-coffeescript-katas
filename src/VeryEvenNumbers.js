/**
 * @param number
 */
function isVeryEvenNumber(number) {
    let sum = number;
    while (sum >= 10) {
        sum = sum.toString().split('').map(element => parseInt(element)).reduce((a, c) => a + c);
    }
    return sum % 2 === 0;
}

module.exports = isVeryEvenNumber;

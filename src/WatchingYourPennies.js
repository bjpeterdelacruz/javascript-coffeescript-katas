/**
 * @param cash
 * @param expenses
 * @param rate
 */
function manageMoney(cash, expenses, rate) {
    let balance = cash;
    if (balance < expenses) {
        return 'You ran out of money after 0 months';
    }
    const interest = 1 + (rate / 100);
    for (let month = 1; month <= 12; month++) {
        balance *= interest;
        balance -= expenses;
        if (balance < expenses && month < 12) {
            return `You ran out of money after ${month} months`;
        }
    }
    balance = Math.round(balance * 100) / 100;
    return `You still have $${balance.toFixed(2)}`;
}

module.exports = manageMoney;

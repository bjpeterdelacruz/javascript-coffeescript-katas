/**
 * @param weight
 */
function divide(weight) {
    return !(weight < 3 || weight % 2 === 1);
}

module.exports = divide;

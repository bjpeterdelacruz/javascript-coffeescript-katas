/**
 * @param input
 */
function getMilitaryTime(input) {
    const isMorning = input.includes('AM');
    const arr = input.replace(/AM|PM/, '').split(':');
    if (isMorning) {
        if (arr[0] === '12') {
            arr[0] = '00';
        }
    }
    else if (arr[0] !== '12') {
        arr[0] = (parseInt(arr[0]) + 12).toString();
    }
    return arr.join(':');
}

module.exports = getMilitaryTime;

const BARK  = 'woof woof';
const SLEEP = 'zzzzzzzzz....';

/**
 * @param bark
 */
function dogBarkByDefault(bark) {
    return bark === undefined ? BARK : SLEEP;
}

/**
 * @param bark
 */
function dogBarkOnlyIfToldSo(bark) {
    return bark ? BARK : SLEEP;
}

/**
 * @param dontBark
 */
function dogDontBarkByDefault(dontBark) {
    return dontBark === undefined ? SLEEP : BARK;
}

/**
 * @param dontBark
 */
function dogDontBarkOnlyIfToldSo(dontBark) {
    return dontBark ? SLEEP : BARK;
}

module.exports = { dogBarkByDefault, dogBarkOnlyIfToldSo,
    dogDontBarkByDefault, dogDontBarkOnlyIfToldSo, BARK, SLEEP };

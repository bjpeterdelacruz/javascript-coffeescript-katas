/**
 * @param expr
 */
function expressionOut(expr) {
    const numbers = new Map();
    numbers.set('1', 'One');
    numbers.set('2', 'Two');
    numbers.set('3', 'Three');
    numbers.set('4', 'Four');
    numbers.set('5', 'Five');
    numbers.set('6', 'Six');
    numbers.set('7', 'Seven');
    numbers.set('8', 'Eight');
    numbers.set('9', 'Nine');
    numbers.set('10', 'Ten');

    const operators = new Map();
    operators.set('+', 'Plus');
    operators.set('-', 'Minus');
    operators.set('*', 'Times');
    operators.set('/', 'Divided By');
    operators.set('**', 'To The Power Of');
    operators.set('=', 'Equals');
    operators.set('!=', 'Does Not Equal');

    const result = [];
    const arr = expr.split(' ');
    for (let idx = 0; idx < arr.length; idx++) {
        if (idx % 2 === 0) {
            result.push(numbers.get(arr[idx]));
        }
        else if (!operators.has(arr[idx])) {
            return 'That\'s not an operator!';
        }
        else {
            result.push(operators.get(arr[idx]));
        }
    }
    return result.join(' ');
}

module.exports = expressionOut;

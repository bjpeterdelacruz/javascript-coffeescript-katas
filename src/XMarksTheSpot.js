/**
 * @param input
 */
function xMarksTheSpot(input) {
    let x = [];
    for (let rowIdx = 0; rowIdx < input.length; rowIdx++) {
        const row = input[rowIdx];
        for (let columnIdx = 0; columnIdx < row.length; columnIdx++) {
            if (row[columnIdx] !== 'x') {
                continue;
            }
            if (x.length === 2) {
                return [];
            }
            x = [rowIdx, columnIdx];
        }
    }
    return x;
}

module.exports = xMarksTheSpot;

/**
 * @param a
 * @param x
 */
function check(a, x) {
    return new Set(a).has(x);
}

module.exports = check;

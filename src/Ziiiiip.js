/**
 * @param keys
 * @param values
 */
function zipObject(keys, values) {
    const obj = {};
    if (keys === undefined) {
        return obj;
    }
    if (values === undefined) {
        if (typeof keys[0] === 'string') {
            for (const key of keys) {
                obj[key] = undefined;
            }
            return obj;
        }
        for (const key of keys) {
            obj[key[0]] = key[1];
        }
        return obj;
    }
    let idx = 0;
    for (const key of keys) {
        obj[key] = values[idx++];
    }
    return obj;
}

module.exports = zipObject;

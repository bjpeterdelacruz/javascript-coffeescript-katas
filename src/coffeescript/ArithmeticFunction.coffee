# Make a Function that Does Arithmetic!
# Author: BJ Peter Dela Cruz

# This function does not use an if statement but rather a map that contains functions that will
# perform an artithmetic operation.
arithmetic = (a, b, operator) ->
  ops = new Map()
  ops.set("add", (a, b) => a + b)
  ops.set("subtract", (a, b) => a - b)
  ops.set("multiply", (a, b) => a * b)
  ops.set("divide", (a, b) => a / b)
  ops.get(operator)(a, b)

module.exports = arithmetic

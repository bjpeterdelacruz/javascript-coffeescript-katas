# Arrays Similar
# Author: BJ Peter Dela Cruz

# This function determines whether two arrays are similar, that is, they both contain the same
# elements and the same number of times the elements occur.
arraysSimilar = (arr1, arr2) ->
  if arr1.length isnt arr2.length
    return false
  map1 = new Map
  map2 = new Map
  for value, index in arr1
    if map1.get(value)
      map1.set(value, map1.get(value) + 1)
    else
      map1.set(value, 1)
    value2 = arr2[index]
    if map2.get(value2)
      map2.set(value2, map2.get(value2) + 1)
    else
      map2.set(value2, 1)
  keys = new Set(map1.keys())
  Array.from(map2.keys()).forEach((key) => keys.add(key))
  for key in Array.from(keys)
    if map1.get(key) isnt map2.get(key)
      return false
  return true

module.exports = arraysSimilar

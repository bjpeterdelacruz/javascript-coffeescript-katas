# Averages of Numbers
# Author: BJ Peter Dela Cruz

# This function returns an array of integers, each representing the average of an integer in the
# given array and its follower.
averages = (numbers) ->
  if numbers == null || numbers.length < 2
    return []
  results = []
  current = 1
  while current < numbers.length
    results.push (numbers[current - 1] + numbers[current]) / 2
    current++
  results

module.exports = averages

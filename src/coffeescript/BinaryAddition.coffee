# Binary Addition
# Author: BJ Peter Dela Cruz

# This function adds two numbers together and returns sum in binary representation.
addBinary = (a, b) ->
  (a + b).toString 2

module.exports = addBinary

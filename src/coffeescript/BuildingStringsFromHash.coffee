# Building Strings from a Hash
# Author: BJ Peter Dela Cruz

# This function builds a string representation of the given map.
solution = (pairs) ->
  (key + " = " + pairs[key] for key of pairs).join(",")

module.exports = solution

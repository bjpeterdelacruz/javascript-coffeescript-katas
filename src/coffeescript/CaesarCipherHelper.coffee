# Caesar Cipher Helper
# Author: BJ Peter Dela Cruz

# This class will encode and decode a string using a Caesar cipher. The string will be shifted
# forward or backward by the number of the spots that the cipher was initialized to. The letters
# in the encoded or decoded string will be in uppercase.
class CaesarCipher
  alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"

  constructor: (shift) ->
    @shift = shift

  process: (string, encode) ->
    number = if encode then @shift else -1 * @shift
    arr = []
    for character in string
      index = alphabet.indexOf character.toUpperCase()
      if index is -1
        arr.push character
      else
        index = (index + number) % alphabet.length
        if index < 0
          index += alphabet.length
        arr.push alphabet[index]
    arr.join("")

  encode: (string) ->
    @process(string, true)

  decode: (string) ->
    @process(string, false)

module.exports = CaesarCipher

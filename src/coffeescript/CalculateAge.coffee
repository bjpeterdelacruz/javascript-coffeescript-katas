# Calculate Julia's Age
# Author: BJ Peter Dela Cruz

# Julia is X times older than her brother, and she is also Y times as old as him.
# This function will calculate Julia's age given plus (X) and times (Y).
age = (plus, times) ->
  otherAge = plus / (times - 1)
  plus + otherAge

module.exports = age

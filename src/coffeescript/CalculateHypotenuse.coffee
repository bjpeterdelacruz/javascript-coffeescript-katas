# Calculate Hypotenuse
# Author: BJ Peter Dela Cruz

# This function calculates the hypotenuse of a right-angled triangle. The result will be
# rounded to three decimal places. If the inputs for the sides of the triangle are not
# numbers, an error will be thrown.
calculateHypotenuse = (a, b) ->
  if a is null or b is null
    throw 'Error'
  if a.toString().match(/[^0-9]/) or b.toString().match(/[^0-9]/)
    throw 'Error'
  c = Math.pow((a * a) + (b * b), 0.5)
  Math.round(c * 1000) / 1000

module.exports = calculateHypotenuse

# Capitalize the First Letter of a String
# Author: BJ Peter Dela Cruz

# Disables the toUpperCase method on the CodeWars server.
String.prototype.toUpperCase = () ->
  this

# Capitalizes the first letter of string if it is a lowercase letter. Otherwise, the string is
# returned unmodified.
String.prototype.capitalize = () ->
  code = this.charCodeAt 0
  if code < 97 or code > 122
    return this.substring 0
  String.fromCharCode(code - 32) + this.substring 1

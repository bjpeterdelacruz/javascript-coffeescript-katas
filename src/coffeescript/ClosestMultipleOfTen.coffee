# Closest Multiple of 10
# Author: BJ Peter Dela Cruz

# This function returns a number that is a multiple of 10 that is closest to the given number.
# For example, given 22 and 25, the function will return 20 and 30, respectively.
closestMultiple10 = (num) ->
  num = parseInt(num)
  str = num.toString()
  lastNum = parseInt(str[str.length - 1])
  if lastNum < 5 then num - lastNum else num + (10 - lastNum)

module.exports = closestMultiple10

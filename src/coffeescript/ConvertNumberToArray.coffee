digitize = (n) ->
  n.toString().split('').reverse().map((element) => parseInt(element))

module.exports = digitize

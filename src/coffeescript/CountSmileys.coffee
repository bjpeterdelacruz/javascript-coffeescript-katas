# Count Smileys
# Author: BJ Peter Dela Cruz

# This function counts the number of smileys in the given array of strings. Each smiley must
# contain a pair of eyes, either ':' or ';'. A smiley face may not have a nose, but if it does,
# it can either be '-' or '~'. Finally, every smiley face must have a smiling mouth, either ')'
# or 'D'. No other characters are allowed except the aforementioned ones.
countSmileys = (arr) ->
  totalSmileys = 0
  for smiley in arr
    if smiley.length > 3 || smiley.length == 0
      continue
    if smiley[0] != ":" && smiley[0] != ";"
      continue
    if smiley.length == 2
      if smiley[1] == ")" || smiley[1] == "D"
        totalSmileys++
    else if smiley[2] != ")" && smiley[2] != "D"
      continue
    else if smiley[1] == "-" || smiley[1] == "~"
      totalSmileys++
  totalSmileys

module.exports = countSmileys

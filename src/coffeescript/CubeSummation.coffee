# Cube Summation
# Author: BJ Peter Dela Cruz

# This function calculates the sum of a cube of numbers in a range that does not include the
# start of the range but does include the end of the range. If the two arguments are the same,
# then zero is returned. The first argument is not necessarily bigger than the second one.
cubeSum = (n, m) ->
  if n is m
    return 0
  smaller = if n > m then m + 1 else n + 1
  bigger = if n < m then m else n
  sum = 0
  while smaller <= bigger
    sum += (smaller * smaller * smaller)
    smaller++
  sum

module.exports = cubeSum

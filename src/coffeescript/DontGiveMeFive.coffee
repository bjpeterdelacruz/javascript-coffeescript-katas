# Don't Give Me Five
# Author: BJ Peter Dela Cruz

# This function counts the number of numbers that do not have 5 in them. The start and end ranges
# are both inclusive.
dontGiveMeFive = (start, end) ->
  (1 for num in [start..end] when num.toString().indexOf("5") is -1).length

module.exports = dontGiveMeFive

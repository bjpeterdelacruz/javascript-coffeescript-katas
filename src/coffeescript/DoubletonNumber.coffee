# Doubleton Number
# Author: BJ Peter Dela Cruz

# This function finds the next doubleton number, which is a number that contains exactly two
# distinct digits. For example, 23, 100, 121212 are doubleton numbers, whereas 123 and 9980
# are not.
doubleton = (num) -> 
  set = new Set()
  while set.size isnt 2
    set = new Set()
    num++
    for char in num.toString()
      set.add(char)
  num

module.exports = doubleton

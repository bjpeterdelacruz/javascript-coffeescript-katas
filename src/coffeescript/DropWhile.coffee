# The dropWhile Function
# Author: BJ Peter Dela Cruz

# This function applies the given predicate to each element in the given array. Once an element is
# reached such that the predicate is not true for that element, a subarray containing that element
# and all the elements after it is returned. The original array is not modified. If the predicate
# is true for all elements, an empty array is returned.
dropWhile = (arr, pred) ->
  idx = 0
  while idx < arr.length and pred(arr[idx])
    idx++
  newArr = []
  while idx < arr.length
    newArr.push arr[idx++]
  newArr

module.exports = dropWhile

# Duplicate Arguments
# Author: BJ Peter Dela Cruz

# This function returns true if the argument list contains duplicate values, false otherwise. A map
# is used to store arguments. If an argument is already present in the map, then the argument list
# contains duplicates.
solution = ->
  map = new Map
  for arg in arguments
    if map[arg]
      return true
    map[arg] = arg
  false

module.exports = solution

# Find the Next Perfect Square
# Author: BJ Peter Dela Cruz

# This function finds the next perfect square if the given number is already a perfect square.
# Otherwise, -1 is returned.
findNextSquare = (sq) ->
  result = parseInt(Math.pow(sq, 0.5).toString())
  if result * result is sq
    result++
    return result * result
  -1

module.exports = findNextSquare

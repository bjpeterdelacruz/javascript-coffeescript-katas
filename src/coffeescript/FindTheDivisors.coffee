divisors = (integer) ->
  results = []
  if integer is 1
    results.push 1
  for num in [2...integer]
    if integer % num is 0
      results.push num
  if results.length > 0 then results else integer + " is prime"

module.exports = divisors

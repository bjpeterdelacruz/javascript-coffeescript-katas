fizzBuzzCuckooClock = (time) ->
  clock = time.split ":"
  hour = clock[0]
  minutes = clock[1]
  if minutes is "30"
    return "Cuckoo"
  if minutes is "00"
    if hour is "00" or hour is "12"
      return "Cuckoo ".repeat(12).trim()
    hr = parseInt hour
    if hr > 12
      return "Cuckoo ".repeat(hr - 12).trim()
    return "Cuckoo ".repeat(hr).trim()
  min = parseInt minutes
  if min % 3 is 0 and min % 5 is 0
    return "Fizz Buzz"
  if min % 3 is 0
    return "Fizz"
  if min % 5 is 0
    return "Buzz"
  "tick"

module.exports = fizzBuzzCuckooClock

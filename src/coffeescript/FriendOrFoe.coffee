friend = (friends) ->
  friends.filter((person) => person.length is 4)

module.exports = friend

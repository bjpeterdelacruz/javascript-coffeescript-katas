class Dinglemouse
  constructor: (@firstName, @lastName) ->
  
  getFullName: () ->
    (@firstName.trim() + " " + @lastName.trim()).trim()

module.exports = Dinglemouse

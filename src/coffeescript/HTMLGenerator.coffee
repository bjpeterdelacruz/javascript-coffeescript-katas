class HTMLGen
  constructor: () ->

  addTag: (str, txt) ->
    "<#{str}>#{txt}</#{str}>"

  a: (txt) ->
    @addTag("a", txt)

  b: (txt) ->
    @addTag("b", txt)

  p: (txt) ->
    @addTag("p", txt)

  body: (txt) ->
    @addTag("body", txt)

  div: (txt) ->
    @addTag("div", txt)

  span: (txt) ->
    @addTag("span", txt)

  title: (txt) ->
    @addTag("title", txt)

  comment: (txt) ->
    "<!--#{txt}-->"

module.exports = HTMLGen

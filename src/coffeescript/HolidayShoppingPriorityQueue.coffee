class HolidayPriorityQueue
  constructor: () ->
    @sortedArray = []
  
HolidayPriorityQueue::addGift = (gift) ->
  @sortedArray.push [gift.priority, gift.gift]
  @sortedArray.sort((a, b) => a[0] - b[0])
  @sortedArray.length

HolidayPriorityQueue::buyGift = ->
  if @sortedArray.length is 0
    return ""
  gift = @sortedArray.shift()
  gift[1]

module.exports = HolidayPriorityQueue

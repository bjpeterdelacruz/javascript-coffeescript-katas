reindeers = (presents) ->
  if presents is 0
    return 2
  if presents < 0 or presents > 180
    throw 'Error'
  if presents <= 30 then 3
  else if presents <= 60 then 4
  else if presents <= 90 then 5
  else if presents <= 120 then 6
  else if presents <= 150 then 7
  else 8

module.exports = reindeers

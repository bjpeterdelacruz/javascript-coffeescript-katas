invert = (array) ->
  array.map((element) => element * -1)

module.exports = invert

hasOneChar = (s) ->
  new Set(s.split("")).size is 1

module.exports = hasOneChar

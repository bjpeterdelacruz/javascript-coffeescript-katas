isTriangle = (a, b, c) ->
   a isnt 0 and b isnt 0 and c isnt 0 and a + b > c and a + c > b and b + c > a

module.exports = isTriangle

areaLargestSquare = (r) -> (r * r) * 2

module.exports = areaLargestSquare

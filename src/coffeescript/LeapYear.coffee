isLeapYear = (year) ->
  if year % 400 is 0
    return true
  if year % 100 is 0
    return false
  year % 4 is 0

module.exports = isLeapYear

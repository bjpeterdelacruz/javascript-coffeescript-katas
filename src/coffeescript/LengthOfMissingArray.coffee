getLengthOfMissingArray = (arrayOfArrays) ->
  if arrayOfArrays is null or arrayOfArrays.length is 0
    return 0
  len = arrayOfArrays.filter((array) => array is null or array.length is 0).length
  if len > 0
    return 0
  array = arrayOfArrays.map((array) => array.length).sort((a, b) => a - b)
  set = new Set(array)
  num = array[0]
  while set.has(num)
    num++
  num;

module.exports = getLengthOfMissingArray

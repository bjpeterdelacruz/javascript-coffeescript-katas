filter_list = (list) ->
  (elem for elem in list when typeof elem isnt "string")

module.exports = filter_list

magicalWell = ($a, $b, $n) ->
  total = 0
  while $n-- > 0
    total += ($a * $b)
    $a++
    $b++
  total

module.exports = magicalWell

makeAcronym = (string) ->
  if typeof string isnt "string"
    return "Not a string"
  if string is ""
    return ""
  if !string.match /^[A-Za-z\s]+$/
    return "Not letters"
  abbr = ""
  for words in string.split(/\s+/)
    abbr += words[0].toUpperCase()
  abbr

module.exports = makeAcronym

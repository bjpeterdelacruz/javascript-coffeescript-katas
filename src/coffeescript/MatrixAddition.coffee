matrixAddition = (a, b) ->
  results = []
  for row in a
    results.push row
  rowIdx = 0
  for row in a
    colIdx = 0
    for col in row
      results[rowIdx][colIdx] = col + b[rowIdx][colIdx]
      colIdx++
    rowIdx++
  results

module.exports = matrixAddition

maxGap = (nums) ->
  nums.sort((a, b) => a - b)
  maximum = 0
  idx = 1
  while idx < nums.length
    current = Math.abs(nums[idx - 1] - nums[idx])
    if current > maximum
      maximum = current
    idx++
  maximum

module.exports = maxGap

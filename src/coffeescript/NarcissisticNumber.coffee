narcissistic = (value) ->
  str = value.toString()
  sum = 0
  for number in str
    sum += Math.pow(parseInt(number), str.length)
  sum == value

module.exports = narcissistic

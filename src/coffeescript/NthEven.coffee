nthEven = (n) ->
  (n * 2) - 2

module.exports = nthEven

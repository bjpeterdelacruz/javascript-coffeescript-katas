root = (x, n) ->
  Math.pow(x, 1 / n)

module.exports = root

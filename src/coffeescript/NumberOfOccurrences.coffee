Array.prototype.numberOfOccurrences = (nr) ->
  count = 0
  for element in this
    if element is nr
      count++
  count

module.exports = Array.prototype.numberOfOccurrences

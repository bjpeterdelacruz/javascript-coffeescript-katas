# Convert Number to Sequence of Bits
# Author: BJ Peter Dela Cruz

# This function converts a number to a bit string. If the minimum length of the string is not
# provided, the minimum length of 8 will be used. Zeros will be padded to the left side of the
# string up to the minimum length.
Number.prototype.toBits = (length) ->
  min = 8
  if length isnt undefined
    min = length
  bits = this.toString 2
  while bits.length < min
    bits = "0" + bits
  bits

module.exports = Number.prototype.toBits

onlyOne = () ->
  map = new Map
  for argument in arguments
    if argument
      if map["true"]
        return false
      map["true"] = true
  map["true"] is true

module.exports = onlyOne

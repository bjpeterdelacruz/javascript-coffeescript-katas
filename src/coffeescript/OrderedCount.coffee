orderedCount = (text) ->
  map = new Map
  str = ""
  for character in text
    if str.indexOf(character) is -1
      str += character
    if map[character]
      map[character]++
    else
      map[character] = 1
  arr = []
  for character in str
    arr.push [character, map[character]]
  arr

module.exports = orderedCount

getMissingIngredients = (recipe, added) ->
  if Object.keys(added).length is 0
    return recipe
  maximum = 0
  missing = {}
  for k,v of recipe
    if added[k]
      amount = v
      count = 1
      while added[k] > amount
        amount += v
        count++
      if count > maximum
        maximum = count
  for k,v of recipe
    amount = v * maximum
    if amount isnt added[k]
      if added[k]
        missing[k] = amount - added[k]
      else
        missing[k] = amount
  missing

module.exports = getMissingIngredients

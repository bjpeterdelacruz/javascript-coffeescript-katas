pluck = (objs, name) ->
  result = []
  for object in objs
    entries = Object.entries object
    found = false
    for entry in entries
      if entry[0] is name
        result.push entry[1]
        found = true
    if !found
      result.push undefined
  result

module.exports = pluck

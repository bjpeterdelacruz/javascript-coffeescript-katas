buildRowText = (index, character) ->
  arr = [" ", " ", " ", " ", " ", " ", " ", " ", " "]
  arr[index] = character
  "|" + arr.join("|") + "|"

module.exports = buildRowText

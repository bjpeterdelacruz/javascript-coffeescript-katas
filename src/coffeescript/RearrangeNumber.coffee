maxRedigit = (num) ->
  if num < 100 || num > 999
    return null
  arr = (parseInt char for char in num.toString())
  parseInt arr.sort((a, b) -> b - a).join("")

module.exports = maxRedigit

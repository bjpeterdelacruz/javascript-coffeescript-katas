removeUrlAnchor = (url) ->
  if url.indexOf("#") is -1
    return url
  url.substring(0, url.indexOf("#"))

module.exports = removeUrlAnchor

removeDuplicateWords = (words) ->
  arr = []
  set = new Set()
  for word in words.split(" ")
    if !set.has word
      set.add word
      arr.push word
  arr.join(" ")

module.exports = removeDuplicateWords

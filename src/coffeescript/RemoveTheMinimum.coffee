removeSmallest = (numbers) ->
  arr = []
  min = Infinity
  minIndex = 0
  for num, index in numbers
    if num < min
      min = num
      minIndex = index
    arr.push num
  arr.splice(minIndex, 1)
  arr

module.exports = removeSmallest

nextHappyYear = (year) ->
  nextYear = year + 1
  while new Set(nextYear.toString().split("")).size isnt 4
    nextYear++
  nextYear

module.exports = nextHappyYear

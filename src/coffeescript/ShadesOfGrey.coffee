shadesOfGrey = (n) ->
  if n <= 0
    return []
  max = n
  if n > 254
    max = 254
  colors = []
  number = 1
  while number <= max
    color = number.toString(16)
    if color.length < 2
      color = "0" + color
    colors.push("#" + color.repeat(3))
    number++
  colors

module.exports = shadesOfGrey

class Person

  constructor: (name) ->
    @name = name

  greet: (anotherPerson) ->
    "Hello #{anotherPerson}, my name is #{@name}"

class RudePerson extends Person

  constructor: (name) ->
    super(name)

  greet: (anotherPerson) ->
    "I'm #{@name}, now go away #{anotherPerson}"

module.exports = { Person, RudePerson }

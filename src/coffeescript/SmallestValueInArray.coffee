min = (arr, toReturn) ->
  smallest_idx = 0
  smallest_value = Infinity
  for value, idx in arr
    if value < smallest_value
      smallest_idx = idx
      smallest_value = value
  if toReturn is "value" then smallest_value else smallest_idx

module.exports = min

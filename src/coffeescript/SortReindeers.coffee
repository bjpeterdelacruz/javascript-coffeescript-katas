sortReindeer = (reindeerNames) ->
  names = new Map()
  for reindeer in reindeerNames
    arr = reindeer.split(" ")
    if names.has arr[1]
      names.get(arr[1]).push(reindeer)
    else
      names.set(arr[1], [reindeer])
  keys = Array.from names.keys()
  keys.sort()
  arr = []
  for name in keys
    reindeers = names.get(name)
    for reindeer in reindeers
      arr.push(reindeer)
  arr

module.exports = sortReindeer

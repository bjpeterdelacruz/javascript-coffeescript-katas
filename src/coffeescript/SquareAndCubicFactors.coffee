factors = (n) ->
  squares = []
  cubics = []
  count = 1
  while ++count <= n
    if n % (count * count) is 0
      squares.push count
    if n % (count * count * count) is 0
      cubics.push count
  [squares, cubics]

module.exports = factors

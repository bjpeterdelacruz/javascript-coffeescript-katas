sumMix = (x) ->  
  x.map((element) => parseInt(element)).reduce((acc, curr) => acc + curr)

module.exports = sumMix

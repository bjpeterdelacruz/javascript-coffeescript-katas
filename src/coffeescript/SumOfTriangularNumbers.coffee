sumTriangularNumbers = (n) ->
  if n <= 0
    return 0
  count = 1
  sum = 0
  arr = []
  while count <= n
    sum += count++
    arr.push sum
  arr.reduce((acc, curr) => acc + curr)

module.exports = sumTriangularNumbers

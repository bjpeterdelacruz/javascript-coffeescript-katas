takeWhile = (arr, pred) ->
  idx = 0
  newArr = []
  while idx < arr.length and pred(arr[idx])
    newArr.push arr[idx++]
  newArr

module.exports = takeWhile

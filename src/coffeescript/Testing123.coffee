number = (lines) ->
  arr = []
  lineNumber = 1
  for line in lines
    arr.push(lineNumber++ + ": " + line)
  arr

module.exports = number

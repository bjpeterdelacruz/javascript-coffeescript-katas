# The Old Switcheroo 2
# Author: BJ Peter Dela Cruz

# This function replaces all alphabetic characters with their position number in the alphabet, e.g.
# a = 1, b = 2, c = 3, etc. If a character in the string is a number or symbol, it is simply left
# in place in the string.
encode = (str) ->
  arr = str.split ''
  result = []
  for char in arr
    code = char.charCodeAt 0
    if code >= 65 and code <= 90
      result.push(code - 64)
    else if code >= 97 and code <= 122
      result.push(code - 96)
    else
      result.push(char)
  result.join ''

module.exports = encode

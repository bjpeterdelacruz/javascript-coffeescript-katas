getOxfordSentence = (items) ->
  if items is null or items.length is 0
    return ""
  if items.length is 1
    return items[0]
  if items.length is 2
    return items[0] + " and " + items[1]
  results = items.join(", ")
  newString = results.substring(0, results.lastIndexOf(", ") + 1) + " and"
  newString += results.substring(results.lastIndexOf(",") + 1)
  newString

module.exports = getOxfordSentence

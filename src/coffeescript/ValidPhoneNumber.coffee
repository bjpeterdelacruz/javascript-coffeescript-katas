validPhoneNumber = (phoneNumber) ->
  phoneNumber.match(/^\(\d\d\d\) \d\d\d-\d\d\d\d$/g) isnt null

module.exports = validPhoneNumber

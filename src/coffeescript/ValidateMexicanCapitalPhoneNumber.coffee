isValidMXPhoneNumber = (str) ->
  str.match(/^(\((56|55)\)|(55|56))\s?\d\d\s?\d\d\s?\d\d$/g) isnt null

module.exports = isValidMXPhoneNumber

class Vector
  constructor: (@components) ->

  performOperation: (components, operation) ->
    if components.length isnt @components.length
      throw 'Error'
    arr = []
    index = 0
    sum = 0
    for value in @components
      if operation is "add"
        arr.push(value + components[index++])
      else if operation is "sub"
        arr.push(value - components[index++])
      else if operation is "pow"
        sum += value * components[index++]
      else
        sum += value * value
    if operation is "pow"
      return sum
    if operation is "norm"
      return Math.pow(sum, 0.5)
    new Vector(arr)

  add: (vector) ->
    @performOperation(vector.components, "add")

  subtract: (vector) ->
    @performOperation(vector.components, "sub")

  dot: (vector) ->
    @performOperation(vector.components, "pow")

  norm: () ->
    @performOperation(@components, "norm")

  toString: () ->
    "(" + @components.join(",") + ")"

  equals: (vector) ->
    idx = 0
    if !(vector instanceof Vector) or vector.components.length isnt @components.length
      return false
    for value in @components
      if value isnt vector.components[idx++]
        return false
    return true

module.exports = Vector

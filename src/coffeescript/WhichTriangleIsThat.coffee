typeOfTriangle = (sideA, sideB, sideC) ->
  a = parseFloat sideA
  b = parseFloat sideB
  c = parseFloat sideC
  if isNaN(a) or isNaN(b) or isNaN(c)
    'Not a valid triangle';
  else if a + b <= c or a + c <= b or b + c <= a
    'Not a valid triangle'
  else if a is b and b is c
    'Equilateral'
  else if a is b or b is c or a is c
    'Isosceles'
  else 'Scalene'

module.exports = typeOfTriangle

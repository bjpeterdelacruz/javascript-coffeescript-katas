const { comparator, shuffle } = require('../src/12DaysOfChristmas');

describe('12 Days of Christmas Tests', () => {
    const doTest = function(lines) {
        const shuffled = shuffle(lines);
        const actual = shuffled.sort(comparator);
        const expected = lines;
        expect(actual).toEqual(expected);
    };

    it('verse1', () => {
        const lines = [
            'On the 1st day of Christmas my true love gave to me',
            'a partridge in a pear tree.'
        ];
        doTest(lines);
    });

    it('verse2', () => {
        const lines = [
            'On the 2nd day of Christmas my true love gave to me',
            '2 turtle doves and',
            'a partridge in a pear tree.'
        ];
        doTest(lines);
    });

    it('verse3', () => {
        const lines = [
            'On the 3rd day of Christmas my true love gave to me',
            '3 French hens,',
            '2 turtle doves and',
            'a partridge in a pear tree.'
        ];
        doTest(lines);
    });

    it('verse4', () => {
        const lines = [
            'On the 4th day of Christmas my true love gave to me',
            '4 calling birds,',
            '3 French hens,',
            '2 turtle doves and',
            'a partridge in a pear tree.'
        ];
        doTest(lines);
    });

    it('verse5', () => {
        const lines = [
            'On the 5th day of Christmas my true love gave to me',
            '5 golden rings,',
            '4 calling birds,',
            '3 French hens,',
            '2 turtle doves and',
            'a partridge in a pear tree.'
        ];
        doTest(lines);
    });

    it('verse6', () => {
        const lines = [
            'On the 6th day of Christmas my true love gave to me',
            '6 geese a laying,',
            '5 golden rings,',
            '4 calling birds,',
            '3 French hens,',
            '2 turtle doves and',
            'a partridge in a pear tree.'
        ];
        doTest(lines);
    });

    it('verse7', () => {
        const lines = [
            'On the 7th day of Christmas my true love gave to me',
            '7 swans a swimming,',
            '6 geese a laying,',
            '5 golden rings,',
            '4 calling birds,',
            '3 French hens,',
            '2 turtle doves and',
            'a partridge in a pear tree.'
        ];
        doTest(lines);
    });

    it('verse8', () => {
        const lines = [
            'On the 8th day of Christmas my true love gave to me',
            '8 maids a milking,',
            '7 swans a swimming,',
            '6 geese a laying,',
            '5 golden rings,',
            '4 calling birds,',
            '3 French hens,',
            '2 turtle doves and',
            'a partridge in a pear tree.'
        ];
        doTest(lines);
    });

    it('verse9', () => {
        const lines = [
            'On the 9th day of Christmas my true love gave to me',
            '9 ladies dancing,',
            '8 maids a milking,',
            '7 swans a swimming,',
            '6 geese a laying,',
            '5 golden rings,',
            '4 calling birds,',
            '3 French hens,',
            '2 turtle doves and',
            'a partridge in a pear tree.'
        ];
        doTest(lines);
    });

    it('verse10', () => {
        const lines = [
            'On the 10th day of Christmas my true love gave to me',
            '10 lords a leaping,',
            '9 ladies dancing,',
            '8 maids a milking,',
            '7 swans a swimming,',
            '6 geese a laying,',
            '5 golden rings,',
            '4 calling birds,',
            '3 French hens,',
            '2 turtle doves and',
            'a partridge in a pear tree.'
        ];
        doTest(lines);
    });

    it('verse11', () => {
        const lines = [
            'On the 11th day of Christmas my true love gave to me',
            '11 pipers piping,',
            '10 lords a leaping,',
            '9 ladies dancing,',
            '8 maids a milking,',
            '7 swans a swimming,',
            '6 geese a laying,',
            '5 golden rings,',
            '4 calling birds,',
            '3 French hens,',
            '2 turtle doves and',
            'a partridge in a pear tree.'
        ];
        doTest(lines);
    });

    it('verse12', () => {
        const lines = [
            'On the 12th day of Christmas my true love gave to me',
            '12 drummers drumming,',
            '11 pipers piping,',
            '10 lords a leaping,',
            '9 ladies dancing,',
            '8 maids a milking,',
            '7 swans a swimming,',
            '6 geese a laying,',
            '5 golden rings,',
            '4 calling birds,',
            '3 French hens,',
            '2 turtle doves and',
            'a partridge in a pear tree.'
        ];
        doTest(lines);
    });
});

const solution = require('../src/1RMCalculator');

describe('1RM Calculator Tests', () => {
    it('Equal Tests', () => {
        expect(solution(135, 20)).toEqual(282);
        expect(solution(200, 8)).toEqual(253);
        expect(solution(270, 2)).toEqual(289);
        expect(solution(360, 1)).toEqual(360);
        expect(solution(400, 0)).toEqual(0);
    });
});

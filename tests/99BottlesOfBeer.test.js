const solution = require('../src/99BottlesOfBeer');

describe('99 Bottles of Beer Tests', () => {
    it('Equal Tests', () => {
        const array = solution();
        expect(array[0]).toEqual('99 bottles of beer on the wall, 99 bottles of beer.');
        expect(array[2]).toEqual('98 bottles of beer on the wall, 98 bottles of beer.');
        expect(array[array.length - 2]).toEqual('No more bottles of beer on the wall, no more bottles of beer.');
        expect(array[array.length - 1]).toEqual('Go to the store and buy some more, 99 bottles of beer on the wall.');
    });
});

const solution = require('../src/ARuleOfDivisibilityBy13');

describe('A Rule of Divisibility by 13 Tests', () => {
    it('Equal Test', () => {
        expect(solution(8529)).toEqual(79);
        expect(solution(85299258)).toEqual(31);
        expect(solution(5634)).toEqual(57);
        expect(solution(1111111111)).toEqual(71);
        expect(solution(987654321)).toEqual(30);
    });
});

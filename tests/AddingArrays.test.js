const solution = require('../src/AddingArrays');

describe('Adding Arrays Tests', () => {
    it('Equal Test', () => {
        expect(solution([
            ['J', 'L', 'L', 'M'],
            ['u', 'i', 'i', 'a'],
            ['s', 'v', 'f', 'n'],
            ['t', 'e', 'e', '']])).toEqual('Just Live Life Man');
        expect(solution([
            ['I'],
            ['', 'L'],
            ['', 'o', 'Y'],
            ['', 'v', 'o'],
            ['', 'e', 'u']])).toEqual('I Love You');
        expect(solution([
            [ 'T', 'M', 'i', 't', 'p', 'o', 't', 'c' ],
            [ 'h', 'i', 's', 'h', 'o', 'f', 'h', 'e' ],
            [ 'e', 't', '', 'e', 'w', '', 'e', 'l' ],
            [ '', 'o', '', '', 'e', '', '', 'l' ],
            [ '', 'c', '', '', 'r', '', '', '' ],
            [ '', 'h', '', '', 'h', '', '', '' ],
            [ '', 'o', '', '', 'o', '', '', '' ],
            [ '', 'n', '', '', 'u', '', '', '' ],
            [ '', 'd', '', '', 's', '', '', '' ],
            [ '', 'r', '', '', 'e', '', '', '' ],
            [ '', 'i', '', '', '', '', '', '' ],
            [ '', 'a', '', '', '', '', '', '' ] ])).toEqual('The Mitochondria is the powerhouse of the cell');
    });
});

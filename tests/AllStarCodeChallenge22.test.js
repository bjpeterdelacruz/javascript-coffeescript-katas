const solution = require('../src/AllStarCodeChallenge22');

describe('All Star Code Challenge #22 Tests', () => {
    it('Equal Test', () => {
        expect(solution(3600)).toEqual('1 hour(s) and 0 minute(s)');
        expect(solution(3601)).toEqual('1 hour(s) and 0 minute(s)');
        expect(solution(3500)).toEqual('0 hour(s) and 58 minute(s)');
        expect(solution(323500)).toEqual('89 hour(s) and 51 minute(s)');
        expect(solution(0)).toEqual('0 hour(s) and 0 minute(s)');
    });
});

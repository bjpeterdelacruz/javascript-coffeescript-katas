const solution = require('../src/AllStarCodeChallenge28');

describe('All Star Code Challenge #28 Tests', () => {
    it('Equal Test', () => {
        expect(solution(60, 'f')).toEqual(140);
        expect(solution(32, 'c')).toEqual(0);
        expect(solution(50)).toEqual(10);
        expect(() => solution(100, 'w')).toThrow();
    });
});

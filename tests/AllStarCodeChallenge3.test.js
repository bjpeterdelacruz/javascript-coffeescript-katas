const solution = require('../src/AllStarCodeChallenge3');

describe('All Star Code Challenge #3 Tests', () => {
    it('Equal Test', () => {
        expect(solution('drake')).toEqual('drk');
        expect(solution('matcha udon')).toEqual('mtch dn');
        expect(solution('aeiou')).toEqual('');
    });
});

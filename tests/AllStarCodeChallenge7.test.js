const solution = require('../src/AllStarCodeChallenge7');

describe('All Star Code Challenge #7 Tests', () => {
    it('Equal Test', () => {
        expect(solution(5)).toEqual(1 + 1 / Math.pow(3, 2) + 1 / Math.pow(5, 2));
        expect(solution(9)).toEqual(1 + 1 / Math.pow(3, 2) + 1 / Math.pow(5, 2) + 1 / Math.pow(7, 2) + 1 / Math.pow(9, 2));
        expect(solution(-1)).toEqual(0);
        expect(solution(8)).toEqual(0);
    });
});

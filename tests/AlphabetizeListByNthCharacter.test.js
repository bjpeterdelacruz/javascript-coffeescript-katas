const solution = require('../src/AlphabetizeListByNthCharacter');

describe('Alphabetize List By Nth Character Tests', () => {
    it('Equal Test', () => {
        const arr = [
            ['bat, bell, bill, ball', 2, 'ball, bat, bell, bill', 'Sort by the second letter'],
            ['he, hi, ha, ho', 2, 'ha, he, hi, ho', 'Sort by the second letter'],
            ['words, wordz, wordy, wording', 5, 'wording, words, wordy, wordz', 'Sort by the fifth letter'],
            ['zephyr, yellow, wax, a, ba, cat', 1, 'a, ba, cat, wax, yellow, zephyr', 'Sort by the first letter'],
            ['hello, how, are, you, doing, today', 3, 'today, are, doing, hello, you, how', 'Sort by the third letter']
        ];
        for (const element of arr) {
            expect(solution(element[0], element[1])).toEqual(element[2]);
        }
    });
});

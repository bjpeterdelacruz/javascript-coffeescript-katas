const solution = require('../src/ArbitraryWordWrapping');

describe('Arbitrary Word Wrapping Tests', () => {
    it('Equal Test', () => {
        let input = 'The quick brown fox jumped over the lazy developer.';
        let expectedResult ='The quick brown fox jump-\ned over the lazy develop-\ner.';
        expect(solution(input)).toEqual(expectedResult);

        input = 'abc def ghi jkl mn hello    world';
        expectedResult ='abc def ghi jkl mn hello\n   world';
        expect(solution(input)).toEqual(expectedResult);

        input = 'abc def ghi jkl mn hello';
        expectedResult ='abc def ghi jkl mn hello';
        expect(solution(input)).toEqual(expectedResult);

        input = '1234567890123456789012345 1234567890';
        expectedResult ='1234567890123456789012345\n 1234567890';
        expect(solution(input)).toEqual(expectedResult);

        input = 'Lorem ipsum dolor sit.\nConsectetur cras amet.';
        expectedResult = 'Lorem ipsum dolor sit.\nConsectetur cras amet.';
        expect(solution(input)).toEqual(expectedResult);
    });
});

const solution = require('../src/AreTheySquare');

describe('Are They Square Tests', () => {
    it('Equal Test', () => {
        expect(solution([])).toEqual(undefined);
        expect(solution([1, 4, 9, 16, 25])).toEqual(true);
        expect(solution([1, 2, 9, 16, 25])).toEqual(false);
    });
});

require('../src/ArrayArithmetic');

describe('Array Arithmetic Tests', () => {
    it('Equal Test', () => {
        expect([1, 2, 3] + 4).toEqual(10);
        expect([1, 1, 1] - 3).toEqual(0);
        expect(+[10, -10, 10]).toEqual(10);
        expect(+[]).toEqual(0);
        expect(['a', 'b'] + ['c', 'd']).toEqual('abcd');
        expect('' + ['c', 'o', 'd', 'e', 'w', 'a', 'r', 's']).toEqual('codewars');
    });
});

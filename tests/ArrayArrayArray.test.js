const solution = require('../src/ArrayArrayArray');

describe('Array Array Array Tests', () => {
    it('Equal Test', () => {
        expect(solution([9, 3])).toEqual([[9, 3], [9, 3], [9, 3], [9, 3], [9, 3], [9, 3], [9, 3], [9, 3], [9, 3], [9, 3], [9, 3], [9, 3]]);
        expect(solution(['a', 3])).toEqual([['a', 3], ['a', 3], ['a', 3]] );
        expect(solution([6, 'c'])).toEqual([[6, 'c'], [6, 'c'], [6, 'c'], [6, 'c'], [6, 'c'], [6, 'c']]);
        expect(solution(['a', 'b'])).toEqual('Void!');
        expect(solution([1, 0])).toEqual([[1, 0]]);
    });
});

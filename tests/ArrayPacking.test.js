const solution = require('../src/ArrayPacking');

describe('Array Packing Tests', () => {
    it('Equal Test', () => {
        expect(solution([24, 85, 0])).toEqual(21784);
        expect(solution([23, 45, 39])).toEqual(2567447);

        expect(solution([0, 0, 1])).toEqual(parseInt('10000000000000000', 2));
        expect(solution([0, 1, 0])).toEqual(parseInt('100000000', 2));
        expect(solution([1, 0, 0])).toEqual(1);
    });
});

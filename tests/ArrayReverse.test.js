require('../src/ArrayReverse');

describe('Array Reverse Tests', () => {
    it('Equal Test', () => {
        expect([4, 3, 2, 1].reverse()).toEqual([1, 2, 3, 4]);
        expect([3, 2, 1].reverse()).toEqual([1, 2, 3]);
        expect([2, 1].reverse()).toEqual([1, 2]);
        expect([1].reverse()).toEqual([1]);
        expect([].reverse()).toEqual([]);
    });
});

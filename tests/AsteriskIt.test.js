const solution = require('../src/AsteriskIt');

describe('Asterisk It Tests', () => {
    it('Equal Test', () => {
        expect(solution(5312708)).toEqual('531270*8');
        expect(solution(9682135)).toEqual('96*8*2135');
        expect(solution(2222)).toEqual('2*2*2*2');
        expect(solution(1111)).toEqual('1111');
        expect(solution(9999)).toEqual('9999');
        expect(solution('0000')).toEqual('0*0*0*0');
        expect(solution(8)).toEqual('8');
        expect(solution(2)).toEqual('2');
        expect(solution(0)).toEqual('0');
        expect(solution([1, 4, 64, 68, 67, 23, 1])).toEqual('14*6*4*6*8*67231');
    });
});

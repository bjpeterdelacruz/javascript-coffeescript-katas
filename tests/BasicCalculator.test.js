const solution = require('../src/BasicCalculator');

describe('Basic Calculator Tests', () => {
    it('Equal Test', () => {
        expect(solution(1, '+', 2)).toEqual(3);
        expect(solution(3, '-', 2)).toEqual(1);
        expect(solution(6, '*', 6)).toEqual(36);
        expect(solution(5, '/', 2)).toEqual(2.5);
        expect(solution(5, '/', 0)).toEqual(null);
        expect(solution(5, '**', 3)).toEqual(null);
    });
});

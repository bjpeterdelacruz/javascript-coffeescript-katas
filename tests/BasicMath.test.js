const solution = require('../src/BasicMath');

describe('Basic Math Tests', () => {
    it('Equal Test', () => {
        expect(solution('1plus2plus3plus4')).toEqual('10');
        expect(solution('1minus2minus3minus4')).toEqual('-8');
        expect(solution('1plus2plus3minus4')).toEqual('2');
        expect(solution('1minus2plus3minus4')).toEqual('-2');
        expect(solution('1plus2minus3plus4minus5')).toEqual('-1');

        expect(solution('100plus25minus30minus5minus45')).toEqual('45');
    });
});

const solution = require('../src/BattleOfTheCharacters');

describe('Battle of the Characters Tests', () => {
    it('Equal Test', () => {
        expect(solution('One', 'Two')).toEqual('Two');
        expect(solution('One', 'Neo')).toEqual('One');
        expect(solution('One', 'neO')).toEqual('Tie!');
        expect(solution('Foo', 'BAR')).toEqual('Tie!');
        expect(solution('Four', 'Five')).toEqual('Four');

        expect(solution('QWERTY', 'ASDFGH')).toEqual('QWERTY');
        expect(solution('QAZWSX', 'VFREDC')).toEqual('QAZWSX');
        expect(solution('SKDUFR', 'QWEVPD')).toEqual('QWEVPD');
        expect(solution('PLOKIJ', 'ZNMASK')).toEqual('ZNMASK');
        expect(solution('FREDAS', 'TSOBES')).toEqual('TSOBES');
    });
});

const solution = require('../src/BinToHex');

/**
 * @param min
 * @param max
 */
function randomIntFromInterval(min, max) { // min and max included
    return Math.floor(Math.random() * (max - min + 1) + min);
}

describe('Bin to Hex and Back Tests', () => {
    it('Bin to Hex Test', () => {
        for (let idx = 0; idx < 5_000; idx++) {
            const rand = randomIntFromInterval(1, 10_000_000_000);
            expect(solution.binToHex(rand.toString(2))).toEqual(rand.toString(16));
            expect(solution.hexToBin(rand.toString(16))).toEqual(rand.toString(2));
        }
    });
});

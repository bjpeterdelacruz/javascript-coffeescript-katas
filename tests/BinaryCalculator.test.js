const { calculate, binStringToInt, intToBinString } = require('../src/BinaryCalculator');

describe('Binary Calculator Tests', () => {
    it('Equal Test', () => {
        expect(calculate('1', '1', 'add')).toEqual('10');
        expect(calculate('1', '1', 'multiply')).toEqual('1');
        expect(calculate('10', '10', 'multiply')).toEqual('100');
        expect(calculate('100', '10', 'subtract')).toEqual('10');

        expect(calculate('10', '-100', 'add')).toEqual('-10');
        expect(calculate('100', '-10', 'subtract')).toEqual('110');

        expect(binStringToInt('1001')).toEqual(9);
        expect(binStringToInt('-1011')).toEqual(-11);

        expect(intToBinString(-9)).toEqual('-1001');
        expect(intToBinString(12)).toEqual('1100');
    });
});

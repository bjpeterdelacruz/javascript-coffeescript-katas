const solution = require('../src/BinaryString');

describe('Binary String Tests', () => {
    it('Equal Test', () => {
        for (let number = 0; number < 7777; number++) {
            expect(parseInt(solution(number), 2)).toEqual(number);
        }
    });
});

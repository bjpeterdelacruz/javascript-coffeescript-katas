const solution = require('../src/BinaryZoo');

describe('Binary Zoo Tests', () => {
    it('Equal Test', () => {
        expect(solution({ giraffe: '1010', zebra: '100' })).toEqual(14);
        expect(solution({})).toEqual(0);
    });
});

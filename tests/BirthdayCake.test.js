const solution = require('../src/BirthdayCake');

describe('Birthday Cake Tests', () => {
    it('Equal Test', () => {
        expect(solution(900, 'abcdef')).toEqual('That was close!');
        expect(solution(56, 'ifkhchlhfd')).toEqual('Fire!');
        expect(solution(256, 'aaaaaddddr')).toEqual('Fire!');
        expect(solution(333, 'jfmgklfhglbe')).toEqual('Fire!');
        expect(solution(12, 'jaam')).toEqual('Fire!');
    });
});

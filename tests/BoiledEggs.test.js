const solution = require('../src/BoiledEggs');

describe('Boiled Eggs Tests', () => {
    it('Equal Test', () => {
        expect(solution(0)).toEqual(0);
        expect(solution(4)).toEqual(5);
        expect(solution(8)).toEqual(5);
        expect(solution(9)).toEqual(10);
        expect(solution(15)).toEqual(10);
        expect(solution(16)).toEqual(10);
        expect(solution(17)).toEqual(15);
    });
});

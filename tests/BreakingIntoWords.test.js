const solution = require('../src/BreakingIntoWords');

const dictWords = [
    'american', 'four', 'i', 'john', 'may', 'mr', 'mrs', 'us', 'united', 'a',
    'about', 'act', 'after', 'again', 'against', 'all', 'almost', 'along', 'also', 'always',
    'am', 'among', 'an', 'and', 'another', 'any', 'are', 'around', 'as', 'asked',
    'at', 'away', 'back', 'be', 'because', 'become', 'been', 'before', 'began', 'best',
    'better', 'between', 'big', 'both', 'business', 'but', 'by', 'called', 'came', 'can',
    'case', 'certain', 'change', 'child', 'children', 'church', 'city', 'close', 'come', 'company',
    'could', 'country', 'course', 'day', 'days', 'development', 'did', 'didn\'t', 'do', 'does',
    'don\'t', 'done', 'down', 'during', 'each', 'early', 'end', 'enough', 'even', 'ever',
    'every', 'eyes', 'face', 'fact', 'family', 'far', 'feel', 'felt', 'few', 'find',
    'first', 'for', 'form', 'from', 'general', 'get', 'give', 'given', 'go', 'god',
    'going', 'good', 'got', 'govern', 'government', 'great', 'group', 'had', 'hand', 'has',
    'have', 'he', 'head', 'help', 'her', 'here', 'high', 'him', 'himself', 'his',
    'home', 'house', 'how', 'however', 'if', 'important', 'in', 'interest', 'into', 'is',
    'it', 'its', 'just', 'keep', 'kind', 'knew', 'know', 'large', 'last', 'later',
    'law', 'least', 'leave', 'less', 'let', 'life', 'light', 'like', 'line', 'little',
    'long', 'look', 'looked', 'made', 'make', 'man', 'many', 'matter', 'me', 'mean',
    'means', 'members', 'men', 'might', 'mind', 'more', 'most', 'much', 'must', 'my',
    'name', 'national', 'need', 'never', 'new', 'next', 'night', 'no', 'not', 'nothing',
    'now', 'number', 'of', 'off', 'often', 'old', 'on', 'once', 'one', 'only',
    'open', 'or', 'order', 'other', 'others', 'our', 'out', 'over', 'own', 'part',
    'people', 'per', 'place', 'point', 'possible', 'power', 'present', 'president', 'problem', 'program',
    'public', 'put', 'question', 'rather', 'real', 'right', 'room', 'said', 'same', 'say',
    'school', 'second', 'see', 'seem', 'seemed', 'sense', 'service', 'set', 'several', 'she',
    'should', 'show', 'side', 'since', 'small', 'so', 'social', 'some', 'something', 'state',
    'states', 'still', 'such', 'system', 'take', 'tell', 'than', 'that', 'the', 'their',
    'them', 'then', 'there', 'these', 'they', 'thing', 'things', 'think', 'this', 'those',
    'though', 'thought', 'three', 'through', 'time', 'to', 'told', 'too', 'took', 'toward',
    'turn', 'turned', 'two', 'under', 'unite', 'until', 'up', 'upon', 'use', 'used',
    'very', 'want', 'war', 'was', 'water', 'way', 'we', 'week', 'well', 'went',
    'were', 'what', 'when', 'where', 'which', 'while', 'white', 'who', 'why', 'will',
    'with', 'within', 'without', 'word', 'work', 'would', 'year', 'yet', 'you', 'young',
    'your'
];

/**
 * @param sentence
 * @param wordList
 */
function correctSplit(sentence, wordList=[]) { return sentence === wordList.join('') && wordList.every( word => dictWords.includes(word) ); }

describe('song', () => {
    it('Equal Test', () => {
        const sentence = 'onelittletwolittlethreelittleindians';
        const dict = ['one', 'two', 'three', 'little', 'indians'];
        expect(solution(dict, sentence)).toEqual('one little two little three little indians'.split(' '));
    });
});

describe('shakespeare 1', () => {
    it('Equal Test', () => {
        expect(solution(dictWords, 'tobeornottobethatisthequestion')).toEqual('to be or not to be that is the question'.split(' '));
        expect(solution(dictWords, 'outlookedlooknotmoreactbegannameinlike')).toEqual([
            'out',   'looked',
            'look',  'not',
            'more',  'act',
            'began', 'name',
            'in',    'like'
        ]);
        expect(solution(dictWords, 'herethosealmostgowhileoftensuchtakegoif')).toEqual([
            'here',   'those',
            'almost', 'go',
            'while',  'often',
            'such',   'take',
            'go',     'if'
        ]);
        expect(solution(dictWords, 'smalldoesgetalwayschurchalwaysgotgoasother')).toEqual([
            'small',  'does',
            'get',    'always',
            'church', 'always',
            'got',    'go',
            'as',     'other'
        ]);
        expect(solution(dictWords, 'greatmakeimportantfaralsointerestatheadthesesecond')).toEqual([
            'great',     'make',
            'important', 'far',
            'also',      'interest',
            'at',        'head',
            'these',     'second'
        ]);
    });
});
describe('shakespeare 2', () => {
    it('Equal Test', () => {
        expect(solution(dictWords, 'weknowwhatwearebutknownotwhatwemaybe')).toEqual('we know what we are but know not what we may be'.split(' '));
    });
});
describe('may return any word breaks from the dictionary', () => {
    it('Equal Test', () => {
        const sentence = 'webecomewhatwethinkabout';
        const result = solution(dictWords, sentence);
        if (!correctSplit(sentence, result)) {
            if (result != ['we', 'become', 'what', 'we', 'think', 'about'] && result != ['we', 'be', 'come', 'what', 'we', 'think', 'about']) {
                throw Error('test failed');
            }
        }
    });
});
describe('should not find if the word is not in the dictionary', () => {
    it('Equal Test', () => {
        expect(solution(dictWords, 'ifmusicbethefoodofloveplayon')).toEqual(undefined);
        expect(solution(dictWords, 'abewww')).toEqual(undefined);
    });
});
describe('should not require exponential time to complete', () => {
    it('Equal Test', () => {
        const dictionary = [ 'a',
            'aa',
            'aaa',
            'aaaa',
            'aaaaa',
            'aaaaaa',
            'aaaaaaa',
            'aaaaaaaa',
            'aaaaaaaaa',
            'aaaaaaaaaa',
            'aaaaaaaaaaa',
            'aaaaaaaaaaaa',
            'aaaaaaaaaaaaa',
            'aaaaaaaaaaaaaa',
            'aaaaaaaaaaaaaaa',
            'aaaaaaaaaaaaaaaa',
            'aaaaaaaaaaaaaaaaa',
            'aaaaaaaaaaaaaaaaaa',
            'aaaaaaaaaaaaaaaaaaa',
            'aaaaaaaaaaaaaaaaaaaa',
            'aaaaaaaaaaaaaaaaaaaaa',
            'aaaaaaaaaaaaaaaaaaaaaa',
            'aaaaaaaaaaaaaaaaaaaaaaa',
            'aaaaaaaaaaaaaaaaaaaaaaaa',
            'aaaaaaaaaaaaaaaaaaaaaaaaa',
            'aaaaaaaaaaaaaaaaaaaaaaaaaa',
            'aaaaaaaaaaaaaaaaaaaaaaaaaaa',
            'aaaaaaaaaaaaaaaaaaaaaaaaaaaa',
            'aaaaaaaaaaaaaaaaaaaaaaaaaaaaa',
            'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',
            'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',
            'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',
            'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',
            'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',
            'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',
            'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',
            'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',
            'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',
            'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',
            'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',
            'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',
            'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',
            'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',
            'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',
            'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',
            'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',
            'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',
            'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',
            'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',
            'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',
            'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',
            'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',
            'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',
            'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',
            'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',
            'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',
            'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',
            'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',
            'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',
            'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',
            'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',
            'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',
            'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',
            'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',
            'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',
            'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',
            'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',
            'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',
            'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',
            'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',
            'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',
            'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',
            'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',
            'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',
            'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',
            'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',
            'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',
            'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',
            'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',
            'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',
            'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',
            'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',
            'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',
            'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',
            'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',
            'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',
            'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',
            'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',
            'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',
            'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',
            'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',
            'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',
            'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',
            'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',
            'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',
            'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',
            'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',
            'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',
            'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',
            'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',
            'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaab',
            'caaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa' ];

        const sentence = 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa' +
        'aaaaaaaaaaabcaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa';

        expect(solution(dictionary, sentence)).toEqual(['a',
            'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaab',
            'caaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa', 'a']);
    });
});

const solution = require('../src/BrokenPhotocopier');

describe('Broken Photocopier Tests', () => {
    it('Equal Test', () => {
        expect(solution('')).toEqual('');
        expect(solution('0')).toEqual('1');
        expect(solution('10')).toEqual('01');
        expect(solution('1011101')).toEqual('0100010');
    });
});

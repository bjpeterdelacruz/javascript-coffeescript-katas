const solution = require('../src/BuildingACalculator');

describe('Building a Calculator Tests', () => {
    it('Equal Test', () => {
        expect(solution.add(1, 3)).toEqual(4);
        expect(solution.subtract(1, 3)).toEqual(-2);
        expect(solution.multiply(0, 100)).toEqual(0);
        expect(solution.divide(100, 0)).toEqual(false);
        expect(solution.divide(100, 2)).toEqual(50);
    });
});

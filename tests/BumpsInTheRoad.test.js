const solution = require('../src/BumpsInTheRoad');

describe('Bumps in the Road Tests', () => {
    it('Equal Test', () => {
        expect(solution('______________________')).toEqual('Woohoo!');
        expect(solution('n____________________n')).toEqual('Woohoo!');
        expect(solution('n_nnn_nnn_nnn__nnn_nn_')).toEqual('Woohoo!');
        expect(solution('n_nnn_nnn_nnn__nnn_nnn')).toEqual('Car Dead');
    });
});

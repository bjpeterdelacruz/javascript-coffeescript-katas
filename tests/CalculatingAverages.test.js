const solution = require('../src/CalculatingAverages');

describe('Calculating Averages Tests', () => {
    it('Equal Test', () => {
        expect(solution.average()).toEqual(0);
        expect(solution.average(1)).toEqual(1);
        expect(solution.average(1, 2)).toEqual(1.5);
        expect(solution.average(1, 2, 3)).toEqual(2);
    });
});

const solution = require('../src/CanIHaveCake');

describe('Can I Have Cake Tests', () => {
    it('Equal Tests', () => {
        const available = {
            'oil': '200 ml',
            'flour': '600 g',
            'sugar': '110 g',
            'milk':
            '600 ml'
        };
        const recipe = {
            'oil': '3 tbsp',
            'flour': '2 cup',
            'sugar': '1/2 cup',
            'milk': '1 cup'
        };
        const otherRecipe = {
            'oil': '1 tbsp',
            'flour': '4 cup',
            'sugar': '1 cup',
            'milk': '1 cup'
        };

        expect(solution(available, recipe)).toEqual('You can bake');
        expect(solution(available, otherRecipe)).toEqual('Not enough ingredients: need flour, sugar');
        expect(solution({}, recipe)).toEqual('Not enough ingredients: need oil, flour, sugar, milk');
        expect(solution({}, {})).toEqual('You can bake');
        expect(solution(available, { 'oil': '1/4 cup' })).toEqual('You can bake');
        expect(solution({ 'flour': '200 g' }, { 'flour': '1 cup' })).toEqual('You can bake');
        expect(solution({ 'milk': '200 ml' }, { 'milk': '1 cup' })).toEqual('Not enough ingredients: need milk');
        expect(solution({ 'milk': '220 ml' }, { 'milk': '1 cup' })).toEqual('You can bake');

        expect(solution({ 'milk': '200 ml' }, { 'milk': '2 tsp' })).toEqual('You can bake');
        expect(solution({ 'flour': '200 g' }, { 'flour': '2 tbsp' })).toEqual('You can bake');
    });
});

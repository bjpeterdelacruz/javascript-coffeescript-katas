const chuckPushUps = require('../src/ChuckNorrisIPushUps');

describe('Chuck Norris Push Ups Tests', () => {
    it('Equal Tests', () => {
        expect(chuckPushUps('1 "Chuck" 10 "Stop that!" 11 "Your vest looks stupid" 100 101 110')).toEqual(6);
        expect(chuckPushUps('1000 "Did you kick someone in the face today?" 1001 1010 "Will I be making dinner then?!" 1011 110')).toEqual(11);
        expect(chuckPushUps('10000 "Nice Beard" 1111 "Are you wearing denim shorts?" 1110 1101')).toEqual(16);
        expect(chuckPushUps('"Did you kick someone in the face today?" 25')).toEqual('CHUCK SMASH!!');
        expect(chuckPushUps('"Did you kick someone in the face today?"')).toEqual('CHUCK SMASH!!');
        expect(chuckPushUps('1001 1ee1gf00t10h1011tr00 1011')).toEqual(3244);
        expect(chuckPushUps('')).toEqual('FAIL!!');
        expect(chuckPushUps([])).toEqual('FAIL!!');
        expect(chuckPushUps(2)).toEqual('FAIL!!');
    });
});

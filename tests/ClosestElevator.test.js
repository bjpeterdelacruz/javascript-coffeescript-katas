const solution = require('../src/ClosestElevator');

describe('Closest Elevator Tests', () => {
    it('Equal Test', () => {
        expect(solution(0, 0, 0)).toEqual('right');
        expect(solution(0, 1, 0)).toEqual('left');
        expect(solution(0, 2, 0)).toEqual('left');
        expect(solution(1, 0, 0)).toEqual('right');
        expect(solution(1, 1, 0)).toEqual('right');
        expect(solution(1, 2, 0)).toEqual('left');
        expect(solution(2, 0, 0)).toEqual('right');
        expect(solution(2, 1, 0)).toEqual('right');
        expect(solution(2, 2, 0)).toEqual('right');

        expect(solution(0, 0, 1)).toEqual('right');
        expect(solution(0, 1, 1)).toEqual('right');
        expect(solution(0, 2, 1)).toEqual('right');
        expect(solution(1, 0, 1)).toEqual('left');
        expect(solution(1, 1, 1)).toEqual('right');
        expect(solution(1, 2, 1)).toEqual('left');
        expect(solution(2, 0, 1)).toEqual('right');
        expect(solution(2, 1, 1)).toEqual('right');
        expect(solution(2, 2, 1)).toEqual('right');

        expect(solution(0, 0, 2)).toEqual('right');
        expect(solution(0, 1, 2)).toEqual('right');
        expect(solution(0, 2, 2)).toEqual('right');
        expect(solution(1, 0, 2)).toEqual('left');
        expect(solution(1, 1, 2)).toEqual('right');
        expect(solution(1, 2, 2)).toEqual('right');
        expect(solution(2, 0, 2)).toEqual('left');
        expect(solution(2, 1, 2)).toEqual('left');
        expect(solution(2, 2, 2)).toEqual('right');
    });
});

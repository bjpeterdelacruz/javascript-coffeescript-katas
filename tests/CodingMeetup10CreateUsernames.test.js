const solution = require('../src/CodingMeetup10CreateUsernames');

describe('Coding Meetup #10 - Create Usernames Tests', () => {
    it('Equal Test', () => {
        const today = new Date(2021, 5, 15);
        const developers = [
            { firstName: 'Harry', lastName: 'K.', country: 'Brazil', continent: 'Americas', age: 17,
                language: 'JavaScript' },
            { firstName: 'Kseniya', lastName: 'T.', country: 'Belarus', continent: 'Europe', age: 49,
                language: 'Ruby' },
            { firstName: 'Jing', lastName: 'X.', country: 'China', continent: 'Asia', age: 34,
                language: 'JavaScript' },
            { firstName: 'Noa', lastName: 'A.', country: 'Israel', continent: 'Asia', age: 20,
                language: 'Ruby' },
            { firstName: 'Andrei', lastName: 'E.', country: 'Romania', continent: 'Europe', age: 56,
                language: 'C' }
        ];
        const expected = [
            { firstName: 'Harry', lastName: 'K.', country: 'Brazil', continent: 'Americas', age: 17,
                language: 'JavaScript', username: 'harryk2004' },
            { firstName: 'Kseniya', lastName: 'T.', country: 'Belarus', continent: 'Europe', age: 49,
                language: 'Ruby', username: 'kseniyat1972' },
            { firstName: 'Jing', lastName: 'X.', country: 'China', continent: 'Asia', age: 34,
                language: 'JavaScript', username: 'jingx1987' },
            { firstName: 'Noa', lastName: 'A.', country: 'Israel', continent: 'Asia', age: 20,
                language: 'Ruby', username: 'noaa2001' },
            { firstName: 'Andrei', lastName: 'E.', country: 'Romania', continent: 'Europe', age: 56,
                language: 'C', username: 'andreie1965' }
        ];
        expect(solution(developers, today)).toEqual(expected);
    });
});

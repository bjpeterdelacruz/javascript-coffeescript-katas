const solution = require('../src/CodingMeetup11FindTheAverageAge');

describe('Coding Meetup #11 - Find the Average Age', () => {
    it('Equal Test', () => {
        const developers1 = [
            { firstName: 'Harry', lastName: 'K.', country: 'Brazil', continent: 'Americas', age: 17,
                language: 'JavaScript' },
            { firstName: 'Kseniya', lastName: 'T.', country: 'Belarus', continent: 'Europe', age: 49,
                language: 'Ruby' }
        ];
        const developers2 = [
            { firstName: 'Jing', lastName: 'X.', country: 'China', continent: 'Asia', age: 32,
                language: 'JavaScript', username: 'jingx1987' },
            { firstName: 'Noa', lastName: 'A.', country: 'Israel', continent: 'Asia', age: 20,
                language: 'Ruby', username: 'noaa2001' },
            { firstName: 'Andrei', lastName: 'E.', country: 'Romania', continent: 'Europe', age: 55,
                language: 'C', username: 'andreie1965' }
        ];
        const developers3 = [
            { firstName: 'Jing', lastName: 'X.', country: 'China', continent: 'Asia', age: 20,
                language: 'JavaScript', username: 'jingx1987' },
            { firstName: 'Noa', lastName: 'A.', country: 'Israel', continent: 'Asia', age: 22,
                language: 'Ruby', username: 'noaa2001' },
            { firstName: 'Andrei', lastName: 'E.', country: 'Romania', continent: 'Europe', age: 32,
                language: 'C', username: 'andreie1965' },
            { firstName: 'Andrei', lastName: 'E.', country: 'Romania', continent: 'Europe', age: 23,
                language: 'C', username: 'andreie1965' }
        ];
        expect(solution(developers1)).toEqual(33);
        expect(solution(developers2)).toEqual(36);
        expect(solution(developers3)).toEqual(24);
    });
});


const solution = require('../src/CodingMeetup15FindOddNames');

describe('Coding Meetup #15 - Find the Odd Names Tests', () => {
    it('Equal Test', () => {
        const developers = [
            { firstName: 'Ann', lastName: 'Y.', country: 'United States', continent: 'North America', age: 48, language: 'Java',
                meal: 'standard' },
            { firstName: 'Noah', lastName: 'M.', country: 'Switzerland', continent: 'Europe', age: 19, language: 'C',
                meal: 'vegetarian' },
            { firstName: 'Anna', lastName: 'R.', country: 'Liechtenstein', continent: 'Europe', age: 52, language: 'JavaScript',
                meal: 'standard' },
            { firstName: 'Abbie', lastName: 'R.', country: 'Paraguay', continent: 'Americas', age: 29, language: 'Ruby',
                meal: 'vegan' }
        ];

        const expected = [
            { firstName: 'Ann', lastName: 'Y.', country: 'United States', continent: 'North America', age: 48, language: 'Java',
                meal: 'standard' },
            { firstName: 'Abbie', lastName: 'R.', country: 'Paraguay', continent: 'Americas', age: 29, language: 'Ruby',
                meal: 'vegan' }
        ];
        expect(solution(developers)).toEqual(expected);
    });
});

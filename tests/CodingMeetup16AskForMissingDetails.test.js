
const solution = require('../src/CodingMeetup16AskForMissingDetails');

describe('Coding Meetup #16 - Ask for Missing Details Tests', () => {
    it('Equal Test', () => {
        const developers = [
            { firstName: 'Ann', lastName: 'Y.', country: null, continent: 'North America', age: 48, language: 'Java' },
            { firstName: null, lastName: 'M.', country: 'Switzerland', continent: 'Europe', age: 19, language: 'C' },
            { firstName: 'Anna', lastName: 'R.', country: 'Liechtenstein', continent: 'Europe', age: 52, language: 'JavaScript',
                meal: 'standard' },
            { firstName: 'Abbie', lastName: 'R.', country: 'Paraguay', continent: 'Americas', age: null, language: 'Ruby' }
        ];

        const expected = [
            { firstName: 'Ann', lastName: 'Y.', country: null, continent: 'North America', age: 48, language: 'Java',
                question: 'Hi, could you please provide your country.' },
            { firstName: null, lastName: 'M.', country: 'Switzerland', continent: 'Europe', age: 19, language: 'C',
                question: 'Hi, could you please provide your firstName.' },
            { firstName: 'Abbie', lastName: 'R.', country: 'Paraguay', continent: 'Americas', age: null, language: 'Ruby',
                question: 'Hi, could you please provide your age.' }
        ];
        expect(solution(developers)).toEqual(expected);
    });
});

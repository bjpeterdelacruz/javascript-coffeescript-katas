const solution = require('../src/CodingMeetup4FindFirstPythonDeveloper');

describe('Coding Meetup #4 - Find the First Python Developer Tests', () => {
    it('Equal Test', () => {
        const developers = [
            { firstName: 'Mark', lastName: 'G.', country: 'Scotland', continent: 'Europe', age: 22, language: 'JavaScript' },
            { firstName: 'Victoria', lastName: 'T.', country: 'Puerto Rico', continent: 'Americas', age: 30, language: 'Python' },
            { firstName: 'Emma', lastName: 'B.', country: 'Norway', continent: 'Europe', age: 19, language: 'Python' }
        ];
        expect(solution(developers)).toEqual('Victoria, Puerto Rico');

        const developers2 = [
            { firstName: 'Kseniya', lastName: 'T.', country: 'Belarus', continent: 'Europe', age: 29, language: 'JavaScript' },
            { firstName: 'Amar', lastName: 'V.', country: 'Bosnia and Herzegovina', continent: 'Europe', age: 32, language: 'Ruby' }
        ];
        expect(solution(developers2)).toEqual('There will be no Python developers');
    });
});

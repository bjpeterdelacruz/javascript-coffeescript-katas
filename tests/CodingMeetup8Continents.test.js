const solution = require('../src/CodingMeetup8Continents');

describe('Coding Meetup #8 - Will All Continents be Represented Tests', () => {
    it('Equal Test', () => {
        const developers1 = [
            { firstName: 'Daniel', lastName: 'J.', country: 'Aruba', continent: 'Americas', age: 42, language: 'Python' },
            { firstName: 'Kseniya', lastName: 'T.', country: 'Belarus', continent: 'Europe', age: 22, language: 'Ruby' },
            { firstName: 'Sou', lastName: 'B.', country: 'Japan', continent: 'Asia', age: 43, language: 'Ruby' },
            { firstName: 'Hanna', lastName: 'L.', country: 'Hungary', continent: 'Europe', age: 95, language: 'JavaScript' },
            { firstName: 'Jayden', lastName: 'P.', country: 'Jamaica', continent: 'Americas', age: 18, language: 'JavaScript' },
            { firstName: 'Joao', lastName: 'D.', country: 'Portugal', continent: 'Europe', age: 25, language: 'JavaScript' }
        ];
        expect(solution(developers1)).toEqual(false);

        const developers2 = [
            { firstName: 'Alexander', lastName: 'F.', country: 'Russia', continent: 'Europe', age: 89, language: 'Java' },
            { firstName: 'Fatima', lastName: 'K.', country: 'Saudi Arabia', continent: 'Asia', age: 21, language: 'Clojure' },
            { firstName: 'Mark', lastName: 'G.', country: 'Scotland', continent: 'Europe', age: 22, language: 'JavaScript' },
            { firstName: 'Nikola', lastName: 'H.', country: 'Serbia', continent: 'Europe', age: 29, language: 'Python' },
            { firstName: 'Jakub', lastName: 'I.', country: 'Slovakia', continent: 'Asia', age: 28, language: 'Java' },
            { firstName: 'Kseniya', lastName: 'T.', country: 'Belarus', continent: 'Americas', age: 89, language: 'JavaScript' },
            { firstName: 'Luka', lastName: 'J.', country: 'Slovenia', continent: 'Oceania', age: 29, language: 'Clojure' },
            { firstName: 'Mariam', lastName: 'B.', country: 'Egypt', continent: 'Africa', age: 89, language: 'Python' }
        ];
        expect(solution(developers2)).toEqual(true);

        const developers3 = [
            { firstName: 'Daniel', lastName: 'J.', country: 'Aruba', continent: 'Americas', age: 42, language: 'Python' },
            { firstName: 'Kseniya', lastName: 'T.', country: 'Belarus', continent: 'Europe', age: 22, language: 'Ruby' },
            { firstName: 'Joao', lastName: 'D.', country: 'Portugal', continent: 'Europe', age: 25, language: 'JavaScript' }
        ];
        expect(solution(developers3)).toEqual(false);

        const developers4 = [
            { firstName: 'Joao', lastName: 'D.', country: 'Portugal', continent: 'Europe', age: 25, language: 'JavaScript' },
            { firstName: 'Hanna', lastName: 'L.', country: 'Hungary', continent: 'Europe', age: 95, language: 'JavaScript' }
        ];
        expect(solution(developers4)).toEqual(false);
    });
});

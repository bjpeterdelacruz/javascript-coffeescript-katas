const solution = require('../src/CoefficientsOfTheQuadraticEquation');

describe('Coefficients of the Quadratic Equation Tests', () => {
    it('Equal Tests', () => {
        expect(solution(0, 3)).toEqual([1, -3, 0]);
        expect(solution(5, 0)).toEqual([1, -5, 0]);
        expect(solution(-19, -19)).toEqual([1, 38, 19 * 19]);
        expect(solution(4, -9)).toEqual([1, 5, -36]);
        expect(solution(-5, 4)).toEqual([1, 1, -20]);
    });
});

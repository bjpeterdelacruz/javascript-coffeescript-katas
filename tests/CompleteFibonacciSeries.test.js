const solution = require('../src/CompleteFibonacciSeries');

describe('Complete Fibonacci Series Tests', () => {
    it('Equal Test', () => {
        expect(solution(-1)).toEqual([]);
        expect(solution(0)).toEqual([]);
        expect(solution(1)).toEqual([0]);
        expect(solution(2)).toEqual([0, 1]);
        expect(solution(4)).toEqual([0, 1, 1, 2]);

        expect(solution(5).length).toEqual(5);
        expect(solution(5)[4]).toEqual(3);
        expect(solution(13)[12]).toEqual(144);
        expect(solution(-5).length).toEqual(0);
        expect(solution(0).length).toEqual(0);
    });
});

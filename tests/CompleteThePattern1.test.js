const solution = require('../src/CompleteThePattern1');

describe('Complete the Pattern #1 Tests', () => {
    it('Equal Test', () => {
        expect(solution(0)).toEqual('');
        expect(solution(1)).toEqual('1');
        expect(solution(2)).toEqual('1\n22');
        expect(solution(5)).toEqual('1\n22\n333\n4444\n55555');
        expect(solution(10)).toEqual('1\n22\n333\n4444\n55555\n666666\n7777777\n88888888\n999999999\n10101010101010101010');
    });
});

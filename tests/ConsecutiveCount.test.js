const solution = require('../src/ConsecutiveCount');

describe('Consecutive Count Tests', () => {
    it('Equal Test', () => {
        expect(solution(90000, 0)).toEqual(4);
        expect(solution('abcdaaadse', 'a')).toEqual(3);
        expect(solution('abcdaaadse', 'z')).toEqual(0);
        expect(solution('ascasdaiiiasdacasdiiiiicasdasdiiiiiiiiiiisdasdasdiii', 'i')).toEqual(11);

        expect(solution(111111221111, 1)).toEqual(6);
        expect(solution(111111221111, 11)).toEqual(3);
        expect(solution(111112231111, 1)).toEqual(5);
        expect(solution(111112231111, 11)).toEqual(2);

        expect(solution(10100100100, 100)).toEqual(3);
    });
});

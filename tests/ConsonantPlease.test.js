const solution = require('../src/ConsonantPlease');

describe('Consonant Please Tests', () => {
    it('Equal Test', () => {
        expect(solution([[1, 'a', 'C'], ['b', 2, 'd'], ['E', 3, 4]])).toEqual([['A', 'E'], ['C', 'B', 'D']]);
        expect(solution([[0], [], ['x', 'Y', 'z']])).toEqual([[], ['X', 'Y', 'Z']]);
    });
});

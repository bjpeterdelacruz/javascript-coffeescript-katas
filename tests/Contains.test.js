require('../src/Contains');

describe('Contains Tests', () => {
    it('Equal Test', () => {
        expect('Hello, World'.contains('wor')).toEqual(true);
        expect('Hello, World'.contains('wor', true)).toEqual(false);
    });
});

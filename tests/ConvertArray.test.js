const solution = require('../src/ConvertArray');

describe('Convert Array of Strings to Array of Numbers Tests', () => {
    it('Equal Test', () => {
        expect(solution(['1', '2.2', '3e6', '4.0', '5'])).toEqual([1, 2.2, 3000000, 4, 5]);
    });
});

const solution = require('../src/ConvertHashToAnArray');

describe('Convert Hash to an Array Tests', () => {
    it('Equal Test', () => {
        expect(solution({ name: 'Jeremy' })).toEqual([['name', 'Jeremy']]);
        expect(solution({ name: 'Jeremy', age: 24 })).toEqual([['age', 24], ['name', 'Jeremy']]);
        expect(solution({ name: 'Jeremy', age: 24, role: 'Software Engineer' })).toEqual([['age', 24], ['name', 'Jeremy'], ['role', 'Software Engineer']]);
        expect(solution({ product: 'CodeWars', powerLevelOver: 9000 })).toEqual([['powerLevelOver', 9000], ['product', 'CodeWars']]);
        expect(solution({})).toEqual([]);
    });
});

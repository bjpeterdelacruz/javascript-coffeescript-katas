const solution = require('../src/ConvertISBN10ToISBN13');

/**
 *
 */
function randomISBN10() {
    let dashesLeft = 2;
    const isbn = [];
    for (let i = 0; i < 9; i++) {
        isbn.push(Math.floor(Math.random() * 10));
        if ((Math.random() < 0.3 && dashesLeft > 0) || (i === 5 && dashesLeft === 2) || (i === 7 && dashesLeft === 1)) {
            isbn.push('-');
            dashesLeft--;
        }
    }
    const check = Math.floor(Math.random() * 11);
    isbn.push('-', check === 10 ? 'X' : check);
    return isbn.join('');
}

/**
 * @param isbn
 */
function solution2(isbn) {
    const tenDigit = [...isbn.replace(/-/g, '')];
    const twelveDigit = ['9', '7', '8', ...tenDigit.slice(0, -1)];
    const result = twelveDigit.reduce((a, c, i) => a + (i % 2 === 1 ? +c * 3 : +c), 0) % 10;
    const checkDigit = result === 0 ? result : 10 - result;
    return '978-' + [...isbn].slice(0, -1).join('') + checkDigit.toString();
}

describe('Convert ISBN-10 to ISBN-13 Tests', () => {
    it('Equal Test', () => {
        expect(solution('1-85326-158-0')).toEqual('978-1-85326-158-9');
        expect(solution('0-14-143951-3')).toEqual('978-0-14-143951-8');
        expect(solution('0-02-346450-X')).toEqual('978-0-02-346450-8');
        expect(solution('963-14-2164-3')).toEqual('978-963-14-2164-4');
        expect(solution('1-7982-0894-6')).toEqual('978-1-7982-0894-6');
        expect(solution('1-4028-9462-7')).toEqual('978-1-4028-9462-6');
    });

    for (let i = 1; i < 250; i++) {
        const isbn = randomISBN10();
        it(`Test for ISBN (${isbn})`, () => {
            expect(solution(isbn)).toEqual(solution2(isbn));
        });
    }
});

const solution = require('../src/ConvertPascalCase');

describe('Convert PascalCase String Tests', () => {
    it('Equal Tests', () => {
        expect(solution('')).toEqual('');
        expect(solution(1)).toEqual('1');
        expect(solution('TestController')).toEqual('test_controller');
        expect(solution('MoviesAndBooks')).toEqual('movies_and_books');
        expect(solution('App7Test')).toEqual('app7_test');
    });
});

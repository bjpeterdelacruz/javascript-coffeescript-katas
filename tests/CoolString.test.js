const solution = require('../src/CoolString');

describe('Cool String Tests', () => {
    it('Equal Tests', () => {
        expect(solution('aQwFdA')).toEqual(true);
        expect(solution('aBC')).toEqual(false);
        expect(solution('Abc')).toEqual(false);
        expect(solution('AaA')).toEqual(true);
        expect(solution('q q')).toEqual(false);
        expect(solution('wWw1')).toEqual(false);
        expect(solution('2')).toEqual(false);
        expect(solution('aAaAaAa')).toEqual(true);
        expect(solution('    ')).toEqual(false);
    });
});

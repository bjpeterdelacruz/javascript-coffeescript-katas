const solution = require('../src/CountAddressesGroupedByState');

describe('Count Addresses Grouped by State Tests', () => {
    it('Equal Tests', () => {
        expect(solution([]).length).toEqual(0);
        const counts = solution(['AK', 'AR', 'OR', 'CA', 'AK', 'OR', 'OR'].map((state) => {
            return {
                house: 1,
                street: 'Binary St.',
                city: 'Zero Value',
                state: state
            };
        })
        );
        expect(counts).toEqual([
            { count: 2, state: 'AK' },
            { count: 1, state: 'AR' },
            { count: 3, state: 'OR' },
            { count: 1, state: 'CA' }
        ]);
        expect(() => solution([{ house: 1, street: 'Binary St.', city: 'Zero Value' }])).toThrow();
    });
});

const solution = require('../src/CountByX');

describe('Count By X Tests', () => {
    it('Equal Test', () => {
        expect(solution(1, 5)).toEqual([1, 2, 3, 4, 5]);
        expect(solution(3, 10)).toEqual([3, 6, 9, 12, 15, 18, 21, 24, 27, 30]);
    });
});

const solution = require('../src/CountLettersAndDigits');

describe('Count Letters and Digits Tests', () => {
    it('Equal Test', () => {
        expect(solution('hel2!lo')).toEqual(6);
        expect(solution('n!!_ice!!123')).toEqual(7);
        expect(solution('1')).toEqual(1);
        expect(solution('?')).toEqual(0);
        expect(solution('12345f%%%t5t&/6')).toEqual(10);
        expect(solution('aBcDeFg090')).toEqual(10);
    });
});

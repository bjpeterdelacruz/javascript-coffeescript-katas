const solution = require('../src/CountSalutes');

describe('Count Salutes Tests', () => {
    it('Equal Test', () => {
        expect(solution('<---->---<---<-->')).toEqual(4);
        expect(solution('------')).toEqual(0);
        expect(solution('>>>>>>>>>>>>>>>>>>>>>----<->')).toEqual(42);
        expect(solution('<<----<>---<')).toEqual(2);
        expect(solution('>')).toEqual(0);
    });
});

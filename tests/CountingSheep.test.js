const solution = require('../src/CountingSheep');

describe('Counting Sheep Tests', () => {
    it('Equal Test', () => {
        expect(solution([true, true, false, false, false, false, true])).toEqual(3);
        expect(solution([false, false, false])).toEqual(0);
        expect(solution([])).toEqual(0);
    });
});

const solution = require('../src/DangerousDates');

describe('Dangerous Dates Tests', () => {
    it('Equal Test', () => {
        expect(solution(new Date(2021, 5, 28)))
            .toEqual('6/29/2021, 6/30/2021, 7/1/2021, 7/2/2021, 7/3/2021');
        expect(solution(new Date(2000, 1, 27)))
            .toEqual('2/28/2000, 2/29/2000, 3/1/2000, 3/2/2000, 3/3/2000');
    });
});

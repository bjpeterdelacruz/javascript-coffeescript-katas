const solution = require('../src/DayOfTheWeek');

describe('Day of the Week Tests', () => {
    it('Equal Tests', () => {
        expect(solution('1/1/2017')).toEqual('Sunday');
        expect(solution('12/24/2015')).toEqual('Thursday');
        expect(solution('2/24/1982')).toEqual('Wednesday');
        expect(solution('6/2/1952')).toEqual('Monday');
        expect(solution('8/21/1998')).toEqual('Friday');
        expect(solution(new Date(1998, 7, 21))).toEqual('Friday');
    });
});

const solution = require('../src/Decipher');

describe('Decipher Tests', () => {
    it('Equal Test', () => {
        expect(solution('10197115121')).toEqual('easy');
        expect(solution('98')).toEqual('b');
        expect(solution('122')).toEqual('z');
    });
});

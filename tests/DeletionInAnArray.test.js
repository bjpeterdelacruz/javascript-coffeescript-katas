const solution = require('../src/DeletionInAnArray');

describe('Deletion in an Array Tests', () => {
    it('Equal Test', () => {
        const arr = [1, 3, 2, 4, 5, 7, 6, 8, 10, 9];
        solution(arr, (element) => element % 2 === 0);
        expect(arr).toEqual([1, 3, 5, 7, 9]);
    });
});

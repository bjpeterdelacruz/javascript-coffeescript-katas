const solution = require('../src/DiagonalsSum');

describe('Diagonals Sum Tests', () => {
    it('Equal Test', () => {
        expect(solution([])).toEqual(0);
        expect(solution([[4]])).toEqual(8);
        expect(solution([[1, 2], [3, 4]])).toEqual(1 + 2 + 3 + 4);
        expect(solution([[1, 2, 3], [4, 5, 6], [7, 8, 9]])).toEqual(1 + 5 + 9 + 3 + 5 + 7);
        expect(solution([[-2, 5, 3, 2], [9, -6, 5, 1], [3, 2, 7, 3], [-1, 8, -4, 8]])).toEqual(-2 -6 + 7 + 8 + 2 +5 + 2 -1);
    });
});

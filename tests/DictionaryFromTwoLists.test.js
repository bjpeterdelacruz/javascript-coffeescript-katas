const solution = require('../src/DictionaryFromTwoLists');

describe('Dictionary from Two Lists Tests', () => {
    it('Equal Test', () => {
        expect(solution(['a', 'b'], [1, 2, 3])).toEqual({ 'a': 1, 'b': 2 });
        expect(solution([], [])).toEqual({});
        expect(solution(['a', 'b'], [3, 4])).toEqual({ 'a': 3, 'b': 4 });
        expect(solution(['c', 'd'], [5])).toEqual({ 'c': 5, 'd': null });
    });
});

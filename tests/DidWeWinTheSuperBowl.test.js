const solution = require('../src/DidWeWinTheSuperBowl');

describe('Did We Win the Super Bowl Tests', () => {
    it('Equal Test', () => {
        expect(solution([[8, 'pass'], [5, 'sack'], [3, 'sack'], [5, 'run']])).toEqual(false);
        expect(solution([[12, 'pass'], [], [], []])).toEqual(true);
        expect(solution([[2, 'run'], [5, 'pass'], [3, 'sack'], [8, 'pass']])).toEqual(true);
        expect(solution([[5, 'pass'], [6, 'turnover'], [], []])).toEqual(false);
        expect(solution([[5, 'pass'], [5, 'pass'], [10, 'sack'], [10, 'run']])).toEqual(false);
        expect(solution([[5, 'pass'], [5, 'run'], [1, 'run'], []])).toEqual(true);
        expect(solution([[6, 'run'], [7, 'sack'], [10, 'sack'], [23, 'pass']])).toEqual(true);
        expect(solution([[10, 'turnover'], [], [], []])).toEqual(false);
        expect(solution([[8, 'sack'], [5, 'sack'], [6, 'sack'], [30, 'run']])).toEqual(true);
        expect(solution([[3, 'run'], [3, 'run'], [3, 'run'], [10, 'turnover']])).toEqual(false);
        expect(solution([[16, 'pass'], [], [], []])).toEqual(true);
        expect(solution([[13, 'run'], [], [], []])).toEqual(true);
        expect(solution([[4, 'pass'], [5, 'run'], [1, 'run'], [1, 'run']])).toEqual(true);
        expect(solution([[20, 'sack'], [10, 'run'], [10, 'sack'], [35, 'run']])).toEqual(true);
        expect(solution([[10, 'run'], [10, 'sack'], [10, 'pass'], [1, 'sack']])).toEqual(false);
        expect(solution([[8, 'pass'], [3, 'pass'], [], []])).toEqual(true);
        expect(solution([[3, 'pass'], [5, 'pass'], [8, 'turnover'], []])).toEqual(false);
        expect(solution([[2, 'run'], [2, 'pass'], [2, 'run'], [2, 'pass']])).toEqual(false);
        expect(solution([[1, 'pass'], [6, 'pass'], [8, 'pass'], []])).toEqual(true);
        expect(solution([[9, 'run'], [1, 'run'], [3, 'turnover'], []])).toEqual(false);
    });
});

const solution = require('../src/DifferenceBetweenTwoNumbers');

describe('Difference Between Two Numbers Tests', () => {
    it('Equal Test', () => {
        expect(solution([1, 2, 3, 4, 5, 6, 7, 8, 9, 10])).toEqual(1);
        expect(solution([9, 99, 999])).toEqual(900);
        expect(solution([999, 99, 9])).toEqual(900);
        expect(solution([100, 100])).toEqual(0);
        expect(solution([1, 2, 10, 3, 4, 5, 6, 7, 8, 9])).toEqual(1);
        expect(solution([1, 2, 10, 3, 4, 5, 6, 7, 8, 9, 10])).toEqual(0);
        expect(solution([100, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 100])).toEqual(0);
        expect(solution([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50])).toEqual(1);
        expect(solution([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 100])).toEqual(50);
        expect(solution([100, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50])).toEqual(50);
        expect(solution([100, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 100])).toEqual(0);
        expect(solution([100, 100, 100, 100, 100, 100, 100, 100, 100, 100])).toEqual(0);
        expect(solution([1, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100])).toEqual(0);
        expect(solution([100, 100, 100])).toEqual(0);
        expect(solution([100, 100, 99])).toEqual(0);
        expect(solution([100, 99, 100])).toEqual(0);
        expect(solution([99, 100, 100])).toEqual(0);
        expect(solution([100, 99, 99])).toEqual(1);
        expect(solution([99, 100, 99])).toEqual(1);
        expect(solution([99, 100, 99])).toEqual(1);
        expect(solution([99, 99, 99])).toEqual(0);
    });
});

const solution = require('../src/DigitSum');

describe('Digit Sum Tests', () => {
    it('Equal Test', () => {
        expect(solution('1234')).toEqual('1');
        expect(solution('2468')).toEqual('2');
        expect(solution('1011')).toEqual('3');
        expect(solution('7777')).toEqual('1');
    });
});

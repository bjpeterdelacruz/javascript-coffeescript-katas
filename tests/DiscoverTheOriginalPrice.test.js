const solution = require('../src/DiscoverTheOriginalPrice');

describe('Discover the Original Price Tests', () => {
    it('Equal Test', () => {
        expect(solution(75, 25)).toEqual(100);
        expect(solution(25, 75)).toEqual(100);
        expect(solution(75.75, 25)).toEqual(101);

        expect(solution(373.85, 11.2)).toEqual(421);
        expect(solution(458.2, 17.13)).toEqual(552.91);
        expect(solution(101.45, 27)).toEqual(138.97);
    });
});

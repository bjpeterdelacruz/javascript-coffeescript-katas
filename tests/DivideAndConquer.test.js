const solution = require('../src/DivideAndConquer');

describe('Divide and Conquer Tests', () => {
    it('Equal Test', () => {
        expect(solution([])).toEqual(0);
        expect(solution([0, '0'])).toEqual(0);
        expect(solution(['1'])).toEqual(-1);
        expect(solution([2])).toEqual(2);

        expect(solution(['5', '0', 9, 3, 2, 1, '9', 6, 7])).toEqual(14);
        expect(solution(['1', '5', '8', 8, 9, 9, 2, '3'])).toEqual(11);
    });
});

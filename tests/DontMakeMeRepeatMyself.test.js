require('../src/DontMakeMeRepeatMyself');

describe('Don\'t Make Me Repeat Myself Tests', () => {
    it('Equal Test', () => {
        expect('a'.repeat(3)).toEqual('aaa');
        expect('ho '.repeat(3)).toEqual('ho ho ho ');
        expect('na'.repeat(8)).toEqual('nananananananana');
    });
});

const solution = require('../src/DrivingSchoolSeries1');

describe('Driving School Series #1 Tests', () => {
    it('Equal Test', () => {
        expect(solution([10, 10, 10, 18, 20, 20])).toEqual(12);
        expect(solution([21, 22, 24])).toEqual('No pass scores registered.');
        expect(solution([3, 22, 9, 13, 20, 18, 2, 14, 20, 8, 23, 12, 7, 21, 21, 19, 20, 11, 18, 7, 13, 22, 11, 20, 9])).toEqual(10);
        expect(solution([19, 16, 8, 11, 25, 10, 29, 22, 23])).toEqual(11);
        expect(solution([20, 20, 21, 22, 11, 12, 13, 14, 10, 9, 8, 7, 11, 10, 20, 21, 22])).toEqual(11);
        expect(solution([17, 31, 5, 23, 12, 18, 18, 6, 25, 19, 19])).toEqual(13);
        expect(solution([5, 11, 21, 16, 20, 24, 19, 28, 18, 15, 10, 12, 16, 17, 21, 25, 24, 12, 21, 18, 17])).toEqual(14);
    });
});

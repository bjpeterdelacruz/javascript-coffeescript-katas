const solution = require('../src/DrivingSchoolSeries2');

describe('Driving School Series #2 Tests', () => {
    it('Equal Test', () => {
        expect(solution(45)).toEqual(30);
        expect(solution(63)).toEqual(30);
        expect(solution(84)).toEqual(40);
        expect(solution(102)).toEqual(50);
        expect(solution(273)).toEqual(100);
    });
});

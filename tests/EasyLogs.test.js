const solution = require('../src/EasyLogs');

describe('Easy Logs Tests', () => {
    it('Equal Tests', () => {
        expect(solution(1, 2, 3)).toEqual(Infinity);
        expect(solution(10, 2, 3)).toEqual(0.7781512503836435);
        expect(solution(5, 2, 3)).toEqual(1.1132827525593785);
        expect(solution(1000, 2, 3)).toEqual(0.25938375012788123);
        expect(solution(2, 1, 2)).toEqual(1);
        expect(solution(0.00001, 0.002, 0.01)).toEqual(0.9397940008672038);
        expect(solution(0.1, 0.002, 0.01)).toEqual(4.69897000433602);
    });
});

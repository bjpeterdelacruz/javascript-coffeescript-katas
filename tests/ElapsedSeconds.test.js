const solution = require('../src/ElapsedSeconds');

describe('Elapsed Seconds Tests', () => {
    it('Equal Test', () => {
        const start = new Date(2013, 1, 1, 0, 0, 1);
        const end1 = new Date(2013, 1, 1, 0, 0, 2);
        const end2 = new Date(2013, 1, 1, 0, 0, 20);
        const end3 = new Date(2013, 1, 1, 0, 1, 20);
        expect(solution(start, end1)).toEqual(1);
        expect(solution(start, end2)).toEqual(19);
        expect(solution(start, end3)).toEqual(79);
    });
});

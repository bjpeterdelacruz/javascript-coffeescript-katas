const solution = require('../src/EnumerableMagic1TrueForAll');

describe('Enumerable Magic #1: True for All Tests', () => {
    it('Equal Test', () => {
        expect(solution([1, 3, 5, 7, 8, 9, 11], (value) => value % 2 === 1)).toEqual(false);
        expect(solution([1, 3, 5, 7, 9, 11], (value) => value % 2 === 1)).toEqual(true);
    });
});

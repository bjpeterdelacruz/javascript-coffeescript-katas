const solution = require('../src/EvaluatePostfixExpression');

describe('Evaluate Postfix Expression Tests', () => {
    it('Equal Tests', () => {
        expect(solution('2 3 +')).toEqual(5);
        expect(solution('20 40 60 + *')).toEqual(2000);
        expect(solution('20 40 + 60 *')).toEqual(3600);
        expect(solution('40 20 20 / -')).toEqual(39);
    });
});

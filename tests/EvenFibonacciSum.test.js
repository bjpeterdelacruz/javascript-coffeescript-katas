const solution = require('../src/EvenFibonacciSum');

describe('Even Fibonacci Sum Tests', () => {
    it('Equal Tests', () => {
        expect(solution(2147483647)).toEqual(1485607536);
        expect(solution(1000000000)).toEqual(350704366);
        expect(solution(100000000)).toEqual(82790070);
        expect(solution(1000000)).toEqual(1089154);
        expect(solution(1000)).toEqual(798);
        expect(solution(100)).toEqual(44);
        expect(solution(5)).toEqual(2);
        expect(solution(8)).toEqual(2);
        expect(solution(10)).toEqual(10);
        expect(solution(1)).toEqual(0);
    });
});

const solution = require('../src/EvenNumbersBeforeFixed');

describe('Even Numbers Before Fixed Tests', () => {
    it('Equal Tests', () => {
        expect(solution([1, 4, 2, 6, 3, 1], 6)).toEqual(2);
        expect(solution([2, 2, 2, 1], 3)).toEqual(-1);
        expect(solution([2, 3, 4, 3], 3)).toEqual(1);
        expect(solution([1, 3, 4, 3], 3)).toEqual(0);
    });
});

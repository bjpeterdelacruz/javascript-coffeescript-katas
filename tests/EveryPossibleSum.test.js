const solution = require('../src/EveryPossibleSum');

describe('Every Possible Sum of Two Digits Tests', () => {
    it('Equal Test', () => {
        expect(solution(156)).toEqual([6, 7, 11]);
        expect(solution(81596)).toEqual([9, 13, 17, 14, 6, 10, 7, 14, 11, 15]);
        expect(solution(3852)).toEqual([11, 8, 5, 13, 10, 7]);
        expect(solution(3264128)).toEqual([5, 9, 7, 4, 5, 11, 8, 6, 3, 4, 10, 10, 7, 8, 14, 5, 6, 12, 3, 9, 10]);
        expect(solution(999999)).toEqual([18, 18, 18, 18, 18, 18, 18, 18, 18, 18, 18, 18, 18, 18, 18]);
    });
});

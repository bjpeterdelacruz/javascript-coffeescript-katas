const solution = require('../src/ExclamationMarksSeriesNumber18');

describe('Exclamation Marks Series #18 Tests', () => {
    it('Equal Test', () => {
        expect(solution('!!!!!')).toEqual(1000);
        expect(solution('!!!!?')).toEqual(800);
        expect(solution('!!!??')).toEqual(500);
        expect(solution('!!!?!')).toEqual(300);
        expect(solution('!!?!!')).toEqual(200);
        expect(solution('!!?!?')).toEqual(100);
        expect(solution('!?!?!')).toEqual(0);
    });
});

const solution = require('../src/ExpandedFormPartII');

describe('Expanded Form Part II Tests', () => {
    it('Equal Tests', () => {
        expect(solution(100.2405)).toEqual('100 + 2/10 + 4/100 + 5/10000');
        expect(solution(432103)).toEqual('400000 + 30000 + 2000 + 100 + 3');
        expect(solution(432103.197501)).toEqual('400000 + 30000 + 2000 + 100 + 3 + 1/10 + 9/100 + 7/1000 + 5/10000 + 1/1000000');

        expect(solution(1.24)).toEqual('1 + 2/10 + 4/100');
        expect(solution(7.304)).toEqual('7 + 3/10 + 4/1000');
        expect(solution(0.04)).toEqual('4/100');
        expect(solution(4.28)).toEqual('4 + 2/10 + 8/100');
    });
});

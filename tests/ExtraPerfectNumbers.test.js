const solution = require('../src/ExtraPerfectNumbers');

describe('Extra Perfect Numbers Tests', () => {
    it('Equal Tests', () => {
        expect(solution(3)).toEqual([1, 3]);
        expect(solution(5)).toEqual([1, 3, 5]);
        expect(solution(7)).toEqual([1, 3, 5, 7]);
        expect(solution(28)).toEqual([1, 3, 5, 7, 9, 11, 13, 15, 17, 19, 21, 23, 25, 27]);
        expect(solution(39)).toEqual([1, 3, 5, 7, 9, 11, 13, 15, 17, 19, 21, 23, 25, 27, 29, 31, 33, 35, 37, 39]);
    });
});

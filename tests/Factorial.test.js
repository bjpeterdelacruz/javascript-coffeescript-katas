const solution = require('../src/Factorial');

describe('Factorial Tests', () => {
    it('Equal Tests', () => {
        expect(solution(0)).toEqual(1);
        expect(solution(1)).toEqual(1);
        expect(solution(2)).toEqual(2);
        expect(solution(3)).toEqual(6);
        expect(solution(4)).toEqual(24);
        expect(solution(5)).toEqual(120);
        expect(solution(6)).toEqual(720);
        expect(solution(7)).toEqual(5040);
        expect(solution(8)).toEqual(40320);
        expect(solution(9)).toEqual(362880);
        expect(solution(10)).toEqual(3628800);
        expect(solution(11)).toEqual(39916800);
        expect(solution(12)).toEqual(479001600);
        expect(() => solution(-1)).toThrow();
        expect(() => solution(13)).toThrow();
    });
});

const solution = require('../src/FactorialFactory');

describe('Factorial Factory Tests', () => {
    it('Equal Tests', () => {
        expect(solution(-1)).toEqual(null);
        expect(solution(0)).toEqual(1);
        expect(solution(1)).toEqual(1);
        expect(solution(2)).toEqual(2);
        expect(solution(10)).toEqual(3628800);
        expect(solution(20)).toEqual(2432902008176640000);
    });
});

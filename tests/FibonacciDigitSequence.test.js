const solution = require('../src/FibonacciDigitSequence');

describe('Fibonacci Digit Sequence Tests', () => {
    it('Equal Tests', () => {
        expect(solution(7, 8, 9)).toEqual(5);
        expect(solution(0, 0, 1000000)).toEqual(0);
        const ss = '78156112358134';
        for (let i = 0; i < ss.length; i++) {
            expect(solution(7, 8, i)).toEqual(+ss[i]);
        }
        expect(solution(3, 0, 986094271)).toEqual(2);
        expect(solution(6, 3, 6227185791)).toEqual(7);
        expect(solution(6, 0, 5652368702)).toEqual(7);
        expect(solution(4, 3, 8126351178)).toEqual(3);
        expect(solution(3, 1, 1823502712)).toEqual(9);
        expect(solution(0, 7, 9873299883)).toEqual(1);
    });
});

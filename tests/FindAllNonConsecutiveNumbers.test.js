const solution = require('../src/FindAllNonConsecutiveNumbers');

describe('Find All Non-Consecutive Numbers Tests', () => {
    it('Tests', () => {
        expect(solution([1])).toEqual([]);
        expect(solution([1, 2, 3, 4, 5, 6, 7, 8, 9, 10])).toEqual([]);
        expect(solution([1, 2, 3, 4, 6, 7, 8, 10])).toEqual([{ i: 4, n: 6 }, { i: 7, n: 10 }]);
        expect(solution([1, 2, 3, 7, 8, 10, 15])).toEqual([{ i: 3, n: 7 }, { i: 5, n: 10 }, { i: 6, n: 15 }]);
        expect(solution([-4, -3, -1, 0, 1, 3, 4])).toEqual([{ i: 2, n: -1 }, { i: 5, n: 3 }]);
    });
});

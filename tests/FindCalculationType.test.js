const solution = require('../src/FindCalculationType');

describe('Find the Calculation Type Tests', () => {
    it('Tests', () => {
        expect(solution(1, 2, 3)).toEqual('addition');
        expect(solution(10, 4, 40)).toEqual('multiplication');
        expect(solution(10, 5, 5)).toEqual('subtraction');
        expect(solution(5, 10, 5)).toEqual('subtraction');
        expect(solution(9, 5, 1.8)).toEqual('division');
    });
});

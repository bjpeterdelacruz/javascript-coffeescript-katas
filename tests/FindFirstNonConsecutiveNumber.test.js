const solution = require('../src/FindFirstNonConsecutiveNumber');

describe('Find the First Non-consecutive Number Type Tests', () => {
    it('Tests', () => {
        expect(solution([1, 2, 3])).toEqual(null);
        expect(solution([1, 3])).toEqual(3);
        expect(solution([1, 2, 3, 4, 6, 8, 9, 10])).toEqual(6);
    });
});

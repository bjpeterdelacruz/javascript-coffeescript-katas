const solution = require('../src/FindMeetingRoom');

describe('Find Meeting Room Tests', () => {
    it('Equal Test', () => {
        expect(solution(['X', 'X', 'X'])).toEqual('None available!');
        expect(solution(['X', 'O', 'X'])).toEqual(1);
        expect(solution(['O', 'O', 'X'])).toEqual(0);
    });
});

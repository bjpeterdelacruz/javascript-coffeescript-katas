const solution = require('../src/FindMinAndMax');

describe('Find Min and Max Tests', () => {
    it('Equal Tests', () => {
        expect(solution([1, 2, 4, 3])).toEqual([1, 4]);
        expect(solution([1, 1])).toEqual([1, 1]);
        expect(solution([2])).toEqual([2, 2]);
        expect(solution([])).toEqual([]);
    });
});

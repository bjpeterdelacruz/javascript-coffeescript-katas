const solution = require('../src/FindSmallestIntegerInArray');

describe('Find Smallest Integer in Array Tests', () => {
    it('Equal Test', () => {
        expect(solution([9, 1, 5, -1, 3, 7])).toEqual(-1);
        expect(solution([9, 13, 5, 11, 3, 7])).toEqual(3);
    });
});

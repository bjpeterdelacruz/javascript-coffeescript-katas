const solution = require('../src/FindTheCapitals');

describe('Find the Capitals Tests', () => {
    it('Equal Tests', () => {
        expect(solution('CodEWaRs')).toEqual([0, 3, 4, 6]);
        expect(solution('BJ Dela Cruz')).toEqual([0, 1, 3, 8]);
    });
});

const solution = require('../src/FindTheRemainder');

describe('Find the Remainder Tests', () => {
    it('Equal Tests', () => {
        expect(solution(17, 5)).toEqual(2);
        expect(solution(13, 72)).toEqual(solution(72, 13));
        expect(isNaN(solution(1, 0))).toEqual(true);
        expect(isNaN(solution(0, 0))).toEqual(true);
        expect(isNaN(solution(0, 1))).toEqual(true);
        expect(solution(-13, -13)).toEqual(-0);
        expect(solution(-60, 340)).toEqual(40);
    });
});

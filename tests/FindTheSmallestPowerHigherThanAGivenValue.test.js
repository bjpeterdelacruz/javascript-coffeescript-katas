const solution = require('../src/FindTheSmallestPowerHigherThanAGivenValue');

describe('Find the Smallest Power Higher Than a Given Value Tests', () => {
    it('Equal Test', () => {
        expect(solution(8, 3)).toEqual(27);
        expect(solution(12385, 3)).toEqual(13824);
        expect(solution(1245678, 5)).toEqual(1419857);
        expect(solution(1245678, 6)).toEqual(1771561);
        expect(solution(4782969, 7)).toEqual(10000000);
    });
});

const solution = require('../src/FindVowels');

describe('Find Vowels Tests', () => {
    it('Equal Test', () => {
        expect(solution('Aiea')).toEqual([1, 2, 3, 4]);
        expect(solution('Super')).toEqual([2, 4]);
        expect(solution('bbb')).toEqual([]);
        expect(solution('')).toEqual([]);
    });
});

const solution = require('../src/FireOnTheBoat');

describe('Fire on the Boat Tests', () => {
    it('Equal Test', () => {
        expect(solution('Boat Rudder Mast Boat Hull Water Fire Boat Deck Hull Fire Propeller Deck Fire Deck Boat Mast')).toEqual(
            'Boat Rudder Mast Boat Hull Water ~~ Boat Deck Hull ~~ Propeller Deck ~~ Deck Boat Mast');
        expect(solution('Mast Deck Engine Water Fire')).toEqual('Mast Deck Engine Water ~~');
        expect(solution('Fire Deck Engine Sail Deck Fire Fire Fire Rudder Fire Boat Fire Fire Captain')).toEqual(
            '~~ Deck Engine Sail Deck ~~ ~~ ~~ Rudder ~~ Boat ~~ ~~ Captain');
        expect(solution('Boat Deck Boat')).toEqual('Boat Deck Boat');
        expect(solution('Fire Boat Captain')).toEqual('~~ Boat Captain');
    });
});

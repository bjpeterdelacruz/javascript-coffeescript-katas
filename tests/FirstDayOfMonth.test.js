const solution = require('../src/FirstDayOfMonth');

describe('First Day of Month that is a Sunday Tests', () => {
    it('Equal Test', () => {
        expect(solution(1901, 2000)).toEqual(171);
        expect(solution(1991, 2017)).toEqual(46);
        expect(solution(1991)).toEqual(2);
        expect(solution(2017)).toEqual(2);
    });
});

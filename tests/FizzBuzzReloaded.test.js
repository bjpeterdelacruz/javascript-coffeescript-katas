const solution = require('../src/FizzBuzzReloaded');

describe('Fizz Buzz Reloaded Tests', () => {
    it('Equal Tests', () => {
        const funcs1 = new Map([['fizz', _ => true]]);
        expect(solution(1, 3, 1, funcs1)).toEqual('fizz fizz fizz');

        const funcs2 = new Map([['fizz', _ => false]]);
        expect(solution(1, 3, 1, funcs2)).toEqual('1 2 3');

        const funcs3 = new Map([['fizz', x => x % 3 === 0], ['buzz', x => x % 5 === 0]]);
        expect(solution(1, 19, 1, funcs3)).toEqual('1 2 fizz 4 buzz fizz 7 8 fizz buzz 11 fizz 13 14 fizzbuzz 16 17 fizz 19');
        expect(solution(19, 1, -1, funcs3)).toEqual('19 fizz 17 16 fizzbuzz 14 13 fizz 11 buzz fizz 8 7 fizz buzz 4 fizz 2 1');

        const funcs4 = new Map([['flash', x => x % 3 === 0], ['bang', x => x % 5 === 0]]);
        expect(solution(15, 3, -4, funcs4)).toEqual('flashbang 11 7 flash');
    });
});

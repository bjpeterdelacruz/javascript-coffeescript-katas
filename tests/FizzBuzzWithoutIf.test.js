const fs = require('fs');
const path = require('path');
const solution = require('../src/FizzBuzzWithoutIf');

describe('FizzBuzz without If Statements Tests', () => {
    it('Equal Test', () => {
        const arr = path.resolve().split('/');
        arr.push('src');
        const filename = path.resolve(arr.join('/'), 'FizzBuzzWithoutIf.js');
        const contents = fs.readFileSync(filename, 'utf8');
        expect(contents.includes('if')).toEqual(false);
        expect(solution(6)).toEqual('Fizz');
        expect(solution(5)).toEqual('Buzz');
        expect(solution(15)).toEqual('FizzBuzz');
        expect(solution(2)).toEqual(2);
    });
});

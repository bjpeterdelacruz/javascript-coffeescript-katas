const solution = require('../src/FoldYourWayToTheMoon');

describe('Fold Your Way to the Moon Tests', () => {
    it('Equal Tests', () => {
        expect(solution(384000000)).toEqual(42);
        expect(solution(0.0000001)).toEqual(0);
        expect(solution(0)).toEqual(0);
        expect(solution(-1)).toEqual(null);
    });
});

const solution = require('../src/ForKidsDateDecryption');

describe('For Kids: Date Decryption Tests', () => {
    it('Equal Tests', () => {
        expect(solution('FC-3-G')).toEqual('2017-01-21');
        expect(solution('Et->-9')).toEqual('1966-12-07');
        expect(solution('F<-9-O')).toEqual('2010-07-29');
        expect(solution('F4-4-4')).toEqual('2002-02-02');
        expect(solution('Fi-=-=')).toEqual('2055-11-11');
    });
});

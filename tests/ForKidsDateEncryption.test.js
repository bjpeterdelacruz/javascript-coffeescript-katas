const solution = require('../src/ForKidsDateEncryption');

describe('For Kids: Date Encryption Tests', () => {
    it('Equal Tests', () => {
        expect(solution('2017-01-21')).toEqual('FC-3-G');
        expect(solution('1966-12-07')).toEqual('Et->-9');
        expect(solution('2010-07-29')).toEqual('F<-9-O');
        expect(solution('2002-02-02')).toEqual('F4-4-4');
        expect(solution('2055-11-11')).toEqual('Fi-=-=');
    });
});

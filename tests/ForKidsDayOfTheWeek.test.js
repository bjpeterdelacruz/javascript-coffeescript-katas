const solution = require('../src/ForKidsDayOfTheWeek');

describe('For Kids: Day of the Week Tests', () => {
    it('Equal Tests', () => {
        expect(solution('3/1/2021')).toEqual('Sunday');
        expect(solution('1/2/2021')).toEqual('Monday');
        expect(solution('30/3/2021')).toEqual('Tuesday');
        expect(solution('31/3/2021')).toEqual('Wednesday');
        expect(solution('22/4/2021')).toEqual('Thursday');
        expect(solution('21/5/2021')).toEqual('Friday');
        expect(solution('26/6/2021')).toEqual('Saturday');
    });
});

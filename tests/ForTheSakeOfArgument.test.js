const solution = require('../src/ForTheSakeOfArgument');

describe('For the Sake of Argument Tests', () => {
    it('Equal Test', () => {
        expect(solution(1, 12, 3, 100)).toEqual(true);
        expect(solution('1', '2', '3', '4')).toEqual(false);
        expect(solution(1)).toEqual(true);
        expect(solution(1, '2', [3, 4], 'a')).toEqual(false);
        expect(solution(1, NaN, 3)).toEqual(true);
        expect(solution(null)).toEqual(false);
    });
});

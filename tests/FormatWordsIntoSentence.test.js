const solution = require('../src/FormatWordsIntoSentence');

describe('Format Words into a Sentence Tests', () => {
    it('Equal Tests', () => {
        expect(solution(null)).toEqual('');
        expect(solution([])).toEqual('');
        expect(solution(['', '', ''])).toEqual('');
        expect(solution(['ninja'])).toEqual('ninja');
        expect(solution(['ninja', '', 'warrior'])).toEqual('ninja and warrior');
        expect(solution(['ninja', 'warrior'])).toEqual('ninja and warrior');
        expect(solution(['ninja', 'code', 'warrior'])).toEqual('ninja, code and warrior');
        expect(solution(['ninja', 'code', 'wars', 'warrior'])).toEqual('ninja, code, wars and warrior');
        expect(solution(['ninja', 'code', 'hello', 'world', 'warrior'])).toEqual('ninja, code, hello, world and warrior');
    });
});

const solution = require('../src/FrugalPizza');

/**
 * @param diameter
 * @param price
 */
function pizzaPriceCheck(diameter, price) {
    const radius = diameter/2;
    const area = Math.PI * Math.pow(radius, 2);
    const result = price/area;
    if (!result) {
        return 0;
    } else {
        return Number(result.toFixed(2));
    }
}

describe('Examples', () => {
    it ('7 inch', () => {
        const testResult = pizzaPriceCheck(7, 4.30);
        const userResult = solution(7, 4.30);
        expect(userResult).toEqual(testResult);
    });
    it ('9 inch', () => {
        const testResult = pizzaPriceCheck(9, 5.95);
        const userResult = solution(9, 5.95);
        expect(userResult).toEqual(testResult);
    });
    it ('12 inch', () => {
        const testResult = pizzaPriceCheck(12, 7.90);
        const userResult = solution(12, 7.90);
        expect(userResult).toEqual(testResult);
    });
});

describe('It', () => {
    it ('should return 0 when given one argument', () => {
        const testResult1 = pizzaPriceCheck(13);
        const userResult1 = solution(13);
        expect(userResult1).toEqual(testResult1);

        const testResult2 = pizzaPriceCheck();
        const userResult2 = solution();
        expect(userResult2).toEqual(testResult2);
    });
    it ('should return 0 when given non-number arguments', () => {
        const testResult1 = pizzaPriceCheck('green', 'white');
        const userResult1 = solution('green', 'white');
        expect(userResult1).toEqual(testResult1);

        const testResult2 = pizzaPriceCheck('white');
        const userResult2 = solution('white');
        expect(userResult2).toEqual(testResult2);

        const testResult3 = pizzaPriceCheck('green', 27.30);
        const userResult3 = solution('green', 27.30);
        expect(userResult3).toEqual(testResult3);
    });
});

const { Node, indexOf } = require('../src/FunWithListsIndexOf');

/**
 * @param arr
 */
function listFromArray(arr) {
    if (arr.length === 0) {
        return null;
    }
    const head = new Node(arr[0]);
    let prev = head;
    for (let idx = 1; idx < arr.length; idx++) {
        const current = new Node(arr[idx]);
        prev.next = current;
        prev = current;
    }
    return head;
}

describe('Fun with Lists: indexOf Tests', () => {
    it('Equal Test', () => {
        expect(indexOf(null, 17)).toEqual(-1);
        expect(indexOf(listFromArray([1, 2, 3]), 2)).toEqual(1);
        expect(indexOf(listFromArray([1, 2, 3]), 4)).toEqual(-1);
        expect(indexOf(listFromArray(['aaa', 'b', 'abc']), 'aaa')).toEqual(0);
        expect(indexOf(listFromArray(['aaa', 'b', 'abc']), 'c')).toEqual(-1);
        expect(indexOf(listFromArray([17, '17', 1.2]), 17)).toEqual(0);
        expect(indexOf(listFromArray([17, '17', 1.2]), '17')).toEqual(1);
        expect(indexOf(listFromArray([1, 2, 3, 3]), 3)).toEqual(2);
    });
});

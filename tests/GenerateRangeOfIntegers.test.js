const solution = require('../src/GenerateRangeOfIntegers');

describe('Generate Range of Integers Tests', () => {
    it('Equal Test', () => {
        expect(solution(1, 5, 2)).toEqual([1, 3, 5]);
        expect(solution(1, 10, 2)).toEqual([1, 3, 5, 7, 9]);
        expect(solution(10, 1, 2)).toEqual([]);
    });
});

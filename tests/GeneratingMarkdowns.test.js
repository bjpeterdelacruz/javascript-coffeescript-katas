const solution = require('../src/GeneratingMarkdowns');

describe('Generating Markdowns Tests', () => {
    it('Equal Test', () => {
        expect(solution('link', 'hyperlink', 'https://en.wikipedia.org/wiki/Hyperlink'))
            .toEqual('[hyperlink](https://en.wikipedia.org/wiki/Hyperlink)');
        expect(solution('img', 'this should be an image', 'https://github.com/codewars/gna.jpg'))
            .toEqual('![this should be an image](https://github.com/codewars/gna.jpg)');

        const code = 'function generateMarkdowns(markdown,text,urlOrLanguage) {\n// write your code here\n};';
        const codeBlock = '```javascript\nfunction generateMarkdowns(markdown,text,urlOrLanguage) {\n// write your code here\n};\n```';
        expect(solution('code', code, 'javascript')).toEqual(codeBlock);
    });
});

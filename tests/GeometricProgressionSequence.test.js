const solution = require('../src/GeometricProgressionSequence');

describe('Geometric Progression Sequence Tests', () => {
    it('Equal Test', () => {
        expect(solution(2, 3, 5)).toEqual('2, 6, 18, 54, 162');
        expect(solution(2, 2, 10)).toEqual('2, 4, 8, 16, 32, 64, 128, 256, 512, 1024');
        expect(solution(1, -2, 10)).toEqual('1, -2, 4, -8, 16, -32, 64, -128, 256, -512');
    });
});

const solution = require('../src/GetAddressesOfAllCellsInRange');

describe('Get the Addresses of All Google Sheets Cells in the Range Tests', () => {
    it('Equal Test', () => {
        expect(solution('A1:C3')).toEqual(['A1', 'B1', 'C1', 'A2', 'B2', 'C2', 'A3', 'B3', 'C3']);
        expect(solution('A3:C1')).toEqual(['A1', 'B1', 'C1', 'A2', 'B2', 'C2', 'A3', 'B3', 'C3']);
        expect(solution('C2:C2')).toEqual([]);
        expect(solution('H10:F15')).toEqual([]);
        expect(solution('A1:C3:E5')).toEqual([]);

        expect(solution('A1:Z1')).toEqual([
            'A1', 'B1', 'C1', 'D1', 'E1',
            'F1', 'G1', 'H1', 'I1', 'J1',
            'K1', 'L1', 'M1', 'N1', 'O1',
            'P1', 'Q1', 'R1', 'S1', 'T1',
            'U1', 'V1', 'W1', 'X1', 'Y1',
            'Z1'
        ]);
        expect(solution('W118:Z124')).toEqual([
            'W118', 'X118', 'Y118', 'Z118',
            'W119', 'X119', 'Y119', 'Z119',
            'W120', 'X120', 'Y120', 'Z120',
            'W121', 'X121', 'Y121', 'Z121',
            'W122', 'X122', 'Y122', 'Z122',
            'W123', 'X123', 'Y123', 'Z123',
            'W124', 'X124', 'Y124', 'Z124'
        ]);
    });
});

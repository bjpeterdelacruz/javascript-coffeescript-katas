const solution = require('../src/GetDecimalNumber');

describe('Get Decimal Number Tests', () => {
    it('Equal Test', () => {
        expect(solution(10)).toEqual(0);
        expect(solution(4.5)).toEqual(0.5);
        expect(solution(-2.15)).toEqual(0.15);
    });
});

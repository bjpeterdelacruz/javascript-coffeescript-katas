const solution = require('../src/GetKeyValuePairsAsArrays');

describe('Get Key Value Pairs as Arrays Tests', () => {
    it('Equal Tests', () => {
        expect(solution({ d: 4, e: 5, f: 6, g: 7 })).toEqual([['d', 'e', 'f', 'g'], [4, 5, 6, 7]]);
        expect(solution({})).toEqual([[], []]);
    });
});

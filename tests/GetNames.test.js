const solution = require('../src/GetNames');

describe('Get Names Tests', () => {
    it('Equal Test', () => {
        const data1 = [
            { name: 'Joe', age: 20 },
            { name: 'Bill', age: 30 },
            { name: 'Kate', age: 23 }
        ];

        const data2 = [
            { name: 'Jane', age: 20 },
            { name: 'Sam', age: 30 }
        ];
        expect(solution(data1)).toEqual(['Joe', 'Bill', 'Kate']);
        expect(solution(data2)).toEqual(['Jane', 'Sam']);
    });
});

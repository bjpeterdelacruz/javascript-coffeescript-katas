const solution = require('../src/GrandmaLearningToText');

describe('Grandma Learning to Text Tests', () => {
    it('Equal Test', () => {
        expect(solution('It took two years to solve.')).toEqual('It 2k 2 years 2 solve.');
        expect(solution('Nothing here')).toEqual('Nothing here');
    });
});

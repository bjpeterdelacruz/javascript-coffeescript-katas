const solution = require('../src/GroupingInString');

describe('Grouping in String Tests', () => {
    it('Equal Test', () => {
        expect(solution('112200')).toEqual(true);
        expect(solution('1222334556667')).toEqual(true);
        expect(solution('001234400522')).toEqual(false);
    });
});

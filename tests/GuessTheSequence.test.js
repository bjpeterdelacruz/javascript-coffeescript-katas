const solution = require('../src/GuessTheSequence');

describe('Guess the Sequence Tests', () => {
    it('Equal Tests', () => {
        expect(solution(5)).toEqual([1, 2, 3, 4, 5]);
        expect(solution(16)).toEqual([1, 10, 11, 12, 13, 14, 15, 16, 2, 3, 4, 5, 6, 7, 8, 9]);
        expect(solution(25)).toEqual([1, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 2, 20, 21, 22, 23, 24, 25, 3, 4, 5, 6, 7, 8, 9]);
    });
});

const solution = require('../src/HTMLDynamicColorStringGeneration');

describe('How The Grinch Almost Ended Christmas Tests', () => {
    it('HTML Dynamic Color String Generation Tests', () => {
        for (let i = 0; i < 5000; i++) {
            expect(/#[A-Fa-f0-9][A-Fa-f0-9][A-Fa-f0-9][A-Fa-f0-9][A-Fa-f0-9][A-Fa-f0-9]/g.test(solution())).toEqual(true);
        }
    });
});

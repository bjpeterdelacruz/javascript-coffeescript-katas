const solution = require('../src/HarvestFestival');

describe('Harvest Festival Tests', () => {
    it('Equal Tests', () => {
        expect(solution('@', 4, 3, 23)).toEqual('----@@@----@@@----@@@----@@@');
        expect(solution('~', 1, 6, 30)).toEqual('-~~~~~~');
        expect(solution('#', 10, 2, 15)).toEqual(
            '----------------------------------------------------------------------------------------------------#');
        expect(solution('*', 2, 4, 8)).toEqual('----*');
        expect(solution('<', 2, 2, 28)).toEqual('--<<--<<');
    });
});

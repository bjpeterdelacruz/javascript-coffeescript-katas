const { head, tail, init, last } = require('../src/HeadTailInitLast');

/**
 *
 */
function randomArray() {
    const res = [Math.floor(51 * Math.random())];
    while (Math.random() < 0.85) {
        res.push(Math.floor(51 * Math.random()));
    }
    return res;
}

describe('Head Tail Init Last Tests', () => {
    it('Equal Tests', () => {
        for (let i = 0; i < 1000; i++) {
            const arr = randomArray();
            expect(head(arr)).toEqual(arr[0]);
            expect(tail(arr)).toEqual(arr.length === 1 ? [] : arr.slice(1));
            expect(init(arr)).toEqual(arr.length === 1 ? [] : arr.slice(0, arr.length - 1));
            expect(last(arr)).toEqual(arr[arr.length - 1]);
        }
    });
});

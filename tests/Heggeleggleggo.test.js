const solution = require('../src/Heggeleggleggo');

describe('Heggeleggleggo Tests', () => {
    it('Equal Tests', () => {
        expect(solution('hello')).toEqual('heggeleggleggo');
        expect(solution('code here')).toEqual('ceggodegge heggeregge');
        expect(solution('FUN KATA')).toEqual('FeggUNegg KeggATeggA');
        expect(solution('egg')).toEqual('egegggegg');
        expect(solution('Hello world')).toEqual('Heggeleggleggo weggoreggleggdegg');
        expect(solution('scrambled eggs')).toEqual('seggceggreggameggbeggleggedegg egegggeggsegg');
        expect(solution('eggy bread')).toEqual('egegggeggyegg beggreggeadegg');
    });
});

const Warrior = require('../src/HelloHappyCodevarrior');

describe('Hello Happy Codevarrior Tests', () => {
    const albert = new Warrior('Al');
    const boris  = new Warrior('Boris');
    const curt   = new Warrior('Curt');
    const hi = 'Hi! my name\'s ';

    it('should return greeting', () => {
        expect(albert.toString()).toEqual(hi + 'Al');
    });

    it('should return name', () => {
        expect(albert.name()).toEqual('Al');
    });

    it('should not change name', () => {
        expect(typeof boris.name).toEqual('function');
        expect(boris.name('Bobo')).toEqual('Bobo');
        expect(curt.name('Corto')).toEqual('Corto');
        expect(boris.name()).toEqual('Bobo');
    });

    it('should not change name to random one', () => {
        let name = (Math.random() + 1).toString(36).substring(7);
        expect(boris.name(name)).toEqual(name);
        name = (Math.random() + 1).toString(36).substring(7);
        expect(curt.name(name)).toEqual(name);
    });
});

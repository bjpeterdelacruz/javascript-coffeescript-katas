const countVegetables = require('../src/HelpSuzukiCountTheVegetables');

describe('countVegetables', () => {
    test('should count single vegetable correctly', () => {
        const result = countVegetables('carrot');
        expect(result).toEqual([[1, 'carrot']]);
    });

    test('should count multiple vegetables correctly', () => {
        const result = countVegetables('carrot potato carrot cabbage');
        expect(result).toEqual([
            [2, 'carrot'],
            [1, 'potato'],
            [1, 'cabbage']
        ]);
    });

    test('should sort vegetables by count and name correctly', () => {
        const result = countVegetables('carrot potato carrot cabbage potato cabbage cabbage');
        expect(result).toEqual([
            [3, 'cabbage'],
            [2, 'potato'],
            [2, 'carrot']
        ]);
    });

    test('should return empty array when no vegetables are present', () => {
        const result = countVegetables('apple banana orange');
        expect(result).toEqual([]);
    });

    test('should ignore non-vegetable words', () => {
        const result = countVegetables('carrot car plane potato');
        expect(result).toEqual([
            [1, 'potato'],
            [1, 'carrot']
        ]);
    });

    test('should handle empty string', () => {
        const result = countVegetables('');
        expect(result).toEqual([]);
    });

    test('should handle mixed case strings and ignore case', () => {
        const result = countVegetables('Carrot CARROT potato CABBAGE cabbage');
        expect(result).toEqual([
            [2, 'carrot'],
            [2, 'cabbage'],
            [1, 'potato']
        ]);
    });

    test('should handle strings with extra spaces', () => {
        const result = countVegetables('  carrot   potato    carrot cabbage  ');
        expect(result).toEqual([
            [2, 'carrot'],
            [1, 'potato'],
            [1, 'cabbage']
        ]);
    });
});
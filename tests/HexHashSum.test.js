const solution = require('../src/HexHashSum');

describe('Hex Hash Sum Tests', () => {
    it('Equal Tests', () => {
        expect(solution(null)).toEqual(0);
        expect(solution('')).toEqual(0);
        expect(solution('Yo')).toEqual(20);
        expect(solution('Hello, World!')).toEqual(91);
        expect(solution('Forty4Three')).toEqual(113);
    });
});

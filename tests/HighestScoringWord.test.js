const solution = require('../src/HighestScoringWord');

describe('Highest Scoring Word Tests', () => {
    it('Equal Tests', () => {
        expect(solution('man i need a taxi up to ubud')).toEqual('taxi');
        expect(solution('what time are we climbing up the volcano')).toEqual('volcano');
        expect(solution('take me to semynak')).toEqual('semynak');
        expect(solution('massage yes massage yes massage')).toEqual('massage');
        expect(solution('take two bintang and a dance please')).toEqual('bintang');
        expect(solution('aa b')).toEqual('aa');
        expect(solution('b aa')).toEqual('b');
        expect(solution('bb d')).toEqual('bb');
        expect(solution('d bb')).toEqual('d');
        expect(solution('aaa b')).toEqual('aaa');
    });
});

const solution = require('../src/HowManyConsecutiveNumbers');

describe('How Many Consecutive Numbers Tests', () => {
    it('Equal Tests', () => {
        expect(solution([4, 10, 6])).toEqual(4);
        expect(solution([1, 2, 3, 4])).toEqual(0);
        expect(solution([])).toEqual(0);
        expect(solution([1])).toEqual(0);
    });
});

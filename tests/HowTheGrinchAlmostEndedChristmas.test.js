const solution = require('../src/HowTheGrinchAlmostEndedChristmas');

describe('How The Grinch Almost Ended Christmas Tests', () => {
    it('Equal Tests', () => {
        expect(solution([
            { name: 'DASHER_V3', distance: 1000, speed: 100 },
            { name: 'DANCER_V2.3', distance: 1000, speed: 10 },
            { name: 'PRANCER_V1.2', distance: 1000, speed: 1 }
        ])).toEqual(['DASHER_V3', 'DANCER_V2.3', 'PRANCER_V1.2']);

        expect(solution([
            { name: 'VIXEN_V1.1', distance: 500, speed: 4 },
            { name: 'COMET_V4', distance: 1000, speed: 500 },
            { name: 'CUPID_V5', distance: 100, speed: 1 }
        ])).toEqual(['COMET_V4', 'CUPID_V5', 'VIXEN_V1.1']);

        expect(solution([{ name: 'PRANCER_V2', distance: 1000, speed: 5 },
            { name: 'COMET_V3', distance: 100, speed: 10 },
            { name: 'DASHER_V1.1', distance: 10000, speed: 100 },
            { name: 'CUPID_V3.5', distance: 200, speed: 2 },
            {
                name: 'VIXEN_V2.1',
                distance: 500,
                speed: 50
            }])).toEqual(['COMET_V3', 'VIXEN_V2.1', 'DASHER_V1.1', 'CUPID_V3.5', 'PRANCER_V2']);
    });
});

const solution = require('../src/IPv6Validation');

describe('IPv6 Address Validation and Contraction Tests', () => {
    it('Equal Tests', () => {
        expect(solution('2001:0470:0000:0064:0000:0000:0000:2')).toEqual('2001:470:0:64::2');
        expect(solution('2A03:2880:2130:CF05:FACE:B00C::1')).toEqual('2a03:2880:2130:cf05:face:b00c:0:1');
        expect(solution('2001:::')).toEqual(false);
        expect(solution('2607:G8B0:4010:801::1004')).toEqual(false);
        expect(solution('2001:470::76::2')).toEqual(false);
        expect(solution('2a02:0cb41:0:0:0:0:0:0:7')).toEqual(false);
        expect(solution('2620:0:863:ed1a:0:1')).toEqual(false);
        expect(solution('2600:1406:34:0:0:0::b819:3854')).toEqual(false);
        expect(solution('001:0470:0000:0064:0000:-000:0000:2')).toEqual(false);
        expect(solution('1:1:0:0:1:0:0:1')).toEqual('1:1::1:0:0:1');
        expect(solution('4B45:64:1091:90A:00:3D73:0040:90AA')).toEqual('4b45:64:1091:90a:0:3d73:40:90aa');
        expect(solution('000:0A00:0C::0:28')).toEqual('0:a00:c::28');
        expect(solution('020B:0DC:3FEE:9701:0A:00BF:50F0:000')).toEqual('20b:dc:3fee:9701:a:bf:50f0:0');
        expect(solution('08:0007:805:080:6208:4E0:04EA:00')).toEqual('8:7:805:80:6208:4e0:4ea:0');
        expect(solution('98::7300:00:00')).toEqual('98::7300:0:0');
        expect(solution('F009:077:00:0B2A:0DE8:300C:790E:4A')).toEqual('f009:77:0:b2a:de8:300c:790e:4a');
        expect(solution('B03:07DA:4090:0::4700:A8:0060')).toEqual('b03:7da:4090::4700:a8:60');
        expect(solution('000E:20A0:0:000:0E90:A:200A:5A50')).toEqual('e:20a0::e90:a:200a:5a50');
        expect(solution('000:754:80:C0B:20:0053:00:C00A')).toEqual('0:754:80:c0b:20:53:0:c00a');
        expect(solution('460:0000::06E:1F50:2023')).toEqual('460::6e:1f50:2023');
        expect(solution('0::260:EE')).toEqual('::260:ee');
        expect(solution('160::0')).toEqual('160::');
        expect(solution('::94')).toEqual('::94');
        expect(solution('1:02:003:0004:0::')).toEqual('1:2:3:4::');
        expect(solution('1234')).toEqual(false);
        expect(solution('7:0082:40:9D6:9805:E:0089:B45A')).toEqual('7:82:40:9d6:9805:e:89:b45a');
        expect(solution('2001:decaf:c0ffee::')).toEqual(false);
        expect(solution('0:0:0:0:0:0:0:0')).toEqual('::');
        expect(solution('0::')).toEqual('::');
        expect(solution('::0')).toEqual('::');
    });
});

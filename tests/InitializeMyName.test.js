const solution = require('../src/InitializeMyName');

describe('Initialize My Name Tests', () => {
    it('Equal Tests', () => {
        expect(solution('Jack')).toEqual('Jack');
        expect(solution('Jack Ryan')).toEqual('Jack Ryan');
        expect(solution('Alice Betty Davidson')).toEqual('Alice B. Davidson');
        expect(solution('Alice Betty Catherine Davidson')).toEqual('Alice B. C. Davidson');
    });
});

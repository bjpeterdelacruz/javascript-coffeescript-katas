const solution = require('../src/IntegersRecreationOne');

describe('Integers Recreation One Tests', () => {
    it('Equal Tests', () => {
        expect(solution(1, 250)).toEqual([[1, 1], [42, 2500], [246, 84100]]);
        expect(solution(42, 250)).toEqual([[42, 2500], [246, 84100]]);
        expect(solution(250, 500)).toEqual([[287, 84100]]);
    });
});

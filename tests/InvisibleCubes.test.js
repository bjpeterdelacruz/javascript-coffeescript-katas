const solution = require('../src/InvisibleCubes');

describe('Invisible Cubes Tests', () => {
    it('Equal Tests', () => {
        expect(solution(0)).toEqual(0);
        expect(solution(1)).toEqual(0);
        expect(solution(2)).toEqual(0);
        expect(solution(3)).toEqual(1);
        expect(solution(4)).toEqual(8);
        expect(solution(7)).toEqual(125);
        expect(solution(12)).toEqual(1000);
        expect(solution(18)).toEqual(4096);
    });
});

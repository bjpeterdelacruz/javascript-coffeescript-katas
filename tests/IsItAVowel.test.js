require('../src/IsItAVowel');

describe('IP Address to Number Tests', () => {
    it('Equal Tests', () => {
        expect('A'.vowel()).toEqual(true);
        expect('E'.vowel()).toEqual(true);
        expect('I'.vowel()).toEqual(true);
        expect('O'.vowel()).toEqual(true);
        expect('U'.vowel()).toEqual(true);
        expect('a'.vowel()).toEqual(true);
        expect('e'.vowel()).toEqual(true);
        expect('i'.vowel()).toEqual(true);
        expect('o'.vowel()).toEqual(true);
        expect('u'.vowel()).toEqual(true);
        expect('z'.vowel()).toEqual(false);
        expect(''.vowel()).toEqual(false);
        expect('ou'.vowel()).toEqual(false);
    });
});

const solution = require('../src/IsSortedAndHow');

describe('Is Sorted and How? Tests', () => {
    it('Equal Tests', () => {
        expect(solution([1, 2])).toEqual('yes, ascending');
        expect(solution([15, 7, 3, -8])).toEqual('yes, descending');
        expect(solution([4, 2, 30])).toEqual('no');
    });
});

const solution = require('../src/JohnAndAnneSweetDate');

describe('John and Anne\'s Sweet Date Tests', () => {
    it('Equal Tests', () => {
        expect(solution(3, 1, 7, 3, 10)).toEqual(1);
        expect(solution(3, 1, 7, 3, 20)).toEqual(2);
        expect(solution(4, 2, 7, 3, 20)).toEqual(1);
        expect(solution(4, 2, 7, 3, 30)).toEqual(3);
        expect(solution(3, 1, 7, 3, 1000)).toEqual(100);
    });
});

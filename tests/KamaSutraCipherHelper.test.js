const KamaSutraCipherHelper = require('../src/KamaSutraCipherHelper');

describe('Kama Sutra Cipher Helper Tests', () => {
    it('Equal Tests', () => {
        const cipher1 = new KamaSutraCipherHelper([
            ['d', 'p'],
            ['n', 'o'],
            ['a', 'w'],
            ['f', 'c'],
            ['h', 's'],
            ['l', 'v'],
            ['m', 'j'],
            ['x', 'b'],
            ['e', 'z'],
            ['r', 'i'],
            ['k', 'y'],
            ['u', 'q'],
            ['t', 'g']
        ]);
        expect(cipher1.encode('mutt')).toEqual('jqgg');
        expect(cipher1.decode('jqgg')).toEqual('mutt');

        const cipher2 = new KamaSutraCipherHelper([
            ['d', 'p'],
            ['n', 'o'],
            ['a', 'w'],
            ['f', 'c']
        ]);
        expect(cipher2.encode('zzzzz')).toEqual('zzzzz');
        expect(cipher2.decode('zzzzz')).toEqual('zzzzz');

        const cipher3 = new KamaSutraCipherHelper([
            ['व', 'ह'],
            ['ॠ', 'ई'],
            ['उ', 'ऌ'],
            ['ऋ', 'ए'],
            ['ओ', 'औ'],
            ['ऐ', 'आ'],
            ['अ', 'इ'],
            ['छ', 'ड'],
            ['ङ', 'प']
        ]);
        expect(cipher3.encode('वॠ')).toEqual('हई');
        expect(cipher3.decode('एऔ')).toEqual('ऋओ');
    });
});

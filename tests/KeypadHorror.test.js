const solution = require('../src/KeypadHorror');

describe('Keypad Horror Tests', () => {
    it('Equal Test', () => {
        expect(solution('0789456123')).toEqual('0123456789');
        expect(solution('000')).toEqual('000');
        expect(solution('94561')).toEqual('34567');
        expect(solution('')).toEqual('');
    });
});

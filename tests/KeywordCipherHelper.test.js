const KeywordCipher = require('../src/KeywordCipherHelper');

describe('Keyword Cipher Helper Tests', () => {
    it('Equal Tests', () => {
        const cipher1 = new KeywordCipher('abcdefghijklmnopqrstuvwxyz', 'purplepineapple');
        expect(cipher1.encode('abc')).toEqual('pur');
        expect(cipher1.decode('pur')).toEqual('abc');

        const cipher2 = new KeywordCipher('ABCDEFGHIJKLMNOPQRSTUVWXYZ', 'KEYWORD');
        expect(cipher2.encode('ABCHIJ')).toEqual('KEYABC');
        expect(cipher2.decode('KEYABC')).toEqual('ABCHIJ');
        expect(cipher2.encode('abcdefg')).toEqual('abcdefg');
        expect(cipher2.decode('abcdefg')).toEqual('abcdefg');
    });
});

const solution = require('../src/LargestProduct');

describe('Largest Product Tests', () => {
    it('Equal Tests', () => {
        expect(solution('123834539327238239583')).toEqual(3240);
        expect(solution('92494737828244222221111111532909999')).toEqual(5292);
        expect(solution('92494737828244222221111111532909999')).toEqual(5292);
        expect(solution('92494737828244222221111111532909999')).toEqual(5292);
        expect(solution('02494037820244202221011110532909999')).toEqual(0);

        expect(solution('123')).toEqual(0);
        expect(solution('12345')).toEqual(120);
    });
});

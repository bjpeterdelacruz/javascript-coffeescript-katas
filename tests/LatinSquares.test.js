const solution = require('../src/LatinSquares');

describe('Latin Squares Tests', () => {
    it('Equal Test', () => {
        const fourByFour = [
            [1, 2, 3, 4],
            [2, 3, 4, 1],
            [3, 4, 1, 2],
            [4, 1, 2, 3]
        ];
        expect(solution(4).sort()).toEqual(fourByFour.sort());
        const sevenBySeven = [
            [1, 2, 3, 4, 5, 6, 7],
            [2, 3, 4, 5, 6, 7, 1],
            [3, 4, 5, 6, 7, 1, 2],
            [4, 5, 6, 7, 1, 2, 3],
            [5, 6, 7, 1, 2, 3, 4],
            [6, 7, 1, 2, 3, 4, 5],
            [7, 1, 2, 3, 4, 5, 6]
        ];
        expect(solution(7).sort()).toEqual(sevenBySeven.sort());
    });
});

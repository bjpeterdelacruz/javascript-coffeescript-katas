const solution = require('../src/LibraryOpeningTimes');

describe('Library Opening Times Tests', () => {
    it('Equal Test', () => {
        expect(solution('Monday 09:30')).toEqual('Library closes at 20:00');
        expect(solution('Saturday 00:00')).toEqual('Library opens: today 10:00');
        expect(solution('Tuesday 20:00')).toEqual('Library opens: Wednesday 08:00');
        expect(solution('MoNDay 07:59')).toEqual('Library opens: today 08:00');
        expect(solution('Tuesday 13:61')).toEqual('Invalid time!');
        expect(solution('wednsay 12:40')).toEqual('Invalid time!');
        expect(solution('Saturday 18:00')).toEqual('Library opens: Sunday 12:00');
        expect(solution('Saturday 24:00')).toEqual('Invalid time!');
    });
});

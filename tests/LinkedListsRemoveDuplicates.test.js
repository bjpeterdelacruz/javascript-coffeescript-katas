const { removeDuplicates, Node } = require('../src/LinkedListsRemoveDuplicates');

describe('Linked Lists - Remove Duplicates Tests', () => {
    it('Equal Test', () => {
        expect(removeDuplicates(null)).toBeNull();
        expect(removeDuplicates(new Node(23))).toEqual(new Node(23));

        const list1 = new Node(23);
        list1.next = new Node(23);
        list1.next.next = new Node(23);
        expect(removeDuplicates(list1)).toEqual(new Node(23));

        const list2 = new Node(23);
        list2.next = new Node(24);
        list2.next.next = new Node(25);
        expect(removeDuplicates(list2)).toEqual(list2);

        const list3 = new Node(23);
        list3.next = new Node(23);
        list3.next.next = new Node(23);
        list3.next.next.next = new Node(24);
        list3.next.next.next.next = new Node(24);

        const expected = new Node(23);
        expected.next = new Node(24);
        expect(removeDuplicates(list3)).toEqual(expected);
    });
});

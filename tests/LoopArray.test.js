const solution = require('../src/LoopArray');

describe('Loop Array Tests', () => {
    it('Equal Test', () => {
        expect(solution([1, 5, 87, 45, 8, 8], 'left', 2)).toEqual([87, 45, 8, 8, 1, 5]);
        expect(solution([1, 5, 87, 45, 8, 8], 'right', 2)).toEqual([8, 8, 1, 5, 87, 45]);
    });
});

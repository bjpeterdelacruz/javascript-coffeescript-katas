const { symmetricDifference, isSquareNumber, makeMatrix } = require('../src/MakeAMatrix');

describe('Make a Matrix Tests', () => {
    it('Equal Test', () => {
        expect(symmetricDifference([1, 2, 3], [3, 4, 5])).toEqual([1, 2, 4, 5]);

        expect(isSquareNumber(7)).toEqual(false);
        expect(isSquareNumber(9)).toEqual(true);

        expect(makeMatrix([0, 1, 2, 3, 4, 5], [5, 6, 7, 8, 9])).toEqual([[0, 1, 2], [3, 4, 6], [7, 8, 9]]);
        expect(makeMatrix([1, 2, 3, 4, 5], [5, 6, 7, 8, 9])).toEqual([[1, 2], [3, 4]]);
        expect(makeMatrix([1, 2, 3, 4, 5], [2, 3, 4, 5, 6])).toEqual([[1]]);
        expect(makeMatrix([1, 2, 3, 4, 5], [1, 2, 3, 4, 5])).toEqual([]);
    });
});

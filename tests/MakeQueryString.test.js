const solution = require('../src/MakeQueryString');

describe('Make Query String Tests', () => {
    it('Equal Test', () => {
        expect(solution({ foo: 1, bar: 2 })).toEqual('foo=1&bar=2');
        expect(solution({ foo: 1, bar: [2, 3] })).toEqual('foo=1&bar=2&bar=3');
    });
});

const solution = require('../src/MakeTheSmallWordsBig');

describe('Make the Small Words Big Tests', () => {
    it('Equal Test', () => {
        expect(solution('The quick brown fox jumps over the lazy dog')).toEqual('THE qck brwn FOX jmps vr THE lzy DOG');
        expect(solution('I\'m just a small word from a small word family')).toEqual('I\'M jst A smll wrd frm A smll wrd fmly');
        expect(solution('It\'s the job that\'s never started as takes longest to finish')).toEqual('t\'s THE JOB tht\'s nvr strtd AS tks lngst TO fnsh');
        expect(solution('To be, or not to be: that is the question')).toEqual('TO BE, OR NOT TO BE: tht IS THE qstn');
    });
});

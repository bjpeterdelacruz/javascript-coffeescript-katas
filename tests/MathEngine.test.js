const solution = require('../src/MathEngine');

describe('Math Engine Tests', () => {
    it('Equal Test', () => {
        expect(solution([1, 2, 3, -4, -5])).toEqual(-3);
        expect(solution([2])).toEqual(2);
        expect(solution([-5])).toEqual(-4);
        expect(solution([0, -5])).toEqual(-5);
        expect(solution([1, 2, 3])).toEqual(6);
        expect(solution([1, 0, 3, -4, -5])).toEqual(-9);
        expect(solution([])).toEqual(1);
        expect(solution(null)).toEqual(0);
        expect(solution([0])).toEqual(0);
        expect(solution([1, 2, 3, -2, -4])).toEqual(0);
        expect(solution([-2, -3])).toEqual(-4);
    });
});

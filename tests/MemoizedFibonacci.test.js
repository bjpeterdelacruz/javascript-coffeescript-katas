const solution = require('../src/MemoizedFibonacci');

describe('Memoized Fibonacci Tests', () => {
    it('Equal Test', () => {
        expect(solution(70)).toEqual(190392490709135);
        expect(solution(60)).toEqual(1548008755920);
        expect(solution(50)).toEqual(12586269025);
    });
});

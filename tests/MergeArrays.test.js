const solution = require('../src/MergeArrays');

describe('Merge Arrays Tests', () => {
    it('Equal Test', () => {
        expect(solution([1, 2, 3], ['a', 'b', 'c', 'd', 'e'])).toEqual([1, 'a', 2, 'b', 3, 'c', 'd', 'e']);
        expect(solution([], [1, 2, 3])).toEqual([1, 2, 3]);
        expect(solution([1, 2, 3], [])).toEqual([1, 2, 3]);
        expect(solution([1, 2, 3], ['x', 'y', 'z'])).toEqual([1, 'x', 2, 'y', 3, 'z']);
    });
});

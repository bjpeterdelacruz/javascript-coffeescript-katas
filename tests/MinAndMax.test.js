const solution = require('../src/MinAndMax');

describe('Min and Max Tests', () => {
    it('Equal Test', () => {
        expect(solution(500, 505, 10)).toEqual([505, 505]);
        expect(solution(100, 200, 10)).toEqual([109, 190]);
        expect(solution(123, 456, 5)).toEqual([131, 410]);
        expect(solution(99, 501, 5)).toEqual([104, 500]);
        expect(solution(99, 234, 1)).toEqual([100, 100]);
        expect(solution(99, 234, 19)).toEqual([199, 199]);
        expect(solution(99, 5001, 27)).toEqual([999, 4995]);
        expect(solution(99, 5001, 28)).toEqual([1999, 4996]);
        expect(solution(2000, 7000, 3)).toEqual([2001, 3000]);
    });
});

const solution = require('../src/MirrorMirrorOnTheWall');

const tests = [
    { input: [], expected: [] },
    { input: [1], expected: [1] },
    { input: [2, 1], expected: [1, 2, 1] },
    { input: [2, 3, 1], expected: [1, 2, 3, 2, 1] },
    { input: [-8, 42, 18, 0, -16], expected: [-16, -8, 0, 18, 42, 18, 0, -8, -16] }
];

describe('Mirror, Mirror, On the Wall Tests', () => {
    it('Equal Test', () => {
        for (const test of tests) {
            expect(solution(test.input)).toEqual(test.expected);
        }
    });
});

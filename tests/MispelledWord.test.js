const solution = require('../src/MispelledWord');

describe('Mispelled Word Tests', () => {
    it('Equal Test', () => {
        expect(solution('versed', 'xersed')).toEqual(true);
        expect(solution('versed', 'applb')).toEqual(false);
        expect(solution('versed', 'v5rsed')).toEqual(true);
        expect(solution('1versed', 'versed')).toEqual(true);

        expect(solution('versed', 'versed1')).toEqual(true);
        expect(solution('aaversed', 'versed')).toEqual(false);
        expect(solution('1versed', 'v5rsed')).toEqual(false);
    });
});

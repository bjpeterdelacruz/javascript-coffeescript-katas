const solution = require('../src/MissingAlphabets');

describe('Missing Alphabets Tests', () => {
    it('Equal Test', () => {
        expect(solution('abcdefghijklmnopqrstuvwxy')).toEqual('z');
        expect(solution('abcdefghijklmnopqrstuvwxyz')).toEqual('');
        expect(solution('aabbccddeeffgghhiijjkkllmmnnooppqqrrssttuuvvwwxxyy')).toEqual('zz');
        expect(solution('abbccddeeffgghhiijjkkllmmnnooppqqrrssttuuvvwwxxy')).toEqual('ayzz');
        expect(solution('codewars')).toEqual('bfghijklmnpqtuvxyz');
    });
});

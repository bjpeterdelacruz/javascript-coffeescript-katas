const solution = require('../src/MostValuableCharacter');

describe('Most Valuable Character Tests', () => {
    it('Equal Test', () => {
        expect(solution('a')).toEqual('a');
        expect(solution('aa')).toEqual('a');
        expect(solution('bcd')).toEqual('b');
        expect(solution('axyzxyz')).toEqual('x');
        expect(solution('dcbadcba')).toEqual('a');
        expect(solution('aabccc')).toEqual('c');
        expect(solution('efgefg')).toEqual('e');
        expect(solution('efghijefghi')).toEqual('e');
        expect(solution('acefacef')).toEqual('a');
        expect(solution('acefacefacef')).toEqual('a');
    });
});

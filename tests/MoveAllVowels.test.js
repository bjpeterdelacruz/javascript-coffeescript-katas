const solution = require('../src/MoveAllVowels');

describe('Move All Vowels Tests', () => {
    it('Equal Test', () => {
        expect(solution('day')).toEqual('dya');
        expect(solution('apple')).toEqual('pplae');
        expect(solution('peace')).toEqual('pceae');
        expect(solution('maker')).toEqual('mkrae');
        expect(solution('programming')).toEqual('prgrmmngoai');
        expect(solution('javascript')).toEqual('jvscrptaai');
        expect(solution('python')).toEqual('pythno');
        expect(solution('ruby')).toEqual('rbyu');
        expect(solution('haskell')).toEqual('hskllae');
        expect(solution('clojure')).toEqual('cljroue');
    });
});

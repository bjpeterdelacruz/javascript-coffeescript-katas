const solution = require('../src/MultipleOfIndex');

describe('Multiple of Index Tests', () => {
    it('Equal Test', () => {
        expect(solution([22, -6, 32, 82, 9, 25])).toEqual([-6, 32, 25]);
        expect(solution([68, -1, 1, -7, 10, 10])).toEqual([-1, 10]);
        expect(solution([11, -11])).toEqual([-11]);
        expect(solution([-56, -85, 72, -26, -14, 76, -27, 72, 35, -21, -67, 87, 0, 21, 59, 27, -92, 68])).toEqual([-85, 72, 0, 68]);
        expect(solution([28, 38, -44, -99, -13, -54, 77, -51])).toEqual([38, -44, -99]);
        expect(solution([-1, -49, -1, 67, 8, -60, 39, 35])).toEqual([-49, 8, -60, 35]);
    });
});

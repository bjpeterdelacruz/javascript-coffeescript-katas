const solution = require('../src/MultiplyWordInString');

describe('Multiply Word in String Tests', () => {
    it('Equal Tests', () => {
        expect(solution('This is a string', 3, 5)).toEqual('string-string-string-string-string');
        expect(solution('Creativity is the process of having original ideas that have value. It is a process; it\'s not random.', 8, 10)).toEqual('that-that-that-that-that-that-that-that-that-that');
        expect(solution('Self-control means wanting to be effective at some random point in the infinite radiations of my spiritual existence', 1, 1)).toEqual('means');
        expect(solution('Is sloppiness in code caused by ignorance or apathy? I don\'t know and I don\'t care.', 6, 8)).toEqual('ignorance-ignorance-ignorance-ignorance-ignorance-ignorance-ignorance-ignorance');
        expect(solution('Everything happening around me is very random. I am enjoying the phase, as the journey is far more enjoyable than the destination.', 2, 5)).toEqual('around-around-around-around-around');
    });
});

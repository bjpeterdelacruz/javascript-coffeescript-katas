const solution = require('../src/MultiplyingStringsAsNumbersPartII');

describe('Multiplying Strings as Numbers Part II Tests', () => {
    it('Equal Tests', () => {

        expect(solution('2', '3')).toEqual('6');
        expect(solution('30', '69')).toEqual('2070');
        expect(solution('11', '85')).toEqual('935');

        expect(solution('.11', '-2')).toEqual('-0.22');
        expect(solution('.11', '.85')).toEqual('0.0935');
        expect(solution('.1', '.002')).toEqual('0.0002');
        expect(solution('1', '.0002')).toEqual('0.0002');
        expect(solution('0.25', '0.4')).toEqual('0.1');
        expect(solution('0.90', '0.9')).toEqual('0.81');
        expect(solution('.5', '20')).toEqual('10');

        expect(solution('-0.00', '0.0000')).toEqual('0');
        expect(solution('-0.01', '0.0000')).toEqual('0');
        expect(solution('2.01', '3.0000')).toEqual('6.03');
        expect(solution('2', '-3.000001')).toEqual('-6.000002');
        expect(solution('-5.0908', '-123.1')).toEqual('626.67748');

        expect(solution('2', '3')).toEqual('6');
        expect(solution('30', '69')).toEqual('2070');
        expect(solution('11', '85')).toEqual('935');
        expect(solution('2', '0')).toEqual('0');
        expect(solution('0', '30')).toEqual('0');
        expect(solution('0000001', '3')).toEqual('3');
        expect(solution('1009', '03')).toEqual('3027');
        expect(solution('98765', '56894')).toEqual('5619135910');
        expect(solution('-0010203030048756473.66210', '27745376262008574736326.2761300000000')).toEqual('-283086907715328055255654708118725434244.516915673');
        expect(solution('586084736227728377283728.27', '7586374672263726736.374')).toEqual('4446258398718405600244891754243162055662141.09298');
        expect(solution('9007199254740991', '9007199254740991')).toEqual('81129638414606663681390495662081');
        expect(solution(
            '1340586741446208561415076401547656465529763608271147629314168593373' +
            '0764670623871593614308087232270425896609011907191074144041104905681',
            '7889836825523689737523844996601262120950664406362073445706614896191' +
            '3958679342222168229104018536613339605800723430772103944554687793460')).toEqual(
            '105770106404711015832388555213103828682974044337230384563472032555734830186886' +
                '851955791910923057941646911656656063739572710461083397852009280193992648681629' +
                '282768214143952558750069010328643648576989388834888824785122416195400972053959' +
                '6432961225189466793536575308646260');
    });
});

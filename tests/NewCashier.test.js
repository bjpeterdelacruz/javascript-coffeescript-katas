const solution = require('../src/NewCashier');

describe('New Cashier Tests', () => {
    it('Equal Tests', () => {
        expect(solution('milkshakepizzachickenfriescokeburgerpizzasandwichmilkshakepizza')).toEqual(
            'Burger Fries Chicken Pizza Pizza Pizza Sandwich Milkshake Milkshake Coke');
        expect(solution('pizzachickenfriesburgercokemilkshakefriessandwich')).toEqual(
            'Burger Fries Fries Chicken Pizza Sandwich Milkshake Coke');
    });
});

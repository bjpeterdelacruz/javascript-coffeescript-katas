const solution = require('../src/NextBirthdayOfTheWeekFinder');

describe('Next Birthday of the Week Tests', () => {
    it('Equal Tests', () => {
        expect(solution(new Date(1990, 10, 16))).toEqual(11);
        expect(solution(new Date(2012, 5, 20))).toEqual(6);
        expect(solution(new Date(1975, 2, 22))).toEqual(5);
    });
});

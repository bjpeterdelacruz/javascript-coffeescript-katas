const solution = require('../src/NiceArray');

describe('Next Birthday of the Week Tests', () => {
    it('Equal Tests', () => {
        expect(solution([])).toEqual(false);
        expect(solution([4, 2, 3])).toEqual(true);
        expect(solution([2, 3, 5, 4])).toEqual(true);
        expect(solution([2, 2, 3, 3])).toEqual(true);
        expect(solution([4, 2, 1])).toEqual(false);
        expect(solution([2, 3, 6, 4])).toEqual(false);
    });
});

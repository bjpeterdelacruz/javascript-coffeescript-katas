const solution = require('../src/NoLoops2YouOnlyNeedOne');

describe('No Loops 2 - You Only Need One Tests', () => {
    it('Equal Test', () => {
        expect(solution([1, 2, 3, 4, 5], 5)).toEqual(true);
        expect(solution([1, 2, 3, 4, 5], 10)).toEqual(false);
    });
});

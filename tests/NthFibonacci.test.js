const solution = require('../src/NthFibonacci');

describe('Nth Fibonacci Tests', () => {
    it('Equal Test', () => {
        expect(solution(1)).toEqual(0);
        expect(solution(2)).toEqual(1);
        expect(solution(3)).toEqual(1);
        expect(solution(4)).toEqual(2);
        expect(solution(5)).toEqual(3);
        expect(solution(6)).toEqual(5);
        expect(solution(7)).toEqual(8);
        expect(solution(8)).toEqual(13);
        expect(solution(9)).toEqual(21);
        expect(solution(10)).toEqual(34);
    });
});

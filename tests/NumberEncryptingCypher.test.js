const solution = require('../src/NumberEncryptingCypher');

describe('Number Encrypting Cypher Tests', () => {
    it('Equal Test', () => {
        expect(solution('Hello World')).toEqual('H3110 W0r1d');
        expect(solution('I am your father')).toEqual('1 4m y0ur f47h3r');
        expect(solution('I do not know what else I can test. Be cool. Good luck')).toEqual(
            '1 d0 n07 kn0w wh47 3153 1 c4n 7357. 83 c001. 600d 1uck');
        expect(solution('IlRzEeAaSsGbTtBgOo')).toEqual('112233445566778900');
    });
});

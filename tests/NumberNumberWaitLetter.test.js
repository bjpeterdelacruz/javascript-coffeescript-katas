const solution = require('../src/NumberNumberWaitLetter');

describe('Number Number Wait Letter Tests', () => {
    it('Equal Test', () => {
        expect(solution('24z6 1z23 y369 89z 900b')).toEqual(1414);
        expect(solution('24z6 1x23 y369 89a 900b')).toEqual(1299);
        expect(solution('10a 90x 14b 78u 45a 7b 34y')).toEqual(60);
        expect(solution('111a 222c 444y 777u 999a 888p')).toEqual(1459);
        expect(solution('1z 2t 3q 5x 6u 8a 7b')).toEqual(8);
    });
});

const solution = require('../src/NumberOfCarries');

describe('Number of Carries Tests', () => {
    it('Equal Test', () => {
        expect(solution(543, 3456)).toEqual(0);
        expect(solution(1927, 6426)).toEqual(2);
        expect(solution(9999, 1)).toEqual(4);
        expect(solution(1234, 5678)).toEqual(2);
    });
});

const solution = require('../src/NumberWithThreeRoots');

describe('Number with Three Roots Tests', () => {
    it('Equal Test', () => {
        expect(solution(256)).toEqual(true);
        expect(solution(1000)).toEqual(false);
        expect(solution(6561)).toEqual(true);
        expect(solution(12534)).toEqual(false);
    });
});

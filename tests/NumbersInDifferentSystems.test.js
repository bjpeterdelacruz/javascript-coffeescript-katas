const solution = require('../src/NumbersInDifferentSystems');

describe('Number in Different Systems Tests', () => {
    it('Equal Test', () => {
        expect(solution(5, 2)).toEqual(101);
        expect(solution(5, 8)).toEqual(5);
        expect(solution(250, 2)).toEqual(11111010);
        expect(solution(250, 16)).toEqual('fa');
        expect(solution(135, 2)).toEqual(10000111);
        expect(solution(879, 8)).toEqual(1557);
        expect(solution(3894, 16)).toEqual('f36');
        expect(solution(0, 2)).toEqual(0);
        expect(solution(0, 8)).toEqual(0);
        expect(solution(0, 10)).toEqual(0);
        expect(solution(0, 16)).toEqual(0);
        expect(solution(0, 16)).toEqual(0);

        expect(solution(127)).toEqual(127);
        expect(solution(212855789, 16)).toEqual('cafebed');
    });
});

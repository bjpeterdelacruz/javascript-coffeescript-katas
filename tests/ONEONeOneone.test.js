const solution = require('../src/ONEONeOneone');

describe('ONE ONe One one Tests', () => {
    it('Equal Test', () => {
        expect(solution([1, 1, 0, 0, 0, 1, 1, 1, 0, 1, 0])).toEqual(3);
        expect(solution([1, 1, 0, 0, 1])).toEqual(2);
        expect(solution([1, 1, 1, 1, 1])).toEqual(5);
        expect(solution([0, 0, 0, 0, 0])).toEqual(0);
        expect(solution([0, 0, 0, 0, 1])).toEqual(1);
    });
});

const solution = require('../src/OddOrEvenDetermineThat');

describe('Odd or Even? Determine That! Tests', () => {
    it('Equal Test', () => {
        expect(solution(1)).toEqual('Either');
        expect(solution(6)).toEqual('Odd');
        expect(solution(8)).toEqual('Even');
        expect(solution(0)).toEqual('Even');
        expect(solution(2)).toEqual('Odd');
        expect(solution(3)).toEqual('Either');
        expect(solution(4)).toEqual('Even');
        expect(solution(5)).toEqual('Either');
        expect(solution(7)).toEqual('Either');
        expect(solution(9)).toEqual('Either');
        expect(solution(10)).toEqual('Odd');
        expect(solution(11)).toEqual('Either');
        expect(solution(12)).toEqual('Even');
    });
});

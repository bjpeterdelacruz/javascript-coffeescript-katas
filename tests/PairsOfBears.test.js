const solution = require('../src/PairsOfBears');

describe('Pairs of Bears Tests', () => {
    it('Equal Test', () => {
        expect(solution(7, '8j8mBliB8gimjB8B8jlB')).toEqual(['B8B8B8', false]);
        expect(solution(3, '88Bifk8hB8BB8BBBB888chl8BhBfd')).toEqual(['8BB8B8B88B', true]);
        expect(solution(8, '8')).toEqual(['', false]);
    });
});

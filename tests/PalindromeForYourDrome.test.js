const solution = require('../src/PalindromeForYourDrome');

describe('Palindrome for Your Drome Tests', () => {
    it('Equal Test', () => {
        expect(solution('')).toEqual(true);
        expect(solution('A')).toEqual(true);

        expect(solution('101')).toEqual(true);
        expect(solution('911')).toEqual(false);

        expect(solution('racecar')).toEqual(true);
        expect(solution('kayak')).toEqual(true);
        expect(solution('tarot')).toEqual(false);

        expect(solution('noon')).toEqual(true);
        expect(solution('redder')).toEqual(true);
        expect(solution('rabbit')).toEqual(false);

        expect(solution('RotaTion')).toEqual(false);
        expect(solution('RotaTor')).toEqual(true);

        expect(solution('Abba Zabba, you\'re my only friend')).toEqual(false);
        expect(solution('Amore, Roma')).toEqual(true);
        expect(solution('A man, a plan, a canal - Panama')).toEqual(true);
        expect(solution('No \'x\' in \'Nixon\'')).toEqual(true);
        expect(solution('Sore was I ere I saw Eros')).toEqual(true);

        expect(solution('Underscores_  Serocsrednu_')).toEqual(true);
        expect(solution('Underscores_  _underscoreS')).toEqual(false);
        expect(solution('dont_drop_that_durka_durkrud_akrud_taht_pord_tnod')).toEqual(true);
        expect(solution('dont_drop_that_durka_durk')).toEqual(false);

        expect(solution('Semicolon; Nolocimes')).toEqual(true);
        expect(solution('d-a-s-h hsad')).toEqual(true);
        expect(solution('dont_drop_that_durka_durkrud_akrud_taht_pord_tnod')).toEqual(true);
        expect(solution('dont_drop_that_durka_durk')).toEqual(false);
        expect(solution('Eva: Can I see bees in a cave?')).toEqual(true);
        expect(solution('Madam? I\'m Adam!')).toEqual(true);
    });
});

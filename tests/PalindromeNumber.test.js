const solution = require('../src/PalindromeNumber');

describe('Palindrome Number Tests', () => {
    it('Equal Test', () => {
        expect(solution(7)).toEqual(true);
        expect(solution(77)).toEqual(true);
        expect(solution(-121)).toEqual(false);
        expect(solution(12321)).toEqual(true);
        expect(solution(1232)).toEqual(false);
    });
});

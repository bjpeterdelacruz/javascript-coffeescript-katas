const solution = require('../src/ParseIntegersInArray');

describe('Parse Integers in Array Tests', () => {
    it('Equal Test', () => {
        expect(solution([])).toEqual([]);
        expect(solution(['1', '2', '3'])).toEqual([1, 2, 3]);
    });
});

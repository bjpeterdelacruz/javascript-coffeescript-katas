const solution = require('../src/PenguinOlympics');

describe('Penguin Olympics 1 Tests', () => {
    it('3 Lanes Test', () => {
        const snapshot = `|----p---~---------|
        |----p---~~--------|
        |----p---~~~-------|`;

        const penguins = ['Derek', 'Francis', 'Bob'];

        const expected = 'GOLD: Derek, SILVER: Francis, BRONZE: Bob';

        expect(solution(snapshot, penguins)).toEqual(expected);
    });
});

describe('Penguin Olympics 2 Tests', () => {
    it('4 Lanes Test', () => {
        const snapshot = `|-~~------------~--p-------|
        |~~--~p------------~-------|
        |--------~-p---------------|
        |--------~-p----~~~--------|`;

        const penguins = ['Joline', 'Abigail', 'Jane', 'Gerry'];

        const expected = 'GOLD: Joline, SILVER: Jane, BRONZE: Gerry';

        expect(solution(snapshot, penguins)).toEqual(expected);
    });
});

describe('Penguin Olympics 3 Tests', () => {
    it('Capital Penguin Test', () => {
        const snapshot = `|-~~------------~--P-------|
        |~~--~P------------~-------|
        |--------~-P---------------|
        |--------~-P----~~~--------|`;

        const penguins = ['Joline', 'Abigail', 'Jane', 'Gerry'];

        const expected = 'GOLD: Joline, SILVER: Jane, BRONZE: Gerry';

        expect(solution(snapshot, penguins)).toEqual(expected);
    });
});

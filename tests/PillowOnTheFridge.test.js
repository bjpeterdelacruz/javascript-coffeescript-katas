const solution = require('../src/PillowOnTheFridge');

describe('Pillow on the Fridge Tests', () => {
    it('Equal Test', () => {
        expect(solution(['EvH/KNikBiyxfeyK/miCMj', 'I/HwjnHlFLlahMOKNadps'])).toEqual(false);
        expect(solution(['\\DjQ\\[zv]SpG]Z/[Qm\\eLL', 'amwZArsaGRmibriXBgTRZp'])).toEqual(false);
        expect(solution([ 'n', 'B' ])).toEqual(true);
        expect(solution(['yF[CeAAiNihWEmKxJc/NRMVn', 'rMeIa\\KAfbjuLiTnAQxNw[XB'])).toEqual(true);
        expect(solution(['inECnBMAA/u', 'ABAaIUOUx/M'])).toEqual(true);
        expect(solution([ 'n', 'aB' ])).toEqual(false);
    });
});

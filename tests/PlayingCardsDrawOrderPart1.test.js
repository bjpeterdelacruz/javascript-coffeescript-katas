const solution = require('../src/PlayingCardsDrawOrderPart1');

describe('Playing Cards Draw Order – Part 1 Tests', () => {
    it('Equal Test', () => {
        const FULL_DECK = [
            'AC', '2C', '3C', '4C', '5C', '6C', '7C', '8C', '9C', 'TC', 'JC', 'QC', 'KC', // Clubs
            'AD', '2D', '3D', '4D', '5D', '6D', '7D', '8D', '9D', 'TD', 'JD', 'QD', 'KD', // Diamonds
            'AH', '2H', '3H', '4H', '5H', '6H', '7H', '8H', '9H', 'TH', 'JH', 'QH', 'KH', // Hearts
            'AS', '2S', '3S', '4S', '5S', '6S', '7S', '8S', '9S', 'TS', 'JS', 'QS', 'KS' // Spades
        ];

        const expected = [
            'AC', '3C', '5C', '7C', '9C', 'JC', 'KC', '2D', '4D', '6D', '8D', 'TD', 'QD',
            'AH', '3H', '5H', '7H', '9H', 'JH', 'KH', '2S', '4S', '6S', '8S', 'TS', 'QS',
            '2C', '6C', 'TC', 'AD', '5D', '9D', 'KD', '4H', '8H', 'QH', '3S', '7S', 'JS',
            '4C', 'QC', '7D', '2H', 'TH', '5S', 'KS', '3D', '6H', '9S', 'JD', '8C', 'AS'
        ];

        expect(solution([...FULL_DECK])).toEqual(expected);

        expect(solution(['AC', 'KS'])).toEqual(['AC', 'KS']);
        expect(solution([])).toEqual([]);

        const deck = ['KC', 'KH', 'QC', 'KS', 'KD', 'QH', 'QD', 'QS'];

        expect(solution([...deck])).toEqual(['KC', 'QC', 'KD', 'QD', 'KH', 'QH', 'KS', 'QS']);
    });
});

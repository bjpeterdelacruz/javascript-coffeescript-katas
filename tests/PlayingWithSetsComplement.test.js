const solution = require('../src/PlayingWithSetsComplement');

describe('Playing with Sets: Complement Tests', () => {
    it('Equal Test', () => {
        const A = new Set([1, 2, 3, 4]);
        const B = new Set([1, 3, 5, 7]);
        const AB = new Set([2, 4]);
        const BA = new Set([5, 7]);
        const E = new Set();

        expect(solution(A, A)).toEqual(E);
        expect(solution(A, B)).toEqual(AB);
        expect(solution(B, A)).toEqual(BA);
        expect(solution(A, E)).toEqual(A);
        expect(solution(B, E)).toEqual(B);
        expect(solution(A, B) instanceof Set).toEqual(true);
    });
});

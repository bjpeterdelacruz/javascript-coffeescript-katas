const solution = require('../src/PlayingWithSetsUnion');

describe('Playing with Sets: Union Tests', () => {
    it('Equal Test', () => {
        expect(solution(new Set([1, 2]), new Set([]))).toEqual(new Set([1, 2]));
        expect(solution(new Set([1, 2]), new Set([1, 2]))).toEqual(new Set([1, 2]));
        expect(solution(new Set([1, 2]), new Set([2, 3]))).toEqual(new Set([1, 2, 3]));
        expect(solution(new Set([1, 2]), new Set([2, 3])) instanceof Set).toEqual(true);
    });
});

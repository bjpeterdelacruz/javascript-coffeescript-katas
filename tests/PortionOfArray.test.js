const solution = require('../src/PortionOfArray');

describe('Portion of Array Tests', () => {
    it('Equal Test', () => {
        expect(solution([1, 2, 3, 4], 3, 1)).toEqual([4]);
        expect(solution([1, 2, 3, 4], 3, 2)).toEqual(-1);
        expect(solution([1, 2, 3, 4], 1, 3)).toEqual([2, 3, 4]);
        expect(solution([1, 2, 3, 4], 1, 4)).toEqual(-1);
        expect(solution([1, 2, 3, 4], -1, 2)).toEqual([2, 3]);
        expect(solution([1, 2, 3, 4], -1, 4)).toEqual(-1);
    });
});

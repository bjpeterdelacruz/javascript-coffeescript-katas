const solution = require('../src/Power');

describe('Power Tests', () => {
    it('Equal Test', () => {
        expect(solution(5, 0)).toEqual(1);
        expect(solution(5, 2)).toEqual(25);
        expect(solution(5, 10)).toEqual(Math.pow(5, 10));
        expect(solution(5, -5)).toEqual(1 / (5 * 5 * 5 * 5 * 5));
    });
});

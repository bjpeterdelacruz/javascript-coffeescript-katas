const solution = require('../src/ProductID');

describe('Product ID from URL Tests', () => {
    it('Equal Test', () => {
        expect(solution('http://www.exampleshop.com/goop-candle-p-23985467-31051997.html')).toEqual('23985467');
        expect(solution('http://www.exampleshop.com/letter-p-book-stand-p-192837-11112011.html')).toEqual('192837');
    });
});

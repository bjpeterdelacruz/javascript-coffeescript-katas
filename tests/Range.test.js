const solution = require('../src/Range');

describe('Range Tests', () => {
    it('Equal Test', () => {
        expect(solution(10)).toEqual([0, 1, 2, 3, 4, 5, 6, 7, 8, 9]);
        expect(solution(1, 11)).toEqual([1, 2, 3, 4, 5, 6, 7, 8, 9, 10]);
        expect(solution(0, 30, 5)).toEqual([0, 5, 10, 15, 20, 25]);
        expect(solution(1, 4, 0)).toEqual([1, 1, 1]);
        expect(solution(0)).toEqual([]);
        expect(solution(10, 0)).toEqual([]);
        expect(solution(10, 15, -5)).toEqual([]);
        expect(solution(15, 10, 5)).toEqual([]);
        expect(solution(15, 10, -2)).toEqual([15, 13, 11]);
    });
});

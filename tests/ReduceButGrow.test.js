const solution = require('../src/ReduceButGrow');

describe('Reduce But Grow Tests', () => {
    it('Equal Tests', () => {
        expect(solution([1, 2, 3])).toEqual(6);
        expect(solution([4, 1, 1, 1, 4])).toEqual(16);
        expect(solution([2, 2, 2, 2, 2, 2])).toEqual(64);
    });
});

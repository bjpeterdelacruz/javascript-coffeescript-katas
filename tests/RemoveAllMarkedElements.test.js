require('../src/RemoveAllMarkedElements');

describe('Remove All Marked Elements Tests', () => {
    it('Equal Test', () => {
        const arr = new Array();
        expect(arr.remove_([1, 1, 1, 2, 2, 3, 4, 5, 6], [1, 2, 6])).toEqual([3, 4, 5]);
        expect(arr.remove_(['a', 'b', 'a', 'c'], ['a'])).toEqual(['b', 'c']);
    });
});

const solution = require('../src/RemoveExclamationMarks');

describe('Remove Exclamation Marks Tests', () => {
    it('Equal Test', () => {
        expect(solution('Hi! Hi!!! Bye! Bye')).toEqual('Hi Hi Bye Bye');
        expect(solution('Hello World')).toEqual('Hello World');
    });
});

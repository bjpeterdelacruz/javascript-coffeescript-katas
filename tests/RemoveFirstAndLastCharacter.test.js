const solution = require('../src/RemoveFirstAndLastCharacter');

describe('Remove First and Last Character Tests', () => {
    it('Equal Test', () => {
        expect(solution('Hello World')).toEqual('ello Worl');
        expect(solution('Him')).toEqual('i');
        expect(solution('HW')).toEqual('');
    });
});

const solution = require('../src/ReplaceAllDots');

describe('Replace All Dots Tests', () => {
    it('Equal Test', () => {
        expect(solution('1.2.3.4')).toEqual('1-2-3-4');
        expect(solution('1234')).toEqual('1234');
    });
});

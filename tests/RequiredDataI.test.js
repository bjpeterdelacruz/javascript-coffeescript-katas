const solution = require('../src/RequiredDataI');

describe('Required Data I Tests', () => {
    it('Equal Test', () => {
        expect(solution([-3, -2, -1, 3, 4, -5, -5, 5, -1, -5])).toEqual([10, 7, 5, [[-5], 3]]);
        expect(solution([5, -1, 1, -1, -2, 5, 0, -2, -5, 3])).toEqual([10, 7, 4, [[-2, -1, 5], 2]]);
        expect(solution([-2, 4, 4, -2, -2, -1, 3, 5, -5, 5])).toEqual([10, 6, 3, [[-2], 3]]);
        expect(solution([4, -5, 1, -5, 2, 4, -1, 4, -1, 1])).toEqual([10, 5, 1, [[4], 3]]);
        expect(solution([4, 4, 2, -3, 1, 4, 3, 2, 0, -5, 2, -2, -2, -5])).toEqual([14, 8, 4, [[2, 4], 3]]);
    });
});

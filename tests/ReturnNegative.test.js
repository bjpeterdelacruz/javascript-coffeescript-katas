const solution = require('../src/ReturnNegative');

describe('Return Negative Tests', () => {
    it('Equal Test', () => {
        expect(solution(-10)).toEqual(-10);
        expect(solution(-1)).toEqual(-1);
        expect(solution(0)).toEqual(0);
        expect(solution(1)).toEqual(-1);
        expect(solution(10)).toEqual(-10);
    });
});

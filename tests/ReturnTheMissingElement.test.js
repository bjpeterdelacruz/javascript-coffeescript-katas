const solution = require('../src/ReturnTheMissingElement');

describe('Return the Missing Element Tests', () => {
    it('Equal Test', () => {
        expect(solution([0, 5, 1, 3, 2, 9, 7, 6, 4])).toEqual(8);
        expect(solution([9, 2, 4, 5, 7, 0, 8, 6, 1])).toEqual(3);
        expect(solution([1, 2, 3, 4, 6, 7, 8, 9, 0])).toEqual(5);
    });
});

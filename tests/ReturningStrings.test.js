const solution = require('../src/ReturningStrings');

describe('Returning Strings Tests', () => {
    it('Equal Test', () => {
        expect(solution('Matcha Udon')).toEqual('Hello, Matcha Udon how are you doing today?');
    });
});

const solution = require('../src/ReverseAll');

describe('Reverse All Tests', () => {
    it('Equal Test', () => {
        expect(solution(['abc123!@#\n314159', '987bbc\n#$%\nxyz886'])).toEqual(['zyx688\n%$#\n789cbb', '951413\ncba321#@!']);
        expect(solution(['JOVmG$&&?@\nJcE91632PXFhMAsRli\n30620DOXPZoO149\n5513??%-&@YUpyCmRUAO', '_@!?#^15915736199076984']))
            .toEqual(['^#?!@_48967099163751951', '3155@&-%??OAURmCypUY\n02603OoZPXOD941\nEcJ23619ilRsAMhFXP\nGmVOJ@?&&$']);
    });
});

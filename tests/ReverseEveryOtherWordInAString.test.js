const solution = require('../src/ReverseEveryOtherWordInAString');

describe('Reverse Every Other Word In A String Tests', () => {
    it('Equal Test', () => {
        expect(solution('Did it work?')).toEqual('Did ti work?');
        expect(solution('  I really hope   it   works this time...  ')).toEqual('I yllaer hope ti works siht time...');
        expect(solution('Reverse this string, please!')).toEqual('Reverse siht string, !esaelp');
        expect(solution('   ')).toEqual('');
    });
});

const solution = require('../src/ReverseFactorial');

describe('Reverse Factorial Tests', () => {
    it('Equal Test', () => {
        expect(solution(0)).toEqual('None');
        expect(solution(1)).toEqual('1!');
        expect(solution(120)).toEqual('5!');
        expect(solution(24)).toEqual('4!');
        expect(solution(150)).toEqual('None');
    });
});

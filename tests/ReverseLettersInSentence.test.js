const solution = require('../src/ReverseLettersInSentence');

describe('Reverse Letters in Sentence Tests', () => {
    it('Equal Test', () => {
        expect(solution('Hi Mom')).toEqual('iH moM');
        expect(solution('Super')).toEqual('repuS');
        expect(solution(' A quick  brown fox ')).toEqual(' A kciuq  nworb xof ');
    });
});

const solution = require('../src/ReverseNumber');

describe('Reverse Number Tests', () => {
    it('Equal Test', () => {
        expect(solution(5)).toEqual(5);
        expect(solution(256)).toEqual(652);
        expect(solution(1000)).toEqual(1);
        expect(solution(-615)).toEqual(-516);
        expect(solution(-2.15)).toEqual(-51.2);
    });
});

const solution = require('../src/ReverseNumberInAnyBase');

function test([n, b, out]) {
    expect(solution(n, b)).toEqual(out);
}

describe('Reverse Number in Any Base Tests', () => {
    it('Equal Test', () => {
        [[12n, 2n, 3n], [10n, 5n, 2n], [45n, 30n, 451n], [3n, 4n, 3n], [5n, 1n, 5n]]
            .forEach(test);
    });
});

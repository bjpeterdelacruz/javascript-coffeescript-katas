const solution = require('../src/RockPaperScissors');

describe('Rock Paper Scissors! Tests', () => {
    it('Player 1 Wins', () => {
        expect(solution('rock', 'scissors')).toEqual('Player 1 won!');
        expect(solution('scissors', 'paper')).toEqual('Player 1 won!');
        expect(solution('paper', 'rock')).toEqual('Player 1 won!');
    });

    it('Player 2 Wins', () => {
        expect(solution('scissors', 'rock')).toEqual('Player 2 won!');
        expect(solution('paper', 'scissors')).toEqual('Player 2 won!');
        expect(solution('rock', 'paper')).toEqual('Player 2 won!');
    });

    it('Draw', () => {
        expect(solution('rock', 'rock')).toEqual('Draw!');
        expect(solution('scissors', 'scissors')).toEqual('Draw!');
        expect(solution('paper', 'paper')).toEqual('Draw!');
    });
});

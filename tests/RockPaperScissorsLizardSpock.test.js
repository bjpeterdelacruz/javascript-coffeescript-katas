const solution = require('../src/RockPaperScissorsLizardSpock');

describe('Rock Paper Scissors Lizard Spock Tests', () => {
    it('Player 1 Wins', () => {
        expect(solution('scissors', 'paper')).toEqual('Player 1 Won!');
        expect(solution('scissors', 'lizard')).toEqual('Player 1 Won!');
        expect(solution('paper', 'rock')).toEqual('Player 1 Won!');
        expect(solution('paper', 'spock')).toEqual('Player 1 Won!');
        expect(solution('rock', 'lizard')).toEqual('Player 1 Won!');
        expect(solution('rock', 'scissors')).toEqual('Player 1 Won!');
        expect(solution('lizard', 'spock')).toEqual('Player 1 Won!');
        expect(solution('lizard', 'paper')).toEqual('Player 1 Won!');
        expect(solution('spock', 'scissors')).toEqual('Player 1 Won!');
        expect(solution('spock', 'rock')).toEqual('Player 1 Won!');
    });

    it('Player 2 Wins', () => {
        expect(solution('paper', 'scissors')).toEqual('Player 2 Won!');
        expect(solution('lizard', 'scissors')).toEqual('Player 2 Won!');
        expect(solution('rock', 'paper')).toEqual('Player 2 Won!');
        expect(solution('spock', 'paper')).toEqual('Player 2 Won!');
        expect(solution('lizard', 'rock')).toEqual('Player 2 Won!');
        expect(solution('scissors', 'rock')).toEqual('Player 2 Won!');
        expect(solution('spock', 'lizard')).toEqual('Player 2 Won!');
        expect(solution('paper', 'lizard')).toEqual('Player 2 Won!');
        expect(solution('scissors', 'spock')).toEqual('Player 2 Won!');
        expect(solution('rock', 'spock')).toEqual('Player 2 Won!');
    });

    it('Draw', () => {
        expect(solution('paper', 'paper')).toEqual('Draw!');
    });
});

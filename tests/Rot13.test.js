const solution = require('../src/Rot13');

describe('Rot13 Tests', () => {
    it('Equal Test', () => {
        expect(solution('EBG13 rknzcyr.')).toEqual('ROT13 example.');
    });
});

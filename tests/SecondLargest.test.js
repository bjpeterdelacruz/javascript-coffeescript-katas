/* eslint no-sparse-arrays: "off" */

const { isNumber, secondLargest } = require('../src/SecondLargest');

describe('Second Largest in Array Tests', () => {
    it('Equal Test', () => {
        expect(secondLargest(null)).toEqual(undefined);
        expect(secondLargest([-32, 11, 43, 55, 0, 11])).toEqual(43);
        expect(secondLargest([0, 1, '2', 3])).toEqual(2);
        expect(secondLargest([5, 5, 5, 5, 5, 5])).toEqual(undefined);
        expect(secondLargest(['-1', 2, null, false])).toEqual(-1);
        expect(secondLargest('32 11 44 56')).toEqual(undefined);
        expect(secondLargest([2, true, 0])).toEqual(0);
        expect(secondLargest([,,,, 1, 2, 3])).toEqual(2);
        expect(secondLargest([12, 'four', 2, 3, 1, -33])).toEqual(3);

        expect(isNumber(null)).toEqual(false);
        expect(isNumber(undefined)).toEqual(false);
        expect(isNumber()).toEqual(false);
        expect(isNumber('35c')).toEqual(false);
        expect(isNumber('35')).toEqual(true);
        expect(isNumber(35)).toEqual(true);
    });
});

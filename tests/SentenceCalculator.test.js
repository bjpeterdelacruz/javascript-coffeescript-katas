const solution = require('../src/SentenceCalculator');

describe('Sentence Calculator Tests', () => {
    it('Equal Tests', () => {
        expect(solution('I Love You')).toEqual(170);
        expect(solution('ILoveYou')).toEqual(170);
        expect(solution('AREYOUHUNGRY?')).toEqual(356);
        expect(solution('oops, i did it again!')).toEqual(152);
        expect(solution('Give me 5!')).toEqual(73);
        expect(solution('Give me five!')).toEqual(110);
        expect(solution('cML 8E2PXXOI7xYjy57F!NdwuYbBL0,4BRe2oWN36O0UmH7q.DddKAVdT73yt?AAkJej wrjIU1i90C!lI!s')).toEqual(1237);
        expect(solution('Y,K5?mk?LKe0!BRDoCIf!T?ffJxPMqSfTn i5JmPxiC7sfNSjjxq3,zict118XI1GHBMLPN0FIGZBo.F5!2Tm')).toEqual(1260);
    });
});

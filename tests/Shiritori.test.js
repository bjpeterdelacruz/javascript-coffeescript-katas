const solution = require('../src/Shiritori');

describe('Shiritori Tests', () => {
    it('Equal Tests', () => {
        expect(solution(['dog', 'goose', 'elephant', 'tiger', 'rhino', 'orc', 'cat']))
            .toEqual(['dog', 'goose', 'elephant', 'tiger', 'rhino', 'orc', 'cat']);
        expect(solution(['dog', 'goose', 'tiger', 'cat', 'elephant', 'rhino', 'orc'])).toEqual(['dog', 'goose']);
        expect(solution([])).toEqual([]);
        expect(solution(['', '', '', '', '', ''])).toEqual([]);
        expect(solution(['ab', 'bc', '', 'de', '', '', ''])).toEqual(['ab', 'bc']);
        expect(solution(['ab', 'bc', '', 'de', '', '', ''])).toEqual(['ab', 'bc']);
        expect(solution(['ab', 'cd', 'de'])).toEqual(['ab']);
        expect(solution(['ab'])).toEqual(['ab']);
    });
});

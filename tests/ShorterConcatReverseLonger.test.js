const solution = require('../src/ShorterConcatReverseLonger');

describe('Shortest Concat Reverse Longer Tests', () => {
    it('Equal Tests', () => {
        expect(solution('first', 'abcde')).toEqual('abcdetsrifabcde');
        expect(solution('abcd', 'longest')).toEqual('abcdtsegnolabcd');
        expect(solution('hello', 'hi')).toEqual('hiollehhi');
    });
});

const solution = require('../src/ShuffledArray');

describe('Shuffled Array Tests', () => {
    it('Equal Tests', () => {
        expect(solution([1, 12, 3, 6, 2])).toEqual([1, 2, 3, 6]);
        expect(solution([2, -1, 2, 2, -1])).toEqual([-1, -1, 2, 2]);
        expect(solution([-3, -3])).toEqual([-3]);
        expect(solution([])).toEqual([]);
    });
});

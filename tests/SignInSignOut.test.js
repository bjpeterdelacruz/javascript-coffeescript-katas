const solution = require('../src/SignInSignOut');

describe('Sign In Sign Out Tests', () => {
    it('Equal Tests', () => {
        expect(solution('foo')).toEqual(NaN);
        expect(solution(NaN)).toEqual(NaN);
        expect(solution()).toEqual(NaN);
        expect(solution(0)).toEqual(0);
        expect(solution('0')).toEqual(0);
        expect(solution(Number.POSITIVE_INFINITY)).toEqual(1);
        expect(solution(Number.NEGATIVE_INFINITY)).toEqual(-1);
        expect(solution(0.0001)).toEqual(1);
        expect(solution('0.0001')).toEqual(1);
        expect(solution(1e10)).toEqual(1);
        expect(solution('-1e6')).toEqual(-1);
        expect(solution(-3)).toEqual(-1);
        expect(solution('-3')).toEqual(-1);
        expect(solution(10)).toEqual(1);
        expect(solution('10')).toEqual(1);
        expect(solution(10 - [2])).toEqual(1);
        expect(solution([8])).toEqual(1);
        expect(solution([-42])).toEqual(-1);
        expect(solution(['-42'])).toEqual(-1);
        expect(solution(['42'])).toEqual(1);
        expect(solution([42, 50])).toEqual(NaN);
    });
});

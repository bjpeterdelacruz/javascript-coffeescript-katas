const solution = require('../src/SimpleFun15AdditionWithoutCarrying');

describe('Simple Fun #15 - Addition Without Carrying Tests', () => {
    it('Equal Test', () => {
        expect(solution(456, 1734)).toEqual(1180);
        expect(solution(99999, 0)).toEqual(99999);
        expect(solution(999, 999)).toEqual(888);
        expect(solution(0, 0)).toEqual(0);
    });
});

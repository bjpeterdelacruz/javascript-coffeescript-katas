const solution = require('../src/SimpleFun29IsMAC48Address');

describe('Simple Fun #29 - Is MAC-48 Address? Tests', () => {
    it('Equal Test', () => {
        expect(solution('00-1B-63-84-45-E6')).toEqual(true);
        expect(solution('Z1-1B-63-84-45-E6')).toEqual(false);
        expect(solution('hello world')).toEqual(false);
        expect(solution('FF-FF-FF-FF-FF-FF')).toEqual(true);
        expect(solution('00-00-00-00-00-00')).toEqual(true);
        expect(solution('G0-00-00-00-00-00')).toEqual(false);
        expect(solution('12-34-56-78-9A-BC')).toEqual(true);
        expect(solution('02-03-04-05-06-07-')).toEqual(false);
    });
});

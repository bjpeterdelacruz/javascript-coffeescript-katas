const solution = require('../src/SimpleFun85IsInformationConsistent');

describe('Simple Fun #85 - Is Information Consistent Tests', () => {
    it('Equal Test', () => {
        let evidences = [
            [0, 1, 0, 1],
            [-1, 1, 0, 0],
            [-1, 0, 0, 1]];
        expect(solution(evidences)).toEqual(true);

        evidences = [
            [1, 0],
            [-1, 0],
            [1, 1],
            [1, 1]];
        expect(solution(evidences)).toEqual(false);

        evidences = [
            [1, -1, 0, 1],
            [1, -1, 0, -1]];
        expect(solution(evidences)).toEqual(false);

        evidences = [
            [0, 0, -1],
            [-1, 1, -1],
            [-1, 1, 0],
            [0, 1, 0]];
        expect(solution(evidences)).toEqual(true);
    });
});

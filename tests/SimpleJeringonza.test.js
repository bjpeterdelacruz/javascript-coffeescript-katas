const solution = require('../src/SimpleJeringonza');

describe('Simple Jeringonza Tests', () => {
    it('Equal Test', () => {
        expect(solution('jeringonza')).toEqual('jeperipingoponzapa');
        expect(solution('jEringonzA')).toEqual('jEPEripingoponzAPA');
        expect(solution('Played in Spain and across Latin America'))
            .toEqual('Plapayeped ipin Spapaipin apand apacroposs Lapatipin APAmeperipicapa');
        expect(solution('The quick brown fox jumped over the lazy dog'))
            .toEqual('Thepe qupuipick bropown fopox jupumpeped opoveper thepe lapazy dopog');
        expect(solution('Please give this kata some love if you enjoyed it'))
            .toEqual('Plepeapasepe gipivepe thipis kapatapa sopomepe lopovepe ipif yopoupu epenjopoyeped ipit');
    });
});

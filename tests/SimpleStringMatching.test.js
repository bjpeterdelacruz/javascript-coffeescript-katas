const solution = require('../src/SimpleStringMatching');

describe('Simple String Matching Tests', () => {
    it('Equal Test', () => {
        expect(solution('code*s', 'codewars')).toEqual(true);
        expect(solution('codewar*s', 'codewars')).toEqual( true);
        expect(solution('code*warrior', 'codewars')).toEqual(false);
        expect(solution('c', 'c')).toEqual(true);
        expect(solution('*s', 'codewars')).toEqual(true);
        expect(solution('*s', 's')).toEqual( true);
        expect(solution('s*', 's')).toEqual(true);
        expect(solution('aa', 'aaa')).toEqual( false);
        expect(solution('aa*', 'aaa')).toEqual( true);
        expect(solution('aaa', 'aa')).toEqual( false);
        expect(solution('aaa*', 'aa')).toEqual( false);
        expect(solution('*', 'codewars')).toEqual(true);
        expect(solution('aaa*aaa', 'aaa')).toEqual(false);
    });
});

const solution = require('../src/SimpleStringReversal');

describe('Simple String Reversal Tests', () => {
    it('Equal Tests', () => {
        expect(solution('foo')).toEqual('oof');
        expect(solution('matcha udon')).toEqual('noduah ctam');
        expect(solution('our   \t   code')).toEqual('edo   \t   cruo');
    });
});

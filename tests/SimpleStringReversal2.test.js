const solution = require('../src/SimpleStringReversal2');

describe('Simple String Reversal 2 Tests', () => {
    it('Equal Tests', () => {
        expect(solution('codewars', 1, 5)).toEqual('cawedors');
        expect(solution('codingIsFun', 2, 100)).toEqual('conuFsIgnid');
        expect(solution('FunctionalProgramming', 2, 15)).toEqual('FuargorPlanoitcnmming');
        expect(solution('abcdefghijklmnopqrstuvwxyz', 0, 20)).toEqual('utsrqponmlkjihgfedcbavwxyz');
        expect(solution('abcdefghijklmnopqrstuvwxyz', 5, 20)).toEqual('abcdeutsrqponmlkjihgfvwxyz');
    });
});

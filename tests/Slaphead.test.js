const solution = require('../src/Slaphead');

describe('Slaphead Tests', () => {
    it('Equal Tests', () => {
        expect(solution('---------')).toEqual(['---------', 'Clean!']);
        expect(solution('/---------')).toEqual(['----------', 'Unicorn!']);
        expect(solution('/-----/-')).toEqual(['--------', 'Homer!']);
        expect(solution('--/--/---/-----')).toEqual(['---------------', 'Careless!']);
        expect(solution('--/--/---/-/---')).toEqual(['---------------', 'Careless!']);
        expect(solution('--/--/---/-//--')).toEqual(['---------------', 'Careless!']);
        expect(solution('//////////')).toEqual(['----------', 'Hobo!']);
    });
});

const solution = require('../src/SortAndTransform');

describe('Sort and Transform Tests', () => {
    it('Equal Test', () => {
        expect(solution([121, 122, 123, 124, 125, 120, 122, 132])).toEqual('yzz-xy}-}yx-xy}');
        expect(solution([111, 112, 113, 114, 115, 113, 114, 110])).toEqual('oprn-nors-sron-nors');
        expect(solution([51, 62, 73, 84, 95, 100, 99, 126])).toEqual('3>c~-3>d~-~d>3-3>d~');
        expect(solution([66, 101, 55, 111, 113])).toEqual('Beoq-7Boq-qoB7-7Boq');
        expect(solution([78, 117, 110, 99, 104, 117, 107, 115, 120, 121, 125])).toEqual('Nuy}-Ncy}-}ycN-Ncy}');
        expect(solution([101, 48, 75, 105, 99, 107, 121, 122, 124])).toEqual('e0z|-0Kz|-|zK0-0Kz|');
        expect(solution([80, 117, 115, 104, 65, 85, 112, 115, 66, 76, 62])).toEqual('PuL>->Asu-usA>->Asu');
        expect(solution([91, 100, 111, 121, 51, 62, 81, 92, 63])).toEqual('[d\\?-3>oy-yo>3-3>oy');
        expect(solution([78, 93, 92, 98, 108, 119, 116, 100, 85, 80])).toEqual('N]UP-NPtw-wtPN-NPtw');
        expect(solution([111, 121, 122, 124, 125, 126, 117, 118, 119, 121, 122, 73])).toEqual('oyzI-Io}~-~}oI-Io}~');
        expect(solution([82, 98, 72, 71, 71, 72, 62, 67, 68, 115, 117, 112, 122, 121, 93])).toEqual('Rby]->Cyz-zyC>->Cyz');
        expect(solution([99, 98, 97, 96, 81, 82, 82])).toEqual('cbRR-QRbc-cbRQ-QRbc');
        expect(solution([66, 99, 88, 122, 123, 110])).toEqual('Bc{n-BXz{-{zXB-BXz{');
        expect(solution([66, 87, 98, 59, 57, 50, 51, 52])).toEqual('BW34-23Wb-bW32-23Wb');
    });
});

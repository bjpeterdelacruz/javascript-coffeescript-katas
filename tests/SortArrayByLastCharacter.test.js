const solution = require('../src/SortArrayByLastCharacter');

describe('Sort Array by Last Character Tests', () => {
    it('Equal Test', () => {
        expect(solution(['acvd', 'bcc'])).toEqual(['bcc', 'acvd']);
        expect(solution(['14', '13'])).toEqual(['13', '14']);
        expect(solution(['asdf', 'asdf', '14', '13'])).toEqual(['13', '14', 'asdf', 'asdf']);
        expect(solution(['bsde', 'asdf', 14, '13'])).toEqual(['13', 14, 'bsde', 'asdf']);
        expect(solution(['asdf', 14, '13', 'asdf'])).toEqual(['13', 14, 'asdf', 'asdf']);
        expect(solution(['xxxf', 'aaaf', 'kf', 'f', 'ooooof'])).toEqual(['xxxf', 'aaaf', 'kf', 'f', 'ooooof']);
        expect(solution(['xdxf', 'xcxf', 'xbxf', 'xaxf'])).toEqual(['xdxf', 'xcxf', 'xbxf', 'xaxf']);
        expect(solution(['xdxf', 'xcxa', 'xbxf', 'xaxf'])).toEqual(['xcxa', 'xdxf', 'xbxf', 'xaxf']);
    });
});

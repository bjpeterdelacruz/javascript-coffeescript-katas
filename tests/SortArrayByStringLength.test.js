const solution = require('../src/SortArrayByStringLength');

describe('Sort Array by String Length Tests', () => {
    it('Equal Test', () => {
        expect(solution(['abc', 'a', 'ab'])).toEqual(['a', 'ab', 'abc']);
        expect(solution(['xyz', 'abcd', 'ab', 'a'])).toEqual(['a', 'ab', 'xyz', 'abcd']);
    });
});

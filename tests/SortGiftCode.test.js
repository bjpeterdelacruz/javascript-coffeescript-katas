const solution = require('../src/SortGiftCode');

describe('Sort Gift Code Tests', () => {
    it('Equal Tests', () => {
        expect(solution('afbecd')).toEqual('abcdef');
        expect(solution('pqksuvy')).toEqual('kpqsuvy');
        expect(solution('zyxwvutsrqponmlkjihgfedcba')).toEqual('abcdefghijklmnopqrstuvwxyz');
    });
});

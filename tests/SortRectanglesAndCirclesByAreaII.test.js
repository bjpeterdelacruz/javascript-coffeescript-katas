const solution = require('../src/SortRectanglesAndCirclesByAreaII');

describe('Sort Rectangles and Circles by Area II Tests', () => {
    it('Equal Tests', () => {
        expect(solution([[4.23, 6.43], 1.23, 3.444, [1.342, 3.212]])).toEqual([[1.342, 3.212], 1.23, [4.23, 6.43], 3.444 ]);
        expect(solution([[2, 5], 6])).toEqual([[2, 5], 6]);
        expect(solution([])).toEqual([]);
    });
});

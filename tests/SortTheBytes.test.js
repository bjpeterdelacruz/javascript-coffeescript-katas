const solution = require('../src/SortTheBytes');

describe('Sort the Bytes Tests', () => {
    it('Equal Tests', () => {
        expect(solution(0xdeadbeef)).toEqual(0xefdebead);
        expect(solution(0xdadb0d)).toEqual(0xdbda0d00);
    });
});

const solution = require('../src/SortTheVowels');

describe('Sort the Vowels Tests', () => {
    it('Equal Tests', () => {
        expect(solution('Codewars')).toEqual('C|\n|o\nd|\n|e\nw|\n|a\nr|\ns|');
        expect(solution('Rnd Te5T')).toEqual('R|\nn|\nd|\n |\nT|\n|e\n5|\nT|');
        expect(solution(1337)).toEqual('');
        expect(solution(undefined)).toEqual('');
    });
});

const solution = require('../src/SplitAnArray');

describe('Split an Array Tests', () => {
    it('Equal Tests', () => {
        let test1 = { prop: true };
        let test2 = { prop: false };

        let values = solution([test1, test2], 'prop');

        expect(values[0].length).toEqual(1);
        expect(values[1].length).toEqual(1);
        expect(values[0][0]).toEqual(test1);
        expect(values[1][0]).toEqual(test2);

        test1 = { test: true };
        test2 = { test: false };
        const test3 = { test: [] };
        const test4 = { test: 0 };
        const test5 = { test: 'great' };

        values = solution([test1, test2, test3, test4, test5], 'prop');

        expect(values[0].length).toEqual(0);
        expect(values[1].length).toEqual(5);

        values = solution([test1, test2, test3, test4, test5], 'test');

        expect(values[0].length).toEqual(3);
        expect(values[1].length).toEqual(2);
        expect(values[0][0]).toEqual(test1);
        expect(values[1][0]).toEqual(test2);
        expect(values[0][1]).toEqual(test3);
        expect(values[1][1]).toEqual(test4);
        expect(values[0][2]).toEqual(test5);

        values = solution([test1, test3, test5], 'test');

        expect(values[0].length).toEqual(3);
        expect(values[1].length).toEqual(0);
        expect(values[0][0]).toEqual(test1);
        expect(values[0][1]).toEqual(test3);
        expect(values[0][2]).toEqual(test5);

        values = solution([test2, test4], 'test');

        expect(values[0].length).toEqual(0);
        expect(values[1].length).toEqual(2);
        expect(values[1][0]).toEqual(test2);
        expect(values[1][1]).toEqual(test4);

        expect(solution([test1, test2])).toEqual([[], [test1, test2]]);
        expect(solution([test1, test2], null)).toEqual([[], [test1, test2]]);
        expect(solution([test1, test2], undefined)).toEqual([[], [test1, test2]]);
    });
});

const solution = require('../src/StatisticsForAnAthleticAssociation');

describe('Statistics for an Athletic Association Tests', () => {
    it('Equal Test', () => {
        expect(solution('')).toEqual('');
        expect(solution('01|15|59')).toEqual('Range: 00|00|00 Average: 01|15|59 Median: 01|15|59');
        expect(solution('10|15|00, 10|20|00, 10|25|00, 10|30|00')).toEqual('Range: 00|15|00 Average: 10|22|30 Median: 10|22|30');
        expect(solution('01|15|59, 1|47|6, 01|17|20, 1|32|34, 2|3|17')).toEqual('Range: 00|47|18 Average: 01|35|15 Median: 01|32|34');
        expect(solution('01|15|59, 1|47|16, 01|17|20, 1|32|34, 2|17|17')).toEqual('Range: 01|01|18 Average: 01|38|05 Median: 01|32|34');
        expect(solution('02|15|59, 2|47|16, 02|17|20, 2|32|34, 2|17|17, 2|22|00, 2|31|41')).toEqual('Range: 00|31|17 Average: 02|26|18 Median: 02|22|00');
    });
});

const solution = require('../src/StonesOnTheTable');

describe('Stones on the Table Tests', () => {
    it('Equal Test', () => {
        expect(solution('RRGGBB')).toEqual(3);
        expect(solution('RGBRGB')).toEqual(0);
        expect(solution('BGRBBGGBRRR')).toEqual(4);
        expect(solution('GBBBGGRRGRB')).toEqual(4);
        expect(solution('GBRGGRBBBBRRGGGB')).toEqual(7);
    });
});

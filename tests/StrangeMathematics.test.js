const solution = require('../src/StrangeMathematics');

describe('Strange Mathematics Tests', () => {
    it('Equal Test', () => {
        expect(solution(11, 3)).toEqual(5);
        expect(solution(6, 3)).toEqual(3);
    });
});

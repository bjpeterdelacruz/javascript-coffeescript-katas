const solution = require('../src/StringBreakers');

describe('String Breakers Tests', () => {
    it('Equal Test', () => {
        expect(solution(5, 'This is an example string')).toEqual('Thisi'+'\n'+'sanex'+'\n'+'ample'+'\n'+'strin'+'\n'+'g');
        expect(solution(5, 'hello hello hello')).toEqual('hello'+'\n'+'hello'+'\n'+'hello');
        expect(solution(5, 'hi hi')).toEqual('hihi');
    });
});

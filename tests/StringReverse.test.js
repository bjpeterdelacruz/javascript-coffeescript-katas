require('../src/StringReverse');

describe('String Reverse Tests', () => {
    it('Equal Test', () => {
        expect('ABCabcDEFdef'.reverse()).toEqual('fedFEDcbaCBA');
        expect('Hello world!'.reverse()).toEqual('!dlrow olleH');
    });
});

const solution = require('../src/StringToInt');

describe('String to Integer Conversion Tests', () => {
    it('Equal Test', () => {
        expect(solution([])).toEqual('NaN');
        expect(solution(undefined)).toEqual('NaN');
        expect(solution('5 abc')).toEqual('NaN');
        expect(solution('   5   ')).toEqual(5);
        expect(solution('123')).toEqual(123);
        expect(solution('123.45')).toEqual('NaN');
        expect(solution('-12345')).toEqual('NaN');
        expect(solution('123 45')).toEqual('NaN');
    });
});

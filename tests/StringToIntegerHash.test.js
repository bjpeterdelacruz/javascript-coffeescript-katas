const { hashCode, pow32 } = require('../src/StringToIntegerHash');

describe('String to Integer Hash Tests', () => {
    it('Equal Test', () => {
        expect(hashCode('hello world')).toEqual(1794106052);
        expect(hashCode('hello dave')).toEqual(-1605272966);
        expect(hashCode('Codewars is awesome!!!')).toEqual(718208364);

        expect(pow32(50000, 2)).toEqual(-1794967296);
    });
});

const solution = require('../src/SubstringInstanceCount');

describe('Substring Instance Count Tests', () => {
    it('Equal Test', () => {
        expect(solution('aa_bb_cc_bb_ee', 'b')).toEqual(4);
        expect(solution('aa_bb_cc_bb_ee', 'bb')).toEqual(2);
        expect(solution('aa_bb_cc_dd_ee', 'f')).toEqual(0);
        expect(solution('aa_bb_cc_dd_ee')).toEqual(0);
    });
});

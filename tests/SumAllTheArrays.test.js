const solution = require('../src/SumAllTheArrays');

describe('Sum All the Arrays Tests', () => {
    it('Equal Test', () => {
        expect(solution([1, 2, 3, 4])).toEqual(10);
        expect(solution([1, [2, [3]], 4, [5, [6, [4]]]])).toEqual(25);
        expect(solution([1, ['2', ['3']], 4, [5, ['6', 4]]])).toEqual(14);
    });
});

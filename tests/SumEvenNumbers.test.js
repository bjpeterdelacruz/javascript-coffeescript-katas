const solution = require('../src/SumEvenNumbers');

describe('Sum Even Numbers Tests', () => {
    it('Equal Test', () => {
        expect(solution([])).toEqual(0);
        expect(solution([1, 3])).toEqual(0);
        expect(solution([1, 3, 2, 5, 4, 7, 9, 6, 10, 12, 11])).toEqual(34);
    });
});

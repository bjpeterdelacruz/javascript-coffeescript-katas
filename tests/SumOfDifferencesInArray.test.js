const solution = require('../src/SumOfDifferencesInArray');

describe('Sum of Differences in Array Tests', () => {
    it('Equal Tests', () => {
        expect(solution([1, 2, 10])).toEqual(9);
        expect(solution([-3, -2, -1])).toEqual(2);
        expect(solution([1, 1, 1, 1, 1])).toEqual(0);
        expect(solution([-17, 17])).toEqual(34);
        expect(solution([])).toEqual(0);
        expect(solution([0])).toEqual(0);
        expect(solution([-1])).toEqual(0);
        expect(solution([1])).toEqual(0);
    });
});

const solution = require('../src/SumOfOddCubedNumbers');

describe('Sum of Odd Cubed Numbers Tests', () => {
    it('Equal Tests', () => {
        expect(solution([1, 0, 3, 2, 4])).toEqual(28);
        expect(solution([1, 2, 3, undefined])).toEqual(undefined);
        expect(solution([0, 1, '3', 4, 2])).toEqual(28);
        expect(solution(['a', 1, 0, 3, 2])).toEqual(undefined);
        expect(solution([-1, 3, 1, 4, 2])).toEqual(27);
        expect(solution([-1, -5])).toEqual(-126);
        expect(solution([-1, '0.2'])).toEqual(undefined);
    });
});

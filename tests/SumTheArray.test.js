require('../src/SumTheArray');

describe('Sum The Array Tests', () => {
    it('Equal Tests', () => {
        expect([1, 2, 3, 4, 5].sum()).toEqual(15);
        expect([].sum()).toEqual(0);
    });
});

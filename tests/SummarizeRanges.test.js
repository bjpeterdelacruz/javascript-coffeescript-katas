const solution = require('../src/SummarizeRanges');

describe('Summarize Ranges Tests', () => {
    it('Equal Test', () => {
        expect(solution([])).toEqual([]);
        expect(solution([1, 1, 1, 1])).toEqual(['1']);
        expect(solution([1, 2, 3, 4])).toEqual(['1->4']);
        expect(solution([0, 1, 2, 5, 6, 9])).toEqual(['0->2', '5->6', '9']);
        expect(solution([0, 1, 2, 3, 3, 3, 4, 5, 6, 7])).toEqual(['0->7']);
        expect(solution([0, 1, 2, 3, 3, 3, 4, 4, 5, 6, 7])).toEqual(['0->7']);
        expect(solution([0, 1, 2, 3, 3, 3, 4, 4, 5, 6, 7, 7, 9, 9, 10])).toEqual(['0->7', '9->10']);
        expect(solution([-2, 0, 1, 2, 3, 3, 3, 4, 4, 5, 6, 7, 7, 9, 9, 10, 12])).toEqual(['-2', '0->7', '9->10', '12']);
    });
});

const solution = require('../src/SymbolsCounted');

describe('Symbols Counted Tests', () => {
    it('Equal Tests', () => {
        expect(solution('otorhinolaryngological')).toEqual('o5tr2hi2n2l3a2yg2c');
        expect(solution('immunoelectrophoretically')).toEqual('i2m2uno3e3l3c2t2r2phay');
        expect(solution('psychophysicotherapeutics')).toEqual('p3s3y2c3h3o2i2t2e2rau');
        expect(solution('thyroparathyroidectomized')).toEqual('t3h2y2r3o3pa2i2d2e2cmz');
        expect(solution('pneumoencephalographically')).toEqual('p3n2e3umo2c2h2a3l3griy');
        expect(solution('radioimmunoelectrophoresis')).toEqual('r3adi3o4m2une3lctphs2');
        expect(solution('psychoneuroendocrinological')).toEqual('psyc3ho5n3e2ur2di2l2ga');
        expect(solution('hepaticocholangiogastrostomy')).toEqual('h2epa3t3i2c2o5lng2s2rmy');
        expect(solution('spectrophotofluorometrically')).toEqual('sp2e2c2t3r3o5hfl3umiay');
        expect(solution('pseudopseudohypoparathyroidism')).toEqual('p4s3e2u2d3o4h2y2a2r2ti2m');
    });
});

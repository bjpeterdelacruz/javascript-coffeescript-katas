const NameMe = require('../src/ThisIsAProblem');

require('../src/ThisIsAProblem');

describe('This Is a Problem Tests', () => {
    it('Equal Tests', () => {
        const nameMe = new NameMe('John', 'Doe');
        expect(nameMe.firstName).toEqual('John');
        expect(nameMe.lastName).toEqual('Doe');
        expect(nameMe.name).toEqual('John Doe');
    });
});

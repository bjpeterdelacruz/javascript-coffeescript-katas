const solution = require('../src/TrainingJSMaxMinAbs');

describe('Training JS #33: max(), min(), and abs() Tests', () => {
    it('Equal Test', () => {
        expect(solution([1, 3, 5], [9, 8, 7])).toEqual([8, 2]);
        expect(solution([1, 10, 100, 1000], [0, 0, 0, 0])).toEqual([1000, 1]);
        expect(solution([10, 20, 30, 40], [111, 11, 1, -111])).toEqual([151, 9]);
    });
});

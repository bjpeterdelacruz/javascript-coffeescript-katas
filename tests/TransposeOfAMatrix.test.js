require('../src/TransposeOfAMatrix');

describe('Training JS #33: max(), min(), and abs() Tests', () => {
    it('Equal Test', () => {
        expect([[1, 2, 3], [4, 5, 6]].transpose()).toEqual([[1, 4], [2, 5], [3, 6]]);
        expect([[1, 2, 3, 4, 5, 6]].transpose()).toEqual([[1], [2], [3], [4], [5], [6]]);
        expect([[], [], [], [], []].transpose()).toEqual([[]]);
        expect([].transpose()).toEqual([]);
    });
});

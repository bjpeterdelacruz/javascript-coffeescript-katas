const solution = require('../src/TriangleNumberCheck');

describe('Triangle Number Check Tests', () => {
    it('Equal Tests', () => {
        expect(solution(0)).toEqual(true);
        expect(solution(1)).toEqual(true);
        expect(solution(3)).toEqual(true);
        expect(solution(6)).toEqual(true);
        expect(solution(8)).toEqual(false);
        expect(solution(10)).toEqual(true);
        expect(solution(5)).toEqual(false);
        expect(solution('hello')).toEqual(false);
        expect(solution(6.15)).toEqual(false);
    });
});

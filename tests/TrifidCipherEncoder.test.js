const { trifidEncode, trifidDecode } = require('../src/TrifidCipherEncoder');

describe('Encoder Static Tests', () => {
    it('Defense Test', () => {
        const key='EPSDUCVWYM+ZLKXNBTFGORIJHAQ';
        expect(trifidEncode(key, 5, 'DEFENDTHEEASTWALLOFTHECASTLE+')).toEqual('SUEFECPHSEGYYJIXIMFOFOCEJLBSP');
        expect(trifidEncode(key, 6, 'ABCDEFG')).toEqual('RSAMXEG');
    });
    it('Alphabet Test', () => {
        const key='ABCDEFGHIJKLMNOPQRSTUVWXYZ+';
        expect(trifidEncode(key, 5, 'HELLO+WORLD')).toEqual('BOJN+ZOOH+D');
        expect(trifidEncode(key, 7, 'HELLO+AGAIN')).toEqual('BOHBTRYAPVH');
        expect(trifidEncode(key, 3, 'TOP+SECRET')).toEqual('WFPYTTDHZT');
    });
});

describe('Decoder Static Tests', () => {
    it('Defense Test', () => {
        const key='EPSDUCVWYM+ZLKXNBTFGORIJHAQ';
        expect(trifidDecode(key, 5, 'SUEFECPHSEGYYJIXIMFOFOCEJLBSP')).toEqual('DEFENDTHEEASTWALLOFTHECASTLE+');
        expect(trifidDecode(key, 7, 'RSQLPNPQBLB')).toEqual('ABCDEFGHIJK');
        expect(trifidDecode(key, 2, 'JJTAUBLJOEMIESXWYIKM')).toEqual('ATTACK+THE+WEST+WALL');

    });
    it('Alphabet Test', () => {
        const key='ABCDEFGHIJKLMNOPQRSTUVWXYZ+';
        expect(trifidDecode(key, 7, 'WYBYKUFLVW')).toEqual('TOP+SECRET');
        expect(trifidDecode(key, 5, 'RTSJERFYPXX')).toEqual('MYSTERY+BOX');
        expect(trifidDecode(key, 3, 'BVOOF+WORJP')).toEqual('HELLO+WORLD');
    });
});

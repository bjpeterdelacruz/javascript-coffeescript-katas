require('../src/TrimString');

describe('Trim a String Tests', () => {
    it('Equal Tests', () => {
        expect('    foo'.trim()).toEqual('foo');
        expect('foo    '.trim()).toEqual('foo');
        expect('    foo    '.trim()).toEqual('foo');
        expect('        '.trim()).toEqual('');
        expect('    foo bar    '.trim()).toEqual('foo bar');
    });
});

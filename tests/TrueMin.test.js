const solution = require('../src/TrueMin');

/**
 * @param a
 * @param b
 * @param expected
 */
function assert(a, b, expected) {
    expect(solution(a, b)).toEqual(expected);
}

describe('True Min Tests', () => {
    it('Equal Test', () => {
        assert(1.5, 2.5, 1.5);
        assert(2.5, 1.5, 1.5);
        assert(1.5, 1.5, 1.5);
        assert(2.5, 2.5, 2.5);

        assert(1.5, -2.5, -2.5);
        assert(-2.5, 1.5, -2.5);
        assert(1.5, -1.5, -1.5);
        assert(-2.5, 2.5, -2.5);

        assert(0, -2.5, -2.5);
        assert(-2.5, 0, -2.5);
        assert(1.5, 0, 0);
        assert(0, 2.5, 0);
        assert(0, 0, 0);

        assert(1.5, NaN, NaN);
        assert(NaN, 1.5, NaN);
        assert(-1.5, NaN, NaN);
        assert(NaN, -1.5, NaN);
        assert(NaN, NaN, NaN);

        assert(1.5, Infinity, 1.5);
        assert(Infinity, 1.5, 1.5);
        assert(-1.5, Infinity, -1.5);
        assert(Infinity, -1.5, -1.5);
        assert(Infinity, Infinity, Infinity);
        assert(1.5, -Infinity, -Infinity);
        assert(-Infinity, 1.5, -Infinity);
        assert(-1.5, -Infinity, -Infinity);
        assert(-Infinity, -1.5, -Infinity);
        assert(-Infinity, -Infinity, -Infinity);

        assert(null, 2.5, 0);
        assert(2.5, null, 0);
        assert(null, -2.5, -2.5);
        assert(-2.5, null, -2.5);
        assert(null, null, 0);

        assert(undefined, 2.5, NaN);
        assert(2.5, undefined, NaN);
        assert(undefined, -2.5, NaN);
        assert(-2.5, undefined, NaN);
        assert(undefined, undefined, NaN);
    });
});

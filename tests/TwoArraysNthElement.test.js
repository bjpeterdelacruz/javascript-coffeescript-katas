const solution = require('../src/TwoArraysNthElement');

describe('Two Arrays Nth Element Tests', () => {
    it('Equal Tests', () => {
        expect(solution([1, 3, 4], [2, 6, 8], 5)).toEqual(8);
        expect(solution([1, 3, 5], [2, 4], 2)).toEqual(3);
        expect(solution([1, 2, 3], [4, 5, 6], 3)).toEqual(4);
        expect(solution([6, 19, 21, 30, 34, 35, 44, 48], [3, 4, 5, 9, 14, 16, 25, 32, 36, 37, 41, 53], 11)).toEqual(32);
        expect(solution([10, 11], [4, 6], 0)).toEqual(4);
        expect(solution([10, 11], [4, 6], 1)).toEqual(6);
        expect(solution([10, 11], [4, 6], 2)).toEqual(10);
        expect(solution([10, 11], [4, 6], 3)).toEqual(11);
        expect(solution([15, 16, 21, 25, 29, 31], [5, 8, 12, 18, 28], 10)).toEqual(31);
        expect(solution([-6, -5, -4], [4, 5, 6], 3)).toEqual(4);
        expect(solution([-6, -5, -4, 0], [4, 5, 6], 3)).toEqual(0);
    });
});

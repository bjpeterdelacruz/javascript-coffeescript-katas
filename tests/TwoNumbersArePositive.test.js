const solution = require('../src/TwoNumbersArePositive');

describe('Two Numbers Are Positive Tests', () => {
    it('Equal Tests', () => {
        expect(solution(1, -5, 3)).toEqual(true);
        expect(solution(-4, 2, 3)).toEqual(true);
        expect(solution(1, 2, -6)).toEqual(true);
        expect(solution(0, 1, 2)).toEqual(true);
        expect(solution(1, 2, 3)).toEqual(false);
        expect(solution(-4, -5, -6)).toEqual(false);
        expect(solution(-4, -5, 3)).toEqual(false);
        expect(solution(0, 2, 0)).toEqual(false);
        expect(solution(0, -5, 3)).toEqual(false);
    });
});

const solution = require('../src/UniqueInOrder');

describe('Unique In Order Tests', () => {
    it('should work with an empty array', () => {
        expect(solution('')).toEqual([]);
    });
    it('should work with one element', () => {
        expect(solution('A')).toEqual(['A']);
    });
    it('should reduce duplicates', () => {
        expect(solution('AA')).toEqual(['A']);
        expect(solution('AAAABBBCCDAABBB')).toEqual(['A', 'B', 'C', 'D', 'A', 'B']);
        expect(solution('AADD')).toEqual(['A', 'D']);
        expect(solution('AAD')).toEqual(['A', 'D']);
        expect(solution('ADD')).toEqual(['A', 'D']);
    });
    it('should treat lowercase as different from uppercase', () => {
        expect(solution('ABBCcAD')).toEqual(['A', 'B', 'C', 'c', 'A', 'D']);
    });
    it('should work with int arrays', () => {
        expect(solution([1, 2, 3, 3])).toEqual([1, 2, 3]);
    });
    it('should work with char arrays', () => {
        expect(solution(['a', 'b', 'b'])).toEqual(['a', 'b']);
    });
});

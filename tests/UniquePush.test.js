const solution = require('../src/UniquePush');

describe('uniquePush - No Dupes! Tests', () => {
    it('Equal Test', () => {
        expect(solution([], { name: 'Alex', phoneNumber: 123-456-7890 })).toEqual(true);
        expect(solution([], { name: 'Alex' })).toEqual(false);
        expect(solution([{ name: 'Alex', phoneNumber: 123-456-7890 }],
            { name: 'Bob', phoneNumber: 123-456-7890 })).toEqual(false);
        expect(solution([{ name: 'Alex', phoneNumber: 123-456-7890 }],
            { name: 'Bob', phoneNumber: 555-555-5555 })).toEqual(true);
    });
});

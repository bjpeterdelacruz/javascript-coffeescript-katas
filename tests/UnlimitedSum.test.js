const solution = require('../src/UnlimitedSum');

describe('Unlimited Sum Tests', () => {
    it('Equal Tests', () => {
        expect(solution(9)).toEqual(9);
        expect(solution(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 8)).toEqual(99);
        expect(solution('1', 1)).toEqual(1);
        expect(solution('a', [], {}, /.*/g, true, true, true, 1, 3)).toEqual(4);
        expect(solution(2, 40, 1.1)).toEqual(42);
    });
});

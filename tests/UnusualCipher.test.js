const solution = require('../src/UnusualCipher');

describe('Unusual Cipher Tests', () => {
    it('Equal Tests', () => {
        expect(solution('Jackdaws love my big sphinx of quartz', 'playfair jexample')).toEqual('EPBNOEZQ ANAD XF KBH QFBRKE QY SVLIUWM');
        expect(solution('Jackdaws love my big sphinx of quartz', 'playfajr iexample')).toEqual('EPBNOEZQ ANAD XF KBH QFBRKE QY SVLJUWM');
        expect(solution('Pack my box with five dozen liquor jugs', 'playfair jexample')).toEqual('LYBN XF DKG YBPS METD OSVRO PRNWNE RTHQ');
        expect(solution('The quick onyx goblin jumps over the lazy dwarf', 'playfair jexample')).toEqual('ZBX OTRBN QOXG DQCPRK RTIFK QADI UDM AYWF GVLEYM');
        expect(solution('Cwm fjord bank glyphs vext quiz', 'playfair jexample')).toEqual('GUH MEKEC DPON CYFLSZ ADIW NWMT');
        expect(solution('How razorback jumping frogs can level six piqued gymnasts', 'playfair jexample')).toEqual('DSU XFVNEDPBN RTIFRKH YENHQ DLU RDARA KMI YXKVRG HFXOLKZQM');
        expect(solution('Cozy lummox gives smart squid who asks for job pen', 'playfair jexample')).toEqual('DNWF RLIMES GQETM OMQEF IUKST RGV DSFO NKA SER KDAIQR');
        expect(solution('Play', 'play')).toEqual('LAYB');
    });
});

const solution = require('../src/ValidCurlyBraces');

describe('Valid Curly Braces (Code Golf) Tests', () => {
    it('Equal Test', () => {
        expect(solution('{{}}')).toEqual(true);
        expect(solution('{}{}{}{}')).toEqual(true);
        expect(solution('{{{}{}}}')).toEqual(true);
        expect(solution('{{')).toEqual(false);
        expect(solution('{}}')).toEqual(false);
        expect(solution('')).toEqual(true);
    });
});

const solution = require('../src/ValidParentheses');

describe('Valid Parentheses Tests', () => {
    it('Equal Test', () => {
        expect(solution('()')).toEqual(true);
        expect(solution('((()))')).toEqual(true);
        expect(solution('()()()')).toEqual(true);
        expect(solution('(()())()')).toEqual(true);
        expect(solution('()(())((()))(())()')).toEqual(true);

        expect(solution(')(')).toEqual(false);
        expect(solution('()()(')).toEqual(false);
        expect(solution('((())')).toEqual(false);
        expect(solution('())(()')).toEqual(false);
        expect(solution(')()')).toEqual(false);
        expect(solution(')')).toEqual(false);

        expect(solution('(a)(b)')).toEqual(false);
    });
});

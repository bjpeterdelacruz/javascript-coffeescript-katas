const solution = require('../src/ValidateCreditCardExpiryDate');

describe('Validate Credit Card Expiry Date Tests', () => {
    it('Equal Tests', () => {
        const currentDate = new Date(2021, 4, 13);
        expect(solution('03/15', currentDate)).toEqual(false);
        expect(solution('03/33', currentDate)).toEqual(true);
        expect(solution('03-15', currentDate)).toEqual(false);
        expect(solution('03 / 15', currentDate)).toEqual(false);
        expect(solution('03-2015', currentDate)).toEqual(false);

        expect(solution('03-2021', currentDate)).toEqual(false);
        expect(solution('05-2021', currentDate)).toEqual(true);
        expect(solution('12-2021', currentDate)).toEqual(true);
    });
});

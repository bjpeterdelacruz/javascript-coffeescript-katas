const solution = require('../src/ValidateHexadecimal');

describe('Validate Hexadecimal Tests', () => {
    it('Equal Test', () => {
        expect(solution('ff00ff')).toEqual(true);
        expect(solution('f0f')).toEqual(true);
        expect(solution('ff0000')).toEqual(true);
        expect(solution('ff0')).toEqual(true);
        expect(solution('Ff00Fb')).toEqual(true);
        expect(solution('b0F')).toEqual(true);
        expect(solution('b0Ff')).toEqual(false);
        expect(solution('b0g')).toEqual(false);
        expect(solution('ff0gf0')).toEqual(false);
        expect(solution('#ff0gf0')).toEqual(false);
        expect(solution('0xff0gf0')).toEqual(false);
    });
});

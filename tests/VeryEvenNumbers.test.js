const solution = require('../src/VeryEvenNumbers');

describe('Very Even Numbers Tests', () => {
    it('Equal Tests', () => {
        const inputs =   [0,    4,    12,    222,  5,     45,    4554,  1234,  88,    24,   400000220];
        const expected = [true, true, false, true, false, false, false, false, false, true, true     ];
        for (let idx = 0; idx < inputs.length; idx++) {
            expect(solution(inputs[idx])).toEqual(expected[idx]);
        }
    });
});

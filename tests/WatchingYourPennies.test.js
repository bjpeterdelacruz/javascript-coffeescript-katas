const solution = require('../src/WatchingYourPennies');

describe('Watching Your Pennies Tests', () => {
    it('Equal Tests', () => {
        expect(solution(10000, 800, 0.2)).toEqual('You still have $536.35');
        expect(solution(10000, 1000, 0.2)).toEqual('You ran out of money after 10 months');
        expect(solution(1000, 1000, 0.2)).toEqual('You ran out of money after 1 months');
        expect(solution(237438, 2893, 0.8)).toEqual('You still have $224977.24');
        expect(solution(1000, 1100, 0.2)).toEqual('You ran out of money after 0 months');
        expect(solution(120000, 1000, 2)).toEqual('You still have $138776.93');
    });
});

const solution = require('../src/Watermelon');

describe('Watermelon Tests', () => {
    it('Equal Tests', () => {
        expect(solution(1)).toEqual(false);
        expect(solution(2)).toEqual(false);
        for (let number = 3; number < 1000; number += 2) {
            expect(solution(number)).toEqual(false);
        }
        for (let number = 4; number <= 1000; number += 2) {
            expect(solution(number)).toEqual(true);
        }
    });
});

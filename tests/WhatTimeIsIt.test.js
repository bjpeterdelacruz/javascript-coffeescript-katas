const solution = require('../src/WhatTimeIsIt');

describe('What Time Is It Tests', () => {
    it('Equal Tests', () => {
        expect(solution('12:00:01AM')).toEqual('00:00:01');
        expect(solution('01:02:03AM')).toEqual('01:02:03');
        expect(solution('02:04:05AM')).toEqual('02:04:05');
        expect(solution('03:06:07AM')).toEqual('03:06:07');
        expect(solution('04:08:09AM')).toEqual('04:08:09');
        expect(solution('05:10:11AM')).toEqual('05:10:11');
        expect(solution('06:12:13AM')).toEqual('06:12:13');
        expect(solution('07:14:15AM')).toEqual('07:14:15');
        expect(solution('08:16:17AM')).toEqual('08:16:17');
        expect(solution('09:18:19AM')).toEqual('09:18:19');
        expect(solution('10:20:21AM')).toEqual('10:20:21');
        expect(solution('11:22:23AM')).toEqual('11:22:23');
        expect(solution('12:24:25PM')).toEqual('12:24:25');
        expect(solution('01:26:27PM')).toEqual('13:26:27');
        expect(solution('02:28:29PM')).toEqual('14:28:29');
        expect(solution('03:30:31PM')).toEqual('15:30:31');
        expect(solution('04:32:33PM')).toEqual('16:32:33');
        expect(solution('05:34:35PM')).toEqual('17:34:35');
        expect(solution('06:36:37PM')).toEqual('18:36:37');
        expect(solution('07:38:39PM')).toEqual('19:38:39');
        expect(solution('08:40:41PM')).toEqual('20:40:41');
        expect(solution('09:42:43PM')).toEqual('21:42:43');
        expect(solution('10:44:45PM')).toEqual('22:44:45');
        expect(solution('11:46:47PM')).toEqual('23:46:47');
    });
});

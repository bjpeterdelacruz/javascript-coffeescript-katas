const solution = require('../src/WhoLetTheDogsOut');

describe('Who Let the Dogs Out Tests', () => {
    it('Equal Test', () => {
        expect(solution.dogBarkOnlyIfToldSo(true)).toEqual(solution.BARK);
        expect(solution.dogBarkOnlyIfToldSo(false)).toEqual(solution.SLEEP);
        expect(solution.dogBarkOnlyIfToldSo()).toEqual(solution.SLEEP);

        expect(solution.dogDontBarkOnlyIfToldSo(true)).toEqual(solution.SLEEP);
        expect(solution.dogDontBarkOnlyIfToldSo(false)).toEqual(solution.BARK);
        expect(solution.dogDontBarkOnlyIfToldSo()).toEqual(solution.BARK);

        expect(solution.dogDontBarkByDefault()).toEqual(solution.SLEEP);
        expect(solution.dogDontBarkByDefault(false)).toEqual(solution.BARK);

        expect(solution.dogBarkByDefault()).toEqual(solution.BARK);
        expect(solution.dogBarkByDefault(false)).toEqual(solution.SLEEP);

        expect(solution.SLEEP === solution.BARK).toEqual(false);
    });
});

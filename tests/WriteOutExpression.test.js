const solution = require('../src/WriteOutExpression');

describe('Write Out Expression Tests', () => {
    it('Equal Test', () => {
        expect(solution('1 + 3')).toEqual('One Plus Three');
        expect(solution('2 - 10')).toEqual('Two Minus Ten');
        expect(solution('6 ** 9')).toEqual('Six To The Power Of Nine');
        expect(solution('5 = 5')).toEqual('Five Equals Five');
        expect(solution('7 * 4')).toEqual('Seven Times Four');
        expect(solution('2 / 2')).toEqual('Two Divided By Two');
        expect(solution('8 != 5')).toEqual('Eight Does Not Equal Five');
        expect(solution('8 x 5')).toEqual('That\'s not an operator!');
    });
});

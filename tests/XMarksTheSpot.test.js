const solution = require('../src/XMarksTheSpot');

describe('X Marks the Spot Tests', () => {
    it('Equal Test', () => {
        expect(solution([])).toEqual([]);
        expect(solution([['o', 'o'], ['o', 'o']])).toEqual([]);
        expect(solution([['o', 'o'], ['x', 'o']])).toEqual([1, 0]);
        expect(solution([['x', 'o'], ['o', 'x']])).toEqual([]);
    });
});

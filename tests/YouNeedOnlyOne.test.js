const solution = require('../src/YouNeedOnlyOne');

describe('You Need Only One Tests', () => {
    it('Equal Test', () => {
        expect(solution([1, 2, 3, 4], 2)).toEqual(true);
        expect(solution([1, 2, 3, 4], 5)).toEqual(false);
    });
});

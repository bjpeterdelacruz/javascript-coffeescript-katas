const solution = require('../src/Ziiiiip');

describe('Ziiiiip Tests', () => {
    it('Equal Test', () => {
        expect(solution(['fred', 'barney'], [30, 40])).toEqual({ 'fred': 30, 'barney': 40 });
        expect(solution([['fred', 30], ['barney', 40]])).toEqual({ 'fred': 30, 'barney': 40 });
        expect(solution()).toEqual({});
        expect(solution(['fred', 'barney'])).toEqual({ 'fred': undefined, 'barney': undefined });
    });
});

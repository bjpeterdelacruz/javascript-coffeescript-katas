const solution = require('../../out/ArithmeticFunction');

describe('Arithmetic Function Tests', () => {
    it('Equal Test', () => {
        expect(solution(5, 2, 'add')).toEqual(7);
        expect(solution(5, 2, 'subtract')).toEqual(3);
        expect(solution(5, 2, 'multiply')).toEqual(10);
        expect(solution(5, 2, 'divide')).toEqual(2.5);
    });
});

const solution = require('../../out/ArraysSimilar');

describe('Arrays Similar Tests', () => {
    it('Equal Tests', () => {
        const arr1 = [1, 2, 2, 3, 4];
        const arr2 = [2, 1, 2, 4, 3];
        const arr3 = [1, 2, 3, 4];
        const arr4 = [1, 2, 3, '4'];
        const arr5 = [1, 2, 3];
        const arr6 = [1, 2, 2];
        expect(solution(arr1, arr2)).toEqual(true);
        expect(solution(arr1, arr3)).toEqual(false);
        expect(solution(arr3, arr4)).toEqual(false);
        expect(solution(arr5, arr6)).toEqual(false);
        expect(solution([], [])).toEqual(true);
        expect(solution([1], [])).toEqual(false);
        expect(solution([1], [2])).toEqual(false);
    });
});

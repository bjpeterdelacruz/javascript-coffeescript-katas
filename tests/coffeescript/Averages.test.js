const solution = require('../../out/Averages');

describe('Averages of Numbers Tests', () => {
    it('Equal Test', () => {
        expect(solution(null)).toEqual([]);
        expect(solution([])).toEqual([]);
        expect(solution([0])).toEqual([]);
        expect(solution([2, 2, 2, 2, 2])).toEqual([2, 2, 2, 2]);
        expect(solution([2, -2, 2, -2, 2])).toEqual([0, 0, 0, 0]);
        expect(solution([1, 3, 5, 1, -10])).toEqual([2, 4, 3, -4.5]);
    });
});

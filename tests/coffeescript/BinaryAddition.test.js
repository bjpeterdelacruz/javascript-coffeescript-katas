const solution = require('../../out/BinaryAddition');

describe('Binary Addition Tests', () => {
    it('Equal Test', () => {
        expect(solution(1, 2)).toEqual('11');
        expect(solution(4, 2)).toEqual('110');
        expect(solution(6, 1)).toEqual('111');
        expect(solution(16, 32)).toEqual('110000');
    });
});

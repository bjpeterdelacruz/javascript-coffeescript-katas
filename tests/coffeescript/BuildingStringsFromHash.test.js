const solution = require('../../out/BuildingStringsFromHash');

describe('Building Strings from Hash Tests', () => {
    it('Equal Test', () => {
        expect(solution({ a: 1, b: '2' })).toEqual('a = 1,b = 2');
        expect(solution({ c: true, d: [1, 2], e: 0.2 })).toEqual('c = true,d = 1,2,e = 0.2');
    });
});

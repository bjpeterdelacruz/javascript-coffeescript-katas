const CaesarCipher = require('../../out/CaesarCipherHelper');

describe('Caesar Cipher Helper Tests', () => {
    it('Equal Test', () => {
        let c = new CaesarCipher(5);
        expect(c.encode('Codewars')).toEqual('HTIJBFWX');
        expect(c.decode('HTIJBFWX')).toEqual('CODEWARS');

        expect(c.decode('BFKKQJX')).toEqual('WAFFLES');
        expect(c.encode('WAFFLES')).toEqual('BFKKQJX');

        expect(c.encode('@#$%')).toEqual('@#$%');
        expect(c.decode('@#$%')).toEqual('@#$%');

        expect(c.decode(c.encode('/* HeLLo WoRLd */'))).toEqual('/* HELLO WORLD */');

        c = new CaesarCipher(1000);
        expect(c.decode(c.encode('Codewars'))).toEqual('CODEWARS');
        expect(c.decode(c.encode('/* HeLLo WoRLd */'))).toEqual('/* HELLO WORLD */');
    });
});

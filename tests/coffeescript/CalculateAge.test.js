const solution = require('../../out/CalculateAge');

describe('Calculate Age Tests', () => {
    it('Equal Test', () => {
        expect(solution(6, 3)).toEqual(9);
        expect(solution(-15, 0.25)).toEqual(5);
    });
});

const solution = require('../../out/CalculateHypotenuse');

describe('Calculate Hypotenuse Tests', () => {
    it('Equal Tests', () => {
        let examples = [
            [2, 3, 3.606],
            [1, 1, 1.414],
            [3, 4, 5]
        ];
        for (const example of examples) {
            expect(solution(example[0], example[1])).toEqual(example[2]);
        }

        examples = [
            [null, 3],
            ['one', 'two'],
            [-2, 1]
        ];
        for (const example of examples) {
            expect(() => solution(example[0], example[1])).toThrow();
        }
    });
});

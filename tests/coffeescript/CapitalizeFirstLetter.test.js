require('../../out/CapitalizeFirstLetter');

describe('Capitalize First Letter of a String Tests', () => {
    it('Equal Test', () => {
        expect('abc'.toUpperCase().toString()).toEqual('abc');
        expect('hello world!'.capitalize()).toEqual('Hello world!');
        expect('123hello world!'.capitalize()).toEqual('123hello world!');
        expect('Hello world!'.capitalize()).toEqual('Hello world!');
    });
});

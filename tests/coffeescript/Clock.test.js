const solution = require('../../out/Clock');

describe('Clock Tests', () => {
    it('Equal Test', () => {
        expect(solution(1, 1, 1)).toEqual(3661000);
        expect(solution(2, 2, 1)).toEqual(7321000);
    });
});

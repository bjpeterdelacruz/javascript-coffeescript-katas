const solution = require('../../out/ClosestMultipleOfTen');

describe('Closest Multiple of Ten Tests', () => {
    it('Equal Tests', () => {
        expect(solution(22)).toEqual(20);
        expect(solution(35)).toEqual(40);
        expect(solution(47)).toEqual(50);
        expect(solution(122.75)).toEqual(120);
    });
});

const solution = require('../../out/ConvertNumberToArray');

describe('Convert Number to Array Tests', () => {
    it('Equal Tests', () => {
        expect(solution(0)).toEqual([0]);
        expect(solution(1234)).toEqual([4, 3, 2, 1]);
    });
});

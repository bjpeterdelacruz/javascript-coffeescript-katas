const solution = require('../../out/CountSmileys');

describe('Count Smileys Tests', () => {
    it('Equal Test', () => {
        expect(solution([])).toEqual(0);
        expect(solution([])).toEqual(0);
        expect(solution([':)', ';(', ';}', ':-D', ':-|'])).toEqual(2);
        expect(solution([';]', ':[', ';*', ':$', ';-D'])).toEqual(1);
        expect(solution([':)', ':(', ':D', ':O', ':;'])).toEqual(2);
        expect(solution([':-)', ';~D', ':-D', ':_D'])).toEqual(3);
        expect(solution([':---)', '))', ';~~D', ';D'])).toEqual(1);
        expect(solution([';~)', ':)', ':-)', ':--)'])).toEqual(3);
        expect(solution([':o)', ':--D', ';-~)'])).toEqual(0);
    });
});

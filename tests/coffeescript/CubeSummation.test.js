const solution = require('../../out/CubeSummation');

describe('Cube Summation Tests', () => {
    it('Equal Tests', () => {
        expect(solution(2, 3)).toEqual(27);
        expect(solution(3, 2)).toEqual(27);
        expect(solution(0, 4)).toEqual(100);
        expect(solution(17, 14)).toEqual(12384);
        expect(solution(9, 9)).toEqual(0);
        expect(solution(5, 0)).toEqual(225);
    });
});

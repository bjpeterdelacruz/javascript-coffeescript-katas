const solution = require('../../out/DontGiveMeFive');

describe('Don\'t Give Me Five Tests', () => {
    it('Equal Test', () => {
        expect(solution(49, 60)).toEqual(2);
        expect(solution(-10, 10)).toEqual(19);
        expect(solution(-10, -12)).toEqual(3);
    });
});

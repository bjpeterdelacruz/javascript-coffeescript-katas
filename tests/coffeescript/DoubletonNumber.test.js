const solution = require('../../out/DoubletonNumber');

describe('Doubleton Number Tests', () => {
    it('Equal Test', () => {
        expect(solution(120)).toEqual(121);
        expect(solution(1234)).toEqual(1311);
        expect(solution(10)).toEqual(12);
        expect(solution(2)).toEqual(10);
    });
});

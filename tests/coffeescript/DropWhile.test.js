const solution = require('../../out/DropWhile');

/**
 * @param x
 */
function isEven(x) {
    return Math.abs(x) % 2 === 0;
}

describe('Drop While Tests', () => {
    it('Equal Tests', () => {
        const arr1 = [2, 4, 6, 1, 4, 8];
        const arr2 = [1, 4, 5, 7, 6];
        const arr3 = [2, 4, 6, 8, 10, 12];

        expect(solution([], isEven)).toEqual([]);
        expect(solution(arr1, isEven)).toEqual([1, 4, 8]);
        expect(solution(arr2, isEven)).toEqual(arr2);
        expect(solution(arr3, isEven)).toEqual([]);
    });
});

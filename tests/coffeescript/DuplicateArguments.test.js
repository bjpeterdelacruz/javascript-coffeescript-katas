const solution = require('../../out/DuplicateArguments');

describe('Duplicate Arguments Tests', () => {
    it('Equal Tests', () => {
        expect(solution()).toEqual(false);
        expect(solution(1, 2, 3)).toEqual(false);
        expect(solution(1, 2, 3, 2, 5)).toEqual(true);
        expect(solution('1', '2', 3, 4, '5')).toEqual(false);
        expect(solution('1', 2, '1', '4', 5)).toEqual(true);
    });
});

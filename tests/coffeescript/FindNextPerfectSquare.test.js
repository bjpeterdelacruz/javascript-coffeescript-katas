const solution = require('../../out/FindNextPerfectSquare');

describe('Find Next Perfect Square Tests', () => {
    it('Equal Tests', () => {
        expect(solution(121)).toEqual(144);
        expect(solution(122)).toEqual(-1);
    });
});

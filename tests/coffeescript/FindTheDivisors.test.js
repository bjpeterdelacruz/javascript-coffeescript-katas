const solution = require('../../out/FindTheDivisors');

describe('Find the Divisors Tests', () => {
    it('Equal Tests', () => {
        expect(solution(13)).toEqual('13 is prime');
        expect(solution(12)).toEqual([2, 3, 4, 6]);
        expect(solution(2)).toEqual('2 is prime');
        expect(solution(1)).toEqual([1]);
    });
});

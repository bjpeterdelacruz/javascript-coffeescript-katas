const solution = require('../../out/FizzBuzzCuckooClock');

describe('FizzBuzz Cuckoo Clock Tests', () => {
    it('Tests', () => {
        expect(solution('13:34')).toEqual('tick');
        expect(solution('21:00')).toEqual('Cuckoo Cuckoo Cuckoo Cuckoo Cuckoo Cuckoo Cuckoo Cuckoo Cuckoo');
        expect(solution('11:15')).toEqual('Fizz Buzz');
        expect(solution('11:16')).toEqual('tick');
        expect(solution('03:03')).toEqual('Fizz');
        expect(solution('14:30')).toEqual('Cuckoo');
        expect(solution('08:55')).toEqual('Buzz');
        expect(solution('00:00')).toEqual('Cuckoo Cuckoo Cuckoo Cuckoo Cuckoo Cuckoo Cuckoo Cuckoo Cuckoo Cuckoo Cuckoo Cuckoo');
        expect(solution('12:00')).toEqual('Cuckoo Cuckoo Cuckoo Cuckoo Cuckoo Cuckoo Cuckoo Cuckoo Cuckoo Cuckoo Cuckoo Cuckoo');
        expect(solution('04:00')).toEqual('Cuckoo Cuckoo Cuckoo Cuckoo');
        expect(solution('16:00')).toEqual('Cuckoo Cuckoo Cuckoo Cuckoo');
    });
});

const solution = require('../../out/FriendOrFoe');

describe('Friend or Foe Tests', () => {
    it('Equal Tests', () => {
        expect(solution([])).toEqual([]);
        expect(solution(['Jo', 'Joe'])).toEqual([]);
        expect(solution(['Ryan', 'Joe', 'John'])).toEqual(['Ryan', 'John']);
    });
});

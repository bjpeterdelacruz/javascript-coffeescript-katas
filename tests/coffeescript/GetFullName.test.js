const Dinglemouse = require('../../out/GetFullName');

describe('Get Full Name Tests', () => {
    it('Equal Tests', () => {
        expect(new Dinglemouse('Clint', 'Eastwood').getFullName()).toEqual('Clint Eastwood');
        expect(new Dinglemouse('Clint', '').getFullName()).toEqual('Clint');
        expect(new Dinglemouse('', 'Eastwood').getFullName()).toEqual('Eastwood');
        expect(new Dinglemouse('', '').getFullName()).toEqual('');
        expect(new Dinglemouse('Clint   ', '   Eastwood').getFullName()).toEqual('Clint Eastwood');
    });
});

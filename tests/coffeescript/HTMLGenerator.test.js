const HTMLGen = require('../../out/HTMLGenerator');

describe('HTML Generator Tests', () => {
    it('Equal Test', () => {
        const html = new HTMLGen();
        expect(html.a('Hello')).toEqual('<a>Hello</a>');
        expect(html.b('Hello')).toEqual('<b>Hello</b>');
        expect(html.p('Hello')).toEqual('<p>Hello</p>');
        expect(html.body('Hello')).toEqual('<body>Hello</body>');
        expect(html.div('Hello')).toEqual('<div>Hello</div>');
        expect(html.span('Hello')).toEqual('<span>Hello</span>');
        expect(html.title('Hello')).toEqual('<title>Hello</title>');
        expect(html.comment('Hello')).toEqual('<!--Hello-->');
        expect(html.comment(html.div(html.a('Hello')))).toEqual('<!--<div><a>Hello</a></div>-->');
    });
});

const HolidayPriorityQueue = require('../../out/HolidayShoppingPriorityQueue');

describe('Holiday Shopping Priority Queue Tests', () => {
    it('Equal Test', () => {
        const giftList = new HolidayPriorityQueue();
        let result = giftList.addGift( { gift: 'Water Gun', priority: 1 } );
        expect(result).toEqual(1);
        result = giftList.addGift( { gift: 'Bowling Ball', priority: 7 } );
        expect(result).toEqual(2);
        result = giftList.addGift( { gift: 'Toy Truck', priority: 4 } );
        expect(result).toEqual(3);
        result = giftList.addGift( { gift: 'Roller Skates', priority: 3 } );
        expect(result).toEqual(4);

        expect(giftList.buyGift()).toEqual('Water Gun');
        expect(giftList.buyGift()).toEqual('Roller Skates');
        expect(giftList.buyGift()).toEqual('Toy Truck');
        expect(giftList.buyGift()).toEqual('Bowling Ball');
        expect(giftList.buyGift()).toEqual('');
        expect(giftList.buyGift()).toEqual('');
    });
});

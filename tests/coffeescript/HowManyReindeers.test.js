const solution = require('../../out/HowManyReindeers');

describe('How Many Reindeers Tests', () => {
    it('Equal Tests', () => {
        expect(solution(0)).toEqual(2);
        expect(solution(1)).toEqual(3);
        expect(solution(30)).toEqual(3);
        expect(solution(31)).toEqual(4);
        expect(solution(60)).toEqual(4);
        expect(solution(61)).toEqual(5);
        expect(solution(90)).toEqual(5);
        expect(solution(91)).toEqual(6);
        expect(solution(120)).toEqual(6);
        expect(solution(121)).toEqual(7);
        expect(solution(150)).toEqual(7);
        expect(solution(151)).toEqual(8);
        expect(solution(180)).toEqual(8);
        expect(() => solution(181)).toThrow();
        expect(() => solution(200)).toThrow();
        expect(() => solution(-7)).toThrow();
    });
});

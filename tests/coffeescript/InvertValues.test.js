const solution = require('../../out/InvertValues');

describe('Invert Values Tests', () => {
    it('Equal Tests', () => {
        expect(solution([0])).toEqual([-0]);
        expect(solution([-0])).toEqual([0]);
        expect(solution([1, -2, 3, -4, 5])).toEqual([-1, 2, -3, 4, -5]);
    });
});

const solution = require('../../out/IsStringRepeating');

describe('Is String Repeating Tests', () => {
    it('Equal Test', () => {
        expect(solution('a')).toEqual(true);
        expect(solution('aaaaa')).toEqual(true);
        expect(solution('aaaab')).toEqual(false);
        expect(solution('bbbbb')).toEqual(true);
        expect(solution('bbabb')).toEqual(false);
    });
});

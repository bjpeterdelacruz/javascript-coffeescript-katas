const solution = require('../../out/IsTriangle');

describe('Is Triangle Tests', () => {
    it('Equal Tests', () => {
        expect(solution(1, 2, 2)).toEqual(true);
        expect(solution(3, 4, 5)).toEqual(true);
        expect(solution(7, 2, 2)).toEqual(false);
        expect(solution(0, 1, 2)).toEqual(false);
        expect(solution(1, 0, 2)).toEqual(false);
        expect(solution(1, 2, 0)).toEqual(false);
    });
});

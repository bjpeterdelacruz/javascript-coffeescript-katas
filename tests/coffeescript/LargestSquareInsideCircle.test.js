const solution = require('../../out/LargestSquareInsideCircle');

describe('Largest Square Inside a Circle Tests', () => {
    it('Equal Tests', () => {
        expect(solution(5)).toEqual(50);
        expect(solution(7)).toEqual(98);
        expect(solution(15)).toEqual(450);
    });
});

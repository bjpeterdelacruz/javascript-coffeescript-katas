const solution = require('../../out/LeapYear');

describe('Leap Year Tests', () => {
    it('Equal Tests', () => {
        expect(solution(2100)).toEqual(false);

        expect(solution(1234)).toEqual(false);
        expect(solution(1984)).toEqual(true);
        expect(solution(2000)).toEqual(true);
        expect(solution(2010)).toEqual(false);
        expect(solution(2013)).toEqual(false);
    });
});

const getLengthOfMissingArray = require('../../out/LengthOfMissingArray');

describe('Length of Missing Array Tests', () => {
    it('Equal Tests', () => {
        expect(getLengthOfMissingArray(null)).toEqual(0);
        expect(getLengthOfMissingArray([])).toEqual(0);
        expect(getLengthOfMissingArray([[1, 2], null, [3, 4, 5, 6]])).toEqual(0);
        expect(getLengthOfMissingArray([[1, 2], [], [3, 4, 5, 6]])).toEqual(0);
        expect(getLengthOfMissingArray([[1, 2], [1], [3, 4, 5, 6]])).toEqual(3);
        expect(getLengthOfMissingArray([[null, null], [null, null, null], [null], [null, null, null, null, null]])).toEqual(4);
    });
});

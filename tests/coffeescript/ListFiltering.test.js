const solution = require('../../out/ListFiltering');

describe('List Filtering Tests', () => {
    it('Equal Tests', () => {
        expect(solution([4, 1, '3', '5', 2])).toEqual([4, 1, 2]);
        expect(solution([-1, '6', true, 3, false, -2, 4.0])).toEqual([-1, true, 3, false, -2, 4.0]);
    });
});

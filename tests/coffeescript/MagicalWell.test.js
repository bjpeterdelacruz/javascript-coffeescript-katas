const solution = require('../../out/MagicalWell');

describe('Magical Well Tests', () => {
    it('Equal Tests', () => {
        expect(solution(1, 2, 2)).toEqual(8);
        expect(solution(1, 1, 1)).toEqual(1);
        expect(solution(6, 5, 3)).toEqual(128);
        expect(solution(3, 6, 0)).toEqual(0);
    });
});

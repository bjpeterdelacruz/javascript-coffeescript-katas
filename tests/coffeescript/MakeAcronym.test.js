const solution = require('../../out/MakeAcronym');

describe('Make Acronym Tests', () => {
    it('Equal Tests', () => {
        expect(solution('My aunt sally')).toEqual('MAS');
        expect(solution('Please excuse my dear aunt Sally')).toEqual('PEMDAS');
        expect(solution('How much wood would a woodchuck chuck if a woodchuck could chuck wood')).toEqual('HMWWAWCIAWCCW');
        expect(solution('Unique New York')).toEqual('UNY');
        expect(solution('a42')).toEqual('Not letters');
        expect(solution('1111')).toEqual('Not letters');
        expect(solution('314159')).toEqual('Not letters');
        expect(solution(64)).toEqual('Not a string');
        expect(solution([])).toEqual('Not a string');
        expect(solution({})).toEqual('Not a string');
        expect(solution([2, 4, 6, 8])).toEqual('Not a string');
        expect(solution('')).toEqual('');
    });
});

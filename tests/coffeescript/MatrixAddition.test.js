const solution = require('../../out/MatrixAddition');

describe('Manhattan Distance Tests', () => {
    it('Equal Test', () => {
        expect(solution([[1, 2], [3, 4]], [[5, 6], [7, 8]])).toEqual([[6, 8], [10, 12]]);
        expect(solution([[5]], [[3]])).toEqual([[8]]);
        expect(solution([[1, 1, 1], [2, 2, 2], [3, 3, 3]],
            [[4, 4, 4], [5, 5, 5], [6, 6, 6]])).toEqual([[5, 5, 5], [7, 7, 7], [9, 9, 9]]);
    });
});

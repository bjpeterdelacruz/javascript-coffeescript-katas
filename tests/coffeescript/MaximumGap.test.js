const solution = require('../../out/MaximumGap');

describe('Maximuum Gap Tests', () => {
    it('Equal Test', () => {
        expect(solution([13, 10, 2, 9, 5])).toEqual(4);
        expect(solution([13, 3, 5])).toEqual(8);
        expect(solution([-13, 10])).toEqual(23);
        expect(solution([8, -10])).toEqual(18);
        expect(solution([-1, -1, -1])).toEqual(0);
    });
});

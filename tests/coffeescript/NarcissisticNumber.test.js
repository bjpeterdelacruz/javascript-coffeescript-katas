const solution = require('../../out/NarcissisticNumber');

describe('Narcissistic Number Tests', () => {
    it('Equal Test', () => {
        expect(solution(1)).toEqual(true);
        expect(solution(5)).toEqual(true);
        expect(solution(7)).toEqual(true);

        expect(solution(153)).toEqual(true);
        expect(solution(370)).toEqual(true);
        expect(solution(371)).toEqual(true);
        expect(solution(1634)).toEqual(true);

        expect(solution(1652)).toEqual(false);
    });
});

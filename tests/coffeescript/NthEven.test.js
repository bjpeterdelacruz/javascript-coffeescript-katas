const solution = require('../../out/NthEven');

describe('Nth Even Tests', () => {
    it('Equal Tests', () => {
        expect(solution(2)).toEqual(2);
        expect(solution(4)).toEqual(6);
        expect(solution(10)).toEqual(18);
    });
});

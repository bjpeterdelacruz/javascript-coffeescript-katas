const solution = require('../../out/NthRoot');

describe('Nth Root Tests', () => {
    it('Equal Test', () => {
        expect(solution(9, 2)).toEqual(3);
        expect(solution(8, 3)).toEqual(2);
        expect(solution(25, 2)).toEqual(5);
    });
});

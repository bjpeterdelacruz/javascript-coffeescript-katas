require('../../out/NumberOfOccurrences');

describe('Number of Occurrences Tests', () => {
    it('Equal Test', () => {
        expect([1, 5, 2, 5, 3].numberOfOccurrences(5)).toEqual(2);
        expect([].numberOfOccurrences(7)).toEqual(0);
        expect(['a', 'b', 3, 4, 'e'].numberOfOccurrences('b')).toEqual(1);
    });
});

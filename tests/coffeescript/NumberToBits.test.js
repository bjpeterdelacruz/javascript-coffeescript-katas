require('../../out/NumberToBits');

describe('Number to Bits Tests', () => {
    it('Equal Test', () => {
        expect((0).toBits()).toEqual('00000000');
        expect((7).toBits()).toEqual('00000111');
        expect((128).toBits()).toEqual('10000000');
        expect((255).toBits()).toEqual('11111111');

        expect((0).toBits(0)).toEqual('0');
        expect((7).toBits(2)).toEqual('111');
        expect((128).toBits(4)).toEqual('10000000');
        expect((255).toBits(4)).toEqual('11111111');

        expect((128).toBits(16)).toEqual('0000000010000000');
    });
});

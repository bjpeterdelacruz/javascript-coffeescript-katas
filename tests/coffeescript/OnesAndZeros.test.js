const solution = require('../../out/OnesAndZeros');

describe('Ones and Zeros Tests', () => {
    it('Equal Tests', () => {
        expect(solution([0, 0, 1, 1, 0])).toEqual(6);
        expect(solution([1, 1, 0, 1])).toEqual(13);
    });
});

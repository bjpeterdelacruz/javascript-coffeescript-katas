const solution = require('../../out/OnlyOne');

describe('Only One Tests', () => {
    it('Equal Tests', () => {
        expect(solution()).toEqual(false);
        expect(solution(true, false)).toEqual(true);
        expect(solution(true, false, false)).toEqual(true);
        expect(solution(true, false, false, true)).toEqual(false);
        expect(solution(false, false, false, false)).toEqual(false);
    });
});

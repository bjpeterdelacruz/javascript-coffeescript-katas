const solution = require('../../out/OrderedCount');

describe('Ordered Count Tests', () => {
    it('Equal Tests', () => {
        const examples = [
            ['abracadabra', [['a', 5], ['b', 2], ['r', 2], ['c', 1], ['d', 1]]],
            ['Code Wars',  [['C', 1], ['o', 1], ['d', 1], ['e', 1], [' ', 1], ['W', 1], ['a', 1], ['r', 1], ['s', 1]]],
            ['212', [['2', 2], ['1', 1 ]]]
        ];
        for (const example of examples) {
            expect(solution(example[0])).toEqual(example[1]);
        }
    });
});

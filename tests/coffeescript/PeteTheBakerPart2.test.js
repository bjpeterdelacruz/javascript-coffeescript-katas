const solution = require('../../out/PeteTheBakerPart2');

describe('Pete the Baker Part 2 Tests', () => {
    it('Equal Test', () => {
        const recipe = { flour: 200, eggs: 1, sugar: 100 };
        expect(solution(recipe, { flour: 50, eggs: 1 })).toEqual({ flour: 150, sugar: 100 });
        expect(solution(recipe, {})).toEqual(recipe);
        expect(solution(recipe, { flour: 500, sugar: 200 })).toEqual({ flour: 100, eggs: 3, sugar: 100 });
    });
});

const solution = require('../../out/Pluck');

describe('Pluck Tests', () => {
    it('Equal Test', () => {
        const objs = [
            { a: 1, b: 2, c: 3 },
            { a: 4, b: 5, c: 6 },
            { a: 7, b: 8, c: 9 },
            { a: 10, b: 11 }
        ];

        expect(solution(objs, 'a')).toEqual([1, 4, 7, 10]);
        expect(solution(objs, 'b')).toEqual([2, 5, 8, 11]);
        expect(solution(objs, 'c')).toEqual([3, 6, 9, undefined]);
    });
});

const solution = require('../../out/PutLetterInColumn');

describe('Put Letter in Column Tests', () => {
    it('Equal Tests', () => {
        expect(solution(2, 'A')).toEqual('| | |A| | | | | | |');
    });
});

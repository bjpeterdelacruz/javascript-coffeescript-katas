const solution = require('../../out/QuarterOfTheYear');

describe('Quarter of the Year Tests', () => {
    it('Equal Tests', () => {
        expect(solution(1)).toEqual(1);
        expect(solution(2)).toEqual(1);
        expect(solution(3)).toEqual(1);
        expect(solution(4)).toEqual(2);
        expect(solution(5)).toEqual(2);
        expect(solution(6)).toEqual(2);
        expect(solution(7)).toEqual(3);
        expect(solution(8)).toEqual(3);
        expect(solution(9)).toEqual(3);
        expect(solution(10)).toEqual(4);
        expect(solution(11)).toEqual(4);
        expect(solution(12)).toEqual(4);
    });
});

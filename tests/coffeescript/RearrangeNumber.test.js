const solution = require('../../out/RearrangeNumber');

describe('Rearrange Number Tests', () => {
    it('Equal Test', () => {
        expect(solution(1000)).toEqual(null);
        expect(solution(-1)).toEqual(null);
        expect(solution(50)).toEqual(null);

        expect(solution(123)).toEqual(321);
        expect(solution(231)).toEqual(321);
        expect(solution(321)).toEqual(321);
    });
});

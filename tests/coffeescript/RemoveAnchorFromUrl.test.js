const solution = require('../../out/RemoveAnchorFromUrl');

describe('Remove Anchor from URL Tests', () => {
    it('Equal Tests', () => {
        expect(solution('www.codewars.com#about')).toEqual('www.codewars.com');
        expect(solution('www.codewars.com?page=1')).toEqual('www.codewars.com?page=1');
        expect(solution('www.codewars.com/katas/?page=1#about')).toEqual('www.codewars.com/katas/?page=1');
        expect(solution('www.codewars.com/katas/')).toEqual('www.codewars.com/katas/');
    });
});

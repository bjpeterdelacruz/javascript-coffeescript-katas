const solution = require('../../out/RemoveDuplicateWords');

describe('Remove Duplicate Words Tests', () => {
    it('Equal Test', () => {
        expect(solution('alpha beta beta gamma gamma gamma delta alpha beta beta gamma gamma gamma delta'))
            .toEqual('alpha beta gamma delta');
        expect(solution('a a a b b c dd eee eee ff ff ff ff')).toEqual('a b c dd eee ff');
    });
});

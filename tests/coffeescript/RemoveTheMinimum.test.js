const solution = require('../../out/RemoveTheMinimum');

describe('Remove the Minimum Tests', () => {
    it('Equal Tests', () => {
        expect(solution([4, 1, 3, 5, 2])).toEqual([4, 3, 5, 2]);
        expect(solution([-1, 6, 3, 0, -2, 4])).toEqual([-1, 6, 3, 0, 4]);
    });
});

const solution = require('../../out/SeeYouNextHappyYear');

describe('See You Next Happy Year Tests', () => {
    it('Equal Tests', () => {
        expect(solution(1001)).toEqual(1023);
        expect(solution(1123)).toEqual(1203);
        expect(solution(2001)).toEqual(2013);
        expect(solution(2334)).toEqual(2340);
        expect(solution(3331)).toEqual(3401);
    });
});

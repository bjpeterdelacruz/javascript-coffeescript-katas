const { Person, RudePerson } = require('../../out/SimpleCoffeeScriptClasses');

describe('Simple CoffeeScript Classes Tests', () => {
    it('Equal Test', () => {
        const alice = new Person('Alice');
        expect(alice.greet('Bob')).toEqual('Hello Bob, my name is Alice');

        const bob = new RudePerson('Bob');
        expect(bob.greet('Alice')).toEqual('I\'m Bob, now go away Alice');
    });
});

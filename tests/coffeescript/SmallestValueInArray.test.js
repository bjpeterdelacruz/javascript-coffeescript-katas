const solution = require('../../out/SmallestValueInArray');

describe('Smallest Value in Array Tests', () => {
    it('Equal Tests', () => {
        expect(solution([1, 2, 3, 4, 5], 'value')).toEqual(1);
        expect(solution([1, 2, 3, 4, 5], 'index')).toEqual(0);
        expect(solution([500, 250, 750, 5000, 1000, 230], 'value')).toEqual(230);
        expect(solution([500, 250, 750, 5000, 1000, 230], 'index')).toEqual(5);
        expect(solution([750, 50000, 10, 50], 'value')).toEqual(10);
        expect(solution([750, 50000, 10, 50], 'index')).toEqual(2);
    });
});

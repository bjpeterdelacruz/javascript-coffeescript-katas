const solution = require('../../out/SortReindeers');

describe('Sort Reindeers Tests', () => {
    it('Equal Tests', () => {
        const reindeers = ['Dasher Tonoyan', 'Dancer Moore', 'Prancer Chua', 'Vixen Hall',
            'Comet Karavani', 'Cupid Foroutan', 'Donder Jonker', 'Blitzen Claus'];
        const expected = ['Prancer Chua', 'Blitzen Claus', 'Cupid Foroutan', 'Vixen Hall',
            'Donder Jonker', 'Comet Karavani', 'Dancer Moore', 'Dasher Tonoyan'];
        expect(solution(reindeers)).toEqual(expected);

        expect(solution(['E Tonoyan', 'D Karavani', 'C Foroutan', 'A Karavani']))
            .toEqual(['C Foroutan', 'D Karavani', 'A Karavani', 'E Tonoyan']);
    });
});

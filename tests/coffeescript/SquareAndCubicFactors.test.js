const solution = require('../../out/SquareAndCubicFactors');

describe('Square and Cubic Factors Tests', () => {
    it('Equal Test', () => {
        expect(solution(1)).toEqual([[], []]);
        expect(solution(4)).toEqual([[2], []]);
        expect(solution(16)).toEqual([[2, 4], [2]]);
        expect(solution(81)).toEqual([[3, 9], [3]]);

        expect(solution(80)).toEqual([[2, 4], [2]]);
        expect(solution(100)).toEqual([[2, 5, 10], []]);
        expect(solution(5)).toEqual([[], []]);
        expect(solution(120)).toEqual([[2], [2]]);
        expect(solution(18)).toEqual([[3], []]);
        expect(solution(8)).toEqual([[2], [2]]);
    });
});

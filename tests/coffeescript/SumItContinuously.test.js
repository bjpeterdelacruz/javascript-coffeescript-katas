const solution = require('../../out/SumItContinuously');

describe('Sum It Continuously Tests', () => {
    it('Equal Tests', () => {
        expect(solution([1, 2, 3, 4, 5])).toEqual([1, 3, 6, 10, 15]);
        expect(solution([])).toEqual([]);
    });
});

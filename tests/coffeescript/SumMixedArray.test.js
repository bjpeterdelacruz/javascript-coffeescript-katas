const solution = require('../../out/SumMixedArray');

describe('Sum Mixed Array Tests', () => {
    it('Equal Tests', () => {
        expect(solution(['1'])).toEqual(1);
        expect(solution([1, '2', 3, '4', 5])).toEqual(15);
    });
});

const solution = require('../../out/SumOfTriangularNumbers');

describe('Sum of Triangular Numbers Tests', () => {
    it('Equal Tests', () => {
        expect(solution(4)).toEqual(20);
        expect(solution(5)).toEqual(35);
        expect(solution(6)).toEqual(56);
        expect(solution(0)).toEqual(0);
        expect(solution(-1)).toEqual(0);
    });
});

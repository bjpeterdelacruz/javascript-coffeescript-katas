const solution = require('../../out/Testing123');

describe('Testing 1-2-3 Tests', () => {
    it('Equal Tests', () => {
        expect(solution([])).toEqual([]);
        expect(solution(['a'])).toEqual(['1: a']);
        expect(solution(['a', 'b', 'c'])).toEqual(['1: a', '2: b', '3: c']);
    });
});

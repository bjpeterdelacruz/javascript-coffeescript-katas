const solution = require('../../out/TheOldSwitcheroo2');

describe('The Old Switcheroo 2 Tests', () => {
    it('Equal Tests', () => {
        expect(solution('abc')).toEqual('123');
        expect(solution('ABCD')).toEqual('1234');
        expect(solution('ZzzzZ')).toEqual('2626262626');
        expect(solution('abc-#@5')).toEqual('123-#@5');
        expect(solution('this is a long string!! Please [encode] @C0RrEctly'))
            .toEqual('208919 919 1 1215147 1920189147!! 161251195 [51431545] @30181853201225');
    });
});

const getOxfordSentence = require('../../out/TheOxfordComma');

describe('The Oxford Comma Tests', () => {
    it('Equal Tests', () => {
        expect(getOxfordSentence(null)).toEqual('');
        expect(getOxfordSentence([])).toEqual('');
        expect(getOxfordSentence(['Jan'])).toEqual('Jan');
        expect(getOxfordSentence(['Jan', 'Feb'])).toEqual('Jan and Feb');
        expect(getOxfordSentence(['Jan', 'Feb', 'March'])).toEqual('Jan, Feb, and March');
        expect(getOxfordSentence(['Jan', 'Feb', 'March', 'April'])).toEqual('Jan, Feb, March, and April');
        expect(getOxfordSentence([1, 2, 3, 4])).toEqual('1, 2, 3, and 4');
    });
});

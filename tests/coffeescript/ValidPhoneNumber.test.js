const solution = require('../../out/ValidPhoneNumber');

describe('Valid Phone Number Tests', () => {
    it('Equal Test', () => {
        expect(solution('(253) 101-1000')).toEqual(true);
        expect(solution('(2531)101-1000')).toEqual(false);
        expect(solution('(253) 101-100A')).toEqual(false);
        expect(solution('(253) 101 1000')).toEqual(false);
        expect(solution('253 101-1000')).toEqual(false);
    });
});

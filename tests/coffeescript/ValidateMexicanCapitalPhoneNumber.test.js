const solution = require('../../out/ValidateMexicanCapitalPhoneNumber');

describe('Validate Mexican Capital\'s Phone Number Tests', () => {
    it('Equal Tests', () => {
        let tests = ['(56) 84 65 52', '(56) 84 6552', '(56) 846552',
            '(56)846552', '56 84 65 52', '56 84 6552',
            '56 846552', '56846552', '55 95 64 85',
            '55 95 6485', '55 956485', '55956485'];
        for (const test of tests) {
            expect(solution(test)).toEqual(true);
        }

        tests = ['99956485', 'abcdefgh', '(56) 84 6 552',
            '66 846552', '(55 84 65 52', '56) 84 65 52'];
        for (const test of tests) {
            expect(solution(test)).toEqual(false);
        }
    });
});

const Vector = require('../../out/Vector');

describe('Vector Tests', () => {
    it('Equal Test', () => {
        const v = new Vector([1, 2, 3]);
        expect(v.add(new Vector([4, 5, 6]))).toEqual(new Vector([5, 7, 9]));
        expect(v.subtract(new Vector([5, 4, 3]))).toEqual(new Vector([-4, -2, 0]));
        expect(v.dot(new Vector([3, 3, 4]))).toEqual(21);
        expect(v.norm()).toEqual(Math.sqrt(14));
        expect(() => v.add(new Vector([1, 2, 3, 4, 5]))).toThrow();

        expect(v.equals(new Vector([1, 2, 3]))).toEqual(true);
        expect(v.equals(new Vector([1, 2, 3, 4]))).toEqual(false);
        expect(v.equals(null)).toEqual(false);
        expect(v.equals('Invalid argument')).toEqual(false);
        expect(v.equals(new Vector([1, 2, 4]))).toEqual(false);

        expect(new Vector([2, 4, 6, 8, 10]).toString()).toEqual('(2,4,6,8,10)');

        const a = new Vector([1, 2]);
        const b = new Vector([3, 4]);

        expect(a.add(b).equals(new Vector([4, 6]))).toEqual(true);
    });
});

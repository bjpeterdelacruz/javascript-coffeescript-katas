const solution = require('../../out/WhichTriangleIsThat');

describe('Which Triangle is That Tests', () => {
    it('Equal Test', () => {
        expect(solution(1, 1, 1)).toEqual('Equilateral');
        expect(solution(3, 3, 3)).toEqual('Equilateral');
        expect(solution(6, 6, 6)).toEqual('Equilateral');
        expect(solution(4, 4, 2)).toEqual('Isosceles');
        expect(solution(1, 3, 3)).toEqual('Isosceles');
        expect(solution(2, 1, 2)).toEqual('Isosceles');
        expect(solution(2, 4, 3)).toEqual('Scalene');
        expect(solution(9, 5, 7)).toEqual('Scalene');
        expect(solution(6, 4, 3)).toEqual('Scalene');
        expect(solution(1, 4, 3)).toEqual('Not a valid triangle');
        expect(solution(9, 4, 3)).toEqual('Not a valid triangle');
        expect(solution(1, 4, 6)).toEqual('Not a valid triangle');
        expect(solution(1, 'ab', 3)).toEqual('Not a valid triangle');
        expect(solution('b', 4, 3)).toEqual('Not a valid triangle');
        expect(solution(1, 4, 'h45@')).toEqual('Not a valid triangle');
    });
});
